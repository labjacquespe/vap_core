VAP : Versatile Aggregate Profiler
==================================

This readme is relative to the vap_core module.

**[VAP](http://lab-jacques.recherche.usherbrooke.ca/software_en/vap/home/)**

Copyright (C) 2013-2016 Charles Coulombe, Pierre-Étienne Jacques
Université de Sherbrooke. All rights reserved.

Download
-----------
The vap_core binaries can be downloaded [here](https://bitbucket.org/labjacquespe/vap_core/downloads).

Run
---
```
vap_core vap_params.txt
vap_core -p[parameters] vap_params.txt --smoothing_windows 6
vap_core --analysis_mode A --dataset_path data/dataset1 data/dataset2 --refgroup_path=data/group1
vap_core --analysis_mode A --dataset_path data/dataset1 --dataset_path data/dataset2 --refgroup_path=data/group1 vap_params.txt
vap_core --window_size 100 < vap_params.txt
```

Test
----
See examples/README.md in this directory.

Build
-----
See the INSTALL file in this directory.

Documentation
-------------
See the [VAP Documentation](http://lab-jacques.recherche.usherbrooke.ca/software_en/vap/documentation/).
You can also see the doxygen documentation in the doc directory.

For quick help, run 'vap_core --help'.

Release Notes
-------------
See the [VAP Releases Notes](http://lab-jacques.recherche.usherbrooke.ca/software_en/vap/home/)
to find out about the latest changes or read the NEWS file in this directory.

For a complete list of changes, see the ChangeLog file in this directory.

Bug Reports and Feedback
------------------------
You can report a bug [here](https://bitbucket.org/labjacquespe/vap_core/issues?status=new&status=open).
You can also directly contact us via email by writing to vap_support at usherbrooke dot ca.

Any and all feedback is welcome.

Licensing
---------
VAP is released under the [GPL license](http://www.gnu.org/licenses/gpl-3.0.txt). For further details see the COPYING file
in this directory.
