var logger_8h =
[
    [ "loggingOptions", "structvap_1_1logging_options.html", "structvap_1_1logging_options" ],
    [ "logger", "classvap_1_1logger.html", "classvap_1_1logger" ],
    [ "DEFAULT_LOG_FILE_EXTENSION", "logger_8h.html#afa2d98dfa4b260c1b6b073ec727eecdc", null ],
    [ "DEFAULT_LOG_FILENAME", "logger_8h.html#a8c1504752b4feacf6edebb3ced5b46bc", null ],
    [ "LOGGER_STACKTRACE", "logger_8h.html#af49bf3c64ec58c8f53fc10d768eb0882", null ],
    [ "options_t", "logger_8h.html#afc4d2d2643c28c911ae1c8dae194c289", null ]
];