var dir_7abd1dea21c7c194bff775c6e6f63d65 =
[
    [ "abstract_dataset.h", "abstract__dataset_8h.html", [
      [ "_less_dataset", "structvap_1_1internal_1_1__less__dataset.html", "structvap_1_1internal_1_1__less__dataset" ],
      [ "abstract_dataset", "classvap_1_1abstract__dataset.html", "classvap_1_1abstract__dataset" ]
    ] ],
    [ "base_entry.h", "base__entry_8h.html", "base__entry_8h" ],
    [ "bedgraph_dataset.cpp", "bedgraph__dataset_8cpp.html", null ],
    [ "bedgraph_dataset.h", "bedgraph__dataset_8h.html", [
      [ "bedgraph_entry", "structvap_1_1bedgraph__entry.html", "structvap_1_1bedgraph__entry" ],
      [ "bedgraph", "classvap_1_1bedgraph.html", "classvap_1_1bedgraph" ]
    ] ],
    [ "bigwig_dataset.cpp", "bigwig__dataset_8cpp.html", null ],
    [ "bigwig_dataset.h", "bigwig__dataset_8h.html", "bigwig__dataset_8h" ],
    [ "genepred_entry.h", "genepred__entry_8h.html", [
      [ "genepred_entry", "structvap_1_1genepred__entry.html", "structvap_1_1genepred__entry" ]
    ] ],
    [ "genome_dataset.cpp", "genome__dataset_8cpp.html", null ],
    [ "genome_dataset.h", "genome__dataset_8h.html", "genome__dataset_8h" ],
    [ "genome_entry.h", "genome__entry_8h.html", [
      [ "genome_entry", "structvap_1_1genome__entry.html", "structvap_1_1genome__entry" ],
      [ "genome_entry_pointer", "structvap_1_1genome__entry__pointer.html", "structvap_1_1genome__entry__pointer" ]
    ] ],
    [ "gtf_entry.h", "gtf__entry_8h.html", [
      [ "gtf_entry", "structvap_1_1gtf__entry.html", "structvap_1_1gtf__entry" ]
    ] ],
    [ "reference_annotation.h", "reference__annotation_8h.html", "reference__annotation_8h" ],
    [ "reference_coordinates.h", "reference__coordinates_8h.html", "reference__coordinates_8h" ],
    [ "reference_entry.h", "reference__entry_8h.html", [
      [ "reference_entry", "classvap_1_1reference__entry.html", "classvap_1_1reference__entry" ]
    ] ],
    [ "wig_dataset.cpp", "wig__dataset_8cpp.html", null ],
    [ "wig_dataset.h", "wig__dataset_8h.html", [
      [ "wig_entry", "structvap_1_1wig__entry.html", "structvap_1_1wig__entry" ],
      [ "wig", "classvap_1_1wig.html", "classvap_1_1wig" ]
    ] ]
];