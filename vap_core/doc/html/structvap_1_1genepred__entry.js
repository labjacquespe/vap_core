var structvap_1_1genepred__entry =
[
    [ "genepred_entry", "structvap_1_1genepred__entry.html#a33f07ee3714fe8e78a114c4721f5b358", null ],
    [ "~genepred_entry", "structvap_1_1genepred__entry.html#afe53db8b315e18753614e7d8185bf7f1", null ],
    [ "alias", "structvap_1_1genepred__entry.html#ae0fdd4f7342fe9b02e46b7b09040f439", null ],
    [ "cdsEnd", "structvap_1_1genepred__entry.html#a00bfe63887513a3e4a7ff38f309b34bc", null ],
    [ "cdsStart", "structvap_1_1genepred__entry.html#a85a53cee06ec78218d8a758cdb8d1470", null ],
    [ "chromosome", "structvap_1_1genepred__entry.html#a842067975189f19192f61d54c83199cc", null ],
    [ "exonEnds", "structvap_1_1genepred__entry.html#a46c6aab1e644f3794e5799ec14c797ee", null ],
    [ "exonsCount", "structvap_1_1genepred__entry.html#af91002b2177baab4481b59d5e5db80cc", null ],
    [ "exonStarts", "structvap_1_1genepred__entry.html#a10cbffaa09564a4b3e160a98e64a3565", null ],
    [ "extra", "structvap_1_1genepred__entry.html#a6d3bc0e070c524603806ec2998f45478", null ],
    [ "name", "structvap_1_1genepred__entry.html#a85a573a53227cd75f10d3929047f6a1d", null ],
    [ "score", "structvap_1_1genepred__entry.html#ab6299d9eab68ba75c4c3e5964ea25ab2", null ],
    [ "strand", "structvap_1_1genepred__entry.html#a14ee679bc3256aeee97b514cb25b93a7", null ],
    [ "txEnd", "structvap_1_1genepred__entry.html#a2c3be5155295d4f8cf52ee4e3613ef4e", null ],
    [ "txStart", "structvap_1_1genepred__entry.html#ab42e98fbbd5aa56252954d09ca315069", null ]
];