var classvap_1_1abstract__dataset =
[
    [ "entries_t", "classvap_1_1abstract__dataset.html#a91aca59e64ccd94104863d210cd1678a", null ],
    [ "key_type", "classvap_1_1abstract__dataset.html#a723f10f41b60025508a1b759579ba5ef", null ],
    [ "value_type", "classvap_1_1abstract__dataset.html#ab23e12917b9df30dd8b7a970964a01c6", null ],
    [ "abstract_dataset", "classvap_1_1abstract__dataset.html#a71fd5274c90319350c825288cc707af9", null ],
    [ "~abstract_dataset", "classvap_1_1abstract__dataset.html#ab4593ed4a343be5f69ed395dddb2ba47", null ],
    [ "_hasProperties", "classvap_1_1abstract__dataset.html#a2877a1032643de6325f92ea12219411c", null ],
    [ "_parseProperties", "classvap_1_1abstract__dataset.html#a3b7db872cc919bb7746fc6beb15076f7", null ],
    [ "_setDefaultName", "classvap_1_1abstract__dataset.html#a3cdf31fd75133a6a57dbc4350293bd0d", null ],
    [ "load", "classvap_1_1abstract__dataset.html#a51f255a294fe0a544c898b5ded720881", null ],
    [ "loadProperties", "classvap_1_1abstract__dataset.html#a575beda7d98f07ced1b0689b3c6b2159", null ],
    [ "propertyValue", "classvap_1_1abstract__dataset.html#aa438ad6c0c5e67ae0ac9f0153b5ed2a9", null ],
    [ "sort", "classvap_1_1abstract__dataset.html#a39dba32bce0c244bc802d719490eadf4", null ],
    [ "_chunkSize", "classvap_1_1abstract__dataset.html#ad3cb76a6429a3cda26afbfe8df7589b5", null ],
    [ "_freader", "classvap_1_1abstract__dataset.html#ab349523a723535dffb54980dddc4aa00", null ],
    [ "_hasHeader", "classvap_1_1abstract__dataset.html#a171517cf00f5e5fc961e6703a02b4ff7", null ],
    [ "_path", "classvap_1_1abstract__dataset.html#aad286846525f9bab9ca0165530e93ca6", null ],
    [ "_properties", "classvap_1_1abstract__dataset.html#afc1fc2648ba12ad2668a32ba92a41a68", null ]
];