var structvap_1_1gtf__entry =
[
    [ "gtf_entry", "structvap_1_1gtf__entry.html#a173413419def889e66735456b1c3dfe1", null ],
    [ "~gtf_entry", "structvap_1_1gtf__entry.html#a4223c40dbed234f527537eb768bcc768", null ],
    [ "attributes", "structvap_1_1gtf__entry.html#ad658cfba888067797ca639cc8fbf35ef", null ],
    [ "chromosome", "structvap_1_1gtf__entry.html#a5ef4a462ef1056c2ff537c7c2869904c", null ],
    [ "end", "structvap_1_1gtf__entry.html#ad16a839fcf059330c061af0be1f85417", null ],
    [ "feature", "structvap_1_1gtf__entry.html#a982985230635db75ecf477bea77791f9", null ],
    [ "frame", "structvap_1_1gtf__entry.html#ac2e007cbe377e313efc0ae90e4a4fef3", null ],
    [ "score", "structvap_1_1gtf__entry.html#acaf4218a70a2b5371088235a34b08a49", null ],
    [ "source", "structvap_1_1gtf__entry.html#af79ea674824028eb10f7ae63a2910821", null ],
    [ "start", "structvap_1_1gtf__entry.html#a5d17b4b094e579a287f9718a7bd56349", null ],
    [ "strand", "structvap_1_1gtf__entry.html#afa338561415514b666be2e8e41861c37", null ]
];