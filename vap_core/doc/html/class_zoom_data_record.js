var class_zoom_data_record =
[
    [ "ZoomDataRecord", "class_zoom_data_record.html#a846e0a8ad31ad56443fe17129ee4f0ca", null ],
    [ "~ZoomDataRecord", "class_zoom_data_record.html#a95d76456cdf67a6bac5794c6e63064fa", null ],
    [ "ZoomDataRecord", "class_zoom_data_record.html#a4451d3cadf0c8c88562bfb2dc18cedc5", null ],
    [ "getBasesCovered", "class_zoom_data_record.html#a80012b7951e83b4b829bc56ba8f6eb7c", null ],
    [ "getChromEnd", "class_zoom_data_record.html#a456b455ce0bf2b809fe9a1ff2f822d3e", null ],
    [ "getChromId", "class_zoom_data_record.html#a6ec5db7022f0c89e62b5d96a4360851d", null ],
    [ "getChromName", "class_zoom_data_record.html#af9c92358910728a940df25b43233d7cd", null ],
    [ "getChromStart", "class_zoom_data_record.html#a853b6b50f402d9f3c6bbc7905f47a8b2", null ],
    [ "getMaxVal", "class_zoom_data_record.html#ad2b5e7e1be1f80596463a02056c11fe5", null ],
    [ "getMeanVal", "class_zoom_data_record.html#a8a9f560bed515839f7e30af2c8d5f7ab", null ],
    [ "getMinVal", "class_zoom_data_record.html#a1d25577e65e230540876ed90bd4095d1", null ],
    [ "getRecordNumber", "class_zoom_data_record.html#a64e2241fe810c95fd8c2092bff23c6a8", null ],
    [ "getSumData", "class_zoom_data_record.html#a282f411092822c077d8ff6d7156c66bd", null ],
    [ "getSumSquares", "class_zoom_data_record.html#a5e1619760b439f58814c920b51079a3f", null ],
    [ "getZoomLevel", "class_zoom_data_record.html#a0740f977a128450235655088e38c5ca9", null ]
];