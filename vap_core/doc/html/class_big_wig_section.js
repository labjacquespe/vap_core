var class_big_wig_section =
[
    [ "BigWigSection", "class_big_wig_section.html#a1672db58c474db3d63f329f7634bb202", null ],
    [ "~BigWigSection", "class_big_wig_section.html#a135632bcd4d56ad131cd87175ccfd161", null ],
    [ "BigWigSection", "class_big_wig_section.html#a4ec195dc19941aabddbfd9936825f45e", null ],
    [ "getItemCount", "class_big_wig_section.html#a52ddbefb0de4b8dc6253aa0f6e668e78", null ],
    [ "getSectionData", "class_big_wig_section.html#a8edf4dace91bb55035d80336ba05dfe5", null ],
    [ "getSectionDataSize", "class_big_wig_section.html#ad3006c45628d1c65736bd69787538527", null ],
    [ "getSectionHeader", "class_big_wig_section.html#abfdc661e5d687392d818b7e43c94fdff", null ],
    [ "isValidSectionType", "class_big_wig_section.html#a6f92ac11e1130051fab24ef5e369bc60", null ]
];