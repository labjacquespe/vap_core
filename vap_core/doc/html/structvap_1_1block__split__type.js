var structvap_1_1block__split__type =
[
    [ "type", "structvap_1_1block__split__type.html#a00eba708ebd8808c705c9287912184e6", [
      [ "UNKNOWN", "structvap_1_1block__split__type.html#a00eba708ebd8808c705c9287912184e6a696b031073e74bf2cb98e5ef201d4aa3", null ],
      [ "ABSOLUTE", "structvap_1_1block__split__type.html#a00eba708ebd8808c705c9287912184e6a3a0bc063b6db8cae0361657958be836f", null ],
      [ "PERCENTAGE", "structvap_1_1block__split__type.html#a00eba708ebd8808c705c9287912184e6a21ee69ba26b6e644d4dddce593855d84", null ],
      [ "NOT_APPLICABLE", "structvap_1_1block__split__type.html#a00eba708ebd8808c705c9287912184e6aeb7295999e22b161fe9136ac2a60c0d5", null ]
    ] ]
];