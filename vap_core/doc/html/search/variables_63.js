var searchData=
[
  ['cdsend',['cdsEnd',['../structvap_1_1genepred__entry.html#a00bfe63887513a3e4a7ff38f309b34bc',1,'vap::genepred_entry']]],
  ['cdsstart',['cdsStart',['../structvap_1_1genepred__entry.html#a85a53cee06ec78218d8a758cdb8d1470',1,'vap::genepred_entry']]],
  ['chromid_5f',['chromId_',['../class_r_p_tree_node_proxy.html#aff743ff4db343930b24e752046041cd2',1,'RPTreeNodeProxy']]],
  ['chromosome',['chromosome',['../structvap_1_1base__entry.html#a76aa22544b07c66f7fedc995f01d7b4a',1,'vap::base_entry::chromosome()'],['../structvap_1_1coordinates__region.html#a6acced39473eee5c30a217d1c6c86d24',1,'vap::coordinates_region::chromosome()'],['../structvap_1_1genepred__entry.html#a842067975189f19192f61d54c83199cc',1,'vap::genepred_entry::chromosome()'],['../structvap_1_1gtf__entry.html#a5ef4a462ef1056c2ff537c7c2869904c',1,'vap::gtf_entry::chromosome()'],['../structvap_1_1reference__coordinates.html#afbaf2cccccceee7f2daaa7b14240a33a',1,'vap::reference_coordinates::chromosome()'],['../structvap_1_1reference__filter__entry_1_1range__entry.html#ab8ba76589835cb4b9d7d4e19c3793694',1,'vap::reference_filter_entry::range_entry::chromosome()']]],
  ['chromosomekeycache_5f',['chromosomeKeyCache_',['../class_b_p_tree.html#ab791b7dbceccbeddf24740478b893fa9',1,'BPTree']]],
  ['coordinates',['coordinates',['../structvap_1_1reference__coordinates.html#a6e27a431c5bfc14d0eb83cb8dad52054',1,'vap::reference_coordinates']]],
  ['coordinatesofinterest',['coordinatesOfInterest',['../structvap_1_1reference__coordinates.html#a562dbb4c7f8a39ad45125620ed13e182',1,'vap::reference_coordinates']]],
  ['count',['count',['../classvap_1_1block.html#aab95fbc4ddae454630ffdd5b20305e9b',1,'vap::block']]]
];
