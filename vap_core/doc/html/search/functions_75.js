var searchData=
[
  ['updateaggregate',['updateAggregate',['../vap__core_8cpp.html#af604ac3c80bd41293a308fbe2c6bd667',1,'vap_core.cpp']]],
  ['updateaggregatephase1',['updateAggregatePhase1',['../classvap_1_1aggregate__reference__feature.html#ac55487297fa671337a5d670c7127c650',1,'vap::aggregate_reference_feature::updateAggregatePhase1()'],['../classvap_1_1block.html#a9a1e5185783843e8c9f5e448896ec3fa',1,'vap::block::updateAggregatePhase1()']]],
  ['updateaggregatephase2',['updateAggregatePhase2',['../classvap_1_1aggregate__reference__feature.html#a11074a4b3f3cd4a41a84a9aabec643ca',1,'vap::aggregate_reference_feature::updateAggregatePhase2()'],['../classvap_1_1block.html#a6061af6f89f223b0e0d26cf56c4cbc34',1,'vap::block::updateAggregatePhase2()']]],
  ['updatemax',['updateMax',['../classvap_1_1window.html#ab2df5c66a91afd95b96903d869ece330',1,'vap::window']]],
  ['updatemeanstd',['updateMeanStd',['../classvap_1_1window.html#a3543b70beac0965f11317068f8d52b75',1,'vap::window']]],
  ['updatemin',['updateMin',['../classvap_1_1window.html#a9b703b2925780187abc7628a2b807dc9',1,'vap::window']]],
  ['updaterange',['updateRange',['../classvap_1_1reference__feature.html#ac353aa61d6b32e247feffbf7f2b4459c',1,'vap::reference_feature::updateRange()'],['../vap__core_8cpp.html#ad6c18f209be4319aadc23b9382279c0c',1,'updateRange():&#160;vap_core.cpp']]],
  ['updatestddev',['updateStdDev',['../classvap_1_1window.html#a4b7a48879c3fb0495330302e8a453649',1,'vap::window']]],
  ['updatestderrmean',['updateStdErrMean',['../classvap_1_1window.html#ac9e2d727b78d9597a4dbdda1bdb4b225',1,'vap::window']]],
  ['updatetable',['updateTable',['../classvap_1_1reference__feature.html#a256d71b2f67422d6bbae8e5b0cc7a40b',1,'vap::reference_feature']]],
  ['updateweightedmean',['updateWeightedMean',['../vap__core_8cpp.html#a6f1fdae4734624351e0db6f765dcb83f',1,'vap_core.cpp']]],
  ['updateweigthedmean',['updateWeigthedMean',['../classvap_1_1block.html#aa12aa33648ab88232aa0f3f4ab4eaea4',1,'vap::block::updateWeigthedMean()'],['../classvap_1_1reference__feature.html#a17a8cff3dd9c9c0ca43ee06d6ad3c21d',1,'vap::reference_feature::updateWeigthedMean()'],['../classvap_1_1window.html#a5a064d5c2e37a94945f137736c8aedd6',1,'vap::window::updateWeigthedMean()']]],
  ['updateweigthedmeanzeros',['updateWeigthedMeanZeros',['../classvap_1_1reference__feature.html#a29eb7644637fc259ce97209fcbe7b1f9',1,'vap::reference_feature::updateWeigthedMeanZeros()'],['../classvap_1_1window.html#a4ed3bbafac976cd9a5dd423382ecbdd0',1,'vap::window::updateWeigthedMeanZeros()']]]
];
