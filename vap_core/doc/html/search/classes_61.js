var searchData=
[
  ['abstract_5fdataset',['abstract_dataset',['../classvap_1_1abstract__dataset.html',1,'vap']]],
  ['abstract_5fdataset_3c_20bedgraph_5fentry_20_3e',['abstract_dataset&lt; bedgraph_entry &gt;',['../classvap_1_1abstract__dataset.html',1,'vap']]],
  ['abstract_5fdataset_3c_20bigwig_5fentry_20_3e',['abstract_dataset&lt; bigwig_entry &gt;',['../classvap_1_1abstract__dataset.html',1,'vap']]],
  ['abstract_5fdataset_3c_20genome_5fentry_20_3e',['abstract_dataset&lt; genome_entry &gt;',['../classvap_1_1abstract__dataset.html',1,'vap']]],
  ['abstract_5fdataset_3c_20wig_5fentry_20_3e',['abstract_dataset&lt; wig_entry &gt;',['../classvap_1_1abstract__dataset.html',1,'vap']]],
  ['agg_5foutput_5fwindow',['agg_output_window',['../classvap_1_1agg__output__window.html',1,'vap']]],
  ['aggregate_5fdata_5ftype',['aggregate_data_type',['../structvap_1_1aggregate__data__type.html',1,'vap']]],
  ['aggregate_5freference_5ffeature',['aggregate_reference_feature',['../classvap_1_1aggregate__reference__feature.html',1,'vap']]],
  ['analysis_5fmethod',['analysis_method',['../structvap_1_1analysis__method.html',1,'vap']]],
  ['analysis_5fmode',['analysis_mode',['../structvap_1_1analysis__mode.html',1,'vap']]],
  ['annotation_5fcoordinates_5ftype',['annotation_coordinates_type',['../structvap_1_1annotation__coordinates__type.html',1,'vap']]],
  ['annotation_5fstrand',['annotation_strand',['../structvap_1_1annotation__strand.html',1,'vap']]],
  ['array_5fsmart_5fpointer',['array_smart_pointer',['../classvap_1_1util_1_1array__smart__pointer.html',1,'vap::util']]]
];
