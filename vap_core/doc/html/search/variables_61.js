var searchData=
[
  ['aggregatedatatype',['aggregateDataType',['../structvap_1_1parameters__definition.html#a55228a4ddbe7cba7e9088401b41016f9',1,'vap::parameters_definition']]],
  ['alias',['alias',['../structvap_1_1genepred__entry.html#ae0fdd4f7342fe9b02e46b7b09040f439',1,'vap::genepred_entry::alias()'],['../structvap_1_1genome__entry.html#a82cd66bc30a426e038522200cf8b66e7',1,'vap::genome_entry::alias()'],['../structvap_1_1reference__coordinates.html#a6bbc55d7e6d2e347203bd45e0d68a4af',1,'vap::reference_coordinates::alias()']]],
  ['analysismethod',['analysisMethod',['../structvap_1_1parameters__definition.html#a0c6f56c5b2c74f4f34296e0cd72afa0f',1,'vap::parameters_definition']]],
  ['analysismode',['analysisMode',['../structvap_1_1parameters__definition.html#a336d05cad6b6ef66c8c48ae5d2203285',1,'vap::parameters_definition']]],
  ['annotationcoordinatestype',['annotationCoordinatesType',['../structvap_1_1parameters__definition.html#a4f024074721248c9a3b5201ae12dba9a',1,'vap::parameters_definition']]],
  ['annotationspath',['annotationsPath',['../structvap_1_1parameters__definition.html#aacd60726363f3a51d8d221820bea067a',1,'vap::parameters_definition']]],
  ['attributes',['attributes',['../structvap_1_1gtf__entry.html#ad658cfba888067797ca639cc8fbf35ef',1,'vap::gtf_entry']]]
];
