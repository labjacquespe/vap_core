var searchData=
[
  ['zoom_5fformat_5fheader_5fsize',['ZOOM_FORMAT_HEADER_SIZE',['../class_b_b_zoom_level_format.html#a9debdde6bc482d1d9fa48205aa347bb2',1,'BBZoomLevelFormat']]],
  ['zoom_5flevel_5fheader_5fsize',['ZOOM_LEVEL_HEADER_SIZE',['../class_b_b_zoom_level_header.html#a97c886c3b837d0bc44aa1b1e3e2cdfb2',1,'BBZoomLevelHeader']]],
  ['zoomdatabloch',['ZoomDataBLoch',['../class_zoom_data_b_loch.html',1,'ZoomDataBLoch'],['../class_zoom_data_b_loch.html#ab07a5393b7534002377a81ebd373b921',1,'ZoomDataBLoch::ZoomDataBLoch()']]],
  ['zoomdatabloch_2eh',['ZoomDataBLoch.h',['../_zoom_data_b_loch_8h.html',1,'']]],
  ['zoomdatablock',['ZoomDataBlock',['../class_zoom_data_block.html',1,'ZoomDataBlock'],['../class_zoom_data_block.html#a9ce8e9d6d47d278dbf7b22a20794b17e',1,'ZoomDataBlock::ZoomDataBlock()']]],
  ['zoomdatablock_2eh',['ZoomDataBlock.h',['../_zoom_data_block_8h.html',1,'']]],
  ['zoomdatarecord',['ZoomDataRecord',['../class_zoom_data_record.html',1,'ZoomDataRecord'],['../class_zoom_data_record.html#a846e0a8ad31ad56443fe17129ee4f0ca',1,'ZoomDataRecord::ZoomDataRecord()'],['../class_zoom_data_record.html#a4451d3cadf0c8c88562bfb2dc18cedc5',1,'ZoomDataRecord::ZoomDataRecord(int32_t zoomLevel, int32_t recordNumber, std::string chromName, int32_t chromId, int32_t chromStart, int32_t chromEnd, int32_t validCount, float minVal, float maxVal, float sumData, float sumSquares)']]],
  ['zoomdatarecord_2ecpp',['ZoomDataRecord.cpp',['../_zoom_data_record_8cpp.html',1,'']]],
  ['zoomdatarecord_2eh',['ZoomDataRecord.h',['../_zoom_data_record_8h.html',1,'']]],
  ['zoomleveliterator',['ZoomLevelIterator',['../class_zoom_level_iterator.html',1,'ZoomLevelIterator'],['../class_zoom_level_iterator.html#a7e5b28941befebc91dfba22f7f2d3b1c',1,'ZoomLevelIterator::ZoomLevelIterator()'],['../class_zoom_level_iterator.html#a6f254b5525223ee451e54caf1a1e952a',1,'ZoomLevelIterator::ZoomLevelIterator(std::ifstream *fis, BPTree *chromIDTree, RPTree *zoomDataTree, int32_t zoomLevel, RPChromosomeRegion *selectionRegion, bool contained)']]],
  ['zoomleveliterator_2ecpp',['ZoomLevelIterator.cpp',['../_zoom_level_iterator_8cpp.html',1,'']]],
  ['zoomleveliterator_2eh',['ZoomLevelIterator.h',['../_zoom_level_iterator_8h.html',1,'']]]
];
