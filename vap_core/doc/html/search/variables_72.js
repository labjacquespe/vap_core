var searchData=
[
  ['range',['range',['../structvap_1_1reference__filter__entry.html#a566420b5cf8e8bddebf27548de720459',1,'vap::reference_filter_entry']]],
  ['record_5fsize',['RECORD_SIZE',['../class_zoom_data_record.html#a67e18eae702236d770b25677b65ed3e3',1,'ZoomDataRecord']]],
  ['referencepoints',['referencePoints',['../structvap_1_1parameters__definition.html#a37b5ab4d8cd9fcf51cc81e1c54dde05c',1,'vap::parameters_definition']]],
  ['refgroupspaths',['refGroupsPaths',['../structvap_1_1parameters__definition.html#a0ea36ec9ea810ab81ccb60dd89428376',1,'vap::parameters_definition']]],
  ['rounds',['rounds',['../structvap_1_1parameters__definition.html#ac39736328238d5c3680bb4be7f845b13',1,'vap::parameters_definition']]],
  ['rptree_5fheader_5fsize',['RPTREE_HEADER_SIZE',['../class_r_p_tree_header.html#abdd0650099a65d5751a74758e040ab95',1,'RPTreeHeader']]],
  ['rptree_5fmagic_5fhtl',['RPTREE_MAGIC_HTL',['../class_r_p_tree_header.html#a351248467822791703265820c2748f7e',1,'RPTreeHeader']]],
  ['rptree_5fmagic_5flth',['RPTREE_MAGIC_LTH',['../class_r_p_tree_header.html#af82b54b865d491f3bf76b8fa655a9a4f',1,'RPTreeHeader']]],
  ['rptree_5fnode_5fchild_5fitem_5fsize',['RPTREE_NODE_CHILD_ITEM_SIZE',['../class_r_p_tree.html#a8bf2ec3877489d2c164c331c3f45f63f',1,'RPTree']]],
  ['rptree_5fnode_5fformat_5fsize',['RPTREE_NODE_FORMAT_SIZE',['../class_r_p_tree.html#abe8f08e03184980670119e748daa807b',1,'RPTree']]],
  ['rptree_5fnode_5fleaf_5fitem_5fsize',['RPTREE_NODE_LEAF_ITEM_SIZE',['../class_r_p_tree.html#af94038fcc1b91263996dd0b5b92a74f6',1,'RPTree']]]
];
