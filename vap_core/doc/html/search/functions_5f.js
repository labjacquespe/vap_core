var searchData=
[
  ['_5fhasproperties',['_hasProperties',['../classvap_1_1abstract__dataset.html#a8eff49a382f71addbe0afb5207be984f',1,'vap::abstract_dataset']]],
  ['_5finitialize',['_initialize',['../classvap_1_1reference__feature.html#ab2b5af64ac31a1ee4469feb6edb033b4',1,'vap::reference_feature']]],
  ['_5fparseproperties',['_parseProperties',['../classvap_1_1abstract__dataset.html#a3b7db872cc919bb7746fc6beb15076f7',1,'vap::abstract_dataset']]],
  ['_5fset_5f1referencepoint',['_set_1referencePoint',['../classvap_1_1reference__feature.html#aa5a0a4b06e514d0710d9780bda683f00',1,'vap::reference_feature']]],
  ['_5fset_5f2referencepoint',['_set_2referencePoint',['../classvap_1_1reference__feature.html#a818b17fd3454bf338f37536fd8d516fd',1,'vap::reference_feature']]],
  ['_5fset_5f3referencepoint',['_set_3referencePoint',['../classvap_1_1reference__feature.html#afd23ef290bdba997bed4022783a53e81',1,'vap::reference_feature']]],
  ['_5fset_5f4referencepoint',['_set_4referencePoint',['../classvap_1_1reference__feature.html#a885b9f9190ce6c9c6051d96f6bfd140c',1,'vap::reference_feature']]],
  ['_5fset_5f5referencepoint',['_set_5referencePoint',['../classvap_1_1reference__feature.html#aa6a6e9774ce944c5dcfb58975f0c2e72',1,'vap::reference_feature']]],
  ['_5fset_5f6referencepoint',['_set_6referencePoint',['../classvap_1_1reference__feature.html#a2797ce12709b015e948164d99c35a1c9',1,'vap::reference_feature']]],
  ['_5fsetdefaultname',['_setDefaultName',['../classvap_1_1abstract__dataset.html#a3cdf31fd75133a6a57dbc4350293bd0d',1,'vap::abstract_dataset']]],
  ['_5fsetreferencepoints',['_setReferencePoints',['../classvap_1_1reference__feature.html#a8d5dd91156217a6273d0d0c3dd2e9f89',1,'vap::reference_feature']]],
  ['_5fupdatedata',['_updateData',['../classvap_1_1reference__feature.html#af52cb0749c1689ce49f7f96a7788dbb7',1,'vap::reference_feature']]],
  ['_5fupdatedatazeros',['_updateDataZeros',['../classvap_1_1reference__feature.html#a0505acd7ed73f65f2c6a32240472a0b6',1,'vap::reference_feature']]],
  ['_5fupdatewidth',['_updateWidth',['../classvap_1_1coordinates.html#abf2a66606cf370622107bc7540701b91',1,'vap::coordinates']]]
];
