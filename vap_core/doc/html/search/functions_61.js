var searchData=
[
  ['abstract_5fdataset',['abstract_dataset',['../classvap_1_1abstract__dataset.html#af41e76ce1fae956e39cb4af66023bba4',1,'vap::abstract_dataset']]],
  ['accumulatecoverage',['accumulateCoverage',['../classvap_1_1window.html#a1f694688393ca78147aa226aa0b9c20c',1,'vap::window']]],
  ['accumulatevalue',['accumulateValue',['../classvap_1_1window.html#a5a7ff8b498f0be69f15ea293c9ddeafd',1,'vap::window']]],
  ['add',['add',['../classvap_1_1util_1_1reference.html#a34efec99531bf113753ebe270c9c3a61',1,'vap::util::reference']]],
  ['addchrtofilter',['addChrToFilter',['../classvap_1_1group__feature.html#a965cec0f80a9c2ba26b395afe548a2a7',1,'vap::group_feature']]],
  ['adddataset',['addDataset',['../classvap_1_1list__agg__graphs.html#adc1de456439d49796d6d56f606ae4c0c',1,'vap::list_agg_graphs']]],
  ['adddummiesentry',['addDummiesEntry',['../vap__core_8cpp.html#ac2b884e13aff85df952e51f4e88d7684',1,'vap_core.cpp']]],
  ['addgroupname',['addGroupName',['../classvap_1_1group__feature.html#ab171244f86d3b54b946f584c862c95fe',1,'vap::group_feature']]],
  ['addheatmap',['addHeatmap',['../classvap_1_1list__ind__heatmaps.html#aed4b44048358c76a73f598023c0ad704',1,'vap::list_ind_heatmaps']]],
  ['advance',['advance',['../classvap_1_1util_1_1buffer__reader.html#a927746ef2487385cac2f94c203678f77',1,'vap::util::buffer_reader']]],
  ['advancetofirstof',['advanceToFirstOf',['../classvap_1_1util_1_1buffer__reader.html#ad55d82146574def5bb590855c37f66b9',1,'vap::util::buffer_reader']]],
  ['agg_5foutput_5fwindow',['agg_output_window',['../classvap_1_1agg__output__window.html#a15d9a1bde90ff9f4919ae14ebabd6aec',1,'vap::agg_output_window::agg_output_window(bool invalid)'],['../classvap_1_1agg__output__window.html#ad8ab7b66001f7aeca16299c21af42924',1,'vap::agg_output_window::agg_output_window(const agg_output_window &amp;source, uint_t index=-1)'],['../classvap_1_1agg__output__window.html#a7527101561159cb916a858acf1282557',1,'vap::agg_output_window::agg_output_window(const window &amp;source, uint_t index=-1)']]],
  ['aggregate_5freference_5ffeature',['aggregate_reference_feature',['../classvap_1_1aggregate__reference__feature.html#a92e6f70e58ba23e9cf099a1cd9c86d23',1,'vap::aggregate_reference_feature::aggregate_reference_feature(int begin, int end, uint_t rfPoints, uint_t groupIndex, uint_t entryIndex, const std::string &amp;name=&quot;&quot;, const std::string &amp;chromosome=&quot;&quot;, annotation_strand::strand strand=annotation_strand::POSITIVE)'],['../classvap_1_1aggregate__reference__feature.html#a5d953366ab00841e182a12729faf037f',1,'vap::aggregate_reference_feature::aggregate_reference_feature(const coordinates &amp;coordinates, uint_t rfPoints, uint_t groupIndex, uint_t entryIndex, const std::string &amp;name, const std::string &amp;chromosome, annotation_strand::strand strand)']]],
  ['alias',['alias',['../structvap_1_1reference__coordinates.html#a2eda5ea9fce8a8c3ac4d342ea5fe325c',1,'vap::reference_coordinates::alias()'],['../classvap_1_1reference__feature.html#adc879c030aa428a4220f56ebf8dbedc9',1,'vap::reference_feature::alias()']]],
  ['alignment',['alignment',['../classvap_1_1block.html#ac11f979b4583d73935c21b561b7c17bb',1,'vap::block']]],
  ['alternonsenseparameters',['alterNonSenseParameters',['../vap__core_8cpp.html#a182c5164705fc20da61a33b57032f851',1,'vap_core.cpp']]],
  ['annotations_5fname_5flesser_5fcompare',['annotations_name_lesser_compare',['../namespacevap.html#a2c3249cc8ee8ad032872b1a3e4db1b7c',1,'vap']]],
  ['annotations_5freference_5ffilter_5fname_5fgreater_5fcompare',['annotations_reference_filter_name_greater_compare',['../namespacevap.html#a749e7b7469ff50063a6f72e0010f00d5',1,'vap']]],
  ['annotations_5freference_5ffilter_5fname_5flesser_5fcompare',['annotations_reference_filter_name_lesser_compare',['../namespacevap.html#a5deb422d3816df350ed110ba64cdda4f',1,'vap']]],
  ['appendcontent',['appendContent',['../classvap_1_1record.html#a2c095cc36d812ac0f46bb3765025f83f',1,'vap::record::appendContent(const std::string &amp;s)'],['../classvap_1_1record.html#af9bf8ec8a2231bc3e054e693a21dce7e',1,'vap::record::appendContent(const char *s)']]],
  ['applynamefilters',['applyNameFilters',['../namespacevap.html#a9399b672e5c668bd417f261f5cee3dfa',1,'vap']]]
];
