var searchData=
[
  ['name',['name',['../structvap_1_1reference__coordinates.html#a43b7121e132897db368cb25da8613722',1,'vap::reference_coordinates::name()'],['../classvap_1_1reference__feature.html#a8e9ccab4089c8ba40eef760e7e2f2883',1,'vap::reference_feature::name()'],['../classvap_1_1filesystem_1_1file.html#a98ba2eee6113f5433baa7323d0af8df8',1,'vap::filesystem::file::name()']]],
  ['name_5fentry',['name_entry',['../structvap_1_1reference__filter__entry_1_1name__entry.html#aaf18902fff4bc7055d9ad56369c2a928',1,'vap::reference_filter_entry::name_entry']]],
  ['names',['names',['../classvap_1_1reference__filter.html#abc9b9e674557d6f9c301a2a3a220db18',1,'vap::reference_filter']]],
  ['negative_5ffilter',['negative_filter',['../classvap_1_1negative__filter.html#a16ed7b124d4d5dbe06a1c27d491a08d3',1,'vap::negative_filter']]],
  ['next',['next',['../class_zoom_level_iterator.html#a43eb1e593e1af93eaa13984855b8abb5',1,'ZoomLevelIterator']]],
  ['normalizepath',['normalizePath',['../classvap_1_1filesystem_1_1file.html#a065278adeb350384e61916685dc55ead',1,'vap::filesystem::file::normalizePath(const std::string &amp;path)'],['../classvap_1_1filesystem_1_1file.html#afc6b7bebdf6aaa55945513508ef63518',1,'vap::filesystem::file::normalizePath(const char *path)']]]
];
