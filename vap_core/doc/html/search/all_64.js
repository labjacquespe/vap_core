var searchData=
[
  ['dataset_5fchunksize',['DATASET_CHUNKSIZE',['../parameters_8h.html#aa2d5603de51e3cc2721906acea407b2a',1,'parameters.h']]],
  ['dataset_5ffactory',['dataset_factory',['../classvap_1_1dataset__factory.html',1,'vap']]],
  ['dataset_5ffactory',['dataset_factory',['../classvap_1_1dataset__factory.html#a2c01724449c2b24967718079e4834ec8',1,'vap::dataset_factory']]],
  ['dataset_5ffactory_2ecpp',['dataset_factory.cpp',['../dataset__factory_8cpp.html',1,'']]],
  ['dataset_5ffactory_2eh',['dataset_factory.h',['../dataset__factory_8h.html',1,'']]],
  ['datasetchunksize',['datasetChunkSize',['../structvap_1_1parameters__definition.html#a540d7c7e3ca2e9aaad4dc935a5a8886a',1,'vap::parameters_definition']]],
  ['datasets_5fpaths',['DATASETS_PATHS',['../parameters_8h.html#a69cbb1aaacea16f3f525bf43b8313cb9',1,'parameters.h']]],
  ['datasetspaths',['datasetsPaths',['../structvap_1_1parameters__definition.html#a14caa6abd95eb12fc2eacd5b0cf8c7a1',1,'vap::parameters_definition']]],
  ['decompress_5futil_2eh',['decompress_util.h',['../decompress__util_8h.html',1,'']]],
  ['default_5flog_5ffile_5fextension',['DEFAULT_LOG_FILE_EXTENSION',['../logger_8h.html#afa2d98dfa4b260c1b6b073ec727eecdc',1,'logger.h']]],
  ['default_5flog_5ffilename',['DEFAULT_LOG_FILENAME',['../logger_8h.html#a8c1504752b4feacf6edebb3ced5b46bc',1,'logger.h']]],
  ['definition',['definition',['../classvap_1_1parameters.html#a026483d599b9fd4d42e481f80ffb4a54',1,'vap::parameters']]],
  ['defs_2eh',['defs.h',['../defs_8h.html',1,'']]],
  ['delete_5farray_5fpolicy',['delete_array_policy',['../classvap_1_1util_1_1delete__array__policy.html',1,'vap::util']]],
  ['delete_5fscalar_5fpolicy',['delete_scalar_policy',['../classvap_1_1util_1_1delete__scalar__policy.html',1,'vap::util']]],
  ['deleteitem',['deleteItem',['../class_b_p_tree_child_node.html#aef54c90188a6bc807da9870d484f41bb',1,'BPTreeChildNode::deleteItem()'],['../class_b_p_tree_leaf_node.html#a8ee84c10a51f839aa37ec4e5d2a5fabc',1,'BPTreeLeafNode::deleteItem()'],['../class_b_p_tree_node.html#ae6097552b7d40bcebe60a6041e10ab33',1,'BPTreeNode::deleteItem()'],['../class_r_p_tree_child_node.html#a81d8edbbd3db3820900d224932ab828c',1,'RPTreeChildNode::deleteItem()'],['../class_r_p_tree_leaf_node.html#a6540f1b586e30d3bd211c92c5d1de9e4',1,'RPTreeLeafNode::deleteItem()'],['../class_r_p_tree_node.html#a985fd1b1d9a3c574cc87cc624a87c6b6',1,'RPTreeNode::deleteItem()'],['../class_r_p_tree_node_proxy.html#aff89b520ddc3d13c95fc71c905a18957',1,'RPTreeNodeProxy::deleteItem()']]],
  ['determinedatasetformat',['determineDatasetFormat',['../classvap_1_1dataset__factory.html#a05ea40c8ddc41ff5aa381fd657131f0e',1,'vap::dataset_factory']]],
  ['determinefeatureorientationtype',['determineFeatureOrientationType',['../vap__core_8cpp.html#a06e8db0196e17f7eda56312d66c13691',1,'vap_core.cpp']]],
  ['directory',['directory',['../classvap_1_1filesystem_1_1file.html#aa5557d73507477bdba5918119ecf059d',1,'vap::filesystem::file']]],
  ['disjointabove',['disjointAbove',['../class_r_p_chromosome_region.html#a836ddf8b16e6613e9f071999acd19d5f',1,'RPChromosomeRegion']]],
  ['disjointbelow',['disjointBelow',['../class_r_p_chromosome_region.html#afd841a82dfb4cd27bfbad888bc04d97c',1,'RPChromosomeRegion']]],
  ['distance',['distance',['../namespacevap_1_1util.html#aecec1f92443ce5b7f8c94afa9680de33',1,'vap::util']]]
];
