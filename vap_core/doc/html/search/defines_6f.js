var searchData=
[
  ['one_5fgraph_5fper_5fdataset',['ONE_GRAPH_PER_DATASET',['../parameters_8h.html#abde461340f6d81e9cbf631ba6166191b',1,'parameters.h']]],
  ['one_5fgraph_5fper_5fgroup',['ONE_GRAPH_PER_GROUP',['../parameters_8h.html#ac62ce4bea917be9aed93186097319673',1,'parameters.h']]],
  ['one_5fgraph_5fper_5forientation',['ONE_GRAPH_PER_ORIENTATION',['../parameters_8h.html#ac4c8860a2eff3e0f817846ceac0696ee',1,'parameters.h']]],
  ['onerefpt_5fboundary',['ONEREFPT_BOUNDARY',['../parameters_8h.html#abeb3a04d844afbea6af47194f9b298c3',1,'parameters.h']]],
  ['orientation_5fsubgroups',['ORIENTATION_SUBGROUPS',['../parameters_8h.html#a68a44d7231fa16551728a9ca994c00a4',1,'parameters.h']]],
  ['out_5fagg_5fprefix_5ffilename',['OUT_AGG_PREFIX_FILENAME',['../defs_8h.html#a1a0979f94c2f8ba0961c212cf25f3c1f',1,'defs.h']]],
  ['out_5find_5fprefix_5ffilename',['OUT_IND_PREFIX_FILENAME',['../defs_8h.html#acdc5be77933ec6fae243d0aac42f1d49',1,'defs.h']]],
  ['output_5fdirectory',['OUTPUT_DIRECTORY',['../parameters_8h.html#adfde534c812ab6707bf3f61d151fc6ea',1,'parameters.h']]]
];
