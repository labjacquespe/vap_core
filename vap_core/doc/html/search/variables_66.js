var searchData=
[
  ['feature',['feature',['../structvap_1_1gtf__entry.html#a982985230635db75ecf477bea77791f9',1,'vap::gtf_entry']]],
  ['fileoffset_5f',['fileOffset_',['../class_r_p_tree_node_proxy.html#a0ef4ed0a9d9a008a29947f83bf97b767',1,'RPTreeNodeProxy']]],
  ['fis_5f',['fis_',['../class_r_p_tree_node_proxy.html#a6cf0b029c66757619db1541d35dc2ef0',1,'RPTreeNodeProxy']]],
  ['fixedstep_5fitem_5fsize',['FIXEDSTEP_ITEM_SIZE',['../class_big_wig_section_header.html#ab809d8dd7c9eb789606cb07f27cc5be0',1,'BigWigSectionHeader']]],
  ['found',['found',['../structvap_1_1found__item.html#acd1f847802b8738430a9b3ff81c2de22',1,'vap::found_item']]],
  ['frame',['frame',['../structvap_1_1gtf__entry.html#ac2e007cbe377e313efc0ae90e4a4fef3',1,'vap::gtf_entry']]]
];
