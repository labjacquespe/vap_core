var searchData=
[
  ['main',['main',['../vap__core_8cpp.html#a3c04138a5bfe5d72780bb7e82a18e627',1,'vap_core.cpp']]],
  ['makebedgraphdataset',['makeBedgraphDataset',['../classvap_1_1dataset__factory.html#a7f07548922b07e97aaec777e607db7b8',1,'vap::dataset_factory']]],
  ['makebigwigdataset',['makeBigWigDataset',['../classvap_1_1dataset__factory.html#a8a65f9aac73f42a4b8967c162059a2d7',1,'vap::dataset_factory']]],
  ['makegenomedataset',['makeGenomeDataset',['../classvap_1_1dataset__factory.html#aebd2a7b6160ea8ee318a42576c047fae',1,'vap::dataset_factory']]],
  ['makewigdataset',['makeWigDataset',['../classvap_1_1dataset__factory.html#abe4682f6ab71a6a87ccb09041d002d18',1,'vap::dataset_factory']]],
  ['maprangestonames',['mapRangesToNames',['../classvap_1_1reference__filter.html#a0069d09cf3ea32d5980de663dfb1e488',1,'vap::reference_filter::mapRangesToNames(const std::deque&lt; reference_coordinates &gt; &amp;genome, LESSER_COMPARE lesser_cmp, GREATER_COMPARE greater_cmp)'],['../classvap_1_1reference__filter.html#a06bf850f7cb11cec293dd5b940c70430',1,'vap::reference_filter::mapRangesToNames(const std::vector&lt; genome_entry_pointer &gt; &amp;genome, LESSER_COMPARE lesser_cmp, GREATER_COMPARE greater_cmp)']]],
  ['maxsize',['maxSize',['../classvap_1_1util_1_1file__reader.html#ae246d7127b689853133ef42ab76264f9',1,'vap::util::file_reader']]],
  ['mode',['mode',['../classvap_1_1filesystem_1_1file.html#a6b1ffb52ba2e9dfd6a1f3af90e9e4616',1,'vap::filesystem::file']]],
  ['movefile',['moveFile',['../classvap_1_1filesystem_1_1file.html#a7dd757e4578090e84166b982ab3c8925',1,'vap::filesystem::file::moveFile(const std::string &amp;from, const std::string &amp;to)'],['../classvap_1_1filesystem_1_1file.html#ab37f603588bfe751525a01d4adfe169c',1,'vap::filesystem::file::moveFile(const char *from, const char *to)']]]
];
