var searchData=
[
  ['bad_5fnum',['BAD_NUM',['../defs_8h.html#a2da31be477608d1831f675f4e36af2ee',1,'defs.h']]],
  ['base_5fentry',['base_entry',['../structvap_1_1base__entry.html#a04e10d29f5345ee98dbaf9a4d30e9fb8',1,'vap::base_entry']]],
  ['base_5fentry',['base_entry',['../structvap_1_1base__entry.html',1,'vap']]],
  ['base_5fentry_2eh',['base_entry.h',['../base__entry_8h.html',1,'']]],
  ['bbfile_5fheader_5foffset',['BBFILE_HEADER_OFFSET',['../class_b_b_file_reader.html#ad92d3d81132bd7d64337807328db1b56',1,'BBFileReader']]],
  ['bbfile_5fheader_5fsize',['BBFILE_HEADER_SIZE',['../class_b_b_file_header.html#a381e8e0319f9deacf29a2ae38f495971',1,'BBFileHeader']]],
  ['bbfileheader',['BBFileHeader',['../class_b_b_file_header.html',1,'BBFileHeader'],['../class_b_b_file_header.html#a56d9cb8983011773c8f403076944bbf3',1,'BBFileHeader::BBFileHeader(std::string path, int64_t fileOffset)'],['../class_b_b_file_header.html#a5c8cf0cb5491cf3fbb3c1a622460ebb4',1,'BBFileHeader::BBFileHeader(std::string path, std::ifstream &amp;fis, int64_t fileOffset)'],['../class_b_b_file_header.html#a0d56f3fc1858ec96beb755e0077f1eed',1,'BBFileHeader::BBFileHeader(uint32_t magic, uint16_t version, uint16_t zoomLevels, uint64_t chromTreeOffset, uint64_t fullDataOffset, uint64_t fullIndexOffset, uint16_t fieldCount, uint16_t definedFieldCount, uint64_t autoSqlOffset, uint64_t totalSummaryOffset, uint32_t uncompressBuffSize, uint64_t reserved)']]],
  ['bbfileheader_2eh',['BBFileHeader.h',['../_b_b_file_header_8h.html',1,'']]],
  ['bbfilereader',['BBFileReader',['../class_b_b_file_reader.html',1,'BBFileReader'],['../class_b_b_file_reader.html#a80c1cc340d454fc7728764ffab2e5f4f',1,'BBFileReader::BBFileReader()'],['../class_b_b_file_reader.html#aa608b74e94851673770d11675308ef55',1,'BBFileReader::BBFileReader(std::string path, std::ifstream &amp;stream)']]],
  ['bbfilereader_2ecpp',['BBFileReader.cpp',['../_b_b_file_reader_8cpp.html',1,'']]],
  ['bbfilereader_2eh',['BBFileReader.h',['../_b_b_file_reader_8h.html',1,'']]],
  ['bbtotalsummaryblock',['BBTotalSummaryBlock',['../class_b_b_total_summary_block.html',1,'BBTotalSummaryBlock'],['../class_b_b_total_summary_block.html#af75ecbce94432c40af3f000c691bb2a1',1,'BBTotalSummaryBlock::BBTotalSummaryBlock(std::ifstream &amp;fis, int64_t fileOffset)'],['../class_b_b_total_summary_block.html#a2549e5a9b2c83badc0bfc9b346491f96',1,'BBTotalSummaryBlock::BBTotalSummaryBlock(int64_t basesCovered, float minVal, float maxVal, float sumData, float sumSquares)']]],
  ['bbtotalsummaryblock_2ecpp',['BBTotalSummaryBlock.cpp',['../_b_b_total_summary_block_8cpp.html',1,'']]],
  ['bbtotalsummaryblock_2eh',['BBTotalSummaryBlock.h',['../_b_b_total_summary_block_8h.html',1,'']]],
  ['bbzoomlevelformat',['BBZoomLevelFormat',['../class_b_b_zoom_level_format.html',1,'BBZoomLevelFormat'],['../class_b_b_zoom_level_format.html#a418916e3d6d903052f1a6ab4c3419a6f',1,'BBZoomLevelFormat::BBZoomLevelFormat()'],['../class_b_b_zoom_level_format.html#a708f1246c288b609fc85446a8771a416',1,'BBZoomLevelFormat::BBZoomLevelFormat(uint32_t zoomLevel, std::ifstream &amp;fis, uint64_t fileOffset, uint64_t dataSize, uint32_t uncompressBufSize)']]],
  ['bbzoomlevelformat_2ecpp',['BBZoomLevelFormat.cpp',['../_b_b_zoom_level_format_8cpp.html',1,'']]],
  ['bbzoomlevelformat_2eh',['BBZoomLevelFormat.h',['../_b_b_zoom_level_format_8h.html',1,'']]],
  ['bbzoomlevelheader',['BBZoomLevelHeader',['../class_b_b_zoom_level_header.html',1,'BBZoomLevelHeader'],['../class_b_b_zoom_level_header.html#afae3fe68e9cfc35cb0c8e52e51235401',1,'BBZoomLevelHeader::BBZoomLevelHeader()'],['../class_b_b_zoom_level_header.html#ab0b1c0c91e7942b687dafbdc42f74f8d',1,'BBZoomLevelHeader::BBZoomLevelHeader(std::ifstream &amp;fis, uint64_t fileOffset, uint32_t zoomLevel)'],['../class_b_b_zoom_level_header.html#ad1a9b5b3a55aea3e33d74613261ad955',1,'BBZoomLevelHeader::BBZoomLevelHeader(uint32_t zoomLevel, uint32_t reductionLevel, uint32_t reserved, uint64_t dataOffset, uint64_t indexOffset)']]],
  ['bbzoomlevelheader_2ecpp',['BBZoomLevelHeader.cpp',['../_b_b_zoom_level_header_8cpp.html',1,'']]],
  ['bbzoomlevelheader_2eh',['BBZoomLevelHeader.h',['../_b_b_zoom_level_header_8h.html',1,'']]],
  ['bbzoomlevels',['BBZoomLevels',['../class_b_b_zoom_levels.html',1,'BBZoomLevels'],['../class_b_b_zoom_levels.html#a1d039fd78c814221543ca15d178f7612',1,'BBZoomLevels::BBZoomLevels()'],['../class_b_b_zoom_levels.html#abe5d53ff2c83d229826863aa99a1337b',1,'BBZoomLevels::BBZoomLevels(std::ifstream &amp;fis, uint64_t fileOffset, uint32_t zoomLevels, uint32_t uncompressBufSize)']]],
  ['bbzoomlevels_2ecpp',['BBZoomLevels.cpp',['../_b_b_zoom_levels_8cpp.html',1,'']]],
  ['bbzoomlevels_2eh',['BBZoomLevels.h',['../_b_b_zoom_levels_8h.html',1,'']]],
  ['bed',['BED',['../structvap_1_1file__format.html#aef82a48b1ffde6a4da86909871f542b5a7743ec42612caa556e6c495cfef39120',1,'vap::file_format']]],
  ['bedfeature',['BedFeature',['../class_bed_feature.html',1,'BedFeature'],['../class_bed_feature.html#af1c451efc0bc76e0f0c056e9ddff0cc0',1,'BedFeature::BedFeature()'],['../class_bed_feature.html#a654e4e0aa7291fbd7659b1e2140e38ee',1,'BedFeature::BedFeature(int32_t itemIndex, std::string chromosome, int32_t startBase, int32_t endBase, std::string restOfFieldsString)']]],
  ['bedfeature_2eh',['BedFeature.h',['../_bed_feature_8h.html',1,'']]],
  ['bedgraph',['bedgraph',['../classvap_1_1bedgraph.html',1,'vap']]],
  ['bedgraph',['BEDGRAPH',['../structvap_1_1file__format.html#aef82a48b1ffde6a4da86909871f542b5a5373f4ea641e1e7fcf9b287baad39bb3',1,'vap::file_format::BEDGRAPH()'],['../classvap_1_1bedgraph.html#aaed5e07c7668b03eb41a2cd9c69bcefc',1,'vap::bedgraph::bedgraph()'],['../namespace_wig_type_namespace.html#a7c3ff81b366e7e42df0196e922ad0536a25e4aa36d37ba65ded4dd4ef1f76cc9b',1,'WigTypeNamespace::BedGraph()']]],
  ['bedgraph_5fdataset_2ecpp',['bedgraph_dataset.cpp',['../bedgraph__dataset_8cpp.html',1,'']]],
  ['bedgraph_5fdataset_2eh',['bedgraph_dataset.h',['../bedgraph__dataset_8h.html',1,'']]],
  ['bedgraph_5fentry',['bedgraph_entry',['../structvap_1_1bedgraph__entry.html',1,'vap']]],
  ['bedgraph_5fentry',['bedgraph_entry',['../structvap_1_1bedgraph__entry.html#a39223ceceb088451aeb6705f605a24e2',1,'vap::bedgraph_entry']]],
  ['bedgraph_5fitem_5fsize',['BEDGRAPH_ITEM_SIZE',['../class_big_wig_section_header.html#a986d1796c2f31a7f2381f84c079dc230',1,'BigWigSectionHeader']]],
  ['begin',['begin',['../structvap_1_1base__entry.html#a1e1ccc6fe5eea8f03f71865963e17aeb',1,'vap::base_entry::begin()'],['../structvap_1_1coordinates__region.html#a6b5e2409650f47359700dbf6ac9c4728',1,'vap::coordinates_region::begin()'],['../structvap_1_1reference__filter__entry_1_1range__entry.html#a546cdfed4ab28c16ded9dbc931dcdf45',1,'vap::reference_filter_entry::range_entry::begin()'],['../classvap_1_1coordinates.html#ad396d93e4a7e1aaf69bef11424ae9e91',1,'vap::coordinates::begin()'],['../classvap_1_1agg__output__window.html#a3478022c481427152cbc8e88e11c01da',1,'vap::agg_output_window::begin()'],['../classvap_1_1ind__output__window.html#a4776d87a20252d3c3c8cda3ee692975d',1,'vap::ind_output_window::begin()']]],
  ['bigbed_5fmagic_5fhtl',['BIGBED_MAGIC_HTL',['../class_b_b_file_header.html#a996e40efbc089fa7a952a3875ecb5b6e',1,'BBFileHeader']]],
  ['bigbed_5fmagic_5flth',['BIGBED_MAGIC_LTH',['../class_b_b_file_header.html#a95a62658c27dbe7276d32a5b7188a7d4',1,'BBFileHeader']]],
  ['bigbeddatablock',['BigBedDataBlock',['../class_big_bed_data_block.html',1,'BigBedDataBlock'],['../class_big_bed_data_block.html#a39c69a38a763cfea5ebe72f8f2f08610',1,'BigBedDataBlock::BigBedDataBlock()'],['../class_big_bed_data_block.html#af1dc878e60d70847fa0638927b3f1c30',1,'BigBedDataBlock::BigBedDataBlock(std::ifstream *fis, RPTreeLeafNodeItem *leafHitItem, std::map&lt; int32_t, std::string &gt; chromosomeMap, int32_t uncompressBufSize)']]],
  ['bigbeddatablock_2ecpp',['BigBedDataBlock.cpp',['../_big_bed_data_block_8cpp.html',1,'']]],
  ['bigbeddatablock_2eh',['BigBedDataBlock.h',['../_big_bed_data_block_8h.html',1,'']]],
  ['bigbediterator',['BigBedIterator',['../class_big_bed_iterator.html',1,'BigBedIterator'],['../class_big_bed_iterator.html#ad530d490cc6e78f3ec05b8250a87ee48',1,'BigBedIterator::BigBedIterator()']]],
  ['bigbediterator_2ecpp',['BigBedIterator.cpp',['../_big_bed_iterator_8cpp.html',1,'']]],
  ['bigbediterator_2eh',['BigBedIterator.h',['../_big_bed_iterator_8h.html',1,'']]],
  ['bigbyte',['BigByte',['../namespaceendian.html#a25b8bd0eeeaa69416ef479c8c2ddf803',1,'endian']]],
  ['bigdouble',['BigDouble',['../namespaceendian.html#a50c94097dd9608817dd8dbcf61fdb786',1,'endian']]],
  ['bigendiansystem',['BigEndianSystem',['../namespaceendian.html#aa6b8d07f4df1ea23fe55ed55c7997a2f',1,'endian']]],
  ['bigfloat',['BigFloat',['../namespaceendian.html#a9d814214b246cd00dedcd1d4d9e59348',1,'endian']]],
  ['biglong',['BigLong',['../namespaceendian.html#a3ade351282c522ef65c2a4350402c18e',1,'endian']]],
  ['bigshort',['BigShort',['../namespaceendian.html#a38cfcd8b26f8d19e557ffdd99a9fff83',1,'endian']]],
  ['bigwig',['BIGWIG',['../structvap_1_1file__format.html#aef82a48b1ffde6a4da86909871f542b5a5fe271bf660ade74b52e80b45fc39522',1,'vap::file_format::BIGWIG()'],['../classvap_1_1bigwig.html#a7d0e27c5cc21465515f9dc1ce5d20dae',1,'vap::bigwig::bigwig()']]],
  ['bigwig',['bigwig',['../classvap_1_1bigwig.html',1,'vap']]],
  ['bigwig_5fdataset_2ecpp',['bigwig_dataset.cpp',['../bigwig__dataset_8cpp.html',1,'']]],
  ['bigwig_5fdataset_2eh',['bigwig_dataset.h',['../bigwig__dataset_8h.html',1,'']]],
  ['bigwig_5fentry',['bigwig_entry',['../structvap_1_1bigwig__entry.html#a62a6c4da56da4202e24bb605b54ffc05',1,'vap::bigwig_entry']]],
  ['bigwig_5fentry',['bigwig_entry',['../structvap_1_1bigwig__entry.html',1,'vap']]],
  ['bigwig_5fmagic_5fhtl',['BIGWIG_MAGIC_HTL',['../class_b_b_file_header.html#a8965e95af71bdaf90048dd511b13a08c',1,'BBFileHeader']]],
  ['bigwig_5fmagic_5flth',['BIGWIG_MAGIC_LTH',['../class_b_b_file_header.html#abd5a0adf789e63673ebbaf6648b6d1b1',1,'BBFileHeader']]],
  ['bigwigdatablock',['BigWigDataBlock',['../class_big_wig_data_block.html',1,'BigWigDataBlock'],['../class_big_wig_data_block.html#ad463c96fac7304a85631bd487b94c61d',1,'BigWigDataBlock::BigWigDataBlock()'],['../class_big_wig_data_block.html#ac38c93ede5c69f95dcae6c9fa3d4423a',1,'BigWigDataBlock::BigWigDataBlock(std::ifstream *fis, std::vector&lt; RPTreeLeafNodeItem * &gt;::iterator leafIter, int32_t uncompressBufSize)']]],
  ['bigwigdatablock_2ecpp',['BigWigDataBlock.cpp',['../_big_wig_data_block_8cpp.html',1,'']]],
  ['bigwigdatablock_2eh',['BigWigDataBlock.h',['../_big_wig_data_block_8h.html',1,'']]],
  ['bigwigiterator',['BigWigIterator',['../class_big_wig_iterator.html',1,'BigWigIterator'],['../class_big_wig_iterator.html#a76b078b84deb96e2e1e6059a94fa0879',1,'BigWigIterator::BigWigIterator()'],['../class_big_wig_iterator.html#aace85047f570ad1fa6717ead5d7460b4',1,'BigWigIterator::BigWigIterator(std::ifstream *fis, BPTree *chromIDTree, RPTree *chromDataTree, RPChromosomeRegion *selectionRegion, bool contained)'],['../class_big_wig_iterator.html#adabbeae0519db502d5ebe1ded373876d',1,'BigWigIterator::BigWigIterator(const BigWigIterator &amp;other)']]],
  ['bigwigiterator_2ecpp',['BigWigIterator.cpp',['../_big_wig_iterator_8cpp.html',1,'']]],
  ['bigwigiterator_2eh',['BigWigIterator.h',['../_big_wig_iterator_8h.html',1,'']]],
  ['bigwigsection',['BigWigSection',['../class_big_wig_section.html',1,'BigWigSection'],['../class_big_wig_section.html#a1672db58c474db3d63f329f7634bb202',1,'BigWigSection::BigWigSection()'],['../class_big_wig_section.html#a4ec195dc19941aabddbfd9936825f45e',1,'BigWigSection::BigWigSection(std::vector&lt; char &gt; &amp;sectionBuffer, std::map&lt; uint32_t, std::string &gt; *chromosomeMap, std::vector&lt; RPTreeLeafNodeItem * &gt;::iterator leafHitItem)']]],
  ['bigwigsection_2ecpp',['BigWigSection.cpp',['../_big_wig_section_8cpp.html',1,'']]],
  ['bigwigsection_2eh',['BigWigSection.h',['../_big_wig_section_8h.html',1,'']]],
  ['bigwigsectionheader',['BigWigSectionHeader',['../class_big_wig_section_header.html',1,'BigWigSectionHeader'],['../class_big_wig_section_header.html#ac1627b5c0d128bb92049fe22feb71f05',1,'BigWigSectionHeader::BigWigSectionHeader()'],['../class_big_wig_section_header.html#a6d0c69d64015a33416683a053e73ff78',1,'BigWigSectionHeader::BigWigSectionHeader(std::ifstream &amp;bdis)'],['../class_big_wig_section_header.html#ab027546f18dcbf839901bb40c7b53339',1,'BigWigSectionHeader::BigWigSectionHeader(std::stringstream &amp;bdis)']]],
  ['bigwigsectionheader_2ecpp',['BigWigSectionHeader.cpp',['../_big_wig_section_header_8cpp.html',1,'']]],
  ['bigwigsectionheader_2eh',['BigWigSectionHeader.h',['../_big_wig_section_header_8h.html',1,'']]],
  ['bigwigsectionhelper_2ecpp',['BigWigSectionHelper.cpp',['../_big_wig_section_helper_8cpp.html',1,'']]],
  ['bigwigsectionhelper_2eh',['BigWigSectionHelper.h',['../_big_wig_section_helper_8h.html',1,'']]],
  ['block',['block',['../classvap_1_1block.html',1,'vap']]],
  ['block',['block',['../classvap_1_1block.html#ae02c3804a158725636b225458e03d545',1,'vap::block::block(int begin=NPOS, int end=NPOS, uint_t capacity=-1, block_alignment::alignment alignment=block_alignment::LEFT)'],['../classvap_1_1block.html#a65ce779f9ff2a295d3a177697cf2fb29',1,'vap::block::block(const coordinates &amp;coordinates, uint_t capacity=-1, block_alignment::alignment alignment=block_alignment::LEFT)']]],
  ['block_2ecpp',['block.cpp',['../block_8cpp.html',1,'']]],
  ['block_2eh',['block.h',['../block_8h.html',1,'']]],
  ['block_5falignment',['block_alignment',['../structvap_1_1block__alignment.html',1,'vap']]],
  ['block_5falignment',['BLOCK_ALIGNMENT',['../parameters_8h.html#a49386f9129671d48d07ebe48bc4d3493',1,'parameters.h']]],
  ['block_5fsplit_5falignment',['block_split_alignment',['../structvap_1_1block__split__alignment.html',1,'vap']]],
  ['block_5fsplit_5falignment',['BLOCK_SPLIT_ALIGNMENT',['../parameters_8h.html#a6c482c32e61d0c12035ba1db55da8c08',1,'parameters.h']]],
  ['block_5fsplit_5ftype',['BLOCK_SPLIT_TYPE',['../parameters_8h.html#a244ad452305e196e2dc51ff766f2315f',1,'parameters.h']]],
  ['block_5fsplit_5ftype',['block_split_type',['../structvap_1_1block__split__type.html',1,'vap']]],
  ['block_5fsplit_5fvalue',['BLOCK_SPLIT_VALUE',['../parameters_8h.html#a29694c8803e69be1bffbf54e199d2f83',1,'parameters.h']]],
  ['blockalignment',['blockAlignment',['../structvap_1_1parameters__definition.html#af224f17aa023daad7fc3c627dc806e26',1,'vap::parameters_definition']]],
  ['blocks',['blocks',['../classvap_1_1reference__feature.html#a95bf79b88bc42a0da31fdf4f75f78315',1,'vap::reference_feature::blocks() const '],['../classvap_1_1reference__feature.html#a36d0f3277c79646f1eb72e564f042561',1,'vap::reference_feature::blocks()']]],
  ['blocks_5ft',['blocks_t',['../namespacevap.html#aa9f9ae49a0c6ff711921914191bab06c',1,'vap']]],
  ['blocksplitalignement',['blockSplitAlignement',['../structvap_1_1parameters__definition.html#a5d37d25fd0c8ef55588ca2367463ac6a',1,'vap::parameters_definition']]],
  ['blocksplittype',['blockSplitType',['../structvap_1_1parameters__definition.html#a108f85e22230cd26bdbfd71c2351f742',1,'vap::parameters_definition']]],
  ['blocksplitvalue',['blockSplitValue',['../structvap_1_1parameters__definition.html#a760120f545062a9cc6adedb02024c672',1,'vap::parameters_definition']]],
  ['bptree',['BPTree',['../class_b_p_tree.html',1,'BPTree'],['../class_b_p_tree.html#a947d1074e49bc2f13fa93a1027eda48b',1,'BPTree::BPTree()'],['../class_b_p_tree.html#af3979b6123a2d2e0c9e93a82355ed030',1,'BPTree::BPTree(std::ifstream &amp;fis, uint64_t fileOffset)']]],
  ['bptree_2ecpp',['BPTree.cpp',['../_b_p_tree_8cpp.html',1,'']]],
  ['bptree_2eh',['BPTree.h',['../_b_p_tree_8h.html',1,'']]],
  ['bptree_5fheader_5fsize',['BPTREE_HEADER_SIZE',['../class_b_p_tree_header.html#a4b020a624678f0b90b2f7433d74c2611',1,'BPTreeHeader']]],
  ['bptree_5fmagic_5flth',['BPTREE_MAGIC_LTH',['../class_b_p_tree_header.html#a37101d379a9e2b74e1c7a0f0f126188e',1,'BPTreeHeader']]],
  ['bptree_5fnode_5fformat_5fsize',['BPTREE_NODE_FORMAT_SIZE',['../class_b_p_tree.html#acd8894efb2a8108647b78eba299b2396',1,'BPTree']]],
  ['bptree_5fnode_5fitem_5fsize',['BPTREE_NODE_ITEM_SIZE',['../class_b_p_tree.html#a38d6e28b5af53975fa7f9e574ddaa6ca',1,'BPTree']]],
  ['bptreechildnode',['BPTreeChildNode',['../class_b_p_tree_child_node.html',1,'BPTreeChildNode'],['../class_b_p_tree_child_node.html#a72f5598288b7fb30783406f298d023c5',1,'BPTreeChildNode::BPTreeChildNode()'],['../class_b_p_tree_child_node.html#acdbbcebe87182acbbea8323a0e0ae3fe',1,'BPTreeChildNode::BPTreeChildNode(int64_t nodeIndex)']]],
  ['bptreechildnode_2ecpp',['BPTreeChildNode.cpp',['../_b_p_tree_child_node_8cpp.html',1,'']]],
  ['bptreechildnode_2eh',['BPTreeChildNode.h',['../_b_p_tree_child_node_8h.html',1,'']]],
  ['bptreechildnodeitem',['BPTreeChildNodeItem',['../class_b_p_tree_child_node_item.html',1,'BPTreeChildNodeItem'],['../class_b_p_tree_child_node_item.html#ace73664bd1d21b0207474d009d72a5bb',1,'BPTreeChildNodeItem::BPTreeChildNodeItem()']]],
  ['bptreechildnodeitem_2ecpp',['BPTreeChildNodeItem.cpp',['../_b_p_tree_child_node_item_8cpp.html',1,'']]],
  ['bptreechildnodeitem_2eh',['BPTreeChildNodeItem.h',['../_b_p_tree_child_node_item_8h.html',1,'']]],
  ['bptreeheader',['BPTreeHeader',['../class_b_p_tree_header.html',1,'BPTreeHeader'],['../class_b_p_tree_header.html#a0c058dd49e5c818da2c5e0b043dca066',1,'BPTreeHeader::BPTreeHeader()']]],
  ['bptreeheader_2ecpp',['BPTreeHeader.cpp',['../_b_p_tree_header_8cpp.html',1,'']]],
  ['bptreeheader_2eh',['BPTreeHeader.h',['../_b_p_tree_header_8h.html',1,'']]],
  ['bptreeleafnode',['BPTreeLeafNode',['../class_b_p_tree_leaf_node.html',1,'BPTreeLeafNode'],['../class_b_p_tree_leaf_node.html#a1730c83057ef387ed54b3a04ced009f2',1,'BPTreeLeafNode::BPTreeLeafNode()'],['../class_b_p_tree_leaf_node.html#ad00ee577ae44b95f5ac7d5868d1eb497',1,'BPTreeLeafNode::BPTreeLeafNode(int64_t nodeIndex)']]],
  ['bptreeleafnode_2ecpp',['BPTreeLeafNode.cpp',['../_b_p_tree_leaf_node_8cpp.html',1,'']]],
  ['bptreeleafnode_2eh',['BPTreeLeafNode.h',['../_b_p_tree_leaf_node_8h.html',1,'']]],
  ['bptreeleafnodeitem',['BPTreeLeafNodeItem',['../class_b_p_tree_leaf_node_item.html',1,'BPTreeLeafNodeItem'],['../class_b_p_tree_leaf_node_item.html#ab3ee0046c65725a2796d8097c4501d63',1,'BPTreeLeafNodeItem::BPTreeLeafNodeItem()'],['../class_b_p_tree_leaf_node_item.html#a5c10dff20a86ff858101c6cc4147d8fd',1,'BPTreeLeafNodeItem::BPTreeLeafNodeItem(uint64_t leafIndex, std::string chromKey, uint32_t chromID, uint32_t chromSize)']]],
  ['bptreeleafnodeitem_2ecpp',['BPTreeLeafNodeItem.cpp',['../_b_p_tree_leaf_node_item_8cpp.html',1,'']]],
  ['bptreeleafnodeitem_2eh',['BPTreeLeafNodeItem.h',['../_b_p_tree_leaf_node_item_8h.html',1,'']]],
  ['bptreenode',['BPTreeNode',['../class_b_p_tree_node.html',1,'']]],
  ['bptreenode_2eh',['BPTreeNode.h',['../_b_p_tree_node_8h.html',1,'']]],
  ['bptreenodeitem',['BPTreeNodeItem',['../class_b_p_tree_node_item.html',1,'']]],
  ['bptreenodeitem_2eh',['BPTreeNodeItem.h',['../_b_p_tree_node_item_8h.html',1,'']]],
  ['browser_5ftag',['BROWSER_TAG',['../defs_8h.html#a3344edb87934b60c7c325ebb5dc0e54c',1,'defs.h']]],
  ['buffer',['buffer',['../classvap_1_1util_1_1file__reader.html#a2a449baec8a79b45998bc0eef4ab67b1',1,'vap::util::file_reader']]],
  ['buffer_5freader',['buffer_reader',['../classvap_1_1util_1_1buffer__reader.html',1,'vap::util']]],
  ['buffer_5freader',['buffer_reader',['../classvap_1_1util_1_1buffer__reader.html#a9c63d083d0968d98c043a193179e3f3e',1,'vap::util::buffer_reader']]],
  ['buffer_5freader_2eh',['buffer_reader.h',['../buffer__reader_8h.html',1,'']]],
  ['buffer_5fsize',['buffer_size',['../classvap_1_1util_1_1file__reader.html#a708ece0e348f0210c799596f88a1056b',1,'vap::util::file_reader']]],
  ['buffer_5ftype',['buffer_type',['../namespacevap_1_1util.html#a09e325b5bbdf0073482f6ac4ed76d967',1,'vap::util']]],
  ['buildblocks',['buildBlocks',['../classvap_1_1reference__feature.html#ad58c05e6c6e5c405ef6c3278d862551f',1,'vap::reference_feature']]],
  ['buildsortedgenome',['buildSortedGenome',['../vap__core_8cpp.html#a6dc76dd6f06e6d9464cf2c0821f10af3',1,'vap_core.cpp']]],
  ['byte',['byte',['../namespaceendian.html#aabed4b79756978497684d45097e9a867',1,'endian']]],
  ['bytenoswap',['ByteNoSwap',['../namespaceendian.html#a9f244167bc87a9fd4ff62980da515d23',1,'endian']]],
  ['byteswap',['ByteSwap',['../namespaceendian.html#a1bf6afbec88b408cde3c2fa7e236d056',1,'endian']]]
];
