var searchData=
[
  ['name',['name',['../structvap_1_1genepred__entry.html#a85a573a53227cd75f10d3929047f6a1d',1,'vap::genepred_entry::name()'],['../structvap_1_1genome__entry.html#a1786424fc26023b47b64903eae75b280',1,'vap::genome_entry::name()'],['../structvap_1_1reference__annotation.html#af3cdd51244652d416966867bc8a68968',1,'vap::reference_annotation::name()'],['../structvap_1_1reference__coordinates.html#a2089ea5695fe79d68638679efc81da50',1,'vap::reference_coordinates::name()'],['../structvap_1_1reference__filter__entry_1_1name__entry.html#aa84ccccc25c9700909ec341327b6e23d',1,'vap::reference_filter_entry::name_entry::name()'],['../structvap_1_1reference__filter__entry.html#a5972d03f31de433ab4e8212ef26079ba',1,'vap::reference_filter_entry::name()']]],
  ['next',['next',['../structvap_1_1genome__entry__pointer.html#a006a1cd61e4c769a6c002a3507a38654',1,'vap::genome_entry_pointer']]]
];
