var searchData=
[
  ['aggregate_5fdata_5ftype',['AGGREGATE_DATA_TYPE',['../parameters_8h.html#a9835af4b24123f532c427df3a0b460bd',1,'parameters.h']]],
  ['analysis_5fmethod',['ANALYSIS_METHOD',['../parameters_8h.html#a6a30283770d25c8fc70085eb43c0549a',1,'parameters.h']]],
  ['analysis_5fmode',['ANALYSIS_MODE',['../parameters_8h.html#a225103bee6e42c29fcd71487e923af25',1,'parameters.h']]],
  ['annotation_5fcoordinates_5ftype',['ANNOTATION_COORDINATES_TYPE',['../parameters_8h.html#a2ab4223e20b6b917510c7de1f96117d9',1,'parameters.h']]],
  ['annotations_5fpath',['ANNOTATIONS_PATH',['../parameters_8h.html#a8609678f412f59cc5302e458810e13ce',1,'parameters.h']]],
  ['appname',['APPNAME',['../defs_8h.html#a237e4d19d7046d08c4cc00a45808feaf',1,'defs.h']]],
  ['appversion',['APPVERSION',['../defs_8h.html#ae5cf02dc75aa3df36c4de08e5004fc53',1,'defs.h']]],
  ['assert',['ASSERT',['../assert_8h.html#aa06eedd6f738a415870e97a375337d51',1,'assert.h']]]
];
