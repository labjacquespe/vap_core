var searchData=
[
  ['score',['score',['../structvap_1_1gtf__entry.html#acaf4218a70a2b5371088235a34b08a49',1,'vap::gtf_entry']]],
  ['section_5fheader_5fsize',['SECTION_HEADER_SIZE',['../class_big_wig_section_header.html#a42faec2bb23991861b8b105393516d14',1,'BigWigSectionHeader']]],
  ['selectionpath',['selectionPath',['../structvap_1_1parameters__definition.html#a3f08a9f68d395e9a0088d14ede8fa0fc',1,'vap::parameters_definition']]],
  ['smoothingwindows',['smoothingWindows',['../structvap_1_1parameters__definition.html#af95fe5eaab3886da6cac4bc1feacae38',1,'vap::parameters_definition']]],
  ['source',['source',['../structvap_1_1gtf__entry.html#af79ea674824028eb10f7ae63a2910821',1,'vap::gtf_entry']]],
  ['start',['start',['../structvap_1_1gtf__entry.html#a5d17b4b094e579a287f9718a7bd56349',1,'vap::gtf_entry']]],
  ['startbase_5f',['startBase_',['../class_r_p_chromosome_region.html#a67a4f9f9f981b35d92147cf334098656',1,'RPChromosomeRegion']]],
  ['startchromid_5f',['startChromID_',['../class_r_p_chromosome_region.html#a6a8184b5805fed5e0e830f2c8057d433',1,'RPChromosomeRegion']]],
  ['strand',['strand',['../structvap_1_1genepred__entry.html#a14ee679bc3256aeee97b514cb25b93a7',1,'vap::genepred_entry::strand()'],['../structvap_1_1genome__entry.html#aa4ecbd2fcb2e35eb44595f8de7ecd941',1,'vap::genome_entry::strand()'],['../structvap_1_1gtf__entry.html#afa338561415514b666be2e8e41861c37',1,'vap::gtf_entry::strand()'],['../structvap_1_1reference__coordinates.html#a08bcbc53123dd8b3f42ce58ea88f1400',1,'vap::reference_coordinates::strand()']]]
];
