var searchData=
[
  ['genepred_5fentry',['genepred_entry',['../structvap_1_1genepred__entry.html',1,'vap']]],
  ['genome',['genome',['../classvap_1_1genome.html',1,'vap']]],
  ['genome_5fentry',['genome_entry',['../structvap_1_1genome__entry.html',1,'vap']]],
  ['genome_5fentry_5fpointer',['genome_entry_pointer',['../structvap_1_1genome__entry__pointer.html',1,'vap']]],
  ['graph_5forientation',['graph_orientation',['../structvap_1_1graph__orientation.html',1,'vap']]],
  ['group_5ffeature',['group_feature',['../classvap_1_1group__feature.html',1,'vap']]],
  ['group_5ffeature_3c_20vap_3a_3areference_5ffeature_20_3e',['group_feature&lt; vap::reference_feature &gt;',['../classvap_1_1group__feature.html',1,'vap']]],
  ['gtf_5fentry',['gtf_entry',['../structvap_1_1gtf__entry.html',1,'vap']]]
];
