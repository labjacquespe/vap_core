var searchData=
[
  ['zoomdatabloch',['ZoomDataBLoch',['../class_zoom_data_b_loch.html#ab07a5393b7534002377a81ebd373b921',1,'ZoomDataBLoch']]],
  ['zoomdatablock',['ZoomDataBlock',['../class_zoom_data_block.html#a9ce8e9d6d47d278dbf7b22a20794b17e',1,'ZoomDataBlock']]],
  ['zoomdatarecord',['ZoomDataRecord',['../class_zoom_data_record.html#a846e0a8ad31ad56443fe17129ee4f0ca',1,'ZoomDataRecord::ZoomDataRecord()'],['../class_zoom_data_record.html#a4451d3cadf0c8c88562bfb2dc18cedc5',1,'ZoomDataRecord::ZoomDataRecord(int32_t zoomLevel, int32_t recordNumber, std::string chromName, int32_t chromId, int32_t chromStart, int32_t chromEnd, int32_t validCount, float minVal, float maxVal, float sumData, float sumSquares)']]],
  ['zoomleveliterator',['ZoomLevelIterator',['../class_zoom_level_iterator.html#a7e5b28941befebc91dfba22f7f2d3b1c',1,'ZoomLevelIterator::ZoomLevelIterator()'],['../class_zoom_level_iterator.html#a6f254b5525223ee451e54caf1a1e952a',1,'ZoomLevelIterator::ZoomLevelIterator(std::ifstream *fis, BPTree *chromIDTree, RPTree *zoomDataTree, int32_t zoomLevel, RPChromosomeRegion *selectionRegion, bool contained)']]]
];
