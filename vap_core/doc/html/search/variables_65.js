var searchData=
[
  ['end',['end',['../structvap_1_1base__entry.html#a9117e9414d3211feb4326068b4e28a61',1,'vap::base_entry::end()'],['../structvap_1_1coordinates__region.html#a67a5d6108306d00426e9e9f565de6806',1,'vap::coordinates_region::end()'],['../structvap_1_1gtf__entry.html#ad16a839fcf059330c061af0be1f85417',1,'vap::gtf_entry::end()'],['../structvap_1_1reference__filter__entry_1_1range__entry.html#a93e57a731aaac0105f8aa0b8ec033e29',1,'vap::reference_filter_entry::range_entry::end()']]],
  ['endbase_5f',['endBase_',['../class_r_p_chromosome_region.html#aaf58f1148047ec9113a842b58105d0b7',1,'RPChromosomeRegion']]],
  ['endchromid_5f',['endChromID_',['../class_r_p_chromosome_region.html#adb39ce500dbb198d60b1acde9b7e8be3',1,'RPChromosomeRegion']]],
  ['exclusionpath',['exclusionPath',['../structvap_1_1parameters__definition.html#afe83e7265699019bfdd4f509f93ceb95',1,'vap::parameters_definition']]],
  ['exonends',['exonEnds',['../structvap_1_1genepred__entry.html#a46c6aab1e644f3794e5799ec14c797ee',1,'vap::genepred_entry']]],
  ['exons',['exons',['../structvap_1_1genome__entry.html#ac0d817fba2dcbe6e16ac6cf2f4868220',1,'vap::genome_entry']]],
  ['exons_5fcount',['exons_count',['../structvap_1_1genome__entry.html#aee23e710e474b7a04037cfd4a498290d',1,'vap::genome_entry']]],
  ['exonscount',['exonsCount',['../structvap_1_1genepred__entry.html#af91002b2177baab4481b59d5e5db80cc',1,'vap::genepred_entry']]],
  ['exonstarts',['exonStarts',['../structvap_1_1genepred__entry.html#a10cbffaa09564a4b3e160a98e64a3565',1,'vap::genepred_entry']]],
  ['extra',['extra',['../structvap_1_1genepred__entry.html#a6d3bc0e070c524603806ec2998f45478',1,'vap::genepred_entry::extra()'],['../structvap_1_1genome__entry.html#aa7e12cc19f8949757be394be8569626d',1,'vap::genome_entry::extra()']]]
];
