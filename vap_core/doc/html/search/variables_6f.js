var searchData=
[
  ['onegraphperdataset',['oneGraphPerDataset',['../structvap_1_1parameters__definition.html#a2881217ba4a892973b42aaaaa2e244a8',1,'vap::parameters_definition']]],
  ['onegraphpergroup',['oneGraphPerGroup',['../structvap_1_1parameters__definition.html#afa3e7b18408b279f90e7bdb168845fce',1,'vap::parameters_definition']]],
  ['oneorientationpergraph',['oneOrientationPerGraph',['../structvap_1_1parameters__definition.html#a7e446cb989cb0c3fed33751abfcfbe72',1,'vap::parameters_definition']]],
  ['onerefptboundary',['oneRefPtBoundary',['../structvap_1_1parameters__definition.html#afd315a68a8afe6b69dd8f9e9e57f19d5',1,'vap::parameters_definition']]],
  ['orientationsubgroups',['orientationSubgroups',['../structvap_1_1parameters__definition.html#aba16e25c45705bddff9878a649f425e4',1,'vap::parameters_definition']]],
  ['outputdirectory',['outputDirectory',['../structvap_1_1parameters__definition.html#a4c22672cd293fb986fa1c12b1c307756',1,'vap::parameters_definition']]]
];
