var class_r_p_tree_node_proxy =
[
    [ "~RPTreeNodeProxy", "class_r_p_tree_node_proxy.html#a98e1c334d98a013c8f3bce452ea7eb51", null ],
    [ "RPTreeNodeProxy", "class_r_p_tree_node_proxy.html#a174efe274cdc09c0b512c41c197b4de2", null ],
    [ "compareRegions", "class_r_p_tree_node_proxy.html#aa0036e3169cc60e612f37d34d6a32d60", null ],
    [ "deleteItem", "class_r_p_tree_node_proxy.html#aff89b520ddc3d13c95fc71c905a18957", null ],
    [ "getChromosomeBounds", "class_r_p_tree_node_proxy.html#ad76319c5d140c3982a2a2913692a67f6", null ],
    [ "getItem", "class_r_p_tree_node_proxy.html#a758fd47f9950eb0e406f245efd788ae8", null ],
    [ "getItemCount", "class_r_p_tree_node_proxy.html#a7880afde292cfde1415cf4c20a9bf7de", null ],
    [ "insertItem", "class_r_p_tree_node_proxy.html#a7159e4623da71acbd63f5f867c5b30ad", null ],
    [ "isLeaf", "class_r_p_tree_node_proxy.html#a3a9ee386ed2d8893019f2269c6e1f4ce", null ],
    [ "chromId_", "class_r_p_tree_node_proxy.html#aff743ff4db343930b24e752046041cd2", null ],
    [ "fileOffset_", "class_r_p_tree_node_proxy.html#a0ef4ed0a9d9a008a29947f83bf97b767", null ],
    [ "fis_", "class_r_p_tree_node_proxy.html#a6cf0b029c66757619db1541d35dc2ef0", null ]
];