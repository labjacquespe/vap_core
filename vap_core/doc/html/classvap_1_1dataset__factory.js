var classvap_1_1dataset__factory =
[
    [ "dataset_factory", "classvap_1_1dataset__factory.html#a2c01724449c2b24967718079e4834ec8", null ],
    [ "makeBedgraphDataset", "classvap_1_1dataset__factory.html#a7f07548922b07e97aaec777e607db7b8", null ],
    [ "makeBigWigDataset", "classvap_1_1dataset__factory.html#a8a65f9aac73f42a4b8967c162059a2d7", null ],
    [ "makeGenomeDataset", "classvap_1_1dataset__factory.html#aebd2a7b6160ea8ee318a42576c047fae", null ],
    [ "makeWigDataset", "classvap_1_1dataset__factory.html#abe4682f6ab71a6a87ccb09041d002d18", null ]
];