var classvap_1_1reference__feature =
[
    [ "reference_feature", "classvap_1_1reference__feature.html#a0ec002add41d2f38a81b7c4cf844568f", null ],
    [ "reference_feature", "classvap_1_1reference__feature.html#a476e0347e4df39940f7191a0fb6d58d2", null ],
    [ "~reference_feature", "classvap_1_1reference__feature.html#a5efd3c34bf9459b7871936c0b81611ec", null ],
    [ "_initialize", "classvap_1_1reference__feature.html#ab2b5af64ac31a1ee4469feb6edb033b4", null ],
    [ "_set_1referencePoint", "classvap_1_1reference__feature.html#aa5a0a4b06e514d0710d9780bda683f00", null ],
    [ "_set_2referencePoint", "classvap_1_1reference__feature.html#a818b17fd3454bf338f37536fd8d516fd", null ],
    [ "_set_3referencePoint", "classvap_1_1reference__feature.html#afd23ef290bdba997bed4022783a53e81", null ],
    [ "_set_4referencePoint", "classvap_1_1reference__feature.html#a885b9f9190ce6c9c6051d96f6bfd140c", null ],
    [ "_set_5referencePoint", "classvap_1_1reference__feature.html#aa6a6e9774ce944c5dcfb58975f0c2e72", null ],
    [ "_set_6referencePoint", "classvap_1_1reference__feature.html#a2797ce12709b015e948164d99c35a1c9", null ],
    [ "_setReferencePoints", "classvap_1_1reference__feature.html#a8d5dd91156217a6273d0d0c3dd2e9f89", null ],
    [ "_updateData", "classvap_1_1reference__feature.html#af52cb0749c1689ce49f7f96a7788dbb7", null ],
    [ "_updateDataZeros", "classvap_1_1reference__feature.html#a0505acd7ed73f65f2c6a32240472a0b6", null ],
    [ "alias", "classvap_1_1reference__feature.html#a06d0ba934e850d0c8dcda966e6e64e04", null ],
    [ "blocks", "classvap_1_1reference__feature.html#a36d0f3277c79646f1eb72e564f042561", null ],
    [ "blocks", "classvap_1_1reference__feature.html#ab230839ff63e155f037af2ef326b66e7", null ],
    [ "buildBlocks", "classvap_1_1reference__feature.html#ad58c05e6c6e5c405ef6c3278d862551f", null ],
    [ "chromosome", "classvap_1_1reference__feature.html#a295b3dcc7724d8370a817236542b5228", null ],
    [ "cleanUpdateTable", "classvap_1_1reference__feature.html#ab65246c2aa5fbab8a8b3f5c3936998b7", null ],
    [ "clear", "classvap_1_1reference__feature.html#a0a6d89ab05c501eea340dc5c94bc3f89", null ],
    [ "count", "classvap_1_1reference__feature.html#aa3e96146faca2775fb644518f6342f70", null ],
    [ "extra", "classvap_1_1reference__feature.html#ad407265dbce0d7428cce0641013117d4", null ],
    [ "featureOfInterest", "classvap_1_1reference__feature.html#a5c5b8d42e502d23f436be111073dbdff", null ],
    [ "fillBlocks", "classvap_1_1reference__feature.html#ad23ee43f01d5f85655b8e761d94ce760", null ],
    [ "groupIndex", "classvap_1_1reference__feature.html#a3e2e067b08df3c4213b05d93c27ad9f4", null ],
    [ "isCrickOriented", "classvap_1_1reference__feature.html#af1911f54bd05fa5d249b7bee80d766a1", null ],
    [ "isInRange", "classvap_1_1reference__feature.html#a4a61254893c38d7e24065be7242d0846", null ],
    [ "name", "classvap_1_1reference__feature.html#adb2c74fb87c102fa785c25b3a0c8f195", null ],
    [ "orientation", "classvap_1_1reference__feature.html#af2f7a805116b5485ddee0300bca9d055", null ],
    [ "prefillUpdateTable", "classvap_1_1reference__feature.html#a8726ce3fa5483afc8491ce5957111de9", null ],
    [ "referencePoints", "classvap_1_1reference__feature.html#a05f0fb582e1510a103ff2aeed4da16dc", null ],
    [ "resetData", "classvap_1_1reference__feature.html#a6e11e5fd38fea3e830de5694c5bf37b2", null ],
    [ "setAlias", "classvap_1_1reference__feature.html#ae2ce11228bb19dd3aa7124cf089455c8", null ],
    [ "setBlocksCoordinates", "classvap_1_1reference__feature.html#ab9f3bae8c3dbae2a157e1171107cd694", null ],
    [ "setBlocksCoordinates", "classvap_1_1reference__feature.html#ae4f5b87f638ab80b3d7e63ea6498bb5e", null ],
    [ "setChromosome", "classvap_1_1reference__feature.html#a02a050b765fe8311d04eb5694740097d", null ],
    [ "setExtra", "classvap_1_1reference__feature.html#a5682e65c3e97e39ed357e1e9eeded371", null ],
    [ "setFeatureOfInterestCoordinates", "classvap_1_1reference__feature.html#adc66c3addeb91e5945d4b69144ba48be", null ],
    [ "setName", "classvap_1_1reference__feature.html#aaa941dc4d628683c863c28b59b51f82f", null ],
    [ "setOrientation", "classvap_1_1reference__feature.html#a6a57a11ba4f0fbe99dc5ccb4da14305a", null ],
    [ "setStrand", "classvap_1_1reference__feature.html#a944231e8bdfcd94095a3a9d37213d91a", null ],
    [ "strand", "classvap_1_1reference__feature.html#a784c60cddb47efc7d929abea5447a441", null ],
    [ "updateRange", "classvap_1_1reference__feature.html#ac353aa61d6b32e247feffbf7f2b4459c", null ],
    [ "updateTable", "classvap_1_1reference__feature.html#a08bbc427449f2b943badc4df9edfa967", null ],
    [ "updateWeigthedMean", "classvap_1_1reference__feature.html#a17a8cff3dd9c9c0ca43ee06d6ad3c21d", null ],
    [ "updateWeigthedMeanZeros", "classvap_1_1reference__feature.html#a29eb7644637fc259ce97209fcbe7b1f9", null ],
    [ "_alias", "classvap_1_1reference__feature.html#af9aefaf3cdf72f1c5c78115352edffcd", null ],
    [ "_blocks", "classvap_1_1reference__feature.html#ad464265f9e6bfd5903e0edacfc8f2fd7", null ],
    [ "_chromosome", "classvap_1_1reference__feature.html#af7847b428e4db85b645183548c14653c", null ],
    [ "_extra", "classvap_1_1reference__feature.html#a12dfe0bade3869a5f3ce603f0affb071", null ],
    [ "_featureOfInterest", "classvap_1_1reference__feature.html#afc2a5ca1222d1badb7444040a4ab9e5d", null ],
    [ "_groupIndex", "classvap_1_1reference__feature.html#a203094c8b9cea10d2a67d021645d9c85", null ],
    [ "_name", "classvap_1_1reference__feature.html#aab453048cea1eb57c9807c79c3f6a43c", null ],
    [ "_referencePoints", "classvap_1_1reference__feature.html#a018ac8f6f70d8d873200e0f512327b0c", null ],
    [ "_strand", "classvap_1_1reference__feature.html#a2ed04982c6cfc5ba3e8d3fae91c7fffc", null ],
    [ "_type", "classvap_1_1reference__feature.html#a85f295daa1964a9dff8a0319dbfe9b37", null ],
    [ "_updateTable", "classvap_1_1reference__feature.html#af50537be044134b4b2ba3339638a9e80", null ]
];