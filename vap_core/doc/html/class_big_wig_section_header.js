var class_big_wig_section_header =
[
    [ "BigWigSectionHeader", "class_big_wig_section_header.html#ac1627b5c0d128bb92049fe22feb71f05", null ],
    [ "~BigWigSectionHeader", "class_big_wig_section_header.html#abeada24266582dc25b0622b0adc3eda5", null ],
    [ "BigWigSectionHeader", "class_big_wig_section_header.html#a6d0c69d64015a33416683a053e73ff78", null ],
    [ "BigWigSectionHeader", "class_big_wig_section_header.html#ab027546f18dcbf839901bb40c7b53339", null ],
    [ "getChromID", "class_big_wig_section_header.html#a2690007efc811532bce8697035013807", null ],
    [ "getChromosomeEnd", "class_big_wig_section_header.html#a6ef681657f0f535cea086f354c80cf42", null ],
    [ "getChromosomeStart", "class_big_wig_section_header.html#a397e58e0caeff43c4175fecc4ecd60eb", null ],
    [ "getItemCount", "class_big_wig_section_header.html#a8715c02f21a5c8bbeb5b0184c4c3ba6c", null ],
    [ "getItemSpan", "class_big_wig_section_header.html#ad129de85a9f8ff80495c00bd2a24874c", null ],
    [ "getItemStep", "class_big_wig_section_header.html#a973a5574535978df56069801d9ac7f32", null ],
    [ "getItemType", "class_big_wig_section_header.html#af999236b768f27f0d98711161bebb944", null ],
    [ "getItemType", "class_big_wig_section_header.html#a6b8b0abb5b36fccdaa6d1f90e8d51f44", null ],
    [ "getReserved", "class_big_wig_section_header.html#a10986d3e7eca4c3d06ea4c62ebcf0878", null ],
    [ "IsValidType", "class_big_wig_section_header.html#afa29389aa7020164032720cf64e30747", null ]
];