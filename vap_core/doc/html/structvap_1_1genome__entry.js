var structvap_1_1genome__entry =
[
    [ "genome_entry", "structvap_1_1genome__entry.html#a0592ab1e77cf41371a9a3ab2989ce81c", null ],
    [ "alias", "structvap_1_1genome__entry.html#a82cd66bc30a426e038522200cf8b66e7", null ],
    [ "exons", "structvap_1_1genome__entry.html#ac0d817fba2dcbe6e16ac6cf2f4868220", null ],
    [ "exons_count", "structvap_1_1genome__entry.html#aee23e710e474b7a04037cfd4a498290d", null ],
    [ "extra", "structvap_1_1genome__entry.html#aa7e12cc19f8949757be394be8569626d", null ],
    [ "name", "structvap_1_1genome__entry.html#a1786424fc26023b47b64903eae75b280", null ],
    [ "score", "structvap_1_1genome__entry.html#a514ec659908059e03ae7244cb6196cc6", null ],
    [ "strand", "structvap_1_1genome__entry.html#aa4ecbd2fcb2e35eb44595f8de7ecd941", null ]
];