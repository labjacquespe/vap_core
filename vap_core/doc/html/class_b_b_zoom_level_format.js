var class_b_b_zoom_level_format =
[
    [ "BBZoomLevelFormat", "class_b_b_zoom_level_format.html#a418916e3d6d903052f1a6ab4c3419a6f", null ],
    [ "~BBZoomLevelFormat", "class_b_b_zoom_level_format.html#aba4dcac81f1f92cf99aca5c449d624fb", null ],
    [ "BBZoomLevelFormat", "class_b_b_zoom_level_format.html#a708f1246c288b609fc85446a8771a416", null ],
    [ "getZoomDataOffset", "class_b_b_zoom_level_format.html#ad5648a22a3010e7a0dbb5eeae47ff414", null ],
    [ "GetZoomDataSize", "class_b_b_zoom_level_format.html#ab9bca6d3878cdb86e4f7d590429d813e", null ],
    [ "getZoomFormatOffset", "class_b_b_zoom_level_format.html#a2b14e4da5372feb91c17a6e1ca5756ea", null ],
    [ "getZoomIndexOffset", "class_b_b_zoom_level_format.html#a25dddd6dec04726a2090a036187b266e", null ],
    [ "getZoomLevel_", "class_b_b_zoom_level_format.html#a636534b8f1c430508a6b823f49606a01", null ],
    [ "getZoomRecordCount", "class_b_b_zoom_level_format.html#ac9a0baaf478c56c0cea1b01e6c8aefdf", null ]
];