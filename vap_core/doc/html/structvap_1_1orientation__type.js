var structvap_1_1orientation__type =
[
    [ "type", "structvap_1_1orientation__type.html#a2f2107e20db714b0a58c8e8490194415", [
      [ "PPP", "structvap_1_1orientation__type.html#a2f2107e20db714b0a58c8e8490194415ab55e74d4007b674b329d70f5550028ba", null ],
      [ "NNN", "structvap_1_1orientation__type.html#a2f2107e20db714b0a58c8e8490194415a4b6d3609c219ad7d7802c0b66d75a085", null ],
      [ "PPN", "structvap_1_1orientation__type.html#a2f2107e20db714b0a58c8e8490194415ad78752bf2efdf5b5297b959b2242e694", null ],
      [ "PNN", "structvap_1_1orientation__type.html#a2f2107e20db714b0a58c8e8490194415a07f8dd448823f2fa8530132719823de4", null ],
      [ "NPP", "structvap_1_1orientation__type.html#a2f2107e20db714b0a58c8e8490194415a16f7d6a5f452dc92e3a9aee220fe8685", null ],
      [ "NNP", "structvap_1_1orientation__type.html#a2f2107e20db714b0a58c8e8490194415ad6a5e50ec5dfe58dc1ab09c0d68505b7", null ],
      [ "NPN", "structvap_1_1orientation__type.html#a2f2107e20db714b0a58c8e8490194415ae3d31b6d6c1368232482785f7e6d9bd1", null ],
      [ "PNP", "structvap_1_1orientation__type.html#a2f2107e20db714b0a58c8e8490194415ad889658dfa403cfb746d24838295f36b", null ]
    ] ]
];