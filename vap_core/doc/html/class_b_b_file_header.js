var class_b_b_file_header =
[
    [ "BBFileHeader", "class_b_b_file_header.html#a56d9cb8983011773c8f403076944bbf3", null ],
    [ "BBFileHeader", "class_b_b_file_header.html#a5c8cf0cb5491cf3fbb3c1a622460ebb4", null ],
    [ "BBFileHeader", "class_b_b_file_header.html#a0d56f3fc1858ec96beb755e0077f1eed", null ],
    [ "getAutoSqlOffset", "class_b_b_file_header.html#ab9be335a37cee4f1c138afc509a444f3", null ],
    [ "getChromosomeTreeOffset", "class_b_b_file_header.html#a1f374af14ac58366490e1d28c05b248a", null ],
    [ "getDefinedFieldCount", "class_b_b_file_header.html#a2628cbe85727919c1e8606bc233463a5", null ],
    [ "getFieldCount", "class_b_b_file_header.html#a3f06d4fd25b1405c2ebd02c250ddf200", null ],
    [ "getFileHeaderSize", "class_b_b_file_header.html#ad8e618fd802bc633aeac0582d170aa3a", null ],
    [ "getFullDataOffset", "class_b_b_file_header.html#a1f3286bea0b9165ea48b543ad239245d", null ],
    [ "getFullIndexOffset", "class_b_b_file_header.html#a1b29d31b28a4b936313e6ff3488237e5", null ],
    [ "getMagic", "class_b_b_file_header.html#a0542268f9e74722ae4c1eca5fd148d69", null ],
    [ "getPath", "class_b_b_file_header.html#a8509bfc88a58fb8a1e42be704aec9e0a", null ],
    [ "getTotalSummaryOffset", "class_b_b_file_header.html#a16c6700793f7edc806c9830caaed7061", null ],
    [ "getUncompressBuffSize", "class_b_b_file_header.html#a9dbf78523eeab7f9d1ffe66adafc9511", null ],
    [ "getVersion", "class_b_b_file_header.html#aebe1218b2bd6255992f4e3d09160f5e4", null ],
    [ "getZoomLevels", "class_b_b_file_header.html#a101289741a3897921921622fb8dc9c45", null ],
    [ "isBigBed", "class_b_b_file_header.html#a56a938dfd18c7a39c5dd7b1d3875d5dc", null ],
    [ "isBigWig", "class_b_b_file_header.html#a96d60721cd4d20c74801173cd0b0d5ba", null ],
    [ "isHeaderOK", "class_b_b_file_header.html#a30345b8f5ecc1f63027e580681d39683", null ]
];