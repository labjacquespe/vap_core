var class_b_b_total_summary_block =
[
    [ "BBTotalSummaryBlock", "class_b_b_total_summary_block.html#af75ecbce94432c40af3f000c691bb2a1", null ],
    [ "BBTotalSummaryBlock", "class_b_b_total_summary_block.html#a2549e5a9b2c83badc0bfc9b346491f96", null ],
    [ "GetBasesCovered_", "class_b_b_total_summary_block.html#a659e89f1b8f06238a172042b2c375eb1", null ],
    [ "GetMaxVal_", "class_b_b_total_summary_block.html#a1d48a7d0d1bc2ec0386e9911cec1125c", null ],
    [ "GetMinVal_", "class_b_b_total_summary_block.html#a57e3776a82161c704635b7da548df771", null ],
    [ "GetSumData_", "class_b_b_total_summary_block.html#a72285cedd3a41e9e8f114fda2439324b", null ],
    [ "GetSummaryBlockOffset_", "class_b_b_total_summary_block.html#a339a4b9bfa8c04189dc47d646537f394", null ],
    [ "GetSumSquares_", "class_b_b_total_summary_block.html#accc67429e4807d3b3dfbf984db8e3d4f", null ],
    [ "SetBasesCovered_", "class_b_b_total_summary_block.html#a0f9896717fc08ecc93914346162dbba8", null ],
    [ "SetMaxVal_", "class_b_b_total_summary_block.html#a0611d720e85204f2c84cbfb292502a79", null ],
    [ "SetMinVal_", "class_b_b_total_summary_block.html#aaece45125f9635d6c913040906eba92f", null ],
    [ "SetSumData_", "class_b_b_total_summary_block.html#a83a74e533789af43b213be5d0dfc2410", null ],
    [ "SetSummaryBlockOffset_", "class_b_b_total_summary_block.html#a77615add5f363bdaf686a80cc8904538", null ],
    [ "SetSumSquares_", "class_b_b_total_summary_block.html#ae2aa8bfe8884bfdfa6d0ce617a59e151", null ]
];