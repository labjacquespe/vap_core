var class_wig_item =
[
    [ "WigItem", "class_wig_item.html#a5112b51765ba0827d983e9b7e1079980", null ],
    [ "getChromosome", "class_wig_item.html#ac04ba27883437234a8165b06c0b54ad6", null ],
    [ "getEndBase", "class_wig_item.html#a38c29bda8f5d539063331efe20d767f4", null ],
    [ "getItemNumber", "class_wig_item.html#a1af64968b724eace15e4e47e296f8211", null ],
    [ "getStartBase", "class_wig_item.html#acacb78644cc284298fdf12550da80faa", null ],
    [ "getWigValue", "class_wig_item.html#a507505dba3284dc588ed8d056fad9ceb", null ]
];