var structvap_1_1logging_options =
[
    [ "options", "structvap_1_1logging_options.html#a8c1722d6d8d1f7954ed5a8a07199f0ce", [
      [ "NONE", "structvap_1_1logging_options.html#a8c1722d6d8d1f7954ed5a8a07199f0cea874c929b3e418730de0bcd6673312280", null ],
      [ "ALL", "structvap_1_1logging_options.html#a8c1722d6d8d1f7954ed5a8a07199f0cea69ddc4244a2ffc9ca66d8ad0e5151ac2", null ],
      [ "INFORMATIONS", "structvap_1_1logging_options.html#a8c1722d6d8d1f7954ed5a8a07199f0cea84ec1bcd079e6f4218e26f8ba5863127", null ],
      [ "WARNINGS", "structvap_1_1logging_options.html#a8c1722d6d8d1f7954ed5a8a07199f0cead70d02e4d4203a1748eb0d666df157ac", null ],
      [ "ERRORS", "structvap_1_1logging_options.html#a8c1722d6d8d1f7954ed5a8a07199f0cea66566a79e627ad610d377c2ab0b72cb9", null ]
    ] ]
];