var structvap_1_1aggregate__data__type =
[
    [ "type", "structvap_1_1aggregate__data__type.html#afcdbfa983cec42d4f9aad8a902c33a07", [
      [ "UNKNOWN", "structvap_1_1aggregate__data__type.html#afcdbfa983cec42d4f9aad8a902c33a07a696b031073e74bf2cb98e5ef201d4aa3", null ],
      [ "MEAN", "structvap_1_1aggregate__data__type.html#afcdbfa983cec42d4f9aad8a902c33a07a4ea6d1161ea24d7599365f574aff6610", null ],
      [ "MEDIAN", "structvap_1_1aggregate__data__type.html#afcdbfa983cec42d4f9aad8a902c33a07a8ab0c3a037e882577dec378985477074", null ],
      [ "MAX", "structvap_1_1aggregate__data__type.html#afcdbfa983cec42d4f9aad8a902c33a07a26a4b44a837bf97b972628509912b4a5", null ],
      [ "MIN", "structvap_1_1aggregate__data__type.html#afcdbfa983cec42d4f9aad8a902c33a07ace31e2a082d17e038fcc6e3006166653", null ]
    ] ]
];