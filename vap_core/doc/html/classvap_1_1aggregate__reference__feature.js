var classvap_1_1aggregate__reference__feature =
[
    [ "aggregate_reference_feature", "classvap_1_1aggregate__reference__feature.html#a827abb3575e3c324c1236629381ac7ea", null ],
    [ "aggregate_reference_feature", "classvap_1_1aggregate__reference__feature.html#a5d953366ab00841e182a12729faf037f", null ],
    [ "~aggregate_reference_feature", "classvap_1_1aggregate__reference__feature.html#a664781efd41b59e658fbfc64339271b5", null ],
    [ "initializePopulation", "classvap_1_1aggregate__reference__feature.html#a2391b2bc3e52fcb36ed6269d7fccf2ae", null ],
    [ "population", "classvap_1_1aggregate__reference__feature.html#ad11e2b04c851b39dd8efcfd23eda4b1d", null ],
    [ "resetData", "classvap_1_1aggregate__reference__feature.html#abf777ca0ff6e2ea5d17aa068c6f2a8ee", null ],
    [ "updateAggregatePhase1", "classvap_1_1aggregate__reference__feature.html#ac55487297fa671337a5d670c7127c650", null ],
    [ "updateAggregatePhase2", "classvap_1_1aggregate__reference__feature.html#a11074a4b3f3cd4a41a84a9aabec643ca", null ]
];