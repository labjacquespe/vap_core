var dir_32576c21c30ecd71937053a15f1f3645 =
[
    [ "file_extension.h", "file__extension_8h.html", "file__extension_8h" ],
    [ "file_format.h", "file__format_8h.html", "file__format_8h" ],
    [ "file_reader.cpp", "file__reader_8cpp.html", null ],
    [ "file_reader.h", "file__reader_8h.html", [
      [ "file_reader", "classvap_1_1io_1_1file__reader.html", "classvap_1_1io_1_1file__reader" ],
      [ "file_reader_gzip", "classvap_1_1io_1_1file__reader__gzip.html", "classvap_1_1io_1_1file__reader__gzip" ],
      [ "file_reader_bzip2", "classvap_1_1io_1_1file__reader__bzip2.html", "classvap_1_1io_1_1file__reader__bzip2" ],
      [ "file_reader_factory", "classvap_1_1io_1_1file__reader__factory.html", null ]
    ] ],
    [ "file_utils.h", "file__utils_8h.html", "file__utils_8h" ],
    [ "format_guess.cpp", "format__guess_8cpp.html", null ],
    [ "format_guess.h", "format__guess_8h.html", "format__guess_8h" ],
    [ "pcre2cpp.h", "pcre2cpp_8h.html", "pcre2cpp_8h" ],
    [ "regex_strings.h", "regex__strings_8h.html", "regex__strings_8h" ]
];