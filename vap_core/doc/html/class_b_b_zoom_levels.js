var class_b_b_zoom_levels =
[
    [ "BBZoomLevels", "class_b_b_zoom_levels.html#a1d039fd78c814221543ca15d178f7612", null ],
    [ "~BBZoomLevels", "class_b_b_zoom_levels.html#ab03a8d991563ffb4e876c45138251a96", null ],
    [ "BBZoomLevels", "class_b_b_zoom_levels.html#abe5d53ff2c83d229826863aa99a1337b", null ],
    [ "getZoomHeaderCount", "class_b_b_zoom_levels.html#a03fc2257faaf692eeab714b7db333498", null ],
    [ "getZoomHeadersOffset", "class_b_b_zoom_levels.html#a3bad0264dcf509e87a5a6b41a6d66b01", null ],
    [ "getZoomLevelFormats", "class_b_b_zoom_levels.html#a4412fb8f703f4da593b67dbf7f40f226", null ],
    [ "getZoomLevelHeader", "class_b_b_zoom_levels.html#a3beb8ee69bd1ecb285d2b1fc75072aef", null ],
    [ "getZoomLevelHeaders", "class_b_b_zoom_levels.html#aec980331f7a9f9f29545fd90834e5e86", null ],
    [ "getZoomLevelRPTree", "class_b_b_zoom_levels.html#ab2c05640e2814bd6d9facbb35afda341", null ],
    [ "readZoomHeaders", "class_b_b_zoom_levels.html#a2e67b411f9ccb9c30ac72aca3b1aaf47", null ]
];