var reference__filter_8h =
[
    [ "_lesser_reference_filter_name_entry", "structvap_1_1internal_1_1__lesser__reference__filter__name__entry.html", "structvap_1_1internal_1_1__lesser__reference__filter__name__entry" ],
    [ "_lesser_reference_filter_range_entry", "structvap_1_1internal_1_1__lesser__reference__filter__range__entry.html", "structvap_1_1internal_1_1__lesser__reference__filter__range__entry" ],
    [ "reference_filter", "classvap_1_1reference__filter.html", "classvap_1_1reference__filter" ],
    [ "positive_filter", "classvap_1_1positive__filter.html", "classvap_1_1positive__filter" ],
    [ "negative_filter", "classvap_1_1negative__filter.html", "classvap_1_1negative__filter" ],
    [ "applyNameFilters", "reference__filter_8h.html#a9399b672e5c668bd417f261f5cee3dfa", null ],
    [ "genome_ptr_reference_filter_range_greater_compare", "reference__filter_8h.html#a4e5e8eeb85612f39eccda2254ef6d9f6", null ],
    [ "genome_ptr_reference_filter_range_lesser_compare", "reference__filter_8h.html#a9e25f3cbc12bb792884832e84d63a928", null ],
    [ "reference_filter_entry_name_lesser_compare", "reference__filter_8h.html#aed75ec5da29abc91462c49dac9f3c994", null ],
    [ "reference_filter_entry_range_lesser_compare", "reference__filter_8h.html#afaa5159fdb3a1dca9585710513bfbe4f", null ]
];