var classvap_1_1util_1_1file__reader =
[
    [ "buffer_size", "classvap_1_1util_1_1file__reader.html#a708ece0e348f0210c799596f88a1056b", [
      [ "PICO", "classvap_1_1util_1_1file__reader.html#a708ece0e348f0210c799596f88a1056ba6ca8ff59291dba28df8002d96529c7c5", null ],
      [ "NANO", "classvap_1_1util_1_1file__reader.html#a708ece0e348f0210c799596f88a1056ba814efe601fe8fd103e1ba742fd87ed56", null ],
      [ "TINY", "classvap_1_1util_1_1file__reader.html#a708ece0e348f0210c799596f88a1056ba1f668f38bba10161ea34d378b4aacc01", null ],
      [ "SMALL", "classvap_1_1util_1_1file__reader.html#a708ece0e348f0210c799596f88a1056ba0968925df839fd143717e738ea9fbafa", null ],
      [ "MEDIUM", "classvap_1_1util_1_1file__reader.html#a708ece0e348f0210c799596f88a1056ba3ca18a8ba8bb6851f560dda70f4f840a", null ],
      [ "LARGE", "classvap_1_1util_1_1file__reader.html#a708ece0e348f0210c799596f88a1056ba550b59ac6933884742972d186b4c237d", null ],
      [ "XLARGE", "classvap_1_1util_1_1file__reader.html#a708ece0e348f0210c799596f88a1056ba46e770938ce02851a35a52e581ec9743", null ],
      [ "XXLARGE", "classvap_1_1util_1_1file__reader.html#a708ece0e348f0210c799596f88a1056baa77c6f3c14ede6b48d78bcb4b471937d", null ],
      [ "INFINTE", "classvap_1_1util_1_1file__reader.html#a708ece0e348f0210c799596f88a1056ba96086b447ad851ca72ec330d8911d9da", null ]
    ] ],
    [ "file_reader", "classvap_1_1util_1_1file__reader.html#a6dbaad117b61c2277ae457708fd1e793", null ],
    [ "file_reader", "classvap_1_1util_1_1file__reader.html#a453dee6c85a3d307b24341b718dc0c35", null ],
    [ "~file_reader", "classvap_1_1util_1_1file__reader.html#a953381d4b261038876be057f995bf629", null ],
    [ "close", "classvap_1_1util_1_1file__reader.html#a4c1c53b80596c18519d5aa2b3d43ebd2", null ],
    [ "countLimit", "classvap_1_1util_1_1file__reader.html#aa7090c10e2fd71d384f2baabbc73f4ea", null ],
    [ "countReached", "classvap_1_1util_1_1file__reader.html#a4fd9274a2414b8216c08a5c3a5a44990", null ],
    [ "eof", "classvap_1_1util_1_1file__reader.html#a7b0e29a4ffd438e0d0cda6f13a874e69", null ],
    [ "gcount", "classvap_1_1util_1_1file__reader.html#ae774c8bf4dcbed1f52839256e6cea388", null ],
    [ "ignoreLine", "classvap_1_1util_1_1file__reader.html#a9f3b0b0b9bbcb0061c0be0c9a56d0c1a", null ],
    [ "isOpen", "classvap_1_1util_1_1file__reader.html#ad8d714980f6f34a1b30b2b92f1cb374d", null ],
    [ "length", "classvap_1_1util_1_1file__reader.html#aa2ac0d1ee2df8875bf45fe44d8951234", null ],
    [ "lines", "classvap_1_1util_1_1file__reader.html#a61df755354fa3569989af23ccfe8f88e", null ],
    [ "maxSize", "classvap_1_1util_1_1file__reader.html#ae246d7127b689853133ef42ab76264f9", null ],
    [ "open", "classvap_1_1util_1_1file__reader.html#a555c7db31917fd8300556330f1a73146", null ],
    [ "peek", "classvap_1_1util_1_1file__reader.html#a9ed0a8237f5e24c2caaa295826a1e4cc", null ],
    [ "readChunk", "classvap_1_1util_1_1file__reader.html#ae1d630463dc0b69bed34479eb873742e", null ],
    [ "readLine", "classvap_1_1util_1_1file__reader.html#a7e7061f84b480e5662ff5f08556c33e3", null ],
    [ "resetCount", "classvap_1_1util_1_1file__reader.html#a083a7c76da622fdb57e344a30de26f48", null ],
    [ "buffer", "classvap_1_1util_1_1file__reader.html#a2a449baec8a79b45998bc0eef4ab67b1", null ]
];