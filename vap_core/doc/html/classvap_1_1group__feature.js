var classvap_1_1group__feature =
[
    [ "group_feature", "classvap_1_1group__feature.html#adf77f6bec67309ac70f88571ddef5c00", null ],
    [ "group_feature", "classvap_1_1group__feature.html#aaabebfbcb1a3929f79d6d11a187d35fb", null ],
    [ "~group_feature", "classvap_1_1group__feature.html#aa3f34ced4c70a41770a5fdbeebd1485f", null ],
    [ "addChrToFilter", "classvap_1_1group__feature.html#a965cec0f80a9c2ba26b395afe548a2a7", null ],
    [ "addGroupName", "classvap_1_1group__feature.html#ab171244f86d3b54b946f584c862c95fe", null ],
    [ "clean", "classvap_1_1group__feature.html#a9d596856f5c96ed4eb68cf86219362e1", null ],
    [ "containsChr", "classvap_1_1group__feature.html#ac42e141180692c6366820dd426630fff", null ],
    [ "groupCount", "classvap_1_1group__feature.html#a488387a6db7c503064936b6913923b66", null ],
    [ "groupIndex", "classvap_1_1group__feature.html#a8a9f41d505c06c0b832afa4f05951d3e", null ],
    [ "groupName", "classvap_1_1group__feature.html#a2647f26285628ea31a420a32426bc00c", null ],
    [ "isHalfOpen", "classvap_1_1group__feature.html#ae4df3e8bf0f51bcd9f28428ff7a0360f", null ],
    [ "isOneBased", "classvap_1_1group__feature.html#a6c2ad804cbc5905926d13d38553d3ccb", null ],
    [ "operator=", "classvap_1_1group__feature.html#a903b71cd0f8262b0d73611c0f4c30c9c", null ],
    [ "setGroupName", "classvap_1_1group__feature.html#af495ec7930417ed3a0428c338712682d", null ],
    [ "setHalfOpen", "classvap_1_1group__feature.html#a017117dd20ad45f6e981aadbacff2ccc", null ],
    [ "setOneBased", "classvap_1_1group__feature.html#aef5bf243d8a0244f1977ae670548557e", null ],
    [ "sort", "classvap_1_1group__feature.html#a02eed6279fd2833f8a927a5e258fb935", null ]
];