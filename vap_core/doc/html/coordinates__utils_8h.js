var coordinates__utils_8h =
[
    [ "toClosedRange", "coordinates__utils_8h.html#a6c4b289d61d4bae000cd1c33b3dbb047", null ],
    [ "toClosedRange", "coordinates__utils_8h.html#ae4387b85fa60c13b10f9e6500b0ba12e", null ],
    [ "toClosedRange", "coordinates__utils_8h.html#a0ab97134519d6f161cd57fc4db065535", null ],
    [ "toClosedRange", "coordinates__utils_8h.html#a12dd10eab7373773e96a2b71e5fcd589", null ],
    [ "toOneBased", "coordinates__utils_8h.html#ae2584176f4d52f31b4b2279cae311873", null ],
    [ "toOneBased", "coordinates__utils_8h.html#aeaceb3fa9b69baacec035da01257e28e", null ],
    [ "toOneBased", "coordinates__utils_8h.html#ada746478d85f778cf4a39d88367dd9eb", null ],
    [ "toOpenRange", "coordinates__utils_8h.html#a859eedd5aa57789b4ac65b612c074ab8", null ],
    [ "toOpenRange", "coordinates__utils_8h.html#a700a2f8bde871a2db6b23a47113060c2", null ],
    [ "toOpenRange", "coordinates__utils_8h.html#ac516381e51e0d581890a6cd9b5045d38", null ],
    [ "toOpenRange", "coordinates__utils_8h.html#aafaf57a7f85f307ca3d504917103e978", null ],
    [ "toZeroBase", "coordinates__utils_8h.html#aea69993a02fd34fa1b595ec87068b9f1", null ],
    [ "toZeroBase", "coordinates__utils_8h.html#af356f416642867b41495d1708779b15d", null ],
    [ "toZeroBase", "coordinates__utils_8h.html#acff949b964a93999e477d17ad428163a", null ]
];