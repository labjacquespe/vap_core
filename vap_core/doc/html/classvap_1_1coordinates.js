var classvap_1_1coordinates =
[
    [ "coordinates", "classvap_1_1coordinates.html#a3971bc32faa2a8502a252e09a40a74a6", null ],
    [ "coordinates", "classvap_1_1coordinates.html#a4e5c3ec1bc6d1de2dc917e43e09a3917", null ],
    [ "_updateWidth", "classvap_1_1coordinates.html#abf2a66606cf370622107bc7540701b91", null ],
    [ "begin", "classvap_1_1coordinates.html#ae0ca8c8266111f6b1f7b37166db94bf6", null ],
    [ "end", "classvap_1_1coordinates.html#a6835a81cf1049917baca6713f5004b8f", null ],
    [ "isEmpty", "classvap_1_1coordinates.html#a844469220576f73e03486e3dc8d6eddc", null ],
    [ "setBegin", "classvap_1_1coordinates.html#a03cdb0441294bb660704dfa2f03b78ef", null ],
    [ "setCoordinates", "classvap_1_1coordinates.html#a1f197e62950990c825577997e2147e99", null ],
    [ "setEnd", "classvap_1_1coordinates.html#a9de8b751c7c8f484eb93209acef16ec3", null ],
    [ "width", "classvap_1_1coordinates.html#a48acd17685891b6a1c5cca69e52da938", null ],
    [ "_begin", "classvap_1_1coordinates.html#a9e3433ffa5668a66c56501c7dfed5f1a", null ],
    [ "_end", "classvap_1_1coordinates.html#ab37d6202178726d9327b8ba59950b204", null ],
    [ "_width", "classvap_1_1coordinates.html#a75449746b221386de68ea64c254d8985", null ]
];