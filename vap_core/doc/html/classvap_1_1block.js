var classvap_1_1block =
[
    [ "block", "classvap_1_1block.html#a443fb12899b9533050ed8ffea09d2632", null ],
    [ "block", "classvap_1_1block.html#a62acf6ae869d292a3f2bbfa24da0061a", null ],
    [ "~ block", "classvap_1_1block.html#ab2d7eba0e85c5247b4fa437f81500877", null ],
    [ "alignment", "classvap_1_1block.html#a799b39a8768b364a7b08fb11596f02ab", null ],
    [ "capacity", "classvap_1_1block.html#a17f966c7764e593102dbd936c50acd22", null ],
    [ "clear", "classvap_1_1block.html#a31d02bcb4b27a14beba96389abfe2f36", null ],
    [ "extrons", "classvap_1_1block.html#ab9b9bd07f58a4fb41b7b34ca4ee7b7fb", null ],
    [ "extrons", "classvap_1_1block.html#a3ecc73a17a9ae35b4d006c67c974f4bb", null ],
    [ "fillWindows", "classvap_1_1block.html#ad69afbdcc157c513d626c849cb873126", null ],
    [ "findRange", "classvap_1_1block.html#a99e31eeebda154b6f6441470537d0b95", null ],
    [ "initializeMedianAccumulator", "classvap_1_1block.html#ac6095c70f08afc5201c34586c3278e16", null ],
    [ "isEmpty", "classvap_1_1block.html#a156d2f78f1ebb474dbe0a70bbb5d4e81", null ],
    [ "isInRange", "classvap_1_1block.html#a8256914cd7adf3a3ca538298e280a75c", null ],
    [ "resetData", "classvap_1_1block.html#af269755ab8767b07e25f03d3c3227cb8", null ],
    [ "updateAggregatePhase1", "classvap_1_1block.html#a9a1e5185783843e8c9f5e448896ec3fa", null ],
    [ "updateAggregatePhase2", "classvap_1_1block.html#a6061af6f89f223b0e0d26cf56c4cbc34", null ],
    [ "updateWeigthedMean", "classvap_1_1block.html#aa12aa33648ab88232aa0f3f4ab4eaea4", null ],
    [ "windows", "classvap_1_1block.html#a3a9c3d0bfc545e793549c179a56a5380", null ],
    [ "windows", "classvap_1_1block.html#ac31eebc14e482c04e8f60c4bc0718a85", null ],
    [ "count", "classvap_1_1block.html#aab95fbc4ddae454630ffdd5b20305e9b", null ]
];