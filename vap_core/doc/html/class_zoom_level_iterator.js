var class_zoom_level_iterator =
[
    [ "ZoomLevelIterator", "class_zoom_level_iterator.html#a7e5b28941befebc91dfba22f7f2d3b1c", null ],
    [ "~ZoomLevelIterator", "class_zoom_level_iterator.html#ad02e36d354d107f59f9a7b4660045919", null ],
    [ "ZoomLevelIterator", "class_zoom_level_iterator.html#a6f254b5525223ee451e54caf1a1e952a", null ],
    [ "getDataBlock", "class_zoom_level_iterator.html#a59fbef4b467ab761ead2cf44a105cb8f", null ],
    [ "getHitList", "class_zoom_level_iterator.html#adde688b259dd00fbbb6d5f61bde21ab4", null ],
    [ "getHitRegion", "class_zoom_level_iterator.html#af52d46ff84ed09a3d289505654fb8b0f", null ],
    [ "getSelectionRegion", "class_zoom_level_iterator.html#ae45934e23a786d7f5a55f06555d496d1", null ],
    [ "getZoomLevel", "class_zoom_level_iterator.html#ae920ac7ebe0c30f21dff2a185b905617", null ],
    [ "hasNext", "class_zoom_level_iterator.html#a27b9de888aec6faa4ee5f307520e4125", null ],
    [ "isContained", "class_zoom_level_iterator.html#ae0fca30591d534d65b36e699c966c635", null ],
    [ "next", "class_zoom_level_iterator.html#a43eb1e593e1af93eaa13984855b8abb5", null ],
    [ "setSelectionRegion", "class_zoom_level_iterator.html#adec2266e6f29166fb832e775f9f4f82e", null ]
];