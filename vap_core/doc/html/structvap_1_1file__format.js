var structvap_1_1file__format =
[
    [ "format", "structvap_1_1file__format.html#aef82a48b1ffde6a4da86909871f542b5", [
      [ "UNKNOWN", "structvap_1_1file__format.html#aef82a48b1ffde6a4da86909871f542b5ae31d663e655396030713168662482019", null ],
      [ "BEDGRAPH", "structvap_1_1file__format.html#aef82a48b1ffde6a4da86909871f542b5a5373f4ea641e1e7fcf9b287baad39bb3", null ],
      [ "BIGWIG", "structvap_1_1file__format.html#aef82a48b1ffde6a4da86909871f542b5a5fe271bf660ade74b52e80b45fc39522", null ],
      [ "WIG", "structvap_1_1file__format.html#aef82a48b1ffde6a4da86909871f542b5aca4e44b33c3e182f7b4a1a064707c65d", null ],
      [ "GENEPRED", "structvap_1_1file__format.html#aef82a48b1ffde6a4da86909871f542b5a4923ecf881769866219cd94d3ee5a26f", null ],
      [ "GTF", "structvap_1_1file__format.html#aef82a48b1ffde6a4da86909871f542b5afddde4376ac40ffd6252cbf2fb689ae8", null ],
      [ "ANNOTATIONS", "structvap_1_1file__format.html#aef82a48b1ffde6a4da86909871f542b5ac6801bbd638a8be8d9aa573793cc84d6", null ],
      [ "BED", "structvap_1_1file__format.html#aef82a48b1ffde6a4da86909871f542b5a7743ec42612caa556e6c495cfef39120", null ],
      [ "COORDINATES", "structvap_1_1file__format.html#aef82a48b1ffde6a4da86909871f542b5a48799910ad2f2b2dc792660f14aa4556", null ]
    ] ]
];