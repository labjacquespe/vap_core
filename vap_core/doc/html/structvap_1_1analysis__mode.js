var structvap_1_1analysis__mode =
[
    [ "type", "structvap_1_1analysis__mode.html#a24d02bd3cd77f5dd36df6f91798ec3c1", [
      [ "UNKNOWN", "structvap_1_1analysis__mode.html#a24d02bd3cd77f5dd36df6f91798ec3c1a696b031073e74bf2cb98e5ef201d4aa3", null ],
      [ "COORDINATE", "structvap_1_1analysis__mode.html#a24d02bd3cd77f5dd36df6f91798ec3c1acc67df6b110431e6059d25c34d184ede", null ],
      [ "EXON", "structvap_1_1analysis__mode.html#a24d02bd3cd77f5dd36df6f91798ec3c1a0ffde22ab24c41681593a54f868f91e0", null ],
      [ "ANNOTATION", "structvap_1_1analysis__mode.html#a24d02bd3cd77f5dd36df6f91798ec3c1ad2b7fe93e0af4342d2aa893c28f32f1f", null ]
    ] ]
];