var class_bed_feature =
[
    [ "BedFeature", "class_bed_feature.html#af1c451efc0bc76e0f0c056e9ddff0cc0", null ],
    [ "~BedFeature", "class_bed_feature.html#a56e4704ab44da4ce8a75d6b1d0be2152", null ],
    [ "BedFeature", "class_bed_feature.html#a654e4e0aa7291fbd7659b1e2140e38ee", null ],
    [ "getChromosome", "class_bed_feature.html#a9c893b53fca24e01262b77548ca65141", null ],
    [ "getEndBase", "class_bed_feature.html#a98850d1001d27a684ca2df86921c0ddf", null ],
    [ "getItemIndex", "class_bed_feature.html#ac2394a85020d6e0725e46c191ce8efd8", null ],
    [ "getRestOfFields", "class_bed_feature.html#ace81c6ba24ef2ffc99439703606d0470", null ],
    [ "getStartBase", "class_bed_feature.html#a184e6eb3e768533331eefd87293a21a9", null ]
];