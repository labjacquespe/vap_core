var structvap_1_1block__split__alignment =
[
    [ "type", "structvap_1_1block__split__alignment.html#aea9720c747daf90f2b577034df8ed991", [
      [ "UNKNOWN", "structvap_1_1block__split__alignment.html#aea9720c747daf90f2b577034df8ed991a696b031073e74bf2cb98e5ef201d4aa3", null ],
      [ "LEFT", "structvap_1_1block__split__alignment.html#aea9720c747daf90f2b577034df8ed991a684d325a7303f52e64011467ff5c5758", null ],
      [ "RIGHT", "structvap_1_1block__split__alignment.html#aea9720c747daf90f2b577034df8ed991a21507b40c80068eda19865706fdc2403", null ],
      [ "NOT_APPLICABLE", "structvap_1_1block__split__alignment.html#aea9720c747daf90f2b577034df8ed991aeb7295999e22b161fe9136ac2a60c0d5", null ]
    ] ]
];