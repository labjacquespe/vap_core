var structvap_1_1reference__coordinates =
[
    [ "reference_coordinates", "structvap_1_1reference__coordinates.html#a9f4591897ff7740df26c69e4793d0f9d", null ],
    [ "alias", "structvap_1_1reference__coordinates.html#a6bbc55d7e6d2e347203bd45e0d68a4af", null ],
    [ "chromosome", "structvap_1_1reference__coordinates.html#afbaf2cccccceee7f2daaa7b14240a33a", null ],
    [ "coordinates", "structvap_1_1reference__coordinates.html#a6e27a431c5bfc14d0eb83cb8dad52054", null ],
    [ "coordinatesOfInterest", "structvap_1_1reference__coordinates.html#a562dbb4c7f8a39ad45125620ed13e182", null ],
    [ "name", "structvap_1_1reference__coordinates.html#a2089ea5695fe79d68638679efc81da50", null ],
    [ "strand", "structvap_1_1reference__coordinates.html#a08bcbc53123dd8b3f42ce58ea88f1400", null ]
];