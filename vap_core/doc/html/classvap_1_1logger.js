var classvap_1_1logger =
[
    [ "~logger", "classvap_1_1logger.html#a3e5999bf861553f2aed1b869392d9276", null ],
    [ "close", "classvap_1_1logger.html#aa75837b9d0295a2e3b4b5b6d14991856", null ],
    [ "errors", "classvap_1_1logger.html#afb81947c4b060127ad95bbcb0eb13155", null ],
    [ "head", "classvap_1_1logger.html#af9a26bee55ed04e5dcc2d987aab91f6a", null ],
    [ "head", "classvap_1_1logger.html#aa0b53c17d817e909d19003cae780abca", null ],
    [ "log", "classvap_1_1logger.html#a109c553d79bff94f5d5993210574ff36", null ],
    [ "log", "classvap_1_1logger.html#a43c6af8c6f85384cdf31763c7e3406b8", null ],
    [ "log", "classvap_1_1logger.html#a26d100b9d03912379cbce31ed0a850ab", null ],
    [ "log", "classvap_1_1logger.html#a94a224e0cc1b08dd881da58e3d1621a8", null ],
    [ "log", "classvap_1_1logger.html#ad774d57c0ff609161c20df64e03efb38", null ],
    [ "log", "classvap_1_1logger.html#a87e771b8ec144d0e3438760df27f9a34", null ],
    [ "loggingOptions", "classvap_1_1logger.html#ac8e0d70393de682562dd09948e9d9ef3", null ],
    [ "path", "classvap_1_1logger.html#a899e129d77fb76c784720a383f1fd02f", null ],
    [ "setLoggingOptions", "classvap_1_1logger.html#a13a7698b311e0e713cac2ab8da5b0ce2", null ],
    [ "tail", "classvap_1_1logger.html#a7ee9c69d2cafa1a819386fc1768bf892", null ],
    [ "tail", "classvap_1_1logger.html#af1685f8278c0e60d5c47829a7c2860b1", null ]
];