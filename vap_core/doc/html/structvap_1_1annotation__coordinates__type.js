var structvap_1_1annotation__coordinates__type =
[
    [ "type", "structvap_1_1annotation__coordinates__type.html#a8687dfd6ed7fbebffc2a7c6b324e8def", [
      [ "UNKNOWN", "structvap_1_1annotation__coordinates__type.html#a8687dfd6ed7fbebffc2a7c6b324e8defa696b031073e74bf2cb98e5ef201d4aa3", null ],
      [ "CODING_SEQUENCE", "structvap_1_1annotation__coordinates__type.html#a8687dfd6ed7fbebffc2a7c6b324e8defa9d6960563bbc115556c366ffdf6dc0ed", null ],
      [ "TRANSCRIPTION", "structvap_1_1annotation__coordinates__type.html#a8687dfd6ed7fbebffc2a7c6b324e8defa834756ef1ddcac497c4b045eb3ec2ad2", null ],
      [ "NOT_APPLICABLE", "structvap_1_1annotation__coordinates__type.html#a8687dfd6ed7fbebffc2a7c6b324e8defaeb7295999e22b161fe9136ac2a60c0d5", null ]
    ] ]
];