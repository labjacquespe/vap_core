var reference__coordinates_8h =
[
    [ "reference_coordinates", "structvap_1_1reference__coordinates.html", "structvap_1_1reference__coordinates" ],
    [ "coordinates_filter_entry_name_greater_compare", "reference__coordinates_8h.html#acd3cd51c4905a46dad80656f6412f94b", null ],
    [ "coordinates_filter_entry_name_lesser_compare", "reference__coordinates_8h.html#ad35381925e30732a8558c21bb6730e46", null ],
    [ "coordinates_interest_filter_entry_range_greater_compare", "reference__coordinates_8h.html#adfe14701f2e7de93ec886bc8a0fbba31", null ],
    [ "coordinates_interest_filter_entry_range_lesser_compare", "reference__coordinates_8h.html#a2a43056b1dbc45e1ec008e2c33eb0cdb", null ],
    [ "coordinates_interest_range_chromosomic_lesser_compare", "reference__coordinates_8h.html#addb76c126380ddf1160dc82a3af6afa1", null ],
    [ "coordinates_name_lesser_compare", "reference__coordinates_8h.html#a37011d59f4e260581ddf1d7f52c8655e", null ],
    [ "getCoordinatesOfInterest", "reference__coordinates_8h.html#aa4b35c0484e31a70dad1dd7b719469e8", null ]
];