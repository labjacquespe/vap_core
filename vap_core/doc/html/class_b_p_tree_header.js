var class_b_p_tree_header =
[
    [ "BPTreeHeader", "class_b_p_tree_header.html#a0c058dd49e5c818da2c5e0b043dca066", null ],
    [ "getBlockSize", "class_b_p_tree_header.html#a94b0f070946876cc9a4a7ed4d3a21c13", null ],
    [ "GetheaderOffset_", "class_b_p_tree_header.html#ac687cd215a97f92e75085fdb2b0a72cb", null ],
    [ "getItemCount", "class_b_p_tree_header.html#adbee248c6ca9bc215db35027f4e95db3", null ],
    [ "getKeySize", "class_b_p_tree_header.html#a005a0ecc9b8cf328837ceed7b91e9ad5", null ],
    [ "getMagic", "class_b_p_tree_header.html#a0bccf24aa49d95f84a0644df6a79e0da", null ],
    [ "getReserved", "class_b_p_tree_header.html#a4c1dd014e962412a4d10396b6f5f4a8e", null ],
    [ "getValSize", "class_b_p_tree_header.html#a53185fd79e4e2f024000e9328963bde3", null ],
    [ "isHeaderOK", "class_b_p_tree_header.html#a73cdbe04028ebbf8f8973d0e183db1fc", null ],
    [ "setBlockSize", "class_b_p_tree_header.html#af2e89f4b0354b9267f103293621485a7", null ],
    [ "setHeaderOffset", "class_b_p_tree_header.html#a7afc9af381f8518a939ec703f000d943", null ],
    [ "SetHeaderOk", "class_b_p_tree_header.html#ab5bb3a9a9f84c3b5c1a61af743534bb1", null ],
    [ "setItemCount", "class_b_p_tree_header.html#a0c3198c72e7ebd9628484474e6a1a531", null ],
    [ "setKeySize", "class_b_p_tree_header.html#af6c8a5b992a63f5bffbc4eb554859b0a", null ],
    [ "setMagic", "class_b_p_tree_header.html#aa49f1ed99447e39d9e8484701e65b949", null ],
    [ "setReserved", "class_b_p_tree_header.html#a79c8c6024fe3105f127453b50f162a6e", null ],
    [ "setValSize", "class_b_p_tree_header.html#a88d431ebd09e6e6752f4a83f719105de", null ]
];