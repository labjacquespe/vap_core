var hierarchy =
[
    [ "vap::util::internal::_delimiters", "structvap_1_1util_1_1internal_1_1__delimiters.html", null ],
    [ "vap::internal::_less_dataset", "structvap_1_1internal_1_1__less__dataset.html", null ],
    [ "vap::util::internal::_less_update_window", "structvap_1_1util_1_1internal_1_1__less__update__window.html", null ],
    [ "vap::internal::_lesser_reference_filter_name_entry", "structvap_1_1internal_1_1__lesser__reference__filter__name__entry.html", null ],
    [ "vap::internal::_lesser_reference_filter_range_entry", "structvap_1_1internal_1_1__lesser__reference__filter__range__entry.html", null ],
    [ "vap::aggregate_data_type", "structvap_1_1aggregate__data__type.html", null ],
    [ "vap::analysis_method", "structvap_1_1analysis__method.html", null ],
    [ "vap::analysis_mode", "structvap_1_1analysis__mode.html", null ],
    [ "vap::annotation_coordinates_type", "structvap_1_1annotation__coordinates__type.html", null ],
    [ "vap::annotation_strand", "structvap_1_1annotation__strand.html", null ],
    [ "vap::util::array_smart_pointer< TYPE >", "classvap_1_1util_1_1array__smart__pointer.html", null ],
    [ "vap::base_entry", "structvap_1_1base__entry.html", [
      [ "vap::bedgraph_entry", "structvap_1_1bedgraph__entry.html", null ],
      [ "vap::bigwig_entry", "structvap_1_1bigwig__entry.html", null ],
      [ "vap::genome_entry", "structvap_1_1genome__entry.html", null ],
      [ "vap::wig_entry", "structvap_1_1wig__entry.html", null ]
    ] ],
    [ "BBFileHeader", "class_b_b_file_header.html", null ],
    [ "BBFileReader", "class_b_b_file_reader.html", null ],
    [ "BBTotalSummaryBlock", "class_b_b_total_summary_block.html", null ],
    [ "BBZoomLevelFormat", "class_b_b_zoom_level_format.html", null ],
    [ "BBZoomLevelHeader", "class_b_b_zoom_level_header.html", null ],
    [ "BBZoomLevels", "class_b_b_zoom_levels.html", null ],
    [ "BedFeature", "class_bed_feature.html", null ],
    [ "BigBedDataBlock", "class_big_bed_data_block.html", null ],
    [ "BigWigDataBlock", "class_big_wig_data_block.html", null ],
    [ "BigWigSection", "class_big_wig_section.html", null ],
    [ "BigWigSectionHeader", "class_big_wig_section_header.html", null ],
    [ "vap::block_alignment", "structvap_1_1block__alignment.html", null ],
    [ "vap::block_split_alignment", "structvap_1_1block__split__alignment.html", null ],
    [ "vap::block_split_type", "structvap_1_1block__split__type.html", null ],
    [ "BPTree", "class_b_p_tree.html", null ],
    [ "BPTreeHeader", "class_b_p_tree_header.html", null ],
    [ "BPTreeNode", "class_b_p_tree_node.html", [
      [ "BPTreeChildNode", "class_b_p_tree_child_node.html", null ],
      [ "BPTreeLeafNode", "class_b_p_tree_leaf_node.html", null ]
    ] ],
    [ "BPTreeNodeItem", "class_b_p_tree_node_item.html", [
      [ "BPTreeChildNodeItem", "class_b_p_tree_child_node_item.html", null ],
      [ "BPTreeLeafNodeItem", "class_b_p_tree_leaf_node_item.html", null ]
    ] ],
    [ "vap::util::convert", "classvap_1_1util_1_1convert.html", null ],
    [ "vap::coordinates", "classvap_1_1coordinates.html", [
      [ "vap::block", "classvap_1_1block.html", null ],
      [ "vap::reference_feature", "classvap_1_1reference__feature.html", [
        [ "vap::aggregate_reference_feature", "classvap_1_1aggregate__reference__feature.html", null ]
      ] ],
      [ "vap::window", "classvap_1_1window.html", [
        [ "vap::agg_output_window", "classvap_1_1agg__output__window.html", null ],
        [ "vap::ind_output_window", "classvap_1_1ind__output__window.html", null ]
      ] ]
    ] ],
    [ "vap::coordinates_region", "structvap_1_1coordinates__region.html", null ],
    [ "vap::util::delete_array_policy< TYPE >", "classvap_1_1util_1_1delete__array__policy.html", null ],
    [ "vap::util::delete_scalar_policy< TYPE >", "classvap_1_1util_1_1delete__scalar__policy.html", null ],
    [ "deque", null, [
      [ "vap::group_feature< vap::reference_feature >", "classvap_1_1group__feature.html", null ],
      [ "vap::group_feature< TYPE >", "classvap_1_1group__feature.html", null ]
    ] ],
    [ "vap::io::file_reader", "classvap_1_1io_1_1file__reader.html", [
      [ "vap::io::file_reader_bzip2", "classvap_1_1io_1_1file__reader__bzip2.html", null ],
      [ "vap::io::file_reader_gzip", "classvap_1_1io_1_1file__reader__gzip.html", null ]
    ] ],
    [ "vap::io::file_reader_factory", "classvap_1_1io_1_1file__reader__factory.html", null ],
    [ "vap::io::format_guess", "classvap_1_1io_1_1format__guess.html", null ],
    [ "vap::found_item", "structvap_1_1found__item.html", null ],
    [ "vap::genepred_entry", "structvap_1_1genepred__entry.html", null ],
    [ "vap::genome_entry_pointer", "structvap_1_1genome__entry__pointer.html", null ],
    [ "vap::graph_orientation", "structvap_1_1graph__orientation.html", null ],
    [ "vap::gtf_entry", "structvap_1_1gtf__entry.html", null ],
    [ "iterator", null, [
      [ "BigBedIterator", "class_big_bed_iterator.html", null ],
      [ "BigWigIterator", "class_big_wig_iterator.html", null ]
    ] ],
    [ "vap::logger", "classvap_1_1logger.html", null ],
    [ "vap::loggingOptions", "structvap_1_1logging_options.html", null ],
    [ "map", null, [
      [ "vap::abstract_dataset< bedgraph_entry >", "classvap_1_1abstract__dataset.html", [
        [ "vap::bedgraph", "classvap_1_1bedgraph.html", null ]
      ] ],
      [ "vap::abstract_dataset< bigwig_entry >", "classvap_1_1abstract__dataset.html", [
        [ "vap::bigwig", "classvap_1_1bigwig.html", null ]
      ] ],
      [ "vap::abstract_dataset< genome_entry >", "classvap_1_1abstract__dataset.html", [
        [ "vap::genome", "classvap_1_1genome.html", null ]
      ] ],
      [ "vap::abstract_dataset< wig_entry >", "classvap_1_1abstract__dataset.html", [
        [ "vap::wig", "classvap_1_1wig.html", null ]
      ] ],
      [ "vap::abstract_dataset< ENTRY >", "classvap_1_1abstract__dataset.html", null ],
      [ "vap::list_agg_graphs", "classvap_1_1list__agg__graphs.html", null ],
      [ "vap::list_ind_heatmaps", "classvap_1_1list__ind__heatmaps.html", null ],
      [ "vap::util::update_table", "classvap_1_1util_1_1update__table.html", null ]
    ] ],
    [ "vap::mean_dispersion_value", "structvap_1_1mean__dispersion__value.html", null ],
    [ "vap::merge_mid_introns", "structvap_1_1merge__mid__introns.html", null ],
    [ "vap::reference_filter_entry::name_entry", "structvap_1_1reference__filter__entry_1_1name__entry.html", null ],
    [ "vap::oneRefPt_boundary", "structvap_1_1one_ref_pt__boundary.html", null ],
    [ "vap::orientation_type", "structvap_1_1orientation__type.html", null ],
    [ "vap::output_builder", "classvap_1_1output__builder.html", null ],
    [ "vap::parameters", "structvap_1_1parameters.html", null ],
    [ "vap::parser_parameters", "classvap_1_1parser__parameters.html", null ],
    [ "vap::util::parser_string", "classvap_1_1util_1_1parser__string.html", null ],
    [ "pcre2cpp", "classpcre2cpp.html", null ],
    [ "vap::reference_filter_entry::range_entry", "structvap_1_1reference__filter__entry_1_1range__entry.html", null ],
    [ "vap::record", "classvap_1_1record.html", null ],
    [ "vap::recordLevel", "structvap_1_1record_level.html", null ],
    [ "vap::util::reference", "classvap_1_1util_1_1reference.html", null ],
    [ "vap::reference_entry", "classvap_1_1reference__entry.html", [
      [ "vap::reference_annotation", "structvap_1_1reference__annotation.html", null ],
      [ "vap::reference_coordinates", "structvap_1_1reference__coordinates.html", null ],
      [ "vap::reference_feature", "classvap_1_1reference__feature.html", null ]
    ] ],
    [ "vap::reference_filter", "classvap_1_1reference__filter.html", [
      [ "vap::negative_filter", "classvap_1_1negative__filter.html", null ],
      [ "vap::positive_filter", "classvap_1_1positive__filter.html", null ]
    ] ],
    [ "vap::reference_filter_entry", "structvap_1_1reference__filter__entry.html", null ],
    [ "RPChromosomeRegion", "class_r_p_chromosome_region.html", [
      [ "RPTreeLeafNodeItem", "class_r_p_tree_leaf_node_item.html", null ]
    ] ],
    [ "RPTree", "class_r_p_tree.html", null ],
    [ "RPTreeHeader", "class_r_p_tree_header.html", null ],
    [ "RPTreeNode", "class_r_p_tree_node.html", [
      [ "RPTreeChildNode", "class_r_p_tree_child_node.html", null ],
      [ "RPTreeLeafNode", "class_r_p_tree_leaf_node.html", null ],
      [ "RPTreeNodeProxy", "class_r_p_tree_node_proxy.html", null ]
    ] ],
    [ "RPTreeNodeItem", "class_r_p_tree_node_item.html", [
      [ "RPTreeChildNodeItem", "class_r_p_tree_child_node_item.html", null ],
      [ "RPTreeLeafNodeItem", "class_r_p_tree_leaf_node_item.html", null ]
    ] ],
    [ "vap::util::scalar_smart_pointer< TYPE >", "classvap_1_1util_1_1scalar__smart__pointer.html", null ],
    [ "vap::util::smart_pointer< TYPE, DELETER >", "classvap_1_1util_1_1smart__pointer.html", null ],
    [ "vap::util::timer", "classvap_1_1util_1_1timer.html", null ],
    [ "vap_apt_translator", "classvap__apt__translator.html", null ],
    [ "WigItem", "class_wig_item.html", null ],
    [ "ZoomDataBLoch", "class_zoom_data_b_loch.html", null ],
    [ "ZoomDataBlock", "class_zoom_data_block.html", null ],
    [ "ZoomDataRecord", "class_zoom_data_record.html", null ],
    [ "ZoomLevelIterator", "class_zoom_level_iterator.html", [
      [ "EmptyIterator", "class_empty_iterator.html", null ]
    ] ]
];