var class_b_b_zoom_level_header =
[
    [ "BBZoomLevelHeader", "class_b_b_zoom_level_header.html#afae3fe68e9cfc35cb0c8e52e51235401", null ],
    [ "~BBZoomLevelHeader", "class_b_b_zoom_level_header.html#af95c829621873ea473f1f5a6ed407118", null ],
    [ "BBZoomLevelHeader", "class_b_b_zoom_level_header.html#ab0b1c0c91e7942b687dafbdc42f74f8d", null ],
    [ "BBZoomLevelHeader", "class_b_b_zoom_level_header.html#ad1a9b5b3a55aea3e33d74613261ad955", null ],
    [ "getDataOffset", "class_b_b_zoom_level_header.html#a29869b28a3abbdcb7d5ff1cc7ddd1be0", null ],
    [ "getIndexOffset", "class_b_b_zoom_level_header.html#a20676fd1191dee52070d7ca591d1099b", null ],
    [ "getReductionLevel", "class_b_b_zoom_level_header.html#a4539803af3e21bfb6da5cd2430886a2e", null ],
    [ "getReserved", "class_b_b_zoom_level_header.html#a02bb0a3af1eeaf27e2770f3022a223d4", null ],
    [ "getZoomLevel", "class_b_b_zoom_level_header.html#a5b327f39bf3b00181394b8941570645b", null ],
    [ "getZoomLevelheaderOffset", "class_b_b_zoom_level_header.html#a9afe389953f977ede9432760551eee66", null ],
    [ "setDataOffset", "class_b_b_zoom_level_header.html#ac30e05859a514e021b415e70fb978198", null ],
    [ "setIndexOffset", "class_b_b_zoom_level_header.html#ae31c31aabde195dd247272c9885fb209", null ],
    [ "setReductionLevel", "class_b_b_zoom_level_header.html#aa98b193c8db3bc966cca0c56b872441b", null ],
    [ "setReserved", "class_b_b_zoom_level_header.html#aba55884c5456baaedd3e45609570b0f0", null ],
    [ "setZoomLevel", "class_b_b_zoom_level_header.html#a426ce2da3e4a81c4cbd063c25d4bf342", null ],
    [ "setZoomLevelheaderOffset", "class_b_b_zoom_level_header.html#a14f0a181424917a9ac7145affc8730f7", null ]
];