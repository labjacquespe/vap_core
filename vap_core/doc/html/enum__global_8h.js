var enum__global_8h =
[
    [ "WigItemType", "enum__global_8h.html#a7c3ff81b366e7e42df0196e922ad0536", [
      [ "BedGraph", "enum__global_8h.html#a7c3ff81b366e7e42df0196e922ad0536a25e4aa36d37ba65ded4dd4ef1f76cc9b", null ],
      [ "VarStep", "enum__global_8h.html#a7c3ff81b366e7e42df0196e922ad0536a837b9f01023c61ad955461c8c66ed4cd", null ],
      [ "FixedStep", "enum__global_8h.html#a7c3ff81b366e7e42df0196e922ad0536a12353bf08eb5f85f03a8b2885a77c598", null ],
      [ "Unknown", "enum__global_8h.html#a7c3ff81b366e7e42df0196e922ad0536a4cecc8b7b92f9ac72958b761768064bc", null ]
    ] ]
];