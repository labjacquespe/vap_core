/**
 * @file   messages.h
 * @author Charles Coulombe
 *
 * @date 7 January 2013, 10:38
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MESSAGES_H
#define	MESSAGES_H

#include <iostream>
#include <string>

#include <boost/lexical_cast.hpp>
#include "../utils/logging/logger.h"

namespace vap
{
/**
 * Macro to log a message with a unique instance of logger.
 * @warning logger must have been instanciated with @c create(..)
 */
#define LOG(level, str) do{ logger::instance().log(level, str); }while(false)

/** Macro to print a message and a brief stacktrace to @c stdout */
#define LOG_STACKTRACE(level, str, line, file) do{ logger::instance().log(level, str, line, file); }while(false)

/** Macro to print a message to @c stdout */
#define PRINT(str) do{ std::cout << str << std::endl; }while(false)

/** Macro to print an error message to @c stdout */
#define PRINT_ERROR(str) do{ std::cerr << "ERROR: " << str << std::endl; }while(false)

/** Macro to print an warning message to @c stdout */
#define PRINT_WARNING(str) do{ std::cerr << "WARNING: " << str << std::endl; }while(false)

/** Macro to log and print an information message*/
#define PRINT_AND_LOG(str) do{ PRINT(str); LOG(recordLevel::INFORMATION, str); }while(false)

/** Macro to log and print an error message*/
#define PRINT_AND_LOG_ERROR(str) do{ PRINT_ERROR(str); LOG_STACKTRACE(recordLevel::ERROR, str, __LINE__, __FILE__); }while(false)

/** Macro to log and print an warning message*/
#define PRINT_AND_LOG_WARNING(str) do{ PRINT_WARNING(str); LOG(recordLevel::WARNING, str); }while(false)


// =============================================================================
//                              MESSAGES DEFINITION
// =============================================================================

// TODO: Upgrade message system, add context to messages

/////////////////////////////////
// Information messages
/////////////////////////////////
#define MSG_APP_RUNNING(filename) "Running with parameters file: " + filename
#define MSG_READING_GENOME(filename) "Reading genome information: " + filename
#define MSG_READING_FILTERS "Reading filters."
#define MSG_APPLYING_FILTER(sel, exc) "Applying filters: " + boost::lexical_cast<std::string>(sel) +" positives and " + boost::lexical_cast<std::string>(exc) + " negatives"
#define MSG_READING_GROUP(group) "Reading group: " + group
#define MSG_LINKING_FEATURES(size) "Linking " + boost::lexical_cast<std::string>(size) + " features."
#define MSG_PROCESS_DATASET(dataset) "Processing dataset: " + dataset
#define MSG_DATASET_LINESREAD(n) boost::lexical_cast<std::string>(n) + " lines were parsed."
#define MSG_APP_DONE(time) "Analysis took roughly: " + std::to_string(time) + "s to complete."
#define MSG_APP_ROUND_DONE(time) "Round took roughly: " + std::to_string(time) + "s to complete."
#define MSG_ROUND(round) "Round: " + round
#define MSG_EXITNOW "VAP is now halting."
#define MSG_COPYRIGHT "Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques\n" \
                      "This program comes with ABSOLUTELY NO WARRANTY.\n" \
                      "This is free software, and you are welcome to redistribute it\n" \
                      "under certain conditions."
#define MSG_LOG_HEADER(cmd, dir)"Version: " APPNAME " " APPVERSION "\nCommand: " + cmd + "\nOutput directory:" + dir
#define MSG_PROCESSED_DATASETS(n) "Number of datasets processed: " + boost::lexical_cast<std::string>(n) + "."

/////////////////////////////////
// Error messages
/////////////////////////////////
#define MSG_FILE_NOTOPEN_NOTFOUND(filename) "File \"" + filename + "\" cannot be found or open."
#define MSG_EXONS_EMPTY(feature) "Feature \"" + feature + "\" does not have any exons."
#define MSG_INVALID_GENOME(filename) "Genome file \"" + filename + "\" is invalid."

/////////////////////////////////
// Warning messages
/////////////////////////////////
#define MSG_REMOVED_FEATURE(feature) "Feature \"" + feature + "\" was removed (it was never linked)."
#define MSG_UNKNOWN_DATASET(dataset) "Unknown or unsupported dataset format: " + dataset
#define MSG_UNKNOWN_GENOME(genome) "Unknown or unsupported genome format: " + genome
#define MSG_INVALID_CHROMOSOME(chromosome) "Invalid chromosome \"" + chromosome + "\" was removed from dataset."
#define MSG_FILTERED_CHROMOSOME(chromosome) "Unreferenced chromosome \"" + chromosome + "\" was removed from dataset."
#define MSG_UNKNOWN_NUMBER_REFPTS(feature) "Feature \"" + feature + "\" has unknown number of reference points."
#define MSG_UNDEFINED_ANNOTATION_STRAND(feature) "Feature \"" + feature + "\" type cannot be determined due to undefined annotation strand."
#define MSG_UNKNOWN_FILE_FORMAT(file) "File \"" + file + "\" has unknown or invalid format."
#define MSG_MAPPING_NOTADDED(orientation) "Graph mapping was not added for the orientation \"" + orientation + "\"."
#define MSG_EMPTY_REFERENCE_FEATURES "Empty list of features."
#define MSG_DATASET_NOT_READ(dataset) "Dataset \"" + dataset + "\" was not read (no lines were parsed)."
#define MSG_GENOME_NOT_READ(genome) "Genome \"" + genome + "\" was not read (no lines were parsed)."
#define MSG_TRACKTYPE_NOMATCH(dataset, format) "Dataset \"" + dataset + "\" track type does not match with the \"" + format + "\" format."
} /* vap namespace end */
#endif	/* MESSAGES_H */

