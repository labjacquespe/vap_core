/**
 * @file   list_agg_graphs.h
 * @author Charles Coulombe
 *
 * @date 12 October 2014, 12:06
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIST_AGG_GRAPHS_H
#define	LIST_AGG_GRAPHS_H

#include <map>
#include <string>
#include <deque>

#include "../utils/debug/assert.h"
#include "../defs.h"
#include "../utils/file/file_utils.h"
#include "../exception/error.h"
#include "../feature/group_feature.h"
#include "../feature/aggregate_reference_feature.h"
#include "../parameters/parameters.h"

/** Graph mappings file name*/
#define LIST_AGG_GRAPHS_FILENAME "list_agg_graphs.txt"

namespace vap
{

class list_agg_graphs : private std::map< std::string, std::deque< std::string > >
{
private:
    // ----------------------- attributes
    list_agg_graphs(const list_agg_graphs &orig); // deactivate copy constructor

public:
    // ----------------------- constructor
    list_agg_graphs();
    virtual ~list_agg_graphs();

    // ----------------------- methods
    void addDataset(const group_feature<aggregate_reference_feature> &aggregateFeatures, const std::string &datasetName, const parameters &param);
    void write(const std::string &path) const;
};
} /* vap namespace end */

#endif	/* LIST_AGG_GRAPHS_H */

