/**
 * @file   list_agg_graphs.cpp
 * @author Charles Coulombe
 *
 * @date 12 October 2014, 12:06
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "list_agg_graphs.h"

namespace vap
{

list_agg_graphs::list_agg_graphs() { }

list_agg_graphs::~list_agg_graphs() { }

/**
 * Adds the datasets to the graph mappings.
 *
 * @remark
 * The "list_agg_graphs.txt" is used by the interface in order to create
 * correctly the graphs.
 *
 * @param aggregateFeatures A group of aggregate reference features.
 * @param datasetName current dataset name
 * @param param application parameters
 */
void list_agg_graphs::addDataset(const group_feature<aggregate_reference_feature> &aggregateFeatures, const std::string &datasetName, const parameters &param)
{
    // TODO : Make a test unit for this.

    std::string groupName1 = "", groupName2 = "",
            orientation = "",
            prefixFilename = (param.prefixFilename.empty() ? "" : param.prefixFilename + "_"),
            prefix = param.outputDirectory +
            prefixFilename +
            OUT_AGG_PREFIX_FILENAME + "_" + datasetName + "_";

    const std::string ext = ".txt",
            graphPrefix = prefixFilename + GRAPH_PREFIX + "_";

    const bool isOnePerGroup = param.oneGraphPerGroup,
            isOnePerDataset = param.oneGraphPerDataset,
            isOnePerOrientation = param.oneOrientationPerGraph;
    /*
     * Truth table for GraphName
     * Dataset name must be construct with dataset and orientation value.
     * ===================================================================
     * Group    Dataset Ori ||  GraphName               DatasetName
     * ===================================================================
     * 1        1       1   ||  graph_dataset_grp_ori   agg_dataset_grp_ori
     * 1        0       1   ||  graph_grp_ori           agg_dataset_grp_ori
     * 0        0       1   ||  graph_ori               agg_dataset_ori
     * 0        1       1   ||  graph_dataset_ori       agg_dataset_ori
     * 1        1       0   ||  graph_dataset_grp       agg_dataset_grp_ori
     * 1        0       0   ||  graph_grp               agg_dataset_grp_ori
     * 0        1       0   ||  graph_dataset           agg_dataset_ori
     * 0        0       0   ||  graph_all               agg_dataset_ori
     */

    // loop for all N_ORI_SUBGROUPS orientation
    for(uint_t i = 0; i < N_ORI_SUBGROUPS; i++)
    {
        // discard undesired orientation
        if(!param.orientationSubgroups[i])
            continue;

        orientation = graph_orientation::toString(static_cast<graph_orientation::orientation>(i));

        if(isOnePerGroup)
        {
            for(uint_t grpIdx = 0; grpIdx < aggregateFeatures.groupCount(); grpIdx++)
            {
                if(isOnePerDataset && isOnePerOrientation) // case 1 1 1 -> graph_dataset_grp_ori   agg_dataset_grp_ori
                {
                    groupName1 = graphPrefix + datasetName + "_" + aggregateFeatures.groupName(grpIdx) + "_" + orientation;
                    groupName2 = aggregateFeatures.groupName(grpIdx) + "_" + orientation;
                    (*this)[groupName1].push_back(prefix + groupName2 + ext);
                }
                else if(!isOnePerDataset && isOnePerOrientation) // case 1 0 1 -> graph_grp_ori    agg_dataset_grp_ori
                {
                    groupName1 = graphPrefix + aggregateFeatures.groupName(grpIdx) + "_" + orientation;
                    groupName2 = aggregateFeatures.groupName(grpIdx) + "_" + orientation;
                    (*this)[groupName1].push_back(prefix + groupName2 + ext);
                }
                else if(isOnePerDataset && !isOnePerOrientation) // case 1 1 0 ->  graph_dataset_grp       agg_dataset_grp_ori
                {
                    groupName1 = graphPrefix + datasetName + "_" + aggregateFeatures.groupName(grpIdx);
                    groupName2 = aggregateFeatures.groupName(grpIdx);
                    (*this)[groupName1].push_back(prefix + groupName2 + "_" + orientation + ext);
                }
                else if(!isOnePerDataset && !isOnePerOrientation) // case 1 0 0 -> graph_grp   agg_dataset_grp_ori
                {
                    groupName1 = graphPrefix + aggregateFeatures.groupName(grpIdx);
                    groupName2 = aggregateFeatures.groupName(grpIdx);
                    (*this)[groupName1].push_back(prefix + groupName2 + "_" + orientation + ext);
                }
            }
        }
        else if(isOnePerDataset && isOnePerOrientation) // case 0 1 1 -> graph_dataset_ori    agg_dataset_ori
        {
            groupName1 = graphPrefix + datasetName + "_" + orientation;
            (*this)[groupName1].push_back(prefix + orientation + ext);
        }
        else if(!isOnePerDataset && isOnePerOrientation) // case 0 0 1 -> graph_ori   agg_dataset_ori
        {
            groupName1 = graphPrefix + orientation;
            (*this)[groupName1].push_back(prefix + orientation + ext);
        }
        else if(isOnePerDataset && !isOnePerOrientation) // case 0 1 0 -> graph_dataset   agg_dataset_ori
        {
            groupName1 = graphPrefix + datasetName;
            (*this)[groupName1].push_back(prefix + orientation + ext);
        }
        else if(!isOnePerDataset && !isOnePerOrientation) // case 0 0 0 -> graph_all  agg_dataset_ori
        {
            groupName1 = graphPrefix + "all";
            (*this)[groupName1].push_back(prefix + orientation + ext);
        }
        else
        {
            // something is really wrong if we fall down here
            LOG(recordLevel::WARNING, MSG_MAPPING_NOTADDED(orientation));
            ASSERT(false, MSG_MAPPING_NOTADDED(orientation));
        }
    }
}

/**
 * Prints to file the graph mappings.
 *
 * The file's containing the mapping will be deduced of the current parameters
 * and will look like '%prefix_%list_agg_graphs.txt' where %prefix%
 * is the prefix found in the parameter file.
 *
 * @remark
 * The "list_agg_graphs.txt" is used by the interface in order to create
 * correctly the graphs.
 *
 * @param path File's path.
 */
void list_agg_graphs::write(const std::string &path) const
{
    std::string graphName("", 100);

    std::ofstream out(path, std::ios::out | std::ios::trunc);
    if(out.is_open())
    {
        out << "###" << APPNAME << "_" << APPVERSION << ":Graph name###\t###Dataset name###\n";

        // loop for all mappings
        for(std::map< std::string, std::deque< std::string > >::const_iterator it = begin(); it != end(); ++it)
        {
            graphName = filesystem::extract_filename(it->first);

            for(std::deque<std::string>::const_iterator i = it->second.begin(); i != it->second.end(); ++i)
                out << graphName << "\t" << *i << "\n";

            if(it->second.size() > 1)
                out << std::endl;
        }
    }
    else
        ERROR(false, MSG_FILE_NOTOPEN_NOTFOUND(path));
}

} /* vap namespace end */
