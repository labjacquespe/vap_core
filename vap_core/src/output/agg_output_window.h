/**
 * @file   agg_output_window.h
 * @author Charles Coulombe
 *
 * @date 17 December 2012, 16:19
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AGG_OUTPUT_WINDOW_H
#define	AGG_OUTPUT_WINDOW_H

#include <ostream>

#include "../feature/window.h"

namespace vap
{
class agg_output_window : private window
{
private:
    // ----------------------- attributes
    bool _invalid;
    uint_t _index;

    // ----------------------- methods

public:
    // ----------------------- constructors
    agg_output_window(bool invalid);
    agg_output_window(const agg_output_window &source, uint_t index = -1);
    agg_output_window(const window &source, uint_t index = -1);
    ~ agg_output_window();

    // ----------------------- accessors
    bool isInvalid() const;
    uint_t index() const;
    float value() const;
    uint_t count() const;
    float stdDev() const;

    int begin() const;
    int end() const;

    // ----------------------- modifiers
    void setValue(float value);
    void setIndex(uint_t index);

    // ----------------------- methods
    bool isNotPopulated() const;

    agg_output_window &operator=(const window &w);
    agg_output_window &operator=(const agg_output_window &w);

    std::ostream &print(std::ostream &out, const agg_output_window &source, float proportion = 0.0f);
};
} /* vap namespace end */

#endif	/* AGG_OUTPUT_WINDOW_H */

