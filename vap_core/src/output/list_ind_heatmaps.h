/**
 * @file   list_ind_heatmaps.h
 * @author Charles Coulombe
 *
 * @date 12 October 2014, 13:22
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIST_IND_HEATMAPS_H
#define	LIST_IND_HEATMAPS_H

#include <string>
#include <deque>

#include "../utils/debug/assert.h"
#include "../defs.h"
#include "../utils/file/file_utils.h"
#include "../exception/error.h"
#include "../parameters/parameters.h"

/** List of heat maps filenames. */
#define LIST_IND_HEATMAPS_FILENAME "list_ind_heatmaps.txt"

namespace vap
{

class list_ind_heatmaps : private std::map< std::string, std::deque< std::string > >
{
private:
    // ----------------------- attributes
    list_ind_heatmaps(const list_ind_heatmaps &orig); // deactivate copy constructor

public:
    // ----------------------- constructor
    list_ind_heatmaps();
    virtual ~list_ind_heatmaps();

    // ----------------------- methods
    void addHeatmap(const std::string &graphName, const std::string &heatmap);
    void write(const std::string &path) const;
};
} /* vap namespace end */
#endif	/* LIST_IND_HEATMAPS_H */

