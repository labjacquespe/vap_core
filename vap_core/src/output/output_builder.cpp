/**
 * @file   output_builder.cpp
 * @author Charles Coulombe
 *
 * @date 14 December 2012, 09:49
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./output_builder.h"

namespace vap
{

void output_builder::_writeHeaderAgg(graph_orientation::orientation mean,
                                     const group_feature<aggregate_reference_feature> &aggregateFeatures,
                                     const uint_t groupIndex,
                                     const std::string &datasetName,
                                     const parameters &param)
{
    _outAgg << "###" << APPNAME << "_" << APPVERSION << ":Graph" << graph_orientation::toString(mean) << "###" << std::endl << "#";

    if(!param.oneGraphPerGroup)
        for(uint_t i = 0; i < aggregateFeatures.groupCount(); i++)
            _outAgg << "\t" << "<" << aggregateFeatures[static_cast<uint_t>(mean) + (i * N_ORI_SUBGROUPS)].population(0) << "> " << aggregateFeatures.groupName(i) << " : " << datasetName << " : " << graph_orientation::toString(mean) << "\t\t\t";
    else
        _outAgg << "\t" << "<" << aggregateFeatures[static_cast<uint_t>(mean) + (groupIndex * N_ORI_SUBGROUPS)].population(0) << "> " << aggregateFeatures.groupName(groupIndex) << " : " << datasetName << " : " << graph_orientation::toString(mean) << "\t\t";

    _outAgg << std::endl << "#";

    if(!param.oneGraphPerGroup)
        for(uint_t i = 0; i < aggregateFeatures.groupCount(); i++)
            _outAgg << "relative_coordinates" << "\taggregate_signal_" << aggregate_data_type::toString(param.aggregateDataType) << "\tconfidence_value_" << mean_dispersion_value::toString(param.meanDispersionValue) << "\tproportion\t";
    else
        _outAgg << "relative_coordinates" << "\taggregate_signal_" << aggregate_data_type::toString(param.aggregateDataType) << "\tconfidence_value_" << mean_dispersion_value::toString(param.meanDispersionValue) << "\tproportion\t";

    _outAgg << std::endl;
}

void output_builder::_initSequences(std::map<uint_t, std::vector<agg_output_window> > &input, std::map<uint_t, std::vector<agg_output_window> > &output, graph_orientation::orientation mean, const group_feature<aggregate_reference_feature> &aggregateFeatures, const uint_t groupCount, const int groupIdx)
{
    uint_t blockIndex = 0, currentGroupIdx;
    std::vector<block>::const_iterator blockIt;
    for(uint_t grpIdx = 0; grpIdx < groupCount; grpIdx++)
    {
        // get the current group from its index or the iterator
        // groupCount equals 1 only when the user has selected OneGraphPerGroup,
        // otherwise, groupCount equals the number of reference groups.
        currentGroupIdx = (groupIdx >= 0 && groupCount == 1 ? groupIdx : grpIdx);

        for(blockIt = aggregateFeatures[static_cast<uint_t>(mean) + (currentGroupIdx * N_ORI_SUBGROUPS)].blocks().begin(), blockIndex = 0; blockIt != aggregateFeatures[static_cast<uint_t>(mean) + (currentGroupIdx * N_ORI_SUBGROUPS)].blocks().end(); ++blockIt, blockIndex++)
        {
            input[grpIdx].reserve(blockIt->windows().size());
            output[grpIdx].reserve(blockIt->windows().size());
            //            input[grpIdx].insert(input[grpIdx].begin() + input[grpIdx].size(), blockIt->windows().begin(), blockIt->windows().end());

            for(windows_t::const_iterator wIt = blockIt->windows().begin(); wIt != blockIt->windows().end(); ++wIt)
                input[grpIdx].push_back(agg_output_window(*wIt, blockIndex));
        }

        output[grpIdx].resize(input[grpIdx].size(), agg_output_window(false));
    }
}

void output_builder::_outputAgg(graph_orientation::orientation mean,
                                const group_feature<aggregate_reference_feature> &aggregateFeatures,
                                const uint_t groupCount,
                                const int groupIdx,
                                const parameters &param)
{
    ASSERT(groupCount <= aggregateFeatures.groupCount(), "Group count is too big");

    std::map<uint_t, std::vector<agg_output_window> > output, input;
    _initSequences(input, output, mean, aggregateFeatures, groupCount, groupIdx);

    // divide quantity of smoothing windows by 2 because we want the real smoothing offset
    uint_t offset = param.smoothingWindows / 2,
            currentGroupIdx;
    int relativeWindow = 0;
    float proportion = 0.0f;

    bool isAbsolute = param.analysisMethod == analysis_method::type::ABSOLUTE;

    // smooth each data for all group
    for(uint_t grpIdx = 0; grpIdx < groupCount; grpIdx++)
    {
        if(isAbsolute)
        {
            // insert invalid windows into sequences
            _insertInvalidWindows(input[grpIdx], offset, param);
            _insertInvalidWindows(output[grpIdx], offset, param);
        }

        // spline data, divide quantity of smoothing windows by 2 because we want the real smoothing offset
        // eg : 6 smoothing output_window -> 6/2 = 3 windows(offset) on each side of the processed output_window.
        _smooth(input[grpIdx], output[grpIdx], offset);
    }

    relativeWindow = firstRelativeWindow(param);
    for(uint_t windowIdx = 0; windowIdx < output[0].size()/*all groups have the same size*/; ++windowIdx)
    {
        // if we have an invalid
        // all invalid windows are at the same index in all groups.
        // This allows us to test only for the group 0.
        if(output[0][windowIdx].isInvalid())
        {
            // loop for all member of group reference feature
            for(uint_t grpIdx = 0; grpIdx < groupCount; grpIdx++)
                _outAgg << INVALID_WINDOW << "\t" << INVALID_WINDOW << "\t" << INVALID_WINDOW << "\t" << INVALID_WINDOW << "\t";

            _outAgg << std::endl;
        }
        else
        {


            // loop for all member of group reference feature
            for(uint_t grpIdx = 0; grpIdx < groupCount; grpIdx++)
            {
                // get the current group from its index or the iterator
                // groupCount equals 1 only when the user has selected OneGraphPerGroup,
                // otherwise, groupCount equals the number of reference groups.
                currentGroupIdx = (groupIdx >= 0 && groupCount == 1 ? groupIdx : grpIdx);

                //                _outAgg << relativeWindow << "\t";
                //                proportion = output[grpIdx][windowIdx].count() / (float) aggregateFeatures[static_cast<uint_t>(mean) + (grpIdx * N_ORI_SUBGROUPS)].meanPopulation;

                // do not write a NaN value for proportion if its zero (avoid division by zero)
                if(aggregateFeatures[static_cast<uint_t>(mean) + (currentGroupIdx * N_ORI_SUBGROUPS)].population(output[grpIdx][windowIdx].index()) > 0)
                    proportion = output[grpIdx][windowIdx].count() / (float) aggregateFeatures[static_cast<uint_t>(mean) + (currentGroupIdx * N_ORI_SUBGROUPS)].population(output[grpIdx][windowIdx].index());
                else
                    proportion = 0;
#ifdef __DEBUG__
                if(!output[grpIdx][windowIdx].isNotPopulated())
                    _outAgg << relativeWindow << "\t" << output[grpIdx][windowIdx].value() << "\t" << output[grpIdx][windowIdx].stdDev() << "\t" << proportion << "\t" << output[grpIdx][windowIdx].count() << "/" << (float) aggregateFeatures[static_cast<uint_t>(mean) + (currentGroupIdx * N_ORI_SUBGROUPS)].population(output[grpIdx][windowIdx].index()) << "\t";
                else
                    _outAgg << relativeWindow << "\t" << NOT_AVAILABLE << "\t0\t" << proportion << "\t" << output[grpIdx][windowIdx].count() << "/" << (float) aggregateFeatures[static_cast<uint_t>(mean) + (currentGroupIdx * N_ORI_SUBGROUPS)].population(output[grpIdx][windowIdx].index()) << "\t";
#else
                if(!output[grpIdx][windowIdx].isNotPopulated())
                    _outAgg << relativeWindow << "\t" << output[grpIdx][windowIdx].value() << "\t" << output[grpIdx][windowIdx].stdDev() << "\t" << proportion << "\t";
                else
                    _outAgg << relativeWindow << "\t" << NOT_AVAILABLE << "\t0\t" << proportion << "\t";
#endif
            }

            _outAgg << std::endl;
            relativeWindow += isAbsolute ? param.windowSize : 1;
        }
    }
}

void output_builder::_writeHeaderInd(std::ofstream &file, const std::string &groupName, const std::string &datasetName, const parameters &param)
{
    uint_t leftBound = 0,
            offset = std::max((uint_t) 1, param.smoothingWindows / 2), // insert at least 1 invalid window
            blockSize = 0;

    int relativeWindow = firstRelativeWindow(param);
    bool l, r, s, isAbsolute = param.analysisMethod == analysis_method::type::ABSOLUTE;

    file << "###" << APPNAME << "_" << APPVERSION << ":" << groupName << "_" << datasetName << "###" << "\n"
            << "#Individual features values" << "\n"
            << "#Feature\tAlias\tChromosome\tRegionStart\tRegionEnd\tRefFeatureStart\tRefFeatureEnd\tStrand\tOrientation" << "\t";

    // loop for header windows
    for(uint_t blockIdx = 0; blockIdx < param.referencePoints + 1; blockIdx++)
    {
        file << "Block " << blockIdx;
        for(uint_t windowIdx = 0; windowIdx < param.windowsPerBlock[blockIdx] + (isAbsolute ? offset : 0); windowIdx++)
            file << "\t";
    }

    file << "Other informations from genome annotations file\n";
    for(uint_t i = 0; i < NB_IND_TITLE_COLUMNS - 1; ++i)
        file << "\t";

    // loop for header windows
    for(uint_t blockIdx = 0; blockIdx < param.referencePoints + 1; blockIdx++)
    {
        blockSize = param.windowsPerBlock[blockIdx] + (isAbsolute ? offset : 0);

        // calculate the proportion
        leftBound = _leftBound(param.windowsPerBlock[blockIdx], blockIdx, param);

        for(uint_t wIdx = 0, idx = 0; idx < blockSize; idx++)
        {
            l = (param.blockAlignment[blockIdx] == block_alignment::alignment::LEFT && idx >= blockSize - offset);
            r = (param.blockAlignment[blockIdx] == block_alignment::alignment::RIGHT && idx < offset);
            s = (param.blockAlignment[blockIdx] == block_alignment::alignment::SPLIT && idx >= leftBound && idx < leftBound + offset);

            // for left, right align and split, adds "n" invalid windows
            if(isAbsolute && (l || r || s))
            {
                file << "\tInvalid Window";
            }
            else
            {
                file << "\tW" << wIdx << "_" << relativeWindow;
                relativeWindow += isAbsolute ? param.windowSize : 1;
                wIdx++;
            }
        }
    }

    file << std::endl;
}

uint_t output_builder::_leftBound(uint_t blockSize, uint_t blockIdx, const parameters &param)
{
    float proportion = 0.0f;
    uint_t absVal = -1;

    // calculate the proportion
    switch(param.blockSplitType[blockIdx])
    {
        case block_split_type::type::ABSOLUTE:
            absVal = param.blockSplitValue[blockIdx];
            proportion = block::proportion(absVal, (blockSize * param.windowSize), blockSize, param.windowSize, param.blockSplitAlignment[blockIdx] == block_split_alignment::type::LEFT);
            break;

        case block_split_type::type::PERCENTAGE:
            proportion = param.blockSplitValue[blockIdx] / 100.0f;

            if(param.blockSplitAlignment[blockIdx] == block_split_alignment::type::RIGHT)
                proportion = 1 - proportion;

            break;

        default:
            break;
    }

    return (param.windowsPerBlock[blockIdx] * proportion);
}

void output_builder::_outputInd(const std::bitset < N_ORI_SUBGROUPS > &orientationSubgroups,
                                const std::string &groupName,
                                const uint_t groupIndex,
                                const group_feature<aggregate_reference_feature> &aggregateFeatures,
                                const std::vector<const reference_feature *> referenceFeatures,
                                std::ofstream &outInd,
                                const parameters &param,
                                const bool convertToOneBase,
                                const bool convertToHalfOpen)
{
    std::deque<ind_output_window> sequence;

    uint_t leftBound = 0,
            offset = std::max((uint_t) 1, param.smoothingWindows / 2), // insert at least 1 invalid window
            addedWindows = offset * (param.referencePoints + 1);
    uint_t totalNumberOfWindows = (lastRelativeWindow(param) + (firstRelativeWindow(param) * -1)) / param.windowSize;
    uint_t blockSize = 0;
    bool l, r, s, isAbsolute = param.analysisMethod == analysis_method::type::ABSOLUTE;
    int regionStart = 0, regionEnd = 0, rfStart = 0, rfEnd = 0;

    // loop for all groups members
    for(uint_t refIdx = 0; refIdx < referenceFeatures.size(); refIdx++)
    {
        regionStart = referenceFeatures[refIdx]->begin();
        regionEnd = referenceFeatures[refIdx]->end();
        rfStart = referenceFeatures[refIdx]->featureOfInterest().begin();
        rfEnd = referenceFeatures[refIdx]->featureOfInterest().end();

        // We use a zero-based system internally, thus the need
        // to transform to one-based coordinates system.
        // Zero-based : [1-11] -> [0-10] -> -1
        // One-based  : [0-10] -> [1-11] -> +1
        if(convertToOneBase)
        {
            util::toOneBased(regionStart, regionEnd);
            util::toOneBased(rfStart, rfEnd);
        }

        // We use an inclusive range representation internally, thus the need
        // to transform to half-open coordinates system.
        // Half-Open : [1-10] -> [1-11[ -> +1
        // Closed    : [1-11[ -> [1-10] -> -1
        if(convertToHalfOpen)
        {
            util::toOpenRange(regionStart, regionEnd);
            util::toOpenRange(rfStart, rfEnd);
        }

        outInd << referenceFeatures[refIdx]->name() << "\t"
                << referenceFeatures[refIdx]->alias() << "\t"
                << referenceFeatures[refIdx]->chromosome() << "\t"
                << regionStart << "\t"
                << regionEnd << "\t"
                << rfStart << "\t"
                << rfEnd << "\t"
                << annotation_strand::toSymbol(referenceFeatures[refIdx]->strand()) << "\t"
                << orientation_type::toString((orientation_type::type)(abs((int) referenceFeatures[refIdx]->orientation())));

        // loop for all blocks in current member + 1
        for(uint_t blockIdx = 0; blockIdx < referenceFeatures[refIdx]->blocks().size(); blockIdx++)
        {
            blockSize = param.windowsPerBlock[blockIdx] + (isAbsolute ? offset : 0);

            // calculate the proportion
            leftBound = _leftBound(param.windowsPerBlock[blockIdx], blockIdx, param);

            // resize to block size, and fill with not populated window
            sequence.resize(param.windowsPerBlock[blockIdx], ind_output_window(false));

            for(uint_t i = 0; i < referenceFeatures[refIdx]->blocks()[blockIdx].windows().size(); i++)
                sequence[referenceFeatures[refIdx]->blocks()[blockIdx].windows()[i].index()] = referenceFeatures[refIdx]->blocks()[blockIdx].windows()[i];

            // loop for all windows
            for(uint_t windowIdx = 0, idx = 0; idx < blockSize; idx++)
            {
                l = (param.blockAlignment[blockIdx] == block_alignment::alignment::LEFT && idx >= blockSize - offset);
                r = (param.blockAlignment[blockIdx] == block_alignment::alignment::RIGHT && idx < offset);
                s = (param.blockAlignment[blockIdx] == block_alignment::alignment::SPLIT && idx >= leftBound && idx < leftBound + offset);

                // for left, right align and split, adds "n" invalid windows
                if(isAbsolute && (l || r || s))
                    outInd << "\t" << INVALID_WINDOW;
                else
                {
                    sequence[windowIdx].print(outInd);
                    windowIdx++;
                }
            }

            sequence.clear();
        }

        outInd << referenceFeatures[refIdx]->extra() << std::endl;
    }

    // write separator for the total number of columns(9 data fields, windows columns + inv windows)
    for(uint_t k = 0; k < totalNumberOfWindows + addedWindows + NB_IND_TITLE_COLUMNS; k++)
        outInd << "#\t";

    outInd << "\n###Aggregate###\n";

    // loop for all mean
    for(int meanIdx = 0; meanIdx < N_ORI_SUBGROUPS; meanIdx++)
    {
        // check if current mean must be outputted
        if(orientationSubgroups[meanIdx])
        {
            outInd << graph_orientation::toString(static_cast<graph_orientation::orientation>(meanIdx));

            // Output the space equivalent to the title columns (should be equal to NB_IND_TITLE_COLUMNS)
            for(uint_t i = 0; i < NB_IND_TITLE_COLUMNS - 1; ++i)
                outInd << "\t";

            // loop for all bloc in current member + 1
            for(uint_t blockIdx = 0; blockIdx < aggregateFeatures[meanIdx + (groupIndex * N_ORI_SUBGROUPS)].blocks().size(); blockIdx++)
            {
                blockSize = param.windowsPerBlock[blockIdx] + (isAbsolute ? offset : 0);

                // calculate the proportion
                leftBound = _leftBound(param.windowsPerBlock[blockIdx], blockIdx, param);

                // loop for all windows
                for(uint_t windowIdx = 0, idx = 0; idx < blockSize; idx++)
                {
                    l = (param.blockAlignment[blockIdx] == block_alignment::alignment::LEFT && idx >= blockSize - offset);
                    r = (param.blockAlignment[blockIdx] == block_alignment::alignment::RIGHT && idx < offset);
                    s = (param.blockAlignment[blockIdx] == block_alignment::alignment::SPLIT && idx >= leftBound && idx < leftBound + offset);

                    // for left, right align and split, adds "n" invalid windows
                    if((isAbsolute && (l || r || s)))
                        outInd << "\t" << INVALID_WINDOW;
                    else
                    {
                        if(aggregateFeatures[meanIdx + (groupIndex * N_ORI_SUBGROUPS)].blocks()[blockIdx].windows()[windowIdx].isNotPopulated())
                            outInd << "\t" << NOT_AVAILABLE;
                        else
                            outInd << "\t" << aggregateFeatures[meanIdx + (groupIndex * N_ORI_SUBGROUPS)].blocks()[blockIdx].windows()[windowIdx].value();

                        windowIdx++;
                    }
                }
            }

            outInd << std::endl;
        }
    }
    outInd.close();
}

void output_builder::_smooth(const std::vector<agg_output_window> &input, std::vector<agg_output_window> &output, uint_t offset)
{
    // validate offset is not superior to half the size
    ASSERT(offset <= input.size() / 2, "Offset is too big");

    float sum = 0;

    // get the max number of elements that can be considered by the periodic average
    uint_t elements = offset * 2 + 1,
            count = 0; // the actual number of elements considered by the periodic average

    // copy prior offset elements(beginning elements)
    for(uint_t i = 0; i < offset; i++)
        output[i] = input[i];

    uint_t j = offset;
    // no need to consider both extremities
    for(std::vector<agg_output_window>::const_iterator it = input.begin() + offset; it != input.end() - offset; ++it, j++)
    {
        if(it->isInvalid() || it->isNotPopulated())
        {
            output[j].setIndex(it->index());
            continue;
        }

        // compute periodic average within offset bounds
        for(uint_t k = 0; k < elements; k++)
        {
            if(!(it - offset + k)->isInvalid() && !(it - offset + k)->isNotPopulated())
            {
                sum += (it - offset + k)->value();
                count++;
            }
        }

        output[j] = *it;
        output[j].setValue(count == 0 ? sum : sum / count); // update average
        sum = count = 0; // reset sum and count for next calculation
    }

    // copy following elements(ending elements)
    for(uint_t l = input.size() - offset; l < input.size(); l++)
        output[l] = input[l];
}

/**
 * @note works with std::vector or std::deque
 * @param sequence
 * @param n
 * @param param
 */
template <class CONTAINER>
void output_builder::_insertInvalidWindows(CONTAINER &sequence, uint_t n, const parameters &param)
{
    //    float proportion = 0.0f;
    uint_t leftBound = 0,
            it = 0,
            size;

    // insert at least one invalid window
    if(n == 0)
        n = 1;

    // for all reference point 0,1,2,3...
    for(uint_t refPtIdx = 0; refPtIdx < param.referencePoints + 1; refPtIdx++)
    {
        // "it" is an iterator over the block, which typically represents
        // the end of each block
        it += param.windowsPerBlock[refPtIdx];

        // size will be number of output_window in the current block
        // which is required to find the beginning of any block
        size = param.windowsPerBlock[refPtIdx];

        if(param.blockAlignment[refPtIdx] == block_alignment::alignment::LEFT)
        {
            // insert on ending of block as it is left align
            // "it" always represents the end of the current block
            sequence.insert(sequence.begin() + it, n, typename CONTAINER::value_type(true));

            // equilibrate the iterator with the new size
            it += n;
        }
        else if(param.blockAlignment[refPtIdx] == block_alignment::alignment::RIGHT)
        {
            // insert on beginning of block as it is right align
            // "it" always represents the end of the current block, thus
            // to point to the beginning of the block, we need to subtract
            // the size of the current block
            sequence.insert(sequence.begin() + it - size, n, typename CONTAINER::value_type(true));

            // equilibrate the iterator with the new size
            it += n;
        }
        else
        {
            //            float leftProportion = 0.0f;
            //
            //            // calculate the proportion
            //            if(param.blockSplitType[refPtIdx] == block_split_type::ABSOLUTE)
            //            {
            //                uint_t absVal = param.blockSplitValue[refPtIdx];
            //                proportion = block::proportion(absVal, (size * param.windowSize), size, param.windowSize, param.blockSplitAlignment[refPtIdx] == block_split_alignment::LEFT);
            //            }
            //            else
            //            {
            //                // calculate the proportion
            //                proportion = param.blockSplitValue[refPtIdx] / 100.0f;
            //                leftProportion = param.blockSplitAlignment[refPtIdx] == block_split_alignment::LEFT ? proportion : 1 - proportion;
            //            }
            //
            //            leftBound = param.windowsPerBlock[refPtIdx] * leftProportion;

            leftBound = _leftBound(size, refPtIdx, param);

            // insert on cutoff boundary as it is split regarding a certain proportion
            // to do so, find the begining of block as in "rigth align" method,
            // then simply add the boundary.
            sequence.insert(sequence.begin() + it - size + leftBound, n, typename CONTAINER::value_type(true));

            // equilibrate the iterator with the new size
            it += n;
        }
    }
}

output_builder::output_builder() { }

output_builder::~output_builder() { }

void output_builder::outputResults(const std::string &datasetName, const group_feature<reference_feature> &referenceFeatures, const group_feature<aggregate_reference_feature> &aggregateFeatures, const parameters &param)
{
    std::string outAggFilename = std::string(OUT_AGG_PREFIX_FILENAME) + "_" + datasetName + "_",
            outIndFilename = std::string(OUT_IND_PREFIX_FILENAME) + "_" + datasetName + "_",
            filename,
            extension = FILE_EXTENSION,
            prefix = param.outputDirectory + (param.prefixFilename.empty() ? "" : param.prefixFilename + "_"),
            prefixHeatmap = prefix + HEATMAP_PREFIX + "_" + datasetName + "_";

    // if aggregate graphs are required...
    if(param.generateAggregateGraphs)
    {
        // loop for all mean
        for(uint_t i = 0; i < N_ORI_SUBGROUPS; i++)
        {
            // check if current mean must be outputted
            if(param.orientationSubgroups[i])
            {
                graph_orientation::orientation ori = static_cast<graph_orientation::orientation>(i);
                if(param.oneGraphPerGroup)
                {
                    for(uint_t grpIdx = 0; grpIdx < aggregateFeatures.groupCount(); grpIdx++)
                    {
                        // open aggregate file
                        filename = prefix + outAggFilename + aggregateFeatures.groupName(grpIdx) + "_" + graph_orientation::toString(ori) + extension;
                        _outAgg.open(filename.c_str(), std::ios::out | std::ios::trunc);

                        if(_outAgg.is_open())
                        {
                            _writeHeaderAgg(ori, aggregateFeatures, grpIdx, datasetName, param);
                            _outputAgg(ori, aggregateFeatures, 1/*one graph per group*/, grpIdx, param);
                            _outAgg.close();
                        }
                        else
                        {
                            PRINT_AND_LOG_WARNING(MSG_FILE_NOTOPEN_NOTFOUND(filename));
                            ASSERT(false, MSG_FILE_NOTOPEN_NOTFOUND(filename));
                        }
                    }
                }
                else
                {
                    // open aggregate file
                    filename = prefix + outAggFilename + graph_orientation::toString(ori) + extension;
                    _outAgg.open(filename.c_str(), std::ios::out | std::ios::trunc);

                    if(_outAgg.is_open())
                    {
                        _writeHeaderAgg(ori, aggregateFeatures, -1, datasetName, param);
                        _outputAgg(ori, aggregateFeatures, aggregateFeatures.groupCount(), -1, param);
                        _outAgg.close();
                    }
                    else
                    {
                        PRINT_AND_LOG_WARNING(MSG_FILE_NOTOPEN_NOTFOUND(filename));
                        ASSERT(false, MSG_FILE_NOTOPEN_NOTFOUND(filename));
                    }
                }
            }
        }

        // TODO : Move into loop and split into the different cases.
        _graphs.addDataset(aggregateFeatures, datasetName, param);
    }

    // if an output of each reference feature is required...
    if(param.writeIndividualReferences)
    {
        // Creates a temporary structure of the reference features to reorder them into the original order they were listed.
        // This allows us to use the individual file for heat map.
        std::map<uint_t, std::vector<const reference_feature *> > featuresAddr;
        for(group_feature<reference_feature>::const_iterator featureIt = referenceFeatures.begin(); featureIt != referenceFeatures.end(); ++featureIt)
            featuresAddr[featureIt->groupIndex()].push_back(&(*featureIt)); // insert pointer of reference feature

        for(uint_t grpIdx = 0; grpIdx < aggregateFeatures.groupCount(); ++grpIdx)
            std::sort(featuresAddr[grpIdx].begin(), featuresAddr[grpIdx].end(), features_index_less_compare); // sort group RF in their original order

        // creates and open file
        std::ofstream outInd;

        //output a file for each group of reference feature
        for(uint_t grpIdx = 0; grpIdx < aggregateFeatures.groupCount(); grpIdx++)
        {
            filename = prefix + outIndFilename + aggregateFeatures.groupName(grpIdx) + extension;
            outInd.open(filename.c_str(), std::ios::out | std::ios::trunc);

            if(outInd.is_open())
            {
                _writeHeaderInd(outInd, aggregateFeatures.groupName(grpIdx), datasetName, param);
                _outputInd(param.orientationSubgroups, aggregateFeatures.groupName(grpIdx), grpIdx, aggregateFeatures, featuresAddr[grpIdx], outInd, param, referenceFeatures.isOneBased(grpIdx), referenceFeatures.isHalfOpen(grpIdx));
                outInd.close();

                // add 'ind' file to the list of heat map files to be created
                if(param.generateHeatmaps)
                    _heatmaps.addHeatmap((prefixHeatmap + aggregateFeatures.groupName(grpIdx)), filename);
            }
            else
            {
                PRINT_AND_LOG_WARNING(MSG_FILE_NOTOPEN_NOTFOUND(filename));
                ASSERT(false, MSG_FILE_NOTOPEN_NOTFOUND(filename));
            }
        }
    }
}

int output_builder::firstRelativeWindow(const parameters &param)
{
    uint_t windows = 0, i = 0;
    uint_t blockOfInterest = param.analysisMode == analysis_mode::type::EXON ? 1 : std::floor((param.referencePoints + 1) / (float) 2);
    bool isAbsolute = param.analysisMethod == analysis_method::type::ABSOLUTE;

    // add all the number of windows in each prior blocks of the block of interest
    while(i < blockOfInterest)
        windows += param.windowsPerBlock[i++];

    return (-1 * windows * (isAbsolute ? param.windowSize : 1));
}

int output_builder::lastRelativeWindow(const parameters &param)
{
    uint_t windows = 0, i = param.analysisMode == analysis_mode::type::EXON ? 1 : std::floor((param.referencePoints + 1) / (float) 2);
    bool isAbsolute = param.analysisMethod == analysis_method::type::ABSOLUTE;

    // add all the number of windows in each following blocks of the block of interest
    while(i < param.referencePoints + 1)
        windows += param.windowsPerBlock[i++];

    return (windows * (isAbsolute ? param.windowSize : 1));
}

/**
 * Prints to file the graph mappings.
 *
 * The file's containing the mapping will be deduced of the current parameters
 * and will look like '%prefix_%map_graph_datafiles.txt' where %prefix%
 * is the prefix found in the parameter file.
 *
 * @remark
 * The "map_graphs_datafiles.txt" is used by the interface in order to create
 * correctly the graphs.
 *
 * @param param application parameter.
 */
void output_builder::writeListAggGraphs(const parameters &param)
{
    std::string path = param.outputDirectory + (param.prefixFilename.empty() ? "" : param.prefixFilename + "_") + LIST_AGG_GRAPHS_FILENAME;

    _graphs.write(path);
}

/**
 * Prints to file the list of heatmaps files.
 *
 * The file's containing the listing will be deduced of the current parameters
 * and will look like '%prefix_%list_heatmap_datafiles.txt' where %prefix%
 * is the prefix found in the parameter file.
 *
 * @remark
 * The "list_heatmap_datafiles.txt" is used by the interface in order to create
 * correctly all the heatmaps.
 *
 * @param param application parameters.
 */
void output_builder::writeListIndHeatmaps(const parameters &param)
{
    std::string path = param.outputDirectory + (param.prefixFilename.empty() ? "" : param.prefixFilename + "_") + LIST_IND_HEATMAPS_FILENAME;

    _heatmaps.write(path);
}
} /* vap namespace end */
