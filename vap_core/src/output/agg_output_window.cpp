/**
 * @file   agg_output_window.cpp
 * @author Charles Coulombe
 *
 * @date 17 December 2012, 16:19
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./agg_output_window.h"

namespace vap
{
agg_output_window::agg_output_window(bool invalid)
: _invalid(invalid), _index(-1) { }

agg_output_window::agg_output_window(const agg_output_window& source, uint_t index)
: window(source._begin, source._end, -1), _invalid(source._invalid), _index(source._index)
{
    //    coordinates::setCoordinates(source.begin(), source.end());
    _count = source._count;
    _coverage = source._coverage;
    _value = source._value;
    _valueAccumulator = source._valueAccumulator;
    _stdDev = source._stdDev;
    _stdDevAccumulator = source._stdDevAccumulator;
    //    _invalid = source._invalid;
    //    _index = source.index();
}

agg_output_window::agg_output_window(const window& source, uint_t index)
: window(source.begin(), source.end(), -1), _invalid(false), _index(index)
{
    _value = source.value();
    _count = source.count();
    _stdDev = source.stdDev();
    _coverage = source.coverage();
}

agg_output_window::~agg_output_window() { }

/**
 * Checks if this @c output_window is invalid.
 * @return @c True when invalid, @c False otherwise
 */
bool agg_output_window::isInvalid() const
{
    return _invalid;
}

/**
 * Gets the @c block reference index that this @c agg_output_window belongs to.
 * @return index
 */
uint_t agg_output_window::index() const
{
    return _index;
}

/**
 * Gets the number of value that contributed for this @c output_window
 * @note read-only property
 * @return population count
 */
uint_t agg_output_window::count() const
{
    return _count;
}

/**
 * Gets the mean.
 * @note read-only property
 * @return mean value
 */
float agg_output_window::value() const
{
    return _value;
}

/**
 * Gets the standard deviation.
 * @note read-only property
 * @return standard deviation value
 */
float agg_output_window::stdDev() const
{
    return _stdDev;
}

/**
 * Sets mean, <b>WITHOUT</b> updating population count.
 * Used only for splining.
 * @param value new mean
 */
void agg_output_window::setValue(float value)
{
    _value = value;
}

/**
 * Sets the @c block reference index.
 * @param index new index
 */
void agg_output_window::setIndex(uint_t index)
{
    _index = index;
}

/**
 * Checks if @c window has never been populated.
 * @return @c True when @c window population is 0, @c False otherwise.
 */
bool agg_output_window::isNotPopulated() const
{
    // if no value populated this window
    return _count == 0;
}

int agg_output_window::begin() const
{
    return _begin;
}

int agg_output_window::end() const
{
    return _end;
}

agg_output_window &agg_output_window::operator=(const window& source)
{
    coordinates::setCoordinates(source.begin(), source.end());
    _count = source.count();
    _coverage = source.coverage();
    _value = source.value();
    _stdDev = source.stdDev();
    _invalid = false;

    return *this;
}

agg_output_window &agg_output_window::operator=(const agg_output_window& source)
{
    if(this != &source)
    {
        coordinates::setCoordinates(source._begin, source._end);
        _count = source._count;
        _coverage = source._coverage;
        _value = source._value;
        _valueAccumulator = source._valueAccumulator;
        _stdDev = source._stdDev;
        _stdDevAccumulator = source._stdDevAccumulator;
        _invalid = source._invalid;
        _index = source._index;
    }

    return *this;
}

std::ostream &agg_output_window::print(std::ostream& out, const agg_output_window& source, float proportion)
{
    if(source.isInvalid())
        out << "\t" << INVALID_WINDOW << "\t" << INVALID_WINDOW << "\t" << INVALID_WINDOW << "\t";

    else if(source.isNotPopulated())
        out << NOT_AVAILABLE << "\t0\t" << source.count() << "\t";

    else
        out << source.value() << "\t" << source.stdDev() << "\t" << proportion << "\t";

    return out;
}
} /* vap namespace end */