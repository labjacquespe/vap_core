/**
 * @file   ind_output_window.cpp
 * @author Charles Coulombe
 *
 * @date 18 December 2012, 18:29
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./ind_output_window.h"

namespace vap
{
ind_output_window::ind_output_window(bool invalid)
: _invalid(invalid) { }

ind_output_window::ind_output_window(const ind_output_window& source)
: window(source._begin, source._end, -1), _invalid(source._invalid)
{
    // coordinates::setCoordinates(source.begin(), source.end());
    _count = source._count;
    _coverage = source._coverage;
    _value = source._value;
    _valueAccumulator = source._valueAccumulator;
    _stdDev = source._stdDev;
    _stdDevAccumulator = source._stdDevAccumulator;
    // _invalid = source._invalid;
}

ind_output_window::ind_output_window(const window& source)
: window(source.begin(), source.end(), -1), _invalid(false)
{
    _value = source.value();
    _count = source.count();
    _stdDev = source.stdDev();
    _coverage = source.coverage();
}

ind_output_window::~ind_output_window() { }

/**
 * Checks if this @c output_window is invalid.
 * @return @c True when invalid, @c False otherwise
 */
bool ind_output_window::isInvalid() const
{
    return _invalid;
}

/**
 * Gets the mean.
 * @note read-only property
 * @return mean value
 */
float ind_output_window::value() const
{
    return _value;
}

/**
 * Sets mean, <b>WITHOUT</b> updating population count.
 * Used only for splining.
 * @param value new mean
 */
void ind_output_window::setValue(float value)
{
    _value = value;
}

/**
 * Checks if @c window has never been populated.
 * @return @c True when @c window population is 0, @c False otherwise.
 */
bool ind_output_window::isNotPopulated() const
{
    // if no value populated this window
    return _count == 0;
}

int ind_output_window::begin() const
{
    return _begin;
}

int ind_output_window::end() const
{
    return _end;
}

ind_output_window &ind_output_window::operator=(const window& source)
{
    coordinates::setCoordinates(source.begin(), source.end());
    _count = source.count();
    _coverage = source.coverage();
    _value = source.value();
    _stdDev = source.stdDev();
    _invalid = false;

    return *this;
}

ind_output_window &ind_output_window::operator=(const ind_output_window& source)
{
    if(this != &source)
    {
        coordinates::setCoordinates(source._begin, source._end);
        _count = source._count;
        _coverage = source._coverage;
        _value = source._value;
        _valueAccumulator = source._valueAccumulator;
        _stdDev = source._stdDev;
        _stdDevAccumulator = source._stdDevAccumulator;
        _invalid = source._invalid;
    }

    return *this;
}

std::ostream &ind_output_window::print(std::ostream& out)
{
    if(isInvalid())
        out << "\t" << INVALID_WINDOW;
    else if(isNotPopulated())
        out << "\t" << NOT_AVAILABLE;
    else
        out << "\t" << value();

    return out;
}
} /* vap namespace end */
