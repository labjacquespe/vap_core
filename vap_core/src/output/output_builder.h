/**
 * @file   output_builder.h
 * @author Charles Coulombe
 *
 * @date 14 December 2012, 09:49
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OUTPUT_BUILDER_H
#define	OUTPUT_BUILDER_H

// pre-processor directives
#include <deque>
#include <algorithm>
#include <string>
#include <fstream>

#include "./agg_output_window.h"
#include "./ind_output_window.h"
#include "./list_agg_graphs.h"
#include "./list_ind_heatmaps.h"

#include "../utils/debug/assert.h"
#include "../feature/group_feature.h"
#include "../feature/aggregate_reference_feature.h"
#include "../feature/reference_feature.h"
#include "../parameters/parameters.h"
#include "../utils/coordinates/coordinates_utils.h"

#include "../exception/error.h"

#define NB_IND_TITLE_COLUMNS 9
#define FILE_EXTENSION ".txt"

namespace vap
{

class output_builder
{
private:
    // ----------------------- attributes
    std::ofstream _outAgg;

    uint_t _lowerBound;
    uint_t _upperBound;

    list_agg_graphs _graphs;
    list_ind_heatmaps _heatmaps;

    // ----------------------- methods
    // disallow evil copy
    output_builder(const output_builder &orig);
    void operator=(const output_builder &orig);

    void _writeHeaderAgg(graph_orientation::orientation mean,
                         const group_feature<aggregate_reference_feature> &aggregateFeatures,
                         const uint_t groupIndex,
                         const std::string &datasetName,
                         const parameters &param);

    void _writeHeaderInd(std::ofstream &file, const std::string &groupName, const std::string &datasetName, const parameters &param);

    void _outputAgg(graph_orientation::orientation mean,
                    const group_feature<aggregate_reference_feature> &aggregateFeatures,
                    const uint_t groupCount,
                    const int groupIdx,
                    const parameters &param);

    void _outputInd(const std::bitset < N_ORI_SUBGROUPS > &orientationSubgroups,
                    const std::string &group_name,
                    const uint_t group_index,
                    const group_feature<aggregate_reference_feature> &aggregateFeatures,
                    const std::vector<const reference_feature *> referenceFeatures,
                    std::ofstream &outInd,
                    const parameters &param,
                    const bool convertToOneBased,
                    const bool convertToHalfOpen);

    void _smooth(const std::vector<agg_output_window> &input, std::vector<agg_output_window> &output, uint_t offset);

    uint_t _leftBound(uint_t blockSize, uint_t blockIdx, const parameters &param);

    template <class CONTAINER> void _insertInvalidWindows(CONTAINER &sequence, uint_t n, const parameters &param);

    void _initSequences(std::map<uint_t, std::vector<agg_output_window> > &inFput,
                        std::map<uint_t, std::vector<agg_output_window> > &output,
                        graph_orientation::orientation mean,
                        const group_feature<aggregate_reference_feature> &aggregateFeatures,
                        const uint_t groupCount,
                        const int groupIdx);
public:
    // ----------------------- constructors
    output_builder();
    ~output_builder();

    // ----------------------- accessors

    // ----------------------- modifiers

    // ----------------------- methods
    void outputResults(const std::string &datasetName, const group_feature<reference_feature> &referenceFeatures, const group_feature<aggregate_reference_feature> &aggregateFeatures, const parameters &param);

    void writeListAggGraphs(const parameters &param);
    void writeListIndHeatmaps(const parameters &param);

    static int firstRelativeWindow(const parameters &param);
    static int lastRelativeWindow(const parameters &param);
};
} /* vap namespace end */

#endif	/* OUTPUT_BUILDER_H */

