/**
 * @file   list_ind_heatmaps.cpp
 * @author Charles Coulombe
 *
 * @date 12 October 2014, 13:22
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "list_ind_heatmaps.h"

namespace vap
{

list_ind_heatmaps::list_ind_heatmaps() { }

list_ind_heatmaps::~list_ind_heatmaps() { }

/**
 * Adds an heatmap to the heatmaps list.
 *
 * @param graphName Heatmap graph name
 * @param heatmap Heatmap file
 */
void list_ind_heatmaps::addHeatmap(const std::string &graphName, const std::string &heatmap)
{
    (*this)[graphName].push_back(heatmap);
}

/**
 * Prints to file the list of heatmaps files.
 *
 * The file's containing the listing will be deduced of the current parameters
 * and will look like '%prefix_%list_ind_heatmaps.txt' where %prefix%
 * is the prefix found in the parameter file.
 *
 * @remark
 * The "list_ind_heatmaps.txt" is used by the interface in order to create
 * correctly all the heatmaps.
 *
 * @param path File's path.
 */
void list_ind_heatmaps::write(const std::string &path) const
{
    std::ofstream out(path, std::ios::out | std::ios::trunc);
    std::string graphName("", 100);

    if(out.is_open())
    {
        out << "###" << APPNAME << "_" << APPVERSION << ":Graph name###\t###Heatmap file###\n";

       // loop for all mappings
        for(std::map< std::string, std::deque< std::string > >::const_iterator it = begin(); it != end(); ++it)
        {
            graphName = filesystem::extract_filename(it->first);

            for(std::deque<std::string>::const_iterator i = it->second.begin(); i != it->second.end(); ++i)
                out << graphName << "\t" << *i << "\n";

            if(it->second.size() > 1)
                out << std::endl;
        }
    }
    else
        ERROR(false, MSG_FILE_NOTOPEN_NOTFOUND(path));
}
} /* vap namespace end */
