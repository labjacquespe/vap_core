/**
 * @file   ind_output_window.h
 * @author Charles Coulombe
 *
 * @date 18 December 2012, 18:29
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IND_OUTPUT_WINDOW_H
#define	IND_OUTPUT_WINDOW_H

#include <ostream>

#include "../feature/window.h"

namespace vap
{
class ind_output_window : private window
{
private:
    // ----------------------- attributes
    bool _invalid;

    // ----------------------- methods
public:
    // ----------------------- constructors
    ind_output_window(bool invalid);
    ind_output_window(const window &orig);
    ind_output_window(const ind_output_window &orig);
    ~ ind_output_window();

    // ----------------------- accessors
    bool isInvalid() const;
    float value() const;

    int begin() const;
    int end() const;

    // ----------------------- modifiers
    void setValue(float value);

    // ----------------------- methods
    bool isNotPopulated() const;

    ind_output_window &operator=(const window &source);
    ind_output_window &operator=(const ind_output_window &source);

    std::ostream &print(std::ostream &out);
};
} /* vap namespace end */

#endif	/* IND_OUTPUT_WINDOW_H */

