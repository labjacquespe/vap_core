/**
 * @file   reference_filter_entry.h
 * @author Charles Coulombe
 *
 * @date 13 August 2014, 15:54
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REFERENCE_FILTER_ENTRY_H
#define	REFERENCE_FILTER_ENTRY_H

#include <string>
#include <fstream>

namespace vap
{

/**
 * Filter entry.
 *
 * Is either a @c name_entry or a @c range_entry type.
 */
struct reference_filter_entry
{

    /** Filter entry type. */
    enum filter_type
    {
        NAME,
        RANGE
    };

    /** Name entry filter. */
    struct name_entry
    {
        /** Entry value. */
        std::string name;

        /**
         * Construct a new name entry.
         *
         * @param n name value
         */
        name_entry(const std::string &n)
        : name(n)
        {
        }

        /**
         * Prints the content of this entry to @a out.
         *
         * @param out Output stream.
         */
        void print(std::ostream &out) const
        {
            out << name;
        }

        /** Overload */
        friend std::ostream &operator<<(std::ostream &out, const name_entry &e)
        {
            e.print(out);
            return out;
        }
    };

    /** Range entry filter. */
    struct range_entry
    {
        /** Chromosome value. */
        std::string chromosome;

        /** Start position value. */
        int begin;

        /** End position value. */
        int end;

        /**
         * Constructs a new range entry.
         *
         * @param chr Chromosome value.
         * @param b Start value.
         * @param e End value.
         */
        range_entry(const std::string &chr, const int b, const int e) : chromosome(chr), begin(b), end(e)
        {
        }

        /**
         * Prints the content of this entry to @a out.
         *
         * @param out Output stream.
         */
        void print(std::ostream &out) const
        {
            out << chromosome << '\t' << begin << '\t' << end;
        }

        /** Overload. */
        friend std::ostream &operator<<(std::ostream &out, const range_entry &e)
        {
            e.print(out);
            return out;
        }
    };

    range_entry range;
    name_entry name;
    filter_type type;

    /**
     * Constructor of a @c range_entry type.
     *
     * @param chromosome Chromosome value.
     * @param start Begin value.
     * @param end End value.
     */
    reference_filter_entry(const std::string &chromosome, const int start, const int end)
    : range(range_entry(chromosome, start, end)), name(name_entry("")), type(RANGE)
    {
    }

    /**
     * Constructor of a @c range_entry type.
     *
     * @param e Entry.
     */
    reference_filter_entry(const range_entry &e)
    : range(e), name(name_entry("")), type(RANGE)
    {
    }

    /**
     * Constructor of a @c name_entry type.
     *
     * @param n Name value.
     */
    reference_filter_entry(const std::string &n)
    : range(range_entry("", 0, 0)), name(name_entry(n)), type(NAME)
    {
    }

    /**
     * Constructor of a @c name_entry type.
     *
     * @param e Entry.
     */
    reference_filter_entry(const name_entry &e)
    : range(range_entry("", 0, 0)), name(e), type(NAME)
    {
    }

    ~reference_filter_entry()
    {
    }

    /**
     * Prints the content of this entry to @a out.
     *
     * @param out Output stream.
     */
    void print(std::ostream &out) const
    {
        switch(type)
        {
            case NAME:
                name.print(out);
                break;
            case RANGE:
                range.print(out);
                break;
            default:
                break;
        }
    }

    /** Overload. */
    friend std::ostream &operator<<(std::ostream &out, const reference_filter_entry &e)
    {
        e.print(out);
        return out;
    }
};

} /* vap namespace end */

#endif	/* REFERENCE_FILTER_ENTRY_H */
