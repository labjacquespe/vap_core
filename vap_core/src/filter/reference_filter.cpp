/**
 * @file   reference_filter.h
 * @author Charles Coulombe
 *
 * @date 13 August 2014, 15:54
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "reference_filter.h"

namespace vap
{

////////////////////////////////////////////////////////////////////////////////
//                          PRIVATE
////////////////////////////////////////////////////////////////////////////////

/**
 * Insert a @c reference_filter_entry in either names or ranges filter.
 *
 * @param entry Entry to be inserted.
 */
void reference_filter::_insert(const reference_filter_entry &entry)
{
    switch(entry.type)
    {
        case reference_filter_entry::NAME:
            _names.insert(entry.name);
            break;
        case reference_filter_entry::RANGE:
            _ranges.insert(entry.range);
            break;
        default:
            break;
    }
}

/**
 * Parses file buffer.
 *
 * The following line format are supported :
 * - string [#comment]
 * - string integer integer [#comment]
 *
 * The comments are actually ignored through parsing.
 * The file must be tab-delimited.
 *
 * @param parser buffer reader for parsing content
 */
void reference_filter::_parse(util::parser_string &parser)
{
    std::string str = "";
    int start = -1, end = -1;

    // while buffer has character, parse chunk
    if(!parser.eob())
    {
        parser >> str;
        parser.skip();
        parser >> start;
        parser.skip();
        parser >> end;

        if(!str.empty() && start == -1 && end == -1)
            _insert(reference_filter_entry(str));
        else
            _insert(reference_filter_entry(str, start, end));
    }
}

////////////////////////////////////////////////////////////////////////////////
//                          PUBLIC
////////////////////////////////////////////////////////////////////////////////

/** Constructs a new @c reference_filter object. */
reference_filter::reference_filter()
: _ranges(ranges_type()), _names(names_type())
{ }

/** Destroys the object. */
reference_filter::~reference_filter() { }

/**
 * Gets the selected ranges filters.
 *
 * @return ranges filter entries
 */
const reference_filter::ranges_type &reference_filter::ranges() const
{
    return _ranges;
}

/**
 * Gets the selected names filters.
 *
 * @return names filter entries
 */
const reference_filter::names_type &reference_filter::names() const
{
    return _names;
}

/**
 * Gets the number of filters(including names and ranges).
 *
 * @return number of filters
 */
size_t reference_filter::size() const
{
    return _names.size() + _ranges.size();
}

/**
 * Checks if filter is empty.
 *
 * @return @c True when no names or ranges filters are present, @c False otherwise.
 */
bool reference_filter::isEmpty() const
{
    return _names.empty() && _ranges.empty();
}

/**
 * @note If file does not exists, nothing is done nor any error is thrown.
 *
 * @param path
 */
bool reference_filter::load(const std::string &path)
{
    bool wasRead = false;

    std::string line;
    std::unique_ptr<io::file_reader> freader(io::file_reader_factory::create(path));

    // if file exists and we can read it otherwise nothing is done
    if(freader->is_open())
    {
        // make buffer reader stops at end of line regardless of operating system
        util::parser_string parser(&line, " \t\r\n");

        // parse whole file
        while(freader->readline(line))
        {
            _parse(parser);
            parser.resetPosition(); // reset buffer read position
        }

        wasRead = true;
    }

    return wasRead;
}

/**
 * Clears all filter entries.
 */
void reference_filter::clear()
{
    _names.clear();
    _ranges.clear();
}

/** Overload operator= */
reference_filter &reference_filter::operator=(const reference_filter &rf)
{
    if(this != &rf)
    {
        _names.clear();
        _ranges.clear();

        _names = rf._names;
        _ranges = rf._ranges;
    }

    return *this;
}

/** @c reference_filter specialization. */
positive_filter::positive_filter() : reference_filter() { }

/** @c reference_filter specialization. */
negative_filter::negative_filter() : reference_filter() { }

} /* vap namespace end */
