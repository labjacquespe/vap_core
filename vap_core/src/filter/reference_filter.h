/**
 * @file   reference_filter.h
 * @author Charles Coulombe
 *
 * @date 13 August 2014, 15:54
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REFERENCE_FILTER_H
#define	REFERENCE_FILTER_H

#include <string>
#include <deque>
#include <vector>
#include <algorithm>

#include "../defs.h"
#include "reference_filter_entry.h"
#include "../dataset/genome_dataset.h"
#include "../dataset/reference_annotation.h"
#include "../dataset/reference_coordinates.h"

#include "../utils/file/file_reader.h"
#include "../utils/memory/parser_string.h"
#include "../utils/algorithm/algorithms.h"

namespace vap
{

namespace internal
{

/** Ascending @c reference_filter_entry::name_entry ordering functor. */
struct _lesser_reference_filter_name_entry
{

    bool operator()(const reference_filter_entry::name_entry &left, const reference_filter_entry::name_entry &right) const
    {
        return left.name < right.name;
    }
};

/** Ascending @c reference_filter_entry::range_entry ordering functor. */
struct _lesser_reference_filter_range_entry
{

    bool operator()(const reference_filter_entry::range_entry &left, const reference_filter_entry::range_entry &right) const
    {
        // chromosome : true when left chromosome comes before right chromosome, false otherwise
        // begin : true when left begin point comes before right begin point, false otherwise
        // end : true when right end point comes before left end point
        // ordering : chromosome(asc), begin(asc), end(desc)

        if(left.chromosome < right.chromosome)
            return true;

        if(left.chromosome == right.chromosome)
        {
            if(left.begin < right.begin)
                return true;

            else if(left.begin == right.begin && left.end > right.end)
                return true;
        }

        return false;
    }
};
}

class reference_filter
{
private:
    // ----------------------- methods
    reference_filter(const reference_filter& orig);

    void _parse(util::parser_string &parser);
    void _insert(const reference_filter_entry &entry);

    std::set<reference_filter_entry::range_entry, internal::_lesser_reference_filter_range_entry> _ranges;
    std::set<reference_filter_entry::name_entry, internal::_lesser_reference_filter_name_entry> _names;

public:
    typedef std::set<reference_filter_entry::range_entry, internal::_lesser_reference_filter_range_entry> ranges_type;
    typedef std::set<reference_filter_entry::name_entry, internal::_lesser_reference_filter_name_entry> names_type;

    // ----------------------- constructors
    explicit reference_filter();
    virtual ~reference_filter();

    // ----------------------- accessors
    bool isEmpty() const;
    size_t size() const;

    const ranges_type &ranges() const;
    const names_type &names() const;

    // ----------------------- methods
    bool load(const std::string &path);
    void clear();

    template <class LESSER_COMPARE, class GREATER_COMPARE> void mapRangesToNames(const std::deque<reference_coordinates> &genome, LESSER_COMPARE lesser_cmp, GREATER_COMPARE greater_cmp);
    template <class LESSER_COMPARE, class GREATER_COMPARE> void mapRangesToNames(const std::vector<genome_entry_pointer> &genome, LESSER_COMPARE lesser_cmp, GREATER_COMPARE greater_cmp);

    reference_filter &operator=(const reference_filter &rf);
};

/** @c reference_filter specialization. */
class positive_filter : public reference_filter
{
public:
    positive_filter();
};

/** @c reference_filter specialization. */
class negative_filter : public reference_filter
{
public:
    negative_filter();
};

/**
 * Map any range filter into annotations name using the reference genome.
 * All matching annotations will be inserted into the name filter.
 *
 * Ranges is cleared afterwards.
 */
template <class LESSER_COMPARE, class GREATER_COMPARE>
void reference_filter::mapRangesToNames(const std::vector<genome_entry_pointer> &genome, LESSER_COMPARE lesser_cmp, GREATER_COMPARE greater_cmp)
{
    std::vector<genome_entry_pointer>::const_iterator g = genome.begin();
    ranges_type::const_iterator r = _ranges.begin();

    while(g != genome.end() && r != _ranges.end())
    {
        if(greater_cmp(*g, *r))
            ++r; // genome entry is greater than range, advance to the next range
        else if(lesser_cmp(*g, *r))
            ++g; // genome entry is greater than range, advance to the next range
        else
        {
            // elements are equal, select entry value and advance to the next entry
            _names.insert(reference_filter_entry::name_entry(g->main->name));
            ++g;
        }
    }

    _ranges.clear();
}

/**
 * Map any range filter into annotations name using the reference genome.
 * All matching annotations will be inserted into the name filter.
 *
 * Ranges is cleared afterwards.
 */
template <class LESSER_COMPARE, class GREATER_COMPARE>
void reference_filter::mapRangesToNames(const std::deque<reference_coordinates> &genome, LESSER_COMPARE lesser_cmp, GREATER_COMPARE greater_cmp)
{
    std::deque<reference_coordinates>::const_iterator g = genome.begin();
    ranges_type::const_iterator r = _ranges.begin();

    while(g != genome.end() && r != _ranges.end())
    {
        if(greater_cmp(*g, *r))
            ++r; // genome entry is greater than range, advance to the next range
        else if(lesser_cmp(*g, *r))
            ++g; // genome entry is greater than range, advance to the next range
        else
        {
            // elements are equal, select entry value and advance to the next entry
            _names.insert(reference_filter_entry::name_entry(g->name));
            ++g;
        }
    }

    _ranges.clear();
}

//inline bool reference_filter_entry_less_compare(const reference_filter_entry &left, const reference_filter_entry &right)
//{
//    switch(left.type)
//    {
//        case reference_filter_entry::NAME:
//            return left.name.name < right.name.name;
//            break;
//
//        case reference_filter_entry::RANGE:
//            // chromosome : true when left chromosome comes before right chromosome, false otherwise
//            // begin : true when left begin point comes before right begin point, false otherwise
//            // end : true when right end point comes before left end point
//            // ordering : chromosome(asc), begin(asc), end(desc)
//
//            if(left.range.chromosome < right.range.chromosome)
//                return true;
//
//            if(left.range.chromosome == right.range.chromosome)
//            {
//                if(left.range.begin < right.range.begin)
//                    return true;
//
//                else if(left.range.begin == right.range.begin && left.range.end > right.range.end)
//                    return true;
//            }
//
//            return false;
//            break;
//    }
//}

/**
 * Compares @c reference_filter_entry::name_entry by looking at their @c name.
 * Order is alphanumerical and case sensitive.
 *
 * @param left Compared argument.
 * @param right Comparing argument.
 *
 * @return @c True if @a left name is greater than @a right name, @c false otherwise.
 */
inline bool reference_filter_entry_name_lesser_compare(const reference_filter_entry::name_entry &left, const reference_filter_entry::name_entry &right)
{
    return left.name < right.name;
}

/**
 * Compare a @c reference_filter_entry::range_entry by looking at their respective @a chromosome, @a begin and @a end point.
 *
 * Entry will be strictly ordered by ascending values except for the @c end point which will be descending.
 * In order to have a entry to comes before another it must have:
 *      - @a left @a chromosome must come before @a right @a chromosome;
 *      - @a left @a begin point must come before @a right @a begin point;
 *      - @a right @a end point must come before @a left @a end point.
 * Ordering can be resume to : chromosome(asc), begin(asc), end(desc).
 *
 * @note @c chromosome comparison is case insensitive
 *
 * @param left left argument(compared)
 * @param right right argument(comparing)
 *
 * @return @c True when either :
 *      - left chromosome comes before right chromosome;
 *      - left begin point comes before right begin point;
 *      - right end point comes before left end point;
 * @c False otherwise
 */
inline bool reference_filter_entry_range_lesser_compare(const reference_filter_entry::range_entry &left, const reference_filter_entry::range_entry &right)
{
    // chromosome : true when left chromosome comes before right chromosome, false otherwise
    // begin : true when left begin point comes before right begin point, false otherwise
    // end : true when right end point comes before left end point
    // ordering : chromosome(asc), begin(asc), end(desc)

    if(left.chromosome < right.chromosome)
        return true;

    if(left.chromosome == right.chromosome)
    {
        if(left.begin < right.begin)
            return true;

        else if(left.begin == right.begin && left.end > right.end)
            return true;
    }

    return false;
}

/**
 * Compares @c genome_entry_pointer and a @c reference_filter_entry::range by looking at their @c chromosome, @c begin and @c end point.
 * Order is exclusive, range does not allow any overlap. Meaning that @a gep end must be smaller than @a re begin.
 *
 * @param gep Left @c genome_entry_pointer. Compared argument.
 * @param re Right @c reference_filter_entry::name_entry. Comparing argument.
 *
 * @return @c True if :
 *  - @a gep chromosome is smaller than @a re chromosome
 *  - chromosomes are equal and @a gep coordinates of interest end is smaller than @a re begin
 *  @c False otherwise.
 */
inline bool genome_ptr_reference_filter_range_lesser_compare(const genome_entry_pointer &gep, const reference_filter_entry::range_entry &re)
{
    if(gep.main->chromosome < re.chromosome)
        return true;

    if(gep.main->chromosome == re.chromosome)
    {
        if(gep.main->end < re.begin)
            return true;
    }

    return false;
}

/**
 * Compares @c genome_entry_pointer and a @c reference_filter_entry::range by looking at their @c chromosome, @c begin and @c end point.
 * Order is exclusive, range does not allow any overlap. Meaning that @a gep end must be smaller than @a re begin.
 *
 * @param gep Left @c genome_entry_pointer. Compared argument.
 * @param re Right @c reference_filter_entry::name_entry. Comparing argument.
 *
 * @return @c True if :
 *  - @a gep chromosome is smaller than @a re chromosome
 *  - chromosomes are equal and @a gep coordinates of interest end is smaller than @a re begin
 *  @c False otherwise.
 */
inline bool genome_ptr_reference_filter_range_greater_compare(const genome_entry_pointer &gep, const reference_filter_entry::range_entry &re)
{
    if(gep.main->chromosome > re.chromosome)
        return true;

    if(gep.main->chromosome == re.chromosome)
    {
        if(gep.main->begin > re.end)
            return true;
    }

    return false;
}

/**
 * Applies name filters. Selection is done first, then exclusion is applied.
 *
 * @warning All range filters must have been mapped to their names entries.
 * @see mapRangesToNames()
 *
 * @param entries Sequence of element to be filtered.
 * @param selected Positive filters.
 * @param excluded Negative filters.
 * @param lesser_cmp Comparing lesser function.
 * @param greater_cmp Comparing greater function.
 */
template <class ENTRIES, class LESSER_COMPARE, class GREATER_COMPARE>
void applyNameFilters(ENTRIES &entries, const positive_filter &selected, const negative_filter &excluded, LESSER_COMPARE lesser_cmp, GREATER_COMPARE greater_cmp)
{
    // ASSUMPTION : Entries are sorted using the same ordering function as the one passed as argument(LESSER_COMPARE).
    // ASSUMPTION : Selected are sorted using the same ordering function as the one passed as argument(LESSER_COMPARE).
    // ASSUMPTION : Excluded are sorted using the same ordering function as the one passed as argument(LESSER_COMPARE).

    // Do nothing if there's no filter, or no entries
    if((selected.isEmpty() && excluded.isEmpty()) || entries.empty())
        return;

    ENTRIES selectionResult, exclusionResult;

    /*
     *  (sn U sr) -> (en) -> (er) -> (r)
     *
     *   First, make the selection subset :
     *   If not empty :
     *      Select entries which match the names filter.
     *   Else
     *       Selection subset is all entries.
     *
     *   Secondly, make the exclusion subset from the selection result :
     *   If not empty :
     *       Exclude selected entries which match the names filter.
     *   Else
     *       Exclusion result is all selected entries.
     *
     *   Exchange entries with final result.
     */

    // Select entries according to the positive filter.
    if(!selected.names().empty())
        util::select(entries.begin(), entries.end(), selected.names().begin(), selected.names().end(), std::back_inserter(selectionResult), lesser_cmp, greater_cmp);
    else
        selectionResult.swap(entries); // if there's no filter, selection result is all entries

    // Exclude entries according to the negative filter.
    if(!excluded.names().empty())
        util::exclude(selectionResult.begin(), selectionResult.end(), excluded.names().begin(), excluded.names().end(), std::back_inserter(exclusionResult), lesser_cmp, greater_cmp);
    else
        exclusionResult.swap(selectionResult);

    entries.swap(exclusionResult);
}

} /* vap namespace end */

#endif	/* REFERENCE_FILTER_H */

