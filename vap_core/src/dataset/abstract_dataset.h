/**
 * @file   abstract_dataset.h
 * @author Charles Coulombe
 *
 * @date 12 November 2012, 10:33
 * @version 1.0.0
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ABSTRACT_DATASET_H
#define ABSTRACT_DATASET_H

// pre-processor directives
#include <vector>
#include <map>
#include <deque>
#include <algorithm>

#include "../utils/file/file_utils.h"
#include "../utils/text/compare.h"
#include "../utils/file/file_reader.h"
#include "../utils/memory/parser_string.h"
#include "../defs.h"
#include "../messages/messages.h"
#include "../exception/error.h"

namespace vap
{
namespace internal
{
/** dataset key ordering object */
struct _less_dataset
{

    bool operator()(const std::string &l, const std::string &r) const
    {
        return l < r;
    }
};
} /* internal namespace end */

/** Generic dataset to contains ... */
template <class ENTRY>
class abstract_dataset : public std::map< std::string, std::deque<ENTRY>, internal::_less_dataset>
{
protected:
    // type definitions
    typedef std::deque<ENTRY> entries_t;

    // ----------------------- attributes
    std::unique_ptr<io::file_reader> _freader;

    /** dataset properties(key=value) */
    std::map< std::string, std::string > _properties;

    /** data file, used in future reference */
    std::string _path;

    unsigned long long _chunkSize;

    bool _hasHeader;
    // ----------------------- methods
    void _parseProperties(const std::string &line);
    bool _hasProperties() const;
    void _setDefaultName(const std::string &name);

private:
    // ----------------------- attributes


    // ----------------------- methods
    // disallow evil copy
    abstract_dataset(const abstract_dataset &orig);
    void operator=(const abstract_dataset &orig);

public:
    typedef std::string key_type;
    typedef std::deque<ENTRY> value_type;

    // ----------------------- constructors
    abstract_dataset(const std::string &path, unsigned long long chunkSize = -1, std::ios_base::openmode mode = std::ios_base::in);
    ~abstract_dataset();

    // ----------------------- accessors
    std::string propertyValue(const std::string &key) const;

    // ----------------------- modifiers

    // ----------------------- methods
    virtual bool load() = 0; // needs to be implemented in children
    void loadProperties(const std::string &buffer);

    template <class COMPARE>
    void sort(COMPARE compare);
};

////////////////////////////////////////////////////////////////////////////////
//                          PRIVATE
////////////////////////////////////////////////////////////////////////////////

template <class ENTRY>
void abstract_dataset<ENTRY>::_setDefaultName(const std::string &name)
{
    _properties["name"] = name;
}

/**
 * Checks if @c dataset has properties
 * @return @c True when properties were found, @c False otherwise
 */
template <class ENTRY>
bool abstract_dataset<ENTRY>::_hasProperties() const
{
    return _properties.size() > 0;
}

template <class ENTRY>
void abstract_dataset<ENTRY>::_parseProperties(const std::string &line)
{
    // TODO : parse other properties with a utils::parser_string
}
////////////////////////////////////////////////////////////////////////////////
//                          PUBLIC
////////////////////////////////////////////////////////////////////////////////

template <class ENTRY>
abstract_dataset<ENTRY>::abstract_dataset(const std::string &path, unsigned long long chunkSize, std::ios_base::openmode mode)
:_freader(io::file_reader_factory::create(path, mode)), _properties(std::map< std::string, std::string >()), _path(path), _chunkSize(chunkSize), _hasHeader(false)
{
    // sets path and default dataset name in case there's no properties
    _setDefaultName(filesystem::change_extension(filesystem::extract_filename(path), ""));

    ERROR(_freader->is_open(), MSG_FILE_NOTOPEN_NOTFOUND(path));
}

template <class ENTRY>
abstract_dataset<ENTRY>::~abstract_dataset() { }

/**
 * Loads properties by parsing buffer.
 * @note for now, only <i>name</i> is extracted
 * @param buffer line to parse
 */
template <class ENTRY>
void abstract_dataset<ENTRY>::loadProperties(const std::string &buffer)
{
    std::string key = "";
    std::string value = "";
    char garbage;

    util::parser_string bReaderWithQuotes(&buffer, "\t\n\r=\"");
    util::parser_string bReaderWithoutQuotes(&buffer, "\t\n\r =");

    size_t tagIdx = buffer.find("name=");

    if(tagIdx != std::string::npos)
    {
        bReaderWithQuotes.advance(tagIdx);
        bReaderWithoutQuotes.advance(tagIdx);

        // name=AAAAAA desc=asasa
        // name="AA AA" desc=asas

        if(buffer[tagIdx + 5] == '"')
            bReaderWithQuotes >> key >> garbage/* = */ >> garbage /* " */ >> value >> garbage/* " */;
        else
            bReaderWithoutQuotes >> key >> garbage/* = */ >> value;
    }

    _properties[key] = value;
}

/**
 * Gets a property by its key.
 * If @a key is not found, an empty @c std::string is returned.
 * @param key property key
 * @return @c std::string value or empty
 */
template <class ENTRY>
std::string abstract_dataset<ENTRY>::propertyValue(const std::string &key) const
{
    std::map<std::string, std::string>::const_iterator it = _properties.find(key);
    return it != _properties.end() ? it->second : "";
}

/**
 * Sorts all @c chromosome entries.
 * All entries must always be ordered using @a compare function(even with base <code>operator<</code>).
 * @note elements that would compare equal to each other are not guaranteed to keep their original relative order.
 * @param compare comparison function object that, taking two values of the same type than those contained in the range,
 * returns @c True if the first argument goes before the second argument in the specific
 * <i>strict weak ordering</i> it defines, and @c False otherwise.
 */
template <class ENTRY>
template <class COMPARE>
void abstract_dataset<ENTRY>::sort(COMPARE compare)
{
    // define iterator type
    //typedef typename map< std::string, std::deque<ENTRY> >::iterator it_t;
    typedef typename abstract_dataset<ENTRY>::iterator it_t;

    // for each chromosome, sort their entries, chromosomes are already
    // ordered(asc) by their insertion in the container(map)
    // it_t(defined previsouly speaks for itself) points to a node in the container(map)
    // and associated value(second) are entries contained in a vector
    for(it_t it = this->begin(); it != this->end(); ++it)
    {
        // sort each entry base on compare function
        std::sort(it->second.begin(), it->second.end(), compare);
    }
}
} /* vap namespace end */

#endif  /* ABSTRACT_DATASET_H */
