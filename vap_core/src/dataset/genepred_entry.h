 /**
 * @file   genepred_entry.h
 * @author Charles Coulombe
 *
 * @date July 10 2015 10:13
 * @version 1.1.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENEPRED_ENTRY_H
#define	GENEPRED_ENTRY_H

#include <string>
#include <vector>

namespace vap
{
/**
 * GenePred entry.
 * http://genome.ucsc.edu/FAQ/FAQformat#format9
 */
struct genepred_entry
{
    /** Feature name. */
    std::string name;

    /** Feature chromosome. */
    std::string chromosome;

    /** Feature strand. */
    char strand;

    /** Transcription start position. */
    int txStart;

    /** Transcription end position. */
    int txEnd;

    /** Coding region start. */
    int cdsStart;

    /** Coding region end. */
    int cdsEnd;

    /** Number of exon. */
    int exonsCount;

    /** Exon start positions. */
    std::vector<int> exonStarts;

    /** Exon end positions. */
    std::vector<int> exonEnds;

    /** Score. */
    int score;

    /** Feature alias. */
    std::string alias;

    /** Extra information. */
    std::string extra;

    /**
     * Constructor of a @c genepred_entry type.
     *
     * @param nme Name of gene.
     * @param chr Chromosome name.
     * @param strd + or - for strand.
     * @param txSt Transcription start position.
     * @param txEd Transcription end position.
     * @param cdsSt Coding region start.
     * @param cdsEd Coding region end.
     * @param exonCnt Number of exon.
     * @param exonSts Exon start positions.
     * @param exonEds Exon end positions.
     * @param score Score.
     * @param als Alias of gene.
     * @param xtra Gene extra information.
     */
    genepred_entry(const std::string &nme, const std::string &chr, const char strd, const int txSt, const int txEd, const int cdsSt, const int cdsEd, const int exonCnt, const std::vector<int> &exonSts, const std::vector<int> &exonEds, const int scre, const std::string &als, const std::string &xtra)
     : name(nme), chromosome(chr), strand(strd), txStart(txSt), txEnd(txEd), cdsStart(cdsSt), cdsEnd(cdsEd), exonsCount(exonCnt), exonStarts(exonSts), exonEnds(exonEds), score(scre), alias(als), extra(xtra)
    {
    }

    ~genepred_entry()
    {
    }
};
}
#endif	/* GENEPRED_ENTRY_H */
