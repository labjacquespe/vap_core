/**
 * @file   bedgraph_dataset.h
 * @author Charles Coulombe
 *
 * @date 12 November 2012, 14:19
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BEDGRAPH_DATASET_H
#define	BEDGRAPH_DATASET_H

#include <string>

#include "./abstract_dataset.h"
#include "./base_entry.h"
#include "../exception/error.h"
#include "../utils/coordinates/coordinates_utils.h"

namespace vap
{
/***/
struct bedgraph_entry : base_entry
{
    // chromosome, begin and end are inherited from base_entry

    /** Value read from the @c chromosome entry*/
    float value;

    /**
     * Constructs an entry and initializes its content.
     * @param chr chromosome
     * @param b begin
     * @param e end
     * @param val value
     */
    bedgraph_entry(const std::string &chr = "", int b = -1, int e = -1, float val = 0.0f)
    : base_entry(chr, b, e), value(val)
    {}
};

/***/
class bedgraph : public abstract_dataset<bedgraph_entry>
{
private:
    // ----------------------- attributes

    // ----------------------- methods
    // disallow evil copy
    bedgraph(const bedgraph &b);
    void operator=(const bedgraph &b);

    void _parse(util::parser_string &parser);

public:
    // ----------------------- constructors
    bedgraph(const std::string &path, const unsigned long long chunkSize);
    virtual ~ bedgraph();

    // ----------------------- accessors

    // ----------------------- modifiers

    // ----------------------- methods
    bool load();// load implementation from base dataset
};
} /* vap namespace end */

#endif	/* BED_H */

