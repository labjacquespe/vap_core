/**
 * @file   genome_dataset.h
 * @author Charles Coulombe
 *
 * @date 12 November 2012, 11:29
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENOME_DATASET_H
#define	GENOME_DATASET_H

#include <string>

#include "./base_entry.h"
#include "./abstract_dataset.h"
#include "./gtf_entry.h"
#include "./genome_entry.h"

#include "../feature/coordinates.h"
#include "../feature/block.h"
#include "../defs.h"
#include "../exception/error.h"
#include "../utils/text/compare.h"
#include "../utils/coordinates/coordinates_utils.h"
#include "../utils/gtf_utils.h"

namespace vap
{

/***/
class genome : public abstract_dataset<genome_entry>
{
private:
    // ----------------------- attributes
    annotation_coordinates_type::type _coodinatesType;
    analysis_mode::type _analysisMode;

    io::file_format _format;

    // ----------------------- methods
    // disallow evil copy
    genome(const genome &orig);
    void operator=(const genome &orig);

    bool _loadGtf();
    bool _loadGenepred();

    void _loadGtfLine(std::string &line, std::deque<gtf_entry> &gtf);
    void _loadGenepredLine(util::parser_string &parser);

    genome_entry _convertToGenomeEntry(const genepred_entry &in);

public:
    // ----------------------- constructors
    genome(const std::string &path, io::file_format format, analysis_mode::type analysisMode = analysis_mode::type::ANNOTATION, annotation_coordinates_type::type annotationCoordinatesType = annotation_coordinates_type::type::TRANSCRIPTION);
    virtual ~genome();

    // ----------------------- accessors

    // ----------------------- modifiers

    // ----------------------- methods
    bool load(); // load implementation from base dataset

    io::file_format format() const;
};

/**
 * Compare @c genome_entry @c name.
 *
 * @note comparison is case insensitive
 *
 * @param left left argument
 * @param right right argument
 *
 * @return @c True when @a left comes before @c right, @c False otherwise.
 */
inline bool genome_name_compare(const genome_entry_pointer &left, const genome_entry_pointer &right)
{
    return left.main->name < right.main->name;
}
} /* vap namespace end */

#endif	/* GENOME_DATASET_H */

