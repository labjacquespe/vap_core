/**
 * @file   bigwig_dataset.cpp
 * @author Charles Coulombe
 *
 * @date   February 18, 2014, 1:43 PM
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bigwig_dataset.h"

namespace vap
{
////////////////////////////////////////////////////////////////////////////////
//                          PRIVATE
////////////////////////////////////////////////////////////////////////////////

/**
 * Loads an entry into the dataset.
 *
 * The data is retrieved from the BigWigIterator @a it
 * and inserted in the dataset. When chromosome already exists, an entry is added
 * to it, otherwise a new entry with this chromosome is created into the dataset.
 *
 * @param it BW iterator from a particular region
 */
void bigwig::_load(const BigWigIterator &it)
{
    // we can read chunk or line, so we need to loop on all
    // buffered data

    std::string chromosome = "";
    int begin = -1, end = -1;
    float value = 0.0f;

    // read an entry
    // IMPORTANT : There's a WEIRD bug with binary data
    // and encoding that insert an hidden character to the string(theory).
    // Therefore, the string MUST be stripped by getting the c_string
    // equivalent and assigning it to the actual string.
    chromosome = (*it).getChromosome().c_str();
    begin = (*it).getStartBase();
    end = (*it).getEndBase();
    value = (*it).getWigValue();

    // insert or access chromosome, then add the entry
    operator[](chromosome).push_back(bigwig_entry(chromosome, begin, util::toClosedRange(end)/* inclusive range*/, value));
}

/**
 * Enqueues contiguous regions of coordinates from the references
 * features for further optimized query to be sent to the BW reader.
 */
void bigwig::_enqueueRegions()
{
    // Loops on all reference features, the iterator is advanced from the '_findContiguousRegion'
    // method. This loop purpose is to ensure that all RF are done and gives a starting RF while
    // ensuring no iteration past the end.
    std::deque< reference_feature >::const_iterator it = _referenceFeatures->begin();
    while(it != _referenceFeatures->end())
    {
        // enqueues a contiguous region and advance iterator
        _regions.push_back(_findContiguousRegion(it, _referenceFeatures->end()));
    }
}

/**
 * Finds a contiguous region among the reference features
 * and return it.
 *
 * Loops until a break or gap between coordinates appear among RF. The gap, for now,
 * can be of 1 base pair or more.
 * This determines a region to query from the BW reader. This region is implicitly unified
 * in case of overlapping or contained RF.
 *
 * @return contiguous region coordinates
 */
coordinates_region bigwig::_findContiguousRegion(group_feature<reference_feature>::const_iterator &first, const group_feature<reference_feature>::const_iterator &last) const
{
    std::string chr = first->chromosome();

    // Avoid negatives region coordinates for the BW queries.
    // Note : Negatives coordinates are only part of ours RF representation and
    // are not actually possible in a BW or BedGraph dataset.
    int begin = std::max(0, first->begin()),
        end = std::max(0, first->end());

    std::deque< reference_feature >::const_iterator next = first;

    // Loops until a break or gap between coordinates appear among RF. The gap, for now,
    // can be of 1 base pair or more. This determines a region to query from the
    // BW reader. This region is implicitly unified in case of overlapping or contained RF.
    // All the regions are enqueued to be sent to the BW reader later in the process.
    while(++next != last && next->chromosome() == first->chromosome())
    {
        // Check contiguousness by calculating distance between right hand side feature begin coordinate
        // and maximal end coordinate. If distance is greater than 1, there's a gap of at least 1 bp
        // between reference features. This method let us also unify overlapping reference features where
        // calculating the distance between the begin and the end coordinates will result in a negative value.
        if((next->begin() - end) > RF_GAP_THRESHOLD)
            break;

        // Only update end coordinate when the new value is greater. This helps
        // with features who are contained in a bigger RF.
        if(next->end() > end)
            end = std::max(0, next->end()); // avoid negatives coordinate
    }

    // Save current iterator state only if we are not past the end already
    // to avoid iterating past the end. Strangely, iterators can iterate
    // past the end...
    first = next;

    // Creates a contiguous region. The 'end' coordinate is transformed to
    // half-open coordinates system to be fully compatible with BW requirements.
    return coordinates_region(chr, begin, end + 1/*half-open*/);
}

/**
 * Gets the last entry of the @a chromosome node within the dataset.
 * If an entry is found, the last entry is returned, otherwise an empty
 * @c bigwig_entry is returned.
 *
 * @param chromosome Current chromosome node.
 *
 * @return Last inserted entry for the @a chromosome node if found, otherwise
 * an empty @c bigwig_entry.
 */
bigwig_entry bigwig::_lastEntry(const std::string &chromosome) const
{
    // look up the chromosome node
    abstract_dataset<bigwig_entry>::const_iterator it = find(chromosome);

    // if the chromosome node was found
    if(it != end())
        return it->second.back(); // retrieve the last entry from the dataset

    return bigwig_entry(); // empty entry
}

////////////////////////////////////////////////////////////////////////////////
//                          PUBLIC
////////////////////////////////////////////////////////////////////////////////

/**
 * Constructs a BigWig dataset.
 *
 * Initializes and opens associated file stream. Afterwards, contiguous coordinates
 * regions are enqueued for further optimized processing.
 *
 * @param path data file path
 * @param referenceFeatures list of all reference features
 * @param chunkSize load threshold
 */
bigwig::bigwig(const std::string &path, const group_feature<reference_feature> *referenceFeatures, const unsigned long long chunkSize)
: abstract_dataset<bigwig_entry>(path, chunkSize, std::ios::in | std::ios::binary), _bwReader(BBFileReader()), _referenceFeatures(referenceFeatures)
{
    // create a reader associated with the file
    _bwReader.open(path, *static_cast<std::ifstream *>(_freader->fstream()));

    // get the contiguous coordinates regions from the reference features
    _enqueueRegions();
}

/**
 * Classic destructor.
 * Closes the associated file stream.
 */
bigwig::~bigwig()
{ }

/**
 * Loads and parse file defined at @a path.
 *
 * Enqueued regions are sent to the BigWig reader which returns us
 * with the corresponding region data. We then load it in the dataset.
 *
 * @note There's as many entries that are loaded as chunk size allow it.
 */
bool bigwig::load()
{
    // instead of validation we include invalid lines as they will
    // not be mixed with valid lines due to chromosome name or
    // associated values(e.g. 0)

    uint_t count = 0;
    coordinates_region region;
    bigwig_entry lastEntry;
    BigWigIterator it;

    // loops on regions
    while(!_regions.empty())
    {
        region = _regions.front(); // retrieve first region

        // retrieve an iterator from the BW reader for this particular region
        it = _bwReader.getBigWigIterator(region.chromosome, region.begin, region.chromosome, region.end, false);

        /* Compares the last inserted dataset entry with the first entry made up
         * of the current iterator values.
         *
         * An edge case happens when the data span is greater than gap between features,
         * this causes data to fit in more than one window, which is correct, but
         * must not be queried twice and inserted twice in the dataset.
         *
         * Thus, we must check the last data returned by the prior query against
         * the first data returned by the actual query, to make sure they are not
         * the exact same one and avoid inserting it twice in the dataset.
         *
         * In the case, the data is the same, simply advance iterator.*/
        if(!it.isEnd() && region_entry_equal(coordinates_region(lastEntry.chromosome, lastEntry.begin, lastEntry.end), coordinates_region((*it).getChromosome().c_str(), (*it).getStartBase(), (*it).getEndBase() - 1)))
            ++it; // advance to the next data

        // process region in iterator range and within the chunk size.
        while(!it.isEnd() && count < _chunkSize)
        {
            _load(it); // load an entry into the dataset

            // advance iterator and increment the treated lines by 1
            ++it;
            ++count;
        }

        // If iterator range was not entirely traversed, update the first
        // region with a new starting base value : the current iterator start base
        // or in other words, the current start base from the queried region.
        // Since iterator range was not entirely traversed, this mean we have reached
        // the allowed chunk size and thus need to break to allow processing of current
        // loaded data.
        // Otherwise, the region is not needed anymore and is removed from the queue.
        if(!it.isEnd())
        {
            _regions.front().begin = it->getStartBase(); // update start base
            break; // exit while loop to allow processing current loaded data
        }
        else
        {
            _regions.pop_front(); // entire iterator range was traversed, we don't need this region anymore
            lastEntry = _lastEntry(region.chromosome); // store the last dataset entry for further comparison
        }
    }

    return count > 0; // did we load something?
}
}; /* vap namespace end */
