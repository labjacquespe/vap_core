 /**
 * @file   gtf_entry.h
 * @author Charles Coulombe
 *
 * @date July 10 2015 09:45
 * @version 1.1.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GTF_ENTRY_H
#define	GTF_ENTRY_H

#include <string>
#include <map>

namespace vap
{
/**
 * GTF entry.
 * http://genome.ucsc.edu/FAQ/FAQformat.html#format4
 */
struct gtf_entry
{
    /** The name of the sequence. Must be a chromosome or scaffold. */
    std::string chromosome;

    /** The program that generated this feature. */
    std::string source;

    /** The name of this type of feature. */
    std::string feature;

    /** The starting position of the feature in the sequence. The first base is numbered 1. */
    int start;

    /** The ending position of the feature (inclusive). */
    int end;

    /** A score between 0 and 1000. If there is no score value, value should be ".". */
    int score;

    /** Feature strand. Should be '+' or '-' or '.' if unknown. */
    char strand;

    /** If the feature is a coding exon, frame should be a number between 0-2 that represents the reading frame of the first base. If the feature is not a coding exon, the value should be '.'. */
    char frame;

    /**
     * All four features have the same two mandatory attributes at the end of the record:
     *      gene_id value;     A globally unique identifier for the genomic source of the transcript
     *      transcript_id value;     A globally unique identifier for the predicted transcript.
     */
    std::map< std::string, std::string > attributes;

    /**
     * Constructor of a @c gtf_entry type.
     *
     * @param chr The name of the sequence. Must be a chromosome or scaffold.
     * @param src The program that generated this feature.
     * @param ft The name of this type of feature.
     * @param s The starting position of the feature in the sequence. The first base is numbered 1.
     * @param e The ending position of the feature (inclusive).
     * @param sc A score between 0 and 1000.
     * @param strd Feature strand.
     * @param frme Number between 0-2 or '.'.
     * @param att Mandatory attributes : gene_id and transcript_id.
     */
    gtf_entry(const std::string &chr, const std::string &src, const std::string &ft, const int s, const int e, const int sc, const char strd, const char frme, const std::map< std::string, std::string > &att)
    : chromosome(chr), source(src), feature(ft), start(s), end(e), score(sc), strand(strd), frame(frme), attributes(att)
    {
    }

    ~gtf_entry()
    {
    }
};
}
#endif	/* GTF_ENTRY_H */

