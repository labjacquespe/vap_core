/**
 * @file   reference_entry.h
 * @author Charles Coulombe
 *
 * @date 25 August 2014, 13:12
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REFERENCE_ENTRY_H
#define	REFERENCE_ENTRY_H

namespace vap
{

/** Reference entry index in file, sequence, etc. */
class reference_entry
{
private:
    /** Index in file position. */
    unsigned int _index;

public:

    /**
     * Constructs new index entry.
     * @param idx index value
     */
    reference_entry(unsigned int idx) : _index(idx)
    {
    }

    virtual ~reference_entry()
    {
    }

    /**
     * Gets the reference entry index value.
     *
     * @return index
     */
    unsigned int index() const
    {
        return _index;
    }
};

} /* vap namespace end */

#endif	/* REFERENCE_ENTRY_H */