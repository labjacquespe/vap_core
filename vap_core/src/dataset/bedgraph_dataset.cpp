/**
 * @file   bedgraph_dataset.cpp
 * @author Charles Coulombe
 *
 * @date 12 November 2012, 14:19
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./bedgraph_dataset.h"

namespace vap
{
////////////////////////////////////////////////////////////////////////////////
//                          PRIVATE
////////////////////////////////////////////////////////////////////////////////

/**
 * Loads entries and parse buffer.
 * @param buffer
 */
void bedgraph::_parse(util::parser_string &parser)
{
    std::string chromosome = "";
    int begin = -1, end = -1;
    float value = 0.0f;

    //if buffer has characters
    if(!parser.eob())
    {
        // read an entry
        parser >> chromosome;
        parser.skip();
        parser >> begin;
        parser.skip();
        parser >> end;
        parser.skip();
        parser >> value;
        parser.ignoreUntilEndl(); // discard rest until the end of line

        // TODO : Add verification that chromosome exists in a list of chr created from
        //        the group of features, hence the clean method of the dataset is useless.
        //        Add an hashtable for the chr filter.
        //        if(contains(chromosome))

        // insert or access chromosome, then add the entry
        // The BedGraph format uses half-open coordinates system, thus the need to
        // transform to an inclusive range for our internal representation.
        // Half-Open : [1-10] -> [1-11[ -> +1
        // Closed    : [1-11[ -> [1-10] -> -1
        operator [](chromosome).push_back(bedgraph_entry(chromosome, begin, util::toClosedRange(end)/*inclusive range*/, value));
    }

    // reset position to zero because we read from the same buffer each time
    parser.resetPosition();
}
////////////////////////////////////////////////////////////////////////////////
//                          PUBLIC
////////////////////////////////////////////////////////////////////////////////

bedgraph::bedgraph(const std::string &path, const unsigned long long chunkSize)
: abstract_dataset<bedgraph_entry>(path, chunkSize)
{
}

bedgraph::~bedgraph()
{

}

/**
 * Loads and parse file defined at @a path.
 * Finally, all entries are sorted.
 */
bool bedgraph::load()
{
    std::string line = "";
    // create a reader associated with the file reader
    util::parser_string parser(&line, DEFAULT_DELIMITERS);

    // instead of validation we include invalid lines as they will
    // not be mixed with valid lines due to chromosome name or
    // assiociated values(eg 0)

    unsigned long long nlines = 0;

    // read first lines in order to extract properties(if present)
    // or discard useless line(such as "browser" information)
    for(; _freader->readline(line) && nlines < _chunkSize && !_hasHeader; ++nlines)
    {
        // if line starts with "browser" or "#", discard it
        if(util::starts_with(line, BROWSER_TAG) || util::starts_with(line, COMMENT_TAG))
            continue; // discard line

        // if line starts with "track", then extract name property
        else if(util::starts_with(line, TRACK_TAG))
            loadProperties(line);

        // no more line to parse
        else
        {
            _parse(parser);
            break; // pass to data
        }
    }

    _hasHeader = true;

    // process each other lines of the dataset
    for(; _freader->readline(line) && nlines < _chunkSize; ++nlines)
        _parse(parser);

    // sort entries by chromosomes(asc), begin(asc), end(desc)
    sort(entry_compare);
    return nlines > 0;
}
} /* vap namespace end */
