/**
 * @file   genome_dataset.cpp
 * @author Charles Coulombe
 *
 * @date 12 November 2012, 11:29
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./genome_dataset.h"

namespace vap
{
////////////////////////////////////////////////////////////////////////////////
//                          PRIVATE
////////////////////////////////////////////////////////////////////////////////

genome_entry genome::_convertToGenomeEntry(const genepred_entry &ge)
{
    int begin = -1, end = -1;
    exons_t exons;

    // transcription or coordinates
    switch(_coodinatesType)
    {
        case annotation_coordinates_type::type::TRANSCRIPTION:
            {
                begin = ge.txStart;
                end = ge.txEnd;
                break;
            }

        // read cds coordinates
        case annotation_coordinates_type::type::CODING_SEQUENCE:
            {
                begin = ge.cdsStart;
                end = ge.cdsEnd;
                break;
            }

        case annotation_coordinates_type::type::NOT_APPLICABLE:
        case annotation_coordinates_type::type::UNKNOWN:
        default:
            {
                begin = end = -1;
                break;
            }
    }

    if(_analysisMode == analysis_mode::type::EXON)
    {
        // Push exon. No need to convert to inclusive range since exon ends are already
        // converted from convertGtfToGenepred util.
        for(size_t i = 0; i < ge.exonStarts.size() && i < ge.exonEnds.size(); ++i)
            exons.push_back(exon(ge.exonStarts[i], util::toClosedRange(ge.exonEnds[i])));

        // check if we need to raise an error when exons sequence is empty.
        ERROR(!exons.empty(), MSG_EXONS_EMPTY(ge.name));
    }

    return genome_entry(ge.name, ge.chromosome, annotation_strand::toStrand(ge.strand), begin, util::toClosedRange(end), ge.exonsCount, exons, ge.score, ge.alias, ge.extra);
}

/**
 */
bool genome::_loadGtf()
{
    std::deque<gtf_entry> gtf;
    std::deque<genepred_entry> genepred;

    // instead of validation we include invalid lines as they will
    // not be mixed with valid lines due to chromosome name or
    // associated values(eg 0)

    unsigned long long nlines = 0;
    std::string line;

    // discard first line as we have no interest in it(header line)
    if(_freader->peek() == COMMENT_TAG)
    {
        _hasHeader = true;
        _freader->readline(line);
    }

    // todo : possible bug here as it is possible that not all gtf entries are grouped together. Reading in chunks might introduced incorrect read values. Check!!
    // process each other lines in the dataset
    for(; _freader->readline(line) && nlines < _chunkSize; ++nlines)
    {
        if(line.empty() || util::starts_with(line, COMMENT_TAG))
            continue;

        _loadGtfLine(line, gtf);
    }

    // Converts GTF to Genepred format.
    util::convertGtfToGenepred(gtf, genepred);

    // transfer genepred entries into the dataset and also transform them in genome entries
    for(std::deque<genepred_entry>::const_iterator entry = genepred.begin(); entry != genepred.end(); ++entry)
        operator[](entry->chromosome).push_back(_convertToGenomeEntry(*entry)); // insert or access chromosome, then add the entry

    // sort entries by chromosomes(asc), begin(asc), end(desc)
    sort(entry_compare);
    return nlines > 0;
}

/**
 * Parses GTF format line.
 */
void genome::_loadGtfLine(std::string &line, std::deque<gtf_entry> &gtf)
{
    std::string chromosome = "", source = "", feature = "", key = "", value = "";
    int start = -1, end = -1, score = 0;
    char strand = '.', frame = '\0', equal = '\0';
    std::map< std::string, std::string > attributes;

    util::parser_string parser(&line, "\t ;");

    parser >> chromosome;
    parser.skip();
    parser >> source;
    parser.skip();
    parser >> feature;
    parser.skip();
    parser >> start;
    parser.skip();
    parser >> end;
    parser.skip();
    parser >> score;
    parser.skip();
    parser >> strand;
    parser.skip();
    parser >> frame;
    parser.skip();

    // Parse key values attributes
    while(!parser.eob())
    {
        key = value = "";

        parser.skip();
        parser >> key >> equal;
        if(parser.peek() == '"')
            parser.readFromTo(value, '"', '"');
        else
            parser >> value;

        attributes[key] = value;
    }

    gtf.push_back(gtf_entry(chromosome, source, feature, start, end, score, strand, frame, attributes));
}

/**
 * Loads a Genepred format file.
 */
bool genome::_loadGenepred()
{
    std::string line;

    // create reader associated with the file reader
    util::parser_string parser(&line, DEFAULT_DELIMITERS);

    // instead of validation we include invalid lines as they will
    // not be mixed with valid lines due to chromosome name or
    // associated values(eg 0)

    unsigned long long nlines = 0;

    // discard first line as we have no interest in it(header line)
    if(_freader->peek() == COMMENT_TAG)
    {
        _hasHeader = true;
        _freader->readline(line);
    }

    // process each other lines in the dataset
    for(; _freader->readline(line); ++nlines)
        _loadGenepredLine(parser);

    // sort entries by chromosomes(asc), begin(asc), end(desc)
    sort(entry_compare);
    return nlines > 0;
}

/**
 * Parses a Genepred format line
 */
void genome::_loadGenepredLine(util::parser_string &parser)
{
    // we can read chunk or line, so we need to loop on all
    // buffered data

    std::string name = "", chr = "", exonBegins = "", exonEnds = "", alias = "_", extra = "", garbage;
    int begin = -1, end = -1, exonBegin = -1, exonEnd = -1, score = -1;
    uint_t exons_count = 0;
    exons_t exons;

    char strand = '.', comma = '\0';

    //while buffer has characters
    if(!parser.eob())
    {
        // read an entry

        // read name, chr, and strand part
        parser >> name >> util::skip >> chr >> util::skip >> strand >> util::skip;

        // transcription or coordinates
        switch(_coodinatesType)
        {
            case annotation_coordinates_type::type::TRANSCRIPTION:
                {
                    // read tx coordinates and absorbs cds as we will not use them
                    parser >> begin >> util::skip >> end >> util::skip // tx
                           >> garbage >> util::skip >> garbage >> util::skip; // cds
                    break;
                }

            // read cds coordinates
            case annotation_coordinates_type::type::CODING_SEQUENCE:
                {
                    // read cds coordinates and absorbs tx as we will not use them
                    parser >> garbage >> util::skip >> garbage >> util::skip // tx
                           >> begin >> util::skip >> end >> util::skip; // cds
                    break;
                }

            case annotation_coordinates_type::type::NOT_APPLICABLE:
            case annotation_coordinates_type::type::UNKNOWN:
            default:
                {
                    // should not happen but still advance reader so we can
                    // read everything else correctly
                    parser >> garbage >> util::skip >> garbage >> util::skip // tx
                           >> garbage >> util::skip >> garbage >> util::skip; // cds

                    break;
                }
        } /*switch*/


        // read exons coordinates
        parser >> exons_count >> util::skip >> exonBegins >> util::skip >> exonEnds >> util::skip;

        // only read exon when necessary
        if(_analysisMode == analysis_mode::type::EXON)
        {
            util::parser_string exon_begin_reader(&exonBegins, " \t\r\n,"),
                 exon_end_reader(&exonEnds, " \t\r\n,");

            // extract as much exon as possible(while we have begins and ends)
            for(uint_t i = 0; i < exons_count && !exon_begin_reader.eob() && !exon_end_reader.eob(); i++)
            {
                exon_begin_reader >> exonBegin >> comma;
                exon_end_reader >> exonEnd >> comma;

                // add exon into temporary sequence
                exons.push_back(exon(exonBegin, util::toClosedRange(exonEnd)/*inclusive range*/));
            }

            // check if we need to raise an error when exons sequence
            // is empty.
            ERROR(!exons.empty(), MSG_EXONS_EMPTY(name));

            exon_begin_reader.resetPosition();
            exon_end_reader.resetPosition();
        }

        // push extra information, current read position to the end of line
        parser.readUntilEndl(extra);
        util::cut_eol(extra);

        if(!extra.empty())
        {
            util::parser_string xreader(&extra, "\t\n\r");
            xreader >> score >> util::skip >> alias;
            extra.assign(extra.begin() + xreader.position(), extra.end());
            extra.append("\t" + util::itos(score)); // Append back the score into the extra string.
        }

        // insert or access chromosome, then add the entry
        // The GenePred format uses half-open coordinates system, thus the need to
        // transform to an inclusive range for our internal representation.
        // Half-Open : [1-10] -> [1-11[ -> +1
        // Closed    : [1-11[ -> [1-10] -> -1
        operator[](chr).push_back(genome_entry(name,
                                               chr,
                                               annotation_strand::toStrand(strand),
                                               begin,
                                               util::toClosedRange(end), /*inclusive range*/
                                               exons_count,
                                               exons,
                                               score,
                                               alias,
                                               extra));
        exons.clear();
    }

    // reset position to zero because we read from the same buffer each time
    parser.resetPosition();
}
////////////////////////////////////////////////////////////////////////////////
//                          PUBLIC
////////////////////////////////////////////////////////////////////////////////

genome::genome(const std::string &path, io::file_format format, analysis_mode::type analysisMode, annotation_coordinates_type::type annotationCoordinatesType)
: abstract_dataset<genome_entry>(path), _coodinatesType(annotationCoordinatesType), _analysisMode(analysisMode), _format(format)
{ }

genome::~genome()
{
}

/**
 * Reads the genome and sorts its entries.
 */
bool genome::load()
{
    switch(_format)
    {
        case io::file_format::GENEPRED_EXTENDED:
        case io::file_format::GENEPRED:
            return genome::_loadGenepred();
        case io::file_format::GTF:
            return genome::_loadGtf();
        case io::file_format::UNKNOWN:
        default:
            return false;
    }
}

/**
 * Gets the genome type.
 *
 * @see @c io::file_format
 *
 * @return Genome's type.
 */
io::file_format genome::format() const
{
    return _format;
}
} /* vap namespace end */
