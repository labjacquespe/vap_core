/**
 * @file   reference_coordinates.h
 * @author Charles Coulombe
 *
 * @date 12 November 2012, 14:40
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REFERENCE_COORDINATES_DATASET_H
#define	REFERENCE_COORDINATES_DATASET_H

#include <vector>
#include <string>

#include "./reference_entry.h"
#include "../feature/coordinates.h"
#include "../defs.h"
#include "../utils/type/convert.h"
#include "../filter/reference_filter_entry.h"

namespace vap
{

/**
 * Gets the coordinates of interest of a @c reference_coordinates @c coordinates member.
 *
 * @param coords Coordinates of @c reference_coordinates.
 * @param strand Strand of @c reference_coordinates.
 *
 * @return @c coordinates of interest.
 */
inline coordinates getCoordinatesOfInterest(const std::vector<int> &coords, const annotation_strand::strand strand)
{
    switch(coords.size())
    {
        case 1:
            return coordinates(coords[0], coords[0]);

        case 2:
            return coordinates(coords[0], coords[1]);

        case 3:
            if(strand == annotation_strand::strand::NEGATIVE)
                return coordinates(coords[0], coords[1]);
            else
                return coordinates(coords[1], coords[2]);

        case 4:
            return coordinates(coords[1], coords[2]);

        case 5:
            if(strand == annotation_strand::strand::NEGATIVE)
                return coordinates(coords[1], coords[2]);
            else
                return coordinates(coords[2], coords[3]);

        case 6:
            return coordinates(coords[2], coords[3]);

        default:
            break;
    }

    return coordinates(-1, -1);
}

/***/
struct reference_coordinates : public reference_entry
{
    /** feature chromosome */
    std::string chromosome;

    /** array of coordinates */
    std::vector<int> coordinates;

    /**
     * entry strand
     * @see annotation_strand
     */
    annotation_strand::strand strand;

    /** feature name
     * A @c reference_coordinates name is composed of 'chr_strand_coordinates'
     * and each field is separated by underscore. */
    std::string name;

    /** [optional] feature alias */
    std::string alias;

    /** Coordinates of the reference feature of interest */
    vap::coordinates coordinatesOfInterest;

    reference_coordinates(std::string chr = "", annotation_strand::strand strd = annotation_strand::strand::POSITIVE, std::vector<int> coords = std::vector<int>(), const std::string &als = "", const unsigned int idx = -1)
     : reference_entry(idx), chromosome(chr), coordinates(coords), strand(strd), name(""), alias(als), /*begin(coordinates.front()), end(coordinates.back()),*/ coordinatesOfInterest(getCoordinatesOfInterest(coordinates, strand))
    {
        name.reserve(50);
        name = chromosome + "_" + annotation_strand::toSymbol(strand);

        // add coordinates as part of the unique name
        for(unsigned int i = 0; i < coords.size(); ++i)
            name += "_" + util::itos(coordinates[i]);

        if(alias.empty())
            alias = "_";
    }
};

/**
 * Compares @c reference_coordinates by looking at their @c name.
 * Order is alphanumerical and case sensitive.
 *
 * @remark A @c reference_coordinates name is composed of 'chr_strand_coordinates'
 * and each field is separated by underscore.
 *
 * @param l Left @c reference_coordinates. Compared argument.
 * @param r Right @c reference_coordinates. Comparing argument.
 *
 * @return @c True if @a l name is smaller than @a r name, @c false otherwise.
 */
inline bool coordinates_name_lesser_compare(const reference_coordinates &l, const reference_coordinates &r)
{
    return l.name < r.name;
}

/**
 * Compare a @c reference_feature by looking at their respective @a chromosome, @a begin and @a end point.
 *
 * Entry will be strictly ordered by ascending values except for the @c end point which will be descending.
 * In order to have a entry to comes before another it must have:
 *      - @a left @a chromosome must come before @a right @a chromosome;
 *      - @a left @a begin point must come before @a right @a begin point;
 *      - @a right @a end point must come before @a left @a end point.
 * Ordering can be resumed to : chromosome(asc), begin(asc), end(desc).
 *
 * @note @c chromosome comparison is case sensitive
 *
 * @param l left argument(compared)
 * @param r right argument(comparing)
 *
 * @return @c True when either :
 *      - left chromosome comes before right chromosome;
 *      - left begin point comes before right begin point;
 *      - right end point comes before left end point;
 * @c False otherwise
 */
inline bool coordinates_interest_range_chromosomic_lesser_compare(const reference_coordinates &l, const reference_coordinates &r)
{
    // chromosome : true when left chromosome comes before right chromosome, false otherwise
    // begin : true when left begin point comes before right begin point, false otherwise
    // end : true when right end point comes before left end point
    // ordering : chromosome(asc), begin(asc), end(desc)

    if(l.chromosome < r.chromosome)
        return true;

    if(l.chromosome == r.chromosome)
    {
        if(l.coordinatesOfInterest.begin() < r.coordinatesOfInterest.begin())
            return true;

        else if(l.coordinatesOfInterest.begin() == r.coordinatesOfInterest.begin() && l.coordinatesOfInterest.end() > r.coordinatesOfInterest.end())
            return true;
    }

    return false;
}

/**
 * Compares @c reference_coordinates and a @c reference_filter_entry::name_entry by looking at their @c name.
 * Order is alphanumerical and case sensitive.
 *
 * @remark A @c reference_coordinates or @c reference_filter_entry::name_entry name is composed of 'chr_strand_coordinates'
 * and each field is separated by underscore.
 *
 * @param rc Left @c reference_coordinates. Compared argument.
 * @param ne Right @c reference_filter_entry::name_entry. Comparing argument.
 *
 * @return @c True if @a l name is smaller than @a r name, @c false otherwise.
 */
inline bool coordinates_filter_entry_name_lesser_compare(const reference_coordinates &rc, const reference_filter_entry::name_entry &ne)
{
    return rc.name < ne.name;
}

/**
 * Compares @c reference_coordinates and a @c reference_filter_entry::name_entry by looking at their @c name.
 * Order is alphanumerical and case sensitive.
 *
 * @remark A @c reference_coordinates or @c reference_filter_entry::name_entry name is composed of 'chr_strand_coordinates'
 * and each field is separated by underscore.
 *
 * @param rc Left @c reference_coordinates. Compared argument.
 * @param ne Right @c reference_filter_entry::name_entry. Comparing argument.
 *
 * @return @c True if @a l name is greater than @a r name, @c false otherwise.
 */
inline bool coordinates_filter_entry_name_greater_compare(const reference_coordinates &rc, const reference_filter_entry::name_entry &ne)
{
    return rc.name > ne.name;
}

/**
 * Compares @c reference_coordinates and a @c reference_filter_entry::name_entry by looking at their @c coordinates.
 * Order is exclusive, range does not allow any overlap. Meaning that @a rc end must be smaller than @a re begin.
 *
 * @param rc Left @c reference_coordinates. Compared argument.
 * @param re Right @c reference_filter_entry::name_entry. Comparing argument.
 *
 * @return @c True if :
 *  - @a rc chromosome is smaller than @a re chromosome
 *  - chromosomes are equal and @a rc coordinates of interest end is smaller than @a re begin
 *  @c False otherwise.
 */
inline bool coordinates_interest_filter_entry_range_lesser_compare(const reference_coordinates &rc, const reference_filter_entry::range_entry &re)
{
    if(rc.chromosome < re.chromosome)
        return true;

    if(rc.chromosome == re.chromosome)
    {
        if(rc.coordinatesOfInterest.end() < re.begin)
            return true;
    }

    return false;
}

/**
 * Compares @c reference_coordinates and a @c reference_filter_entry::name_entry by looking at their @c coordinates.
 * Order is exclusive, range does not allow any overlap. Meaning that @a rc end must be smaller than @a re begin.
 *
 * @param rc Left @c reference_coordinates. Compared argument.
 * @param re Right @c reference_filter_entry::name_entry. Comparing argument.
 *
 * @return @c True if :
 *  - @a rc chromosome is greater than @a re chromosome
 *  - chromosomes are equal and @a rc coordinates of interest begin is greater than @a re begin
 *  @c False otherwise.
 */
inline bool coordinates_interest_filter_entry_range_greater_compare(const reference_coordinates &rc, const reference_filter_entry::range_entry &re)
{

    if(rc.chromosome > re.chromosome)
        return true;

    if(rc.chromosome == re.chromosome)
    {
        if(rc.coordinatesOfInterest.begin() > re.end)
            return true;
    }

    return false;
}

//inline bool coordinates_interest_range_lesser_compare(const reference_coordinates &l, const reference_coordinates &r)
//{
//    if(l.chromosome < r.chromosome)
//        return true;
//
//    if(l.chromosome == r.chromosome)
//    {
//        if(l.coordinatesOfInterest.end < r.coordinatesOfInterest.begin) // TODO : Feature of interest coordinates
//            return true;
//    }
//
//    return false;
//}
//
//inline bool coordinates_interest_range_greater_compare(const reference_coordinates &l, const reference_coordinates &r)
//{
//    if(l.chromosome > r.chromosome)
//        return true;
//
//    if(l.chromosome == r.chromosome)
//    {
//        if(l.coordinatesOfInterest.begin > r.coordinatesOfInterest.end) // TODO : Feature of interest coordinates
//            return true;
//    }
//
//    return false;
//}
} /* vap namespace end */

#endif	/* REFERENCE_COORDINATES_DATASET_H */

