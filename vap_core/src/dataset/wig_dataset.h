/**
 * @file   wig_dataset.h
 * @author Charles Coulombe
 *
 * @date 6 January 2013, 21:43
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIG_DATASET_H
#define	WIG_DATASET_H

#include "./abstract_dataset.h"
#include "./base_entry.h"
#include "../exception/error.h"
#include "../utils/coordinates/coordinates_utils.h"

namespace vap
{
/***/
struct wig_entry : base_entry
{
    // chromosome, begin and end are inherited from base_entry

    /** Value read from the @c chromosome entry*/
    float value;

    /**
     * Constructs an entry and initializes its content.
     * @param chr chromosome
     * @param b begin
     * @param e end
     * @param val value
     */
    wig_entry(const std::string &chr = "", int b = -1, int e = -1, float val = 0.0f)
    : base_entry(chr, b, e), value(val)
    {}
};

class wig : public abstract_dataset<wig_entry>
{
  private:
    // ----------------------- attributes

    enum wig_type
    {
        VARIABLE_STEP = 0,
        FIXED_STEP
    };

    wig_type _type;

    // ----------------------- methods
    // disallow evil copy
    wig(const wig &orig);
    void operator=(const wig &orig);

    void _loadVariableStep(util::parser_string &parser);
    void _loadFixedStep(util::parser_string &parser);

  public:
    // ----------------------- constructors
    wig(const std::string &path, const unsigned long long chunkSize);
    virtual ~wig();

    // ----------------------- accessors

    // ----------------------- modifiers

    // ----------------------- methods
    bool load();
};
} /* vap namespace end */

#endif	/* WIG_DATASET_H */

