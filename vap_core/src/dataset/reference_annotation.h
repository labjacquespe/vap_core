/**
 * @file   reference_annotation.h
 * @author Charles Coulombe
 *
 * @date 21 December 2012, 15:28
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REFERENCE_ANNOTATION_H
#define	REFERENCE_ANNOTATION_H

#include <string>

#include "./reference_entry.h"

namespace vap
{

struct reference_annotation : public reference_entry
{
    /** annotation name */
    std::string name;

    /**
     * Constructs a @c reference_annotation object.
     *
     * @param nme annotation name
     * @param index annotation index
     */
    reference_annotation(const std::string nme = "", const unsigned int index = -1) : reference_entry(index), name(nme)
    {
    }
};

/**
 * Compare a @c reference_annoation by looking at their respective @a name.
 * Entry will be stricly ordered by ascending values.
 *
 * @note @c name() comparison is case insensitive
 *
 * @param left left argument(compared)
 * @param right right argument(comparing)
 *
 * @return @c True when either @a left comes before @a right, @c False otherwise.
 */
inline bool annotations_name_lesser_compare(const reference_annotation &left, const reference_annotation &right)
{
    return left.name < right.name;
}

/**
 * Compares @c reference_annotation by looking at their @c name.
 * Order is alphanumerical and case sensitive.
 *
 * @param ra Left @c reference_annotation. Comparing argument.
 * @param ne Right @c reference_filter_entry::name_entry. Compared argument.
 *
 * @return @c True if @a ra name is smaller than @a re name, @c false otherwise.
 */
inline bool annotations_reference_filter_name_lesser_compare(const reference_annotation &ra, const reference_filter_entry::name_entry &ne)
{
    return ra.name < ne.name;
}

/**
 * Compares @c reference_annotation by looking at their @c name.
 * Order is alphanumerical and case sensitive.
 *
 * @param ra Left @c reference_annotation. Comparing argument.
 * @param ne Right @c reference_filter_entry::name_entry. Compared argument.
 *
 * @return @c True if @a ra name is greater than @a re name, @c false otherwise.
 */
inline bool annotations_reference_filter_name_greater_compare(const reference_annotation &ra, const reference_filter_entry::name_entry &ne)
{
    return ra.name > ne.name;
}

///**
// *
// * @param ra
// * @param re
// * @return
// */
//inline bool annotations_reference_filter_range_lesser_compare(const reference_annotation &ra, const reference_filter_entry::range_entry &re)
//{
//    return false;
//}
//
///**
// *
// * @param ra
// * @param re
// * @return
// */
//inline bool annotations_reference_filter_range_greater_compare(const reference_annotation &ra, const reference_filter_entry::range_entry &re)
//{
//    return false;
//}
} /* vap namespace end */

#endif	/* REFERENCE_ANNOTATION_H */

