/**
 * @file   base_entry.h
 * @author Charles Coulombe
 *
 * @date 1 December 2012, 22:08
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BASE_ENTRY_H
#define	BASE_ENTRY_H

namespace vap
{
/** base entry for datasets */
struct base_entry
{
    /** feature chromosome */
    std::string chromosome;

    /**
     * Coding sequence or transcription begin point
     * @see analysis_mode
     */
    int begin;

    /**
     * Coding sequence or transcription end point
     * @see analysis_mode
     */
    int end;

    base_entry(const std::string &chr, int b, int e)
    : chromosome(chr), begin(b), end(e)
    {}

    virtual ~base_entry(){}
};

/**
 * Compare an entry by looking at their respective @a chromosome, @a begin and @a end point.
 * Entry will be stricly ordered by ascending values except for the @c end point which will be descending.
 * In order to have a entry to comes before another it must have:
 *      - @a left @a chromosome must come before @a right @a chromosome;
 *      - @a left @a begin point must come before @a right @a begin point;
 *      - @a right @a end point must come before @a left @a end point.
 * Ordering can be resume to : chromosome(asc), begin(asc), end(desc).
 * @note @c chromosome comparison is case insensitive
 * @param left left argument(compared)
 * @param right right argument(comparing)
 * @return @c True when either :
 *      - left chromosome comes before right chromosome;
 *      - left begin point comes before right begin point;
 *      - right end point comes before left end point;
 * @c False otherwise
 */
inline bool entry_compare(const base_entry &left, const base_entry &right)
{
    // chromosome : true when left chromosome comes before right chromosome, false otherwise
    // begin : true when left begin point comes before right begin point, false otherwise
    // end : true when right end point comes before left end point
    // ordering : chromosome(asc), begin(asc), end(desc)

    if(left.chromosome < right.chromosome)
        return true;

    if(left.chromosome == right.chromosome)
    {
        if(left.begin < right.begin)
            return true;

        else if(left.begin == right.begin && left.end > right.end)
            return true;
    }

    return false;
}
} /* vap namespace end */

#endif	/* BASE_ENTRY_H */

