/**
 * @file   genome_entry.h
 * @author Charles Coulombe
 *
 * @date 12 November 2012, 11:29
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENOME_ENTRY_H
#define	GENOME_ENTRY_H

#include "../defs.h"
#include "./base_entry.h"
#include "../feature/block.h"

namespace vap
{
/**
 * Genome entry definition.
 */
struct genome_entry : base_entry
{
    /** feature name*/
    std::string name;

    /**feature strand */
    annotation_strand::strand strand;

    /** number of exons */
    int exons_count;

    /**
     * Exons sequence
     * @see analysis_mode
     */
    exons_t exons;

    /** score */
    int score;

    /** feature alias */
    std::string alias;

    /** extra information */
    std::string extra;

    /**
     * Constructs a @c genome_entry and initializes its content.
     *
     * @param nam name
     * @param chr chromosome
     * @param strd strand
     * @param b begin
     * @param e end
     * @param ex_count number of exons
     * @param ex list of exons
     * @param s score
     * @param al alias
     * @param xtra extra information
     */
    genome_entry(const std::string &nam = "",
                 const std::string &chr = "",
                 annotation_strand::strand strd = annotation_strand::strand::POSITIVE,
                 int b = -1,
                 int e = -1,
                 int ex_count = 0,
                 exons_t ex = exons_t(),
                 int s = -1,
                 const std::string &al = "",
                 const std::string xtra = "")
    : base_entry(chr, b, e), name(nam), strand(strd), exons_count(ex_count), exons(ex), score(s), alias(al), extra(xtra)
    { }
};

struct genome_entry_pointer
{
    const genome_entry *prior;
    const genome_entry *main;
    const genome_entry *next;

    genome_entry_pointer(const genome_entry *pr, const genome_entry *mn, const genome_entry * nt)
    : prior(pr), main(mn), next(nt)
    { }
};
}

#endif	/* GENOME_ENTRY_H */
