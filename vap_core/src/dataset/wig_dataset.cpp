/**
 * @file   wig_dataset.cpp
 * @author Charles Coulombe
 *
 * @date 6 January 2013, 21:43
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./wig_dataset.h"

namespace vap
{
////////////////////////////////////////////////////////////////////////////////
//                          PRIVATE
////////////////////////////////////////////////////////////////////////////////

/**
 * Loads entries and parse buffer of a "variable step" format wiggle.
 * <p>
 *   variableStep  chrom=chrN  [span=windowSize]
 *   chromStartA  dataValueA
 *   chromStartB  dataValueB
 *   ... etc ...  ... etc ...
 * </p>
 * @param buffer
 */
void wig::_loadVariableStep(util::parser_string &parser)
{
    // we can read chunk or line, so we need to loop on all
    // buffered data

    static std::string chromosome("", 100);
    std::string field("", 100), line("", 100);
    util::parser_string tokenReader(&line, " \t=\n\r");

    int begin = -1;
    static uint_t span = 1; // by default UCSC set the span to 1, http://genome.ucsc.edu/goldenPath/help/wiggle.html
    float value = 0.0f;
    char equal;

    //while buffer has characters
    if(!parser.eob())
    {
        parser.readUntilEndl(line);

        if(util::starts_with_digit(line)) // tests first characters
        {
            // data line has been read
            tokenReader >> begin; // parse begin coordinate
            tokenReader.skip(); // skip delimiters
            tokenReader >> value; // parse value
            tokenReader.ignoreUntilEndl(); // discard the rest of the line

            // insert or access chromosome, then add the entry
            // The WIG format uses closed coordinates system.
            operator [](chromosome).push_back(wig_entry(chromosome, util::toZeroBase(begin), util::toZeroBase(begin + span/*inclusive range*/), value));
        }
        else if(util::starts_with(line, "variableStep")) // declaraction line has been read
        {
            std::string spanField;

            // advance reader beyond "variableStep" token, ready to read next field
            tokenReader.advance(12);

            // read declaration line :
            //   variableStep  chrom=chrN  [span=windowSize]
            tokenReader.skip(); // skip delimiters
            tokenReader >> field >> equal >> chromosome;
            tokenReader.readUntilEndl(spanField);

            if(!spanField.empty())
            {
                util::parser_string spanReader(&spanField, " \t=\n\r");
                spanReader.skip(); // skip delimiters
                spanReader >> field >> equal >> span;
                // rest of line is automatically discarded
            }
        }

        // reset position to zero because we read from the same buffer each time
        tokenReader.resetPosition();
    }

    // reset position to zero because we read from the same buffer each time
    parser.resetPosition();
}

/**
 * Loads entries and parse buffer of a "fixed step" format wiggle.
 * <p>
 *   fixedStep  chrom=chrN  start=position  step=stepInterval  [span=windowSize]
 *   dataValue1
 *   dataValue2
 *    ... etc ...
 * </p>
 * @param buffer
 */
void wig::_loadFixedStep(util::parser_string &parser)
{
    // we can read chunk or line, so we need to loop on all
    // buffered data

    static std::string chromosome("", 100);
    std::string field("", 100), line("", 100);
    util::parser_string tokenReader(&line, " \t=\n\r");

    static int begin = -1;
    static uint_t step = 1; // by default UCSC set the step to 1, http://genome.ucsc.edu/goldenPath/help/wiggle.html
    static uint_t span = 1; // by default UCSC set the span to 1, http://genome.ucsc.edu/goldenPath/help/wiggle.html
    float value = 0.0f;
    char equal;

    //while buffer has characters
    if(!parser.eob())
    {
        // read entire line
        parser.readUntilEndl(line);

        if(util::starts_with_digit(line)) // tests 2 first characters
        {
            // data line has been read, parse it
            tokenReader >> value;
            tokenReader.ignoreUntilEndl(); // discard rest of line

            // insert or access chromosome, then add the entry
            // The WIG format uses half-open coordinates system, thus the need to
            // transform to an inclusive range for our internal representation.
            // Half-Open : [1-10] -> [1-11[ -> +1
            // Closed    : [1-11[ -> [1-10] -> -1
            operator [](chromosome).push_back(wig_entry(chromosome, util::toZeroBase(begin), util::toZeroBase(begin + span/*inclusive range*/), value));

            // increase chromosome begin point
            begin += step;
        }
        else if(util::starts_with(line, "fixedStep")) // declaraction line has been read
        {
            std::string spanField = "";

            // advance reader beyond "fixedStep" token, ready to read next field
            tokenReader.advance(9);

            // read declaration line :
            //      fixedStep  chrom=chrN  start=position  step=stepInterval  [span=windowSize]
            tokenReader.skip(); // skip delimiters
            tokenReader >> field >> equal >> chromosome; // parse chromosome info
            tokenReader.skip(); // skip delimiters
            tokenReader >> field >> equal >> begin; // parse begin info
            tokenReader.skip(); // skip delimiters
            tokenReader >> field >> equal >> step; // parse step info

            tokenReader.readUntilEndl(spanField);

            if(!spanField.empty())
            {
                util::parser_string spanReader(&spanField, " \t=\n\r");
                spanReader.skip(); // skip delimiters
                spanReader >> field >> equal >> span;
                // rest of line is automatically discarded
            }
        }

        // other kind of line such as comments or browse tag are automatically discarded

        // reset position to zero because we read from the same buffer each time
        tokenReader.resetPosition();
    }

    // reset position to zero because we read from the same buffer each time
    parser.resetPosition();
}

////////////////////////////////////////////////////////////////////////////////
//                          PUBLIC
////////////////////////////////////////////////////////////////////////////////

wig::wig(const std::string &path, const unsigned long long chunkSize)
: abstract_dataset<wig_entry>(path, chunkSize), _type(wig_type())
{
}

wig::~wig()
{
}

/**
 * Loads and parse file defined at @a path.
 * Finally, all entries are sorted.
 */
bool wig::load()
{
    std::string line;

    // create reader associated with the file reader
    util::parser_string parser(&line, "\n\r");

    // instead of validation we include invalid lines as they will
    // not be mixed with valid lines due to chromosome name or
    // assiociated values(eg 0)

    unsigned long long nlines = 0;

    // read first lines in order to extract properties(if present)
    // or discard useless line(such as "browser" information
    for(; _freader->readline(line) && nlines < _chunkSize && !_hasHeader; ++nlines)
    {
        // if line starts with "browser" or "#", discard it
        if(util::starts_with(line, BROWSER_TAG) || util::starts_with(line, COMMENT_TAG))
            continue; // discard line

            // if line starts with "track", then extract name property
        else if(util::starts_with(line, TRACK_TAG))
            loadProperties(line);

            // no more line to parse
        else
        {
            // do we have a step tag
            if(util::starts_with(line, "variableStep"))
            {
                _type = VARIABLE_STEP;
                _loadVariableStep(parser);
                break; // pass to data
            }
            else if(util::starts_with(line, "fixedStep"))
            {
                _type = FIXED_STEP;
                _loadFixedStep(parser);
                break; // pass to data
            }
        }
    }

    _hasHeader = true;

    switch(_type)
    {
        case VARIABLE_STEP:
        {
            // process each other lines of the dataset
            for(; _freader->readline(line) && nlines < _chunkSize; ++nlines)
                _loadVariableStep(parser);

            break;
        }

        case FIXED_STEP:
        {
            // process each other lines of the dataset
            for(; _freader->readline(line) && nlines < _chunkSize; ++nlines)
                _loadFixedStep(parser);

            break;
        }

        default:
            break;
    }

    // sort entries by chromosomes(asc), begin(asc), end(desc)
    sort(entry_compare);
    return nlines > 0;
}

} /* vap namespace end */
