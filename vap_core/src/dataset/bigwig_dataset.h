/**
 * @file   bigwig_dataset.h
 * @author Charles Coulombe
 *
 * @date   February 18, 2014, 1:43 PM
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BIGWIG_DATASET_H
#define	BIGWIG_DATASET_H

#include <stdint.h>
#include <fstream>
#include <deque>

#include "abstract_dataset.h"
#include "base_entry.h"
#include "../exception/error.h"
#include "../utils/bwreader/BBFileReader.h"
#include "../utils/bwreader/BigWigIterator.h"
#include "../feature/reference_feature.h"
#include "../feature/group_feature.h"
#include "../utils/coordinates/coordinates_utils.h"

namespace vap
{

/** Bigwig magic number. */
//static uint32_t BIGWIG_MAGIC_LTH = 0x888FFC26;

/**
 * Represents a BigWig dataset entry.
 */
struct bigwig_entry : base_entry
{
    // chromosome, begin and end are inherited from base_entry

    /** Value read from the @c chromosome entry*/
    float value;

    /**
     * Constructs an entry and initializes its content.
     *
     * @param chr chromosome
     * @param b begin
     * @param e end
     * @param val value
     */
    bigwig_entry(const std::string &chr = "", int b = -1, int e = -1, float val = 0.0f)
    : base_entry(chr, b, e), value(val)
    {}
};

/**
 * Represents a contiguous coordinates region from the reference features.
 */
struct coordinates_region
{
    /** Region's chromosome.*/
    std::string chromosome;
    /** Region's start base.*/
    int begin;
    /** Region's end base.*/
    int end;

    /**
     * Constructs a coordinates region.

     * @param chr chromosome
     * @param b start base
     * @param e end basee
     */
    coordinates_region(std::string chr = "", int b = -1, int e = -1)
    : chromosome(chr), begin(b), end(e)
    {}
};

class bigwig : public abstract_dataset<bigwig_entry>
{
private:
    // ----------------------- attributes
    BBFileReader _bwReader;

    const group_feature<reference_feature> *_referenceFeatures;
    std::deque< coordinates_region > _regions;

    // ----------------------- methods
    // disallow evil copy
    bigwig(const bigwig &b);
    void operator=(const bigwig &b);

    void _load(const BigWigIterator &it);
    void _enqueueRegions();
    coordinates_region _findContiguousRegion(group_feature<reference_feature>::const_iterator &beginIt, const group_feature<reference_feature>::const_iterator &endIt) const;
    bigwig_entry _lastEntry(const std::string &chromosome) const;

public:
    // ----------------------- constructors
    bigwig(const std::string &path, const group_feature<reference_feature> *referenceFeatures, const unsigned long long chunkSize);
    virtual ~bigwig();

    // ----------------------- methods
    bool load(); // load implementation from base dataset
};

/**
 * Compares two @c coordinates_region entry for strict equality.
 * All fields must be equal.
 *
 * @param left left coordinates region
 * @param right right coordinates region
 *
 * @return @c True when all fields are equal, @c False otherwise.
 */
inline bool region_entry_equal(const coordinates_region &left, const coordinates_region &right)
{
    return left.chromosome == right.chromosome && left.begin == right.begin && left.end == right.end;
}
} /* vap namespace end */
#endif	/* BIGWIG_DATASET_H */

