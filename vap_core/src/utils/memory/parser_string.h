/**
 * @file   parser_string.h
 * @author Charles Coulombe
 *
 * @date 3 December 2012, 20:37
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013,2014 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARSER_STRING_H
#define PARSER_STRING_H

#include <string>
#include <cstdlib>
#include <string.h>
#include <fstream>
#include <vector>
#include <map>
#include "../type/convert.h"

namespace vap
{
namespace util
{
namespace internal
{

/** Delimiters wrapper. */
struct _delimiters
{
    std::string delimiters;

    _delimiters(const std::string &del)
        : delimiters(del)
    {
    }
};

}

/**
 * @brief @c std::string parser.
 *
 * Parses a buffer with specific delimiters.
 * This reader makes all read of every characters explicit.
 *
 * @warning This reader does not consider raw data as c-string with null terminated character
 * so make sure you read end of line or any null terminated character.
 *
 * @pre Buffer type must be @c std::string
 *
 * @note Using any other type of buffer will result in impredictible behavior.
 */

class parser_string
{
private:

    /** Pointer to raw buffer. */
    const std::string *_buffer;

    /** Tokens delimiters. */
    std::string _delimiters;

    /** Parser position. */
    size_t _position;

    // disallow evil copy
    parser_string(const parser_string &source)
        : _buffer(source._buffer), _delimiters(source._delimiters), _position(source._position)
    {
    }

    // disallow evil copy
    parser_string &operator=(const parser_string &source)
    {
        if(&source != this)
        {
            _buffer = source._buffer;
            _delimiters = source._delimiters;
            _position = source._position;
        }

        return *this;
    }

    /**
     * Gets the span to the next available token.
     *
     * @note Span reaches the next available token or end of buffer.
     *
     * @return offset before one of @a delimiters or size of buffer if no delimiters is found.
     */
    size_t _span(const std::string &delimiters) const
    {
        return strcspn(&_buffer->operator[](_position), delimiters.c_str());
    }

    /**
     * Consumes end-of-lines.
     */
    void _consumeEOL(size_t &offset)
    {
        int unix_eol = 0, mac_eol = 0;

        // are we on an EOL?
        // peek character if '\n' or '\r' advance one more but do not eat more than the actual EOL
        // it is possible to find EOL character at the beginning of a line...thus we want to keep it
        while((unix_eol < 1 || mac_eol < 1) && (_buffer->c_str()[_position + offset] == '\n' || _buffer->c_str()[_position + offset] == '\r'))
        {
            // at most there is 2 EOL characters(like Windows systems)
            switch( *&_buffer->operator[](_position + offset))
            {
                case '\n':
                    unix_eol++;
                    break;
                case '\r':
                    mac_eol++;
                    break;
                default:
                    break;
            }

            offset++;
        }
    }

public:

    /** A view(range) on a string. Always prefer, if possible, a string view instead of a string to avoid useless copies. */
    typedef std::pair<std::string::const_iterator, std::string::const_iterator> string_view;

    /**
     * Constructs a @c parser_string and initializes its content.
     *
     * @note By default, the delimiter used is @c '\\n' (end of line).
     *
     * @param buffer @c std::string container to read from
     * @param delimiters characters to be used as delimiters of tokens among the buffer
     */
    parser_string(const std::string *buffer, const std::string &delimiters = "\n")
        : _buffer(buffer), _delimiters(delimiters), _position(0)
    {
    }

    /**
     * Destructs this @c parser_string and reset its content.
     */
    ~parser_string()
    {
        // clear attributes
        _buffer = 0;
    }

    /**
     * Checks if the end of buffer is reached, thus the end of content.
     *
     * @return @c True buffer is exhausted, @c False otherwise
     */
    bool eob() const
    {
        return _position >= _buffer->size();
    }

    /**
     * Gets the current read position.
     *
     * @return read position
     */
    size_t position() const
    {
        return _position;
    }

    /**
     * Advances forward or backward read position.
     *
     * @param n number of position
     */
    void advance(int n)
    {
        _position += n;

        // check upper bound
        if(_position > _buffer->size())
            _position = _buffer->size();
    }

    /**
     * Resets read position to zero.
     */
    void resetPosition()
    {
        _position = 0;
    }

    /**
     * Parses a @c std::string and advance read position beyond read element.
     * If buffer is exhausted, nothing is done.
     *
     * @param out @c std::string read value
     *
     * @return *this
     */
    parser_string &operator>>(std::string &out)
    {
        if(!eob())
        {
            // get span for next available token
            size_t offset = _span(_delimiters);

            // assign content
            out.assign(&_buffer->c_str()[_position], &_buffer->c_str()[_position + offset]);

            // advance read position beyond token
            _position += offset;
        }

        return *this;
    }

    /**
     * Parses a @c std::string and advance read position beyond read element.
     * If buffer is exhausted, nothing is done.
     *
     * @param view string view
     *
     * @return *this
     */
    parser_string &operator>>(string_view &view)
    {
        if(!eob())
        {
            // get span for next available token
            size_t offset = _span(_delimiters);

            // assign content
            // view.first = std::string::const_iterator(&_buffer->c_str()[_position]);
            // view.second = std::string::const_iterator(&_buffer->c_str()[_position + offset]);

            view.first = std::string::const_iterator(_buffer->begin() + _position);
            view.second = std::string::const_iterator(_buffer->begin() + _position + offset);

            // advance read position beyond token
            _position += offset;
        }

        return *this;
    }

    /**
     * Reads a @c char and advance read position beyond read element.
     * If buffer is exhausted, nothing is done.
     *
     * @param out read value
     *
     * @return *this
     */
    parser_string &operator>>(char &out)
    {
        // only advance when we are not at the end of buffer.
        // because char can eat everything,
        // particularly '\0' and garbage characters, thus we need
        // to avoid advancing position on garbage character

        if(!eob())
        {
            // copy character
            out = * &_buffer->operator[](_position);

            // advance read position beyond token
            _position += 1;
        }

        return *this;
    }

    /**
     * Reads a @c int and advance read position beyond read element.
     * If buffer is exhausted, nothing is done.
     *
     * @param out read value
     *
     * @return *this
     */
    parser_string &operator>>(int &out)
    {
        if(!eob())
        {
            // get span for next available token
            size_t offset = _span(_delimiters);

            // read integer
            out = stoi(&_buffer->operator[](_position));

            // advance read position beyond token
            _position += offset;
        }
        return *this;
    }

    /**
     * Reads a <c>unsigned int</c> and advance read position beyond read element.
     * If buffer is exhausted, nothing is done.
     *
     * @param out read value
     *
     * @return *this
     */
    parser_string &operator>>(unsigned int &out)
    {
        if(!eob())
        {
            // get span for next available token
            size_t offset = _span(_delimiters);

            // read unsigned integer
            out = stoui(&_buffer->operator[](_position));

            // advance read position beyond token
            _position += offset;
        }
        return *this;
    }

    /**
     * Reads a @c float and advance read position beyond read element.
     * If buffer is exhausted, nothing is done.
     *
     * @param out read value
     *
     * @return *this
     */
    parser_string &operator>>(float &out)
    {
        if(!eob())
        {
            // get span for next available token
            size_t offset = _span(_delimiters);

            // read unsigned integer
            out = stof(&_buffer->operator[](_position));

            // advance read position beyond token
            _position += offset;
        }
        return *this;
    }

    /**
     * Reads a @c double and advance read position beyond read element.
     * If buffer is exhausted, nothing is done.
     *
     * @param out read value
     *
     * @return *this
     */
    parser_string &operator>>(double &out)
    {
        if(!eob())
        {
            // get span for next available token
            size_t offset = _span(_delimiters);

            // read unsigned integer
            out = stod(&_buffer->operator[](_position));

            // advance read position beyond token
            _position += offset;
        }
        return *this;
    }

    /**
     * Reads a @c parser_string function pointer.
     *
     * @param f @c parser_string function(void)
     *
     * @return *this
     */
    parser_string &operator>>(parser_string &f(parser_string &))
    {
        return f(*this);
    }

    /**
     * Ignores characters.
     * The extraction ends when @a n characters have been discarded or
     * when the character @a delimiter is found, whichever comes first.
     * In the latter case, the @a delimiter character itself is also extracted.
     *
     * @note @a delimiter character is by default @c '\\n'
     *
     * @param n number of characters to ignore
     * @param delimiter character
     */
    void ignore(size_t n, char delimiter = '\n')
    {
        if(eob())
            return;

        // get span for next token
        size_t offset = _span(std::string(1, delimiter));

        // advance to the minimal offset, beyond delimiter
        advance(static_cast<int>(std::min(offset + 1, n)));
    }

    /**
     * Ignores rest of line.
     * The extraction ends when @c EOL is found.
     * In the latter case, the @c EOL characters themselves are also extracted.
     */
    void ignoreUntilEndl()
    {
        if(eob())
            return;

        // get span for next token, this also include the prior \r from Windows system
        size_t offset = _span("\n\r");

        _consumeEOL(offset);

        // advance to offset, positioned on character beyond last EOL delimiter
        advance(static_cast<int>(offset));
    }

    /**
     * Reads rest of line.
     * The extraction ends when @c EOL is found.
     * In the latter case, the @c EOL characters themselves are also extracted.
     *
     * @param out read value
     */
    void readUntilEndl(std::string &out)
    {
        if(eob())
            return;

        // get span for next token(Unix style), this also include the prior \r from Windows system
        size_t offset = _span(std::string("\n\r"));

        _consumeEOL(offset);

        // assign content
        out.assign(&_buffer->c_str()[_position], &_buffer->c_str()[_position + offset]);

        // advance read position beyond token
        advance(static_cast<int>(offset));
    }

    /**
     * Reads characters until @a delimiter is found.
     * In the latter case, the @a delimiter character is also read.
     *
     * @param out read value
     * @param delimiter a character in the buffer
     */
    void readUntil(std::string &out, char delimiter)
    {
        if(eob())
            return;

        // get span for next token
        size_t offset = _span(std::string(1, delimiter)) + 1;

        // assign content
        out.assign(&_buffer->c_str()[_position], &_buffer->c_str()[_position + offset]);

        // advance read position beyond token
        advance(static_cast<int>(offset));
    }

    /**
     * Reads characters between the characters @a from and @a to.
     *
     * @note The @a from and @a to characters are also read.
     *
     * @param out read value
     * @param from left character
     * @param to right character
     */
    void readFromTo(std::string &out, char from, char to)
    {
        if(eob())
            return;

        // get span for next token
        size_t offset = _span(std::string(1, from)) + 1;

        // ignores prior characters from the starting character including it
        advance(static_cast<int>(offset));

        // get span for next token excluding ending character
        offset = _span(std::string(1, to));

        // assign content
        out.assign(&_buffer->c_str()[_position], &_buffer->c_str()[_position + offset]);

        // advance read position beyond token
        advance(static_cast<int>(offset + 1));
    }

    /**
     * Skips delimiters characters until a non delimiter character is found.
     * In the latter case, the character is not read.
     * This effectively moves position of the reader beyond skipped characters.
     *
     * @note delimiters are the @c parser_string token delimiters.
     */
    void skip()
    {
        skip(_delimiters);
    }

    /**
     * Skips characters characters until a non delimiter character is found.
     * In the latter case, the character is not read.
     * This effectively moves position beyond skipped characters.
     *
     * @param characters characters to ignore
     */
    void skip(const std::string &characters)
    {
        if(eob())
            return;

        size_t pos = _buffer->find_first_not_of(characters, _position);

        // advance read position beyond token
        advance(static_cast<int>(pos - _position));
    }

    /**
     * Parse a key-value.
     *
     * Key-value must have the following format : key(a delimiter from the buffer)["]value["].
     */
    void keyvalue(std::string &key, std::string &value)
    {
        if(eob()) return;

        char equal = '\0';

        skip();
        *this >> key >> equal;
        if(peek() == '"')
            readFromTo(value, '"', '"');
        else
            *this >> value;
    }

    /**
     * Parse a key-values string.
     *
     * Key-value must have the following format : key=["]value["].
     */
    void keyvalue(std::vector< std::pair< std::string, std::string > > &out)
    {
        std::string key = "", value = "";

        // Consume all string
        while(!eob())
        {
            keyvalue(key, value);
            out.push_back(std::make_pair(key, value));

            key = "";
            value = "";
        }
    }

    /**
     * Parse a key-values string.
     *
     * Key-value must have the following format : key=["]value["].
     */
    void keyvalue(std::map< std::string, std::string > &out)
    {
        std::string key = "", value = "";

        // Consume all string
        while(!eob())
        {
            keyvalue(key, value);
            out[key] = value;

            key = "";
            value = "";
        }
    }

    /**
     * Peeks next character without reading it.
     *
     * @return next character or '\0' if @c eob() is reached.
     */
    char peek()
    {
        if(eob())
            return '\0';

        return _buffer->operator[](_position);
    }

    /**
     * Peeks ieme character without reading it.
     *
     * @return ieme character or '\0' if @c eob() is reached.
     */
    char peek(size_t offset)
    {
        if(eob() || _position + offset >= _buffer->size())
            return '\0';

        return _buffer->operator[](_position + offset);
    }

}; /* parser_string */

/**
 * Skip buffer delimiters.
 *
 * @param p Parser instance.
 *
 * @return *this
 */
inline parser_string &skip(parser_string &p)
{
    p.skip();
    return p;
}

/**
 * Skip delimiters.
 *
 * @param delimiters Buffer delimiters.
 *
 * @return Delimiters wrapper.
 */
inline internal::_delimiters skip(const std::string &delimiters)
{
    return internal::_delimiters(delimiters);
}

/**
 * Parser overload to accept delimiters wrapper.
 *
 * @param p Parser instance.
 * @param d Delimiters wrapper.
 *
 * @return *this
 */
inline parser_string &operator>>(parser_string &p, const internal::_delimiters &d)
{
    p.skip(d.delimiters);
    return p;
}
}
}
#endif  /* PARSER_STRING_H */
