/*
 * @file   coordinates_utils.h
 * @author Charles Coulombe
 *
 * @date 18 August 2014, 20:53
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COORDINATES_UTILS_H
#define	COORDINATES_UTILS_H

namespace vap
{

namespace util
{

/**
 * Converts a coordinate to its half-closed representation.
 * Closed    : [1-11[ -> [1-10] -> -1
 *
 * @param coordinate coordinate to convert
 */
template <class TYPE>
inline TYPE toClosedRange(TYPE &coordinate)
{
    coordinate -= 1;
    return coordinate;
}

/**
 * Converts a coordinate to its half-closed representation.
 * Closed    : [1-11[ -> [1-10] -> -1
 *
 * @param coordinate coordinate to convert
 */
template <class TYPE>
inline TYPE toClosedRange(const TYPE &coordinate)
{
    return coordinate - 1;
}

/**
 * Converts a range to its half-closed representation.
 * Closed    : [1-11[ -> [1-10] -> -1
 *
 * @param begin Begin coordinate.
 * @param end End coordinates.
 */
template <class TYPE>
inline void toClosedRange(const TYPE &begin, TYPE &end)
{
    end -= 1;
}

/**
 * Converts coordinates to its half-closed representation regarding the @a referencePoints.
 * Closed    : [1-11[ -> [1-10] -> -1
 *
 * The coordinates are converted following the number of @c reference_points :
 * - 1 : None
 * - 2 : Second coordinate
 * - 3 : First and third coordinates
 * - 4 : First and third coordinates
 * - 5 : Second and fourth coordinates
 * - 6 : Second, fourth, sixth coordinates
 *
 * @note Nothing is done if the number of @c reference_points does not match the number of coordinates(coordinates @c size()).
 *
 * @param coordinates coordinates to convert
 * @param size number of reference points
 */
template <class TYPE>
inline void toClosedRange(std::vector<TYPE> &coordinates, const size_t size)
{
    if(coordinates.size() != size)
        return;

    switch(size)
    {
        case 1:
            /*0*/ // Nothing needs to be done.
            break;

        case 2:
            /*0*/ // Nothing needs to be done.
            /*1*/ // Coordinate needs to be converted to closed range.

            toClosedRange(coordinates[1]); /*inclusive range*/
            break;

        case 3:

            /*0*/ // Coordinate needs to be converted to closed range.
            /*1*/ // Nothing needs to be done.
            /*2*/ // Coordinate needs to be converted to closed range.

            toClosedRange(coordinates[0]); /*inclusive range*/
            toClosedRange(coordinates[2]); /*inclusive range*/
            break;

        case 4:
            /*0*/ // Coordinate needs to be converted to closed range.
            /*1*/ // Nothing needs to be done.
            /*2*/ // Coordinate needs to be converted to closed range.
            /*3*/ // Nothing needs to be done.

            toClosedRange(coordinates[0]); /*inclusive range*/
            toClosedRange(coordinates[2]); /*inclusive range*/
            break;

        case 5:
            /*0*/ // Nothing needs to be done.
            /*1*/ // Coordinate needs to be converted to closed range.
            /*2*/ // Nothing needs to be done.
            /*3*/ // Coordinate needs to be converted to closed range.
            /*4*/ // Nothing needs to be done.

            toClosedRange(coordinates[1]); /*inclusive range*/
            toClosedRange(coordinates[3]); /*inclusive range*/
            break;

        case 6:
            /*0*/ // Nothing needs to be done.
            /*1*/ // Coordinate needs to be converted to closed range.
            /*2*/ // Nothing needs to be done.
            /*3*/ // Coordinate needs to be converted to closed range.
            /*4*/ // Nothing needs to be done.
            /*5*/ // Coordinate needs to be converted to closed range.

            toClosedRange(coordinates[1]); /*inclusive range*/
            toClosedRange(coordinates[3]); /*inclusive range*/
            toClosedRange(coordinates[5]); /*inclusive range*/
            break;

        default:
            break;
    }
}

/**
 * Converts a coordinate to an open one.
 * Half-Open : [1-10] -> [1-11[ -> +1
 *
 * @param coordinate coordinate to convert
 */
template<class TYPE>
inline TYPE toOpenRange(TYPE &coordinate)
{
    coordinate += 1;
    return coordinate;
}

/**
 * Converts a coordinate to an open one.
 * Half-Open : [1-10] -> [1-11[ -> +1
 *
 * @param coordinate coordinate to convert
 */
template<class TYPE>
inline TYPE toOpenRange(const TYPE &coordinate)
{
    return coordinate + 1;
}

/**
 * Converts a range to its open representation.
 * Half-Open : [1-10] -> [1-11[ -> +1
 *
 * @param begin Begin coordinate.
 * @param end End coordinates.
 */
template <class TYPE>
inline void toOpenRange(const TYPE &begin, TYPE &end)
{
    end += 1;
}

/**
 * Converts coordinates to open ones regarding the @a referencePoints.
 * Half-Open : [1-10] -> [1-11[ -> +1
 *
 * The coordinates are converted following the number of @c reference_points :
 * - 1 : None
 * - 2 : Second coordinate
 * - 3 : First and third coordinates
 * - 4 : First and third coordinates
 * - 5 : Second and fourth coordinates
 * - 6 : Second, fourth, sixth coordinates
 *
 * @note Nothing is done if the number of @c reference_points does not match the number of coordinates(coordinates @c size()).
 *
 * @param coordinates coordinates to convert
 * @param size number of reference points
 */
template<class TYPE>
inline void toOpenRange(std::vector<TYPE> &coordinates, const size_t size)
{
    if(coordinates.size() != size)
        return;

    switch(size)
    {
        case 1:
            /*0*/ // Nothing needs to be done.
            break;

        case 2:
            /*0*/ // Nothing needs to be done.
            /*1*/ // Coordinate needs to be converted to open range.

            toOpenRange(coordinates[1]); /*inclusive range*/
            break;

        case 3:

            /*0*/ // Coordinate needs to be converted to open range.
            /*1*/ // Nothing needs to be done.
            /*2*/ // Coordinate needs to be converted to open range.

            toOpenRange(coordinates[0]); /*inclusive range*/
            toOpenRange(coordinates[2]); /*inclusive range*/
            break;

        case 4:
            /*0*/ // Coordinate needs to be converted to open range.
            /*1*/ // Nothing needs to be done.
            /*2*/ // Coordinate needs to be converted to open range.
            /*3*/ // Nothing needs to be done.

            toOpenRange(coordinates[0]); /*inclusive range*/
            toOpenRange(coordinates[2]); /*inclusive range*/
            break;

        case 5:
            /*0*/ // Nothing needs to be done.
            /*1*/ // Coordinate needs to be converted to open range.
            /*2*/ // Nothing needs to be done.
            /*3*/ // Coordinate needs to be converted to open range.
            /*4*/ // Nothing needs to be done.

            toOpenRange(coordinates[1]); /*inclusive range*/
            toOpenRange(coordinates[3]); /*inclusive range*/
            break;

        case 6:
            /*0*/ // Nothing needs to be done.
            /*1*/ // Coordinate needs to be converted to open range.
            /*2*/ // Nothing needs to be done.
            /*3*/ // Coordinate needs to be converted to open range.
            /*4*/ // Nothing needs to be done.
            /*5*/ // Coordinate needs to be converted to open range.

            toOpenRange(coordinates[1]); /*inclusive range*/
            toOpenRange(coordinates[3]); /*inclusive range*/
            toOpenRange(coordinates[5]); /*inclusive range*/
            break;

        default:
            break;
    }
}

/**
 * Converts a coordinate to its zero-base representation.
 * Zero-base: [1-10] -> [0-9] -> -1
 *
 * @param begin Begin coordinate.
 * @param end End coordinate.
 */
template <class TYPE>
inline void toZeroBase(TYPE &begin, TYPE &end)
{
    begin -= 1;
    end -= 1;
}

template <class TYPE>
inline TYPE toZeroBase(TYPE &coordinate)
{
    coordinate -= 1;
    return coordinate;
}

template <class TYPE>
inline TYPE toZeroBase(const TYPE &coordinate)
{
    return coordinate - 1;
}

/**
 * Converts a coordinate to its one-base representation.
 * One-base: [0-9] -> [1-10] -> +1
 *
 * @param begin Begin coordinate.
 * @param end End coordinate.
 */
template <class TYPE>
inline void toOneBased(TYPE &begin, TYPE &end)
{
    begin += 1;
    end += 1;
}

template <class TYPE>
inline TYPE toOneBased(TYPE &coordinate)
{
    coordinate += 1;
    return coordinate;
}

template <class TYPE>
inline TYPE toOneBased(const TYPE &coordinate)
{
    return coordinate + 1;
}

} /* util namespace end */
} /* vap namespace end */

#endif	/* COORDINATES_UTILS_H */

