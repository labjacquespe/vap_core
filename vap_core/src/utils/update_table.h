/**
 * @file   update_table.h
 * @author Charles Coulombe
 *
 * @date 15 January 2013, 21:42
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @ingroup utils
 */

#ifndef UPDATE_TABLE_H
#define	UPDATE_TABLE_H

#include <map>
#include <set>
#include <vector>

#include "../feature/window.h"

namespace vap
{
namespace util
{

/** Windows pointer type definition */
typedef window* wptr;

namespace internal
{

/**
 * Class type functor for entries sorting;
 * @c update_table entries are sorted ascendingly by their windows index and pointers.
 */
struct _less_update_window
{

    /**
     * Callback for ordering and uniqueness among the set of windows. Order is such that
     * window's reference index are ascending and uniqueness is done on the window's pointer.
     *
     * @param left left window
     * @param right right window
     * @return @c True when left entry is unique and comes first, @c False otherwise
     */
    bool operator()(const std::pair< uint_t, wptr > &left, const std::pair< uint_t, wptr > &right) const
    {
        return left.first <= right.first && left.second != right.second;
    }
};
} /* internal namespace end */

/**
 * Update table interface.
 *
 * The table keeps tracks of windows who were updated, more precisely the windows
 * where a data point fit, for further reference: when updating the @c aggregate_reference_feature.
 *
 * The table stores the index of the @c block, in which the @c window was updated, as unique key.
 * For each @c block index, it maps a unique set of windows. Theses windows
 * are internally represented as a @c std::pair of the @c window index
 * (reference index of @c window) and a pointer to the actual @c window in memory.
 *
 * @see window
 */
class update_table : public std::map< uint_t, std::set< std::pair< uint_t, wptr >, internal::_less_update_window > >
{
  public:
    /** Update table internal representation. */
    typedef std::map< uint_t, std::set< std::pair< uint_t, wptr >, internal::_less_update_window > > type;

    /**
     * Key representation.
     * Represents the index of the block which was updated.  */
    typedef uint_t key_type;

    /**
     * Value representation.
     * Represents a set of windows which were updated. */
    typedef std::set< std::pair< uint_t, wptr >, internal::_less_update_window > value_type;
};
} /* util namespace end */
} /* vap namespace end */
#endif	/* UPDATE_TABLE_H */

