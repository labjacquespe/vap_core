/**
 * @file   algorithms.h
 * @author Charles Coulombe
 *
 * @date 12 September 2014, 14:20
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



#ifndef ALGORITHMS_H
#define	ALGORITHMS_H

#include <utility>
#include <algorithm>

#include "../../feature/coordinates.h"

namespace vap
{
namespace util
{

/**
 * Returns the intersecting range of (@a begin1 , @a end1) and (@a begin2 , @a end2).
 * An empty range is returned if there's no intersection between both range.
 *
 * @param begin1 Start value of first range.
 * @param end1 End value of first range.
 * @param begin2 Start value of second range.
 * @param end2 End value of second range.
 *
 * @return @c std::pair representing the intersecting range
 */
template <class TYPE>
inline std::pair<TYPE, TYPE> range_intersection(const TYPE begin1, const TYPE end1, const TYPE begin2, const TYPE end2)
{
    TYPE begin = std::max(begin1, begin2); // get the greatest begin value
    TYPE end = std::min(end1, end2); // get the smallest end value

    // Return an empty range if there's no intersection between both range,
    // otherwise return the intersection.
    return end < begin ? std::make_pair(0, 0) : std::make_pair(begin, end);
}

/**
 * Returns the intersecting range of @a left and @a right.
 * An empty range is returned if there's no intersection between both range.
 *
 * @param left First range.
 * @param right Second range.
 *
 * @return @c std::pair representing the intersecting range
 */
template <class TYPE>
inline std::pair<TYPE, TYPE> range_intersection(const std::pair<TYPE, TYPE> &left, const std::pair<TYPE, TYPE> &right)
{
    return range_intersection(left.first, left.second, right.first, right.second);
}

/**
 * Returns the intersecting range of (@a begin1 , @a end1) and (@a begin2 , @a end2).
 * An empty range is returned if there's no intersection between both range.
 *
 * @param begin1 Start value of first range.
 * @param end1 End value of first range.
 * @param begin2 Start value of second range.
 * @param end2 End value of second range.
 *
 * @return @c coordinates representing the intersecting range
 */
inline coordinates range_intersection(const int begin1, const int end1, const int begin2, const int end2)
{
    int begin = std::max(begin1, begin2); // get the greatest begin value
    int end = std::min(end1, end2); // get the smallest end value

    // Return an empty range if there's no intersection between both range,
    // otherwise return the intersection.
    return end < begin ? coordinates(0, 0) : coordinates(begin, end);
}

/**
 * Returns the intersecting range of @a left and @a right.
 * An empty range is returned if there's no intersection between both range.
 *
 * @param left First range.
 * @param right Second range.
 *
 * @return @c coordinates representing the intersecting range
 */
inline coordinates range_intersection(const coordinates &left, const coordinates &right)
{
    return range_intersection(left.begin(), left.end(), right.begin(), right.end());
}

/**
 * Select values based on the intersection of the two sets.
 * The selection consider duplicates elements.
 *
 * @note
 * The range used is [first1,last1), which contains all the elements between first1 and last1, including the element pointed by first1 but not the element pointed by last1.
 * The range used is [first2,last2).
 *
 * @param first1 Input iterator to the initial position of the first sorted sequence.
 * @param last1 Input iterator to the final position of the first sorted sequence.
 * @param first2 Input iterator to the initial position of the second sorted sequence.
 * @param last2 Input iterator to the final position of the second sorted sequence.
 * @param result Output iterator to the initial position of the range where the resulting sequence is stored. The pointed type shall support being assigned the value of an element from the first range.
 *
 * @return An iterator to the end of the constructed range.
 */
template <class INPUTITERATOR1, class INPUTITERATOR2, class OUTPUTITERATOR>
OUTPUTITERATOR select(INPUTITERATOR1 first1, INPUTITERATOR1 last1, INPUTITERATOR2 first2, INPUTITERATOR2 last2, OUTPUTITERATOR result)
{
    // if filters is empty
    while(first2 == last2 && first1 != last1)
    {
        *result = *first1;
        ++first1;
        ++result;
    }

    while(first1 != last1 && first2 != last2)
    {
        if(*first1 > *first2)
            ++first2; // entry is greater than filters, advance to the next filters
        else if(*first1 < *first2)
            ++first1; // entry is smaller than filters, advance to the next entry
        else
        {
            // elements are equal, select entry value and advance to the next entry
            *result = *first1;
            ++first1;
            ++result;
        }
    }

    return result;
}

/**
 * Select values based on the intersection of the two sets.
 * The selection consider duplicates elements.
 *
 * @note
 * The range used is [first1,last1), which contains all the elements between first1 and last1, including the element pointed by first1 but not the element pointed by last1.
 * The range used is [first2,last2).
 *
 * @param first1 Input iterator to the initial position of the first sorted sequence.
 * @param last1 Input iterator to the final position of the first sorted sequence.
 * @param first2 Input iterator to the initial position of the second sorted sequence.
 * @param last2 Input iterator to the final position of the second sorted sequence.
 * @param result Output iterator to the initial position of the range where the resulting sequence is stored. The pointed type shall support being assigned the value of an element from the first range.
 * @param lcmp Binary function that accepts two arguments of the types pointed by the input iterators, and returns a value convertible to bool. The value returned indicates whether the first argument is considered to go before the second in the specific strict weak ordering it defines. The function shall not modify any of its arguments. This can either be a function pointer or a function object.
 * @param gcmp Binary function that accepts two arguments of the types pointed by the input iterators, and returns a value convertible to bool. The value returned indicates whether the first argument is considered to go after the second in the specific strict weak ordering it defines. The function shall not modify any of its arguments. This can either be a function pointer or a function object.
 *
 * @return An iterator to the end of the constructed range.
 */
template <class INPUTITERATOR1, class INPUTITERATOR2, class OUTPUTITERATOR, class LESSER_COMPARE, class GREATER_COMPARE>
OUTPUTITERATOR select(INPUTITERATOR1 first1, INPUTITERATOR1 last1, INPUTITERATOR2 first2, INPUTITERATOR2 last2, OUTPUTITERATOR result, LESSER_COMPARE lcmp, GREATER_COMPARE gcmp)
{
    // if filters is empty
    while(first2 == last2 && first1 != last1)
    {
        *result = *first1;
        ++first1;
        ++result;
    }

    while(first1 != last1 && first2 != last2)
    {
        if(gcmp(*first1, *first2))
            ++first2; // entry is greater than filters, advance to the next filters
        else if(lcmp(*first1, *first2))
            ++first1; // entry is smaller than filters, advance to the next entry
        else
        {
            // elements are equal, select entry value and advance to the next entry
            *result = *first1;
            ++result;
            ++first1;
        }
    }

    return result;
}

/**
 * Select values based on the intersection of the two sets.
 * The selection consider duplicates elements.
 *
 * @note
 * The range used is [first1,last1), which contains all the elements between first1 and last1, including the element pointed by first1 but not the element pointed by last1.
 * The range used is [first2,last2).
 *
 * @param first1 Input iterator to the initial position of the first sorted sequence.
 * @param last1 Input iterator to the final position of the first sorted sequence.
 * @param first2 Input iterator to the initial position of the second sorted sequence.
 * @param last2 Input iterator to the final position of the second sorted sequence.
 * @param result Output iterator to the initial position of the range where the resulting sequence is stored. The pointed type shall support being assigned the value of an element from the first range.
 * @param cmp Binary function that accepts two arguments of the types pointed by the input iterators, and returns a value convertible to bool. The value returned indicates whether the first argument is considered to go before the second in the specific strict weak ordering it defines. The function shall not modify any of its arguments. This can either be a function pointer or a function object.
 *
 * @return An iterator to the end of the constructed range.
 */
template <class INPUTITERATOR1, class INPUTITERATOR2, class OUTPUTITERATOR, class COMPARE>
OUTPUTITERATOR select(INPUTITERATOR1 first1, INPUTITERATOR1 last1, INPUTITERATOR2 first2, INPUTITERATOR2 last2, OUTPUTITERATOR result, COMPARE cmp)
{
    // if filters is empty
    while(first2 == last2 && first1 != last1)
    {
        *result = *first1;
        ++first1;
        ++result;
    }

    while(first1 != last1 && first2 != last2)
    {
        if(cmp(*first2, *first1))
            ++first2; // entry is greater than filters, advance to the next filters
        else if(cmp(*first1, *first2))
            ++first1; // entry is smaller than filters, advance to the next entry
        else
        {
            // elements are equal, select entry value and advance to the next entry
            *result = *first1;
            ++result;
            ++first1;
        }
    }

    return result;
}

/**
 * Exclude values based on the difference of the two sets.
 * The exclusion consider duplicates elements.
 *
 * @note
 * The range used is [first1,last1), which contains all the elements between first1 and last1, including the element pointed by first1 but not the element pointed by last1.
 * The range used is [first2,last2).
 *
 * @param first1 Input iterator to the initial position of the first sorted sequence.
 * @param last1 Input iterator to the final position of the first sorted sequence.
 * @param first2 Input iterator to the initial position of the second sorted sequence.
 * @param last2 Input iterator to the final position of the second sorted sequence.
 * @param result Output iterator to the initial position of the range where the resulting sequence is stored. The pointed type shall support being assigned the value of an element from the first range.
 *
 * @return An iterator to the end of the constructed range.
 */
template <class INPUTITERATOR1, class INPUTITERATOR2, class OUTPUTITERATOR>
OUTPUTITERATOR exclude(INPUTITERATOR1 first1, INPUTITERATOR1 last1, INPUTITERATOR2 first2, INPUTITERATOR2 last2, OUTPUTITERATOR result)
{
    // if filters is empty
    while(first2 == last2 && first1 != last1)
    {
        *result = *first1;
        ++first1;
        ++result;
    }

    while(first1 != last1)
    {
        if(first2 != last2 && *first1 > *first2)
            ++first2; // entry is greater than filters, advance to the next filters
        else
        {
            // entry is smaller than filters or filters is exhausted add not
            // excluded entry
            if(first2 == last2 || *first1 < *first2)
            {
                *result = *first1;
                ++result;
            }

            ++first1;
        }
    }

    return result;
}

/**
 * Exclude values based on the difference of the two sets.
 * The exclusion consider duplicates elements.
 *
 * @note
 * The range used is [first1,last1), which contains all the elements between first1 and last1, including the element pointed by first1 but not the element pointed by last1.
 * The range used is [first2,last2).
 *
 * @param first1 Input iterator to the initial position of the first sorted sequence.
 * @param last1 Input iterator to the final position of the first sorted sequence.
 * @param first2 Input iterator to the initial position of the second sorted sequence.
 * @param last2 Input iterator to the final position of the second sorted sequence.
 * @param result Output iterator to the initial position of the range where the resulting sequence is stored. The pointed type shall support being assigned the value of an element from the first range.
 * @param lcmp Binary function that accepts two arguments of the types pointed by the input iterators, and returns a value convertible to bool. The value returned indicates whether the first argument is considered to go before the second in the specific strict weak ordering it defines. The function shall not modify any of its arguments. This can either be a function pointer or a function object.
 * @param gcmp Binary function that accepts two arguments of the types pointed by the input iterators, and returns a value convertible to bool. The value returned indicates whether the first argument is considered to go after the second in the specific strict weak ordering it defines. The function shall not modify any of its arguments. This can either be a function pointer or a function object.
 *
 * @return An iterator to the end of the constructed range.
 */
template <class INPUTITERATOR1, class INPUTITERATOR2, class OUTPUTITERATOR, class LESSER_COMPARE, class GREATER_COMPARE>
OUTPUTITERATOR exclude(INPUTITERATOR1 first1, INPUTITERATOR1 last1, INPUTITERATOR2 first2, INPUTITERATOR2 last2, OUTPUTITERATOR result, LESSER_COMPARE lcmp, GREATER_COMPARE gcmp)
{
    // if filters is empty
    while(first2 == last2 && first1 != last1)
    {
        *result = *first1;
        ++first1;
        ++result;
    }

    while(first1 != last1)
    {
        if(first2 != last2 && gcmp(*first1, *first2))
            ++first2; // entry is greater than filters, advance to the next filters
        else
        {
            // entry is smaller than filters or filters is exhausted add not
            // excluded entry
            if(first2 == last2 || lcmp(*first1, *first2))
            {
                *result = *first1;
                ++result;
            }

            ++first1;
        }
    }

    return result;
}

/**
 * Exclude values based on the difference of the two sets.
 * The exclusion consider duplicates elements.
 *
 * @note
 * The range used is [first1,last1), which contains all the elements between first1 and last1, including the element pointed by first1 but not the element pointed by last1.
 * The range used is [first2,last2).
 *
 * @param first1 Input iterator to the initial position of the first sorted sequence.
 * @param last1 Input iterator to the final position of the first sorted sequence.
 * @param first2 Input iterator to the initial position of the second sorted sequence.
 * @param last2 Input iterator to the final position of the second sorted sequence.
 * @param result Output iterator to the initial position of the range where the resulting sequence is stored. The pointed type shall support being assigned the value of an element from the first range.
 * @param cmp Binary function that accepts two arguments of the types pointed by the input iterators, and returns a value convertible to bool. The value returned indicates whether the first argument is considered to go before the second in the specific strict weak ordering it defines. The function shall not modify any of its arguments. This can either be a function pointer or a function object.
 *
 * @return An iterator to the end of the constructed range.
 */
template <class INPUTITERATOR1, class INPUTITERATOR2, class OUTPUTITERATOR, class COMPARE>
OUTPUTITERATOR exclude(INPUTITERATOR1 first1, INPUTITERATOR1 last1, INPUTITERATOR2 first2, INPUTITERATOR2 last2, OUTPUTITERATOR result, COMPARE cmp)
{
    // if filters is empty
    while(first2 == last2 && first1 != last1)
    {
        *result = *first1;
        ++first1;
        ++result;
    }

    while(first1 != last1)
    {
        if(first2 != last2 && cmp(*first2, *first1)) // entry is greater than filters, advance to the next filters
            ++first2;
        else
        {
            // entry is smaller than filters or filters is exhausted add not
            // excluded entry
            if(first2 == last2 || cmp(*first1, *first2))
            {
                *result = *first1;
                ++result;
            }

            ++first1;
        }
    }

    return result;
}

} /* util namespace end */

} /* vap namespace end */

#endif	/* ALGORITHMS_H */

