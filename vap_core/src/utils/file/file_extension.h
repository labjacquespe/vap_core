/**
 * @file    file_extension.h
 * @author  Charles Coulombe
 * @date    7 August 2015, 11:55
 * @version 1.2.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FILE_EXTENSION_H
#define FILE_EXTENSION_H

namespace vap
{
namespace io
{
const std::string GZIP_EXT = ".gz";
const std::string BZIP2_EXT = ".bz2";

const std::string BEDGRAPH_SHORT_EXT = ".bg";
const std::string BEDGRAPH_LONG_EXT = ".bedgraph";
const std::string BEDGRAPH_SHORT_GZIP_EXT = ".bg.gz";
const std::string BEDGRAPH_LONG_GZIP_EXT = ".bedgraph.gz";
const std::string BEDGRAPH_SHORT_BZIP2_EXT = ".bg.bz2";
const std::string BEDGRAPH_LONG_BZIP2_EXT = ".bedgraph.bz2";

const std::string BED_LONG_EXT = ".bed";
const std::string BED_LONG_GZIP_EXT = ".bed.gz";
const std::string BED_LONG_BZIP2_EXT = ".bed.bz2";
const std::string BED3_LONG_EXT = ".bed3";
const std::string BED3_LONG_GZIP_EXT = ".bed3.gz";
const std::string BED3_LONG_BZIP2_EXT = ".bed3.bz2";
const std::string BED4_LONG_EXT = ".bed4";
const std::string BED4_LONG_GZIP_EXT = ".bed4.gz";
const std::string BED4_LONG_BZIP2_EXT = ".bed4.bz2";
const std::string BED5_LONG_EXT = ".bed5";
const std::string BED5_LONG_GZIP_EXT = ".bed5.gz";
const std::string BED5_LONG_BZIP2_EXT = ".bed5.bz2";
const std::string BED6_LONG_EXT = ".bed6";
const std::string BED6_LONG_GZIP_EXT = ".bed6.gz";
const std::string BED6_LONG_BZIP2_EXT = ".bed6.bz2";
const std::string BED7_LONG_EXT = ".bed7";
const std::string BED7_LONG_GZIP_EXT = ".bed7.gz";
const std::string BED7_LONG_BZIP2_EXT = ".bed7.bz2";
const std::string BED8_LONG_EXT = ".bed8";
const std::string BED8_LONG_GZIP_EXT = ".bed8.gz";
const std::string BED8_LONG_BZIP2_EXT = ".bed8.bz2";
const std::string BED9_LONG_EXT = ".bed9";
const std::string BED9_LONG_GZIP_EXT = ".bed9.gz";
const std::string BED9_LONG_BZIP2_EXT = ".bed9.bz2";
const std::string BED10_LONG_EXT = ".bed10";
const std::string BED10_LONG_GZIP_EXT = ".bed10.gz";
const std::string BED10_LONG_BZIP2_EXT = ".bed10.bz2";
const std::string BED11_LONG_EXT = ".bed11";
const std::string BED11_LONG_GZIP_EXT = ".bed11.gz";
const std::string BED11_LONG_BZIP2_EXT = ".bed11.bz2";
const std::string BED12_LONG_EXT = ".bed12";
const std::string BED12_LONG_GZIP_EXT = ".bed12.gz";
const std::string BED12_LONG_BZIP2_EXT = ".bed12.bz2";

const std::string REFERENCE_COORDINATES1_LONG_EXT = ".coord1";
const std::string REFERENCE_COORDINATES1_LONG_GZIP_EXT = ".coord1.gz";
const std::string REFERENCE_COORDINATES1_LONG_BZIP2_EXT = ".coord1.bz2";
const std::string REFERENCE_COORDINATES2_LONG_EXT = ".coord2";
const std::string REFERENCE_COORDINATES2_LONG_GZIP_EXT = ".coord2.gz";
const std::string REFERENCE_COORDINATES2_LONG_BZIP2_EXT = ".coord2.bz2";
const std::string REFERENCE_COORDINATES3_LONG_EXT = ".coord3";
const std::string REFERENCE_COORDINATES3_LONG_GZIP_EXT = ".coord3.gz";
const std::string REFERENCE_COORDINATES3_LONG_BZIP2_EXT = ".coord3.bz2";
const std::string REFERENCE_COORDINATES4_LONG_EXT = ".coord4";
const std::string REFERENCE_COORDINATES4_LONG_GZIP_EXT = ".coord4.gz";
const std::string REFERENCE_COORDINATES4_LONG_BZIP2_EXT = ".coord4.bz2";
const std::string REFERENCE_COORDINATES5_LONG_EXT = ".coord5";
const std::string REFERENCE_COORDINATES5_LONG_GZIP_EXT = ".coord5.gz";
const std::string REFERENCE_COORDINATES5_LONG_BZIP2_EXT = ".coord5.bz2";
const std::string REFERENCE_COORDINATES6_LONG_EXT = ".coord6";
const std::string REFERENCE_COORDINATES6_LONG_GZIP_EXT = ".coord6.gz";
const std::string REFERENCE_COORDINATES6_LONG_BZIP2_EXT = ".coord6.bz2";
const std::string REFERENCE_COORDINATES1_SHORT_EXT = ".rc1";
const std::string REFERENCE_COORDINATES1_SHORT_GZIP_EXT = ".rc1.gz";
const std::string REFERENCE_COORDINATES1_SHORT_BZIP2_EXT = ".rc1.bz2";
const std::string REFERENCE_COORDINATES2_SHORT_EXT = ".rc2";
const std::string REFERENCE_COORDINATES2_SHORT_GZIP_EXT = ".rc2.gz";
const std::string REFERENCE_COORDINATES2_SHORT_BZIP2_EXT = ".rc2.bz2";
const std::string REFERENCE_COORDINATES3_SHORT_EXT = ".rc3";
const std::string REFERENCE_COORDINATES3_SHORT_GZIP_EXT = ".rc3.gz";
const std::string REFERENCE_COORDINATES3_SHORT_BZIP2_EXT = ".rc3.bz2";
const std::string REFERENCE_COORDINATES4_SHORT_EXT = ".rc4";
const std::string REFERENCE_COORDINATES4_SHORT_GZIP_EXT = ".rc4.gz";
const std::string REFERENCE_COORDINATES4_SHORT_BZIP2_EXT = ".rc4.bz2";
const std::string REFERENCE_COORDINATES5_SHORT_EXT = ".rc5";
const std::string REFERENCE_COORDINATES5_SHORT_GZIP_EXT = ".rc5.gz";
const std::string REFERENCE_COORDINATES5_SHORT_BZIP2_EXT = ".rc5.bz2";
const std::string REFERENCE_COORDINATES6_SHORT_EXT = ".rc6";
const std::string REFERENCE_COORDINATES6_SHORT_GZIP_EXT = ".rc6.gz";
const std::string REFERENCE_COORDINATES6_SHORT_BZIP2_EXT = ".rc6.bz2";
const std::string REFERENCE_COORDINATES_LONG_EXT = ".coord";
const std::string REFERENCE_COORDINATES_SHORT_EXT = ".rc";
const std::string REFERENCE_COORDINATES_LONG_GZIP_EXT = ".coord.gz";
const std::string REFERENCE_COORDINATES_SHORT_GZIP_EXT = ".rc.gz";
const std::string REFERENCE_COORDINATES_LONG_BZIP2_EXT = ".coord.bz2";
const std::string REFERENCE_COORDINATES_SHORT_BZIP2_EXT = ".rc.bz2";

const std::string GENEPRED_SHORT_EXT = ".gp";
const std::string GENEPRED_LONG_EXT = ".genepred";
const std::string GENEPRED_SHORT_GZIP_EXT = ".gp.gz";
const std::string GENEPRED_LONG_GZIP_EXT = ".genepred.gz";
const std::string GENEPRED_SHORT_BZIP2_EXT = ".gp.bz2";
const std::string GENEPRED_LONG_BZIP2_EXT = ".genepred.bz2";

const std::string GTF_LONG_EXT = ".gtf";
const std::string GTF_LONG_GZIP_EXT = ".gtf.gz";
const std::string GTF_LONG_BZIP2_EXT = ".gtf.bz2";

const std::string WIGGLE_SHORT_EXT = ".wig";
const std::string WIGGLE_LONG_EXT = ".wiggle";
const std::string WIGGLE_SHORT_GZIP_EXT = ".wig.gz";
const std::string WIGGLE_LONG_GZIP_EXT = ".wiggle.gz";
const std::string WIGGLE_SHORT_BZIP2_EXT = ".wig.bz2";
const std::string WIGGLE_LONG_BZIP2_EXT = ".wiggle.bz2";

const std::string BIGWIG_SHORT_EXT = ".bw";
const std::string BIGWIG_LONG_EXT = ".bigwig";
const std::string BIGWIG_SHORT_GZIP_EXT = ".bw.gz";
const std::string BIGWIG_LONG_GZIP_EXT = ".bigwig.gz";
const std::string BIGWIG_SHORT_BZIP2_EXT = ".bw.bz2";
const std::string BIGWIG_LONG_BZIP2_EXT = ".bigwig.bz2";

const std::string BIGBED_SHORT_EXT = ".bb";
const std::string BIGBED_LONG_EXT = ".bigbed";
const std::string BIGBED_SHORT_GZIP_EXT = ".bb.gz";
const std::string BIGBED_LONG_GZIP_EXT = ".bigbed.gz";
const std::string BIGBED_SHORT_BZIP2_EXT = ".bb.bz2";
const std::string BIGBED_LONG_BZIP2_EXT = ".bigbed.bz2";

const std::string REFERENCE_ANNOTATIONS_SHORT_EXT = ".ra";
const std::string REFERENCE_ANNOTATIONS_SHORT_GZIP_EXT = ".ra.gz";
const std::string REFERENCE_ANNOTATIONS_SHORT_BZIP2_EXT = ".ra.bz2";

const std::string REFERENCE_FILTERS_SHORT_EXT = ".rf";
const std::string REFERENCE_FILTERS_SHORT_GZIP_EXT = ".rf.gz";
const std::string REFERENCE_FILTERS_SHORT_BZIP2_EXT = ".rf.bz2";

const std::vector<std::string> SUPPORTED_EXTENSIONS =
{
    GZIP_EXT, BZIP2_EXT,
    BEDGRAPH_SHORT_EXT, BEDGRAPH_LONG_EXT, BEDGRAPH_SHORT_GZIP_EXT, BEDGRAPH_LONG_GZIP_EXT, BEDGRAPH_SHORT_BZIP2_EXT, BEDGRAPH_LONG_BZIP2_EXT,
    BED_LONG_EXT, BED_LONG_GZIP_EXT, BED_LONG_BZIP2_EXT, BED3_LONG_EXT, BED3_LONG_GZIP_EXT, BED3_LONG_BZIP2_EXT, BED4_LONG_EXT, BED4_LONG_GZIP_EXT, BED4_LONG_BZIP2_EXT, BED5_LONG_EXT, BED5_LONG_GZIP_EXT, BED5_LONG_BZIP2_EXT, BED6_LONG_EXT, BED6_LONG_GZIP_EXT, BED6_LONG_BZIP2_EXT, BED7_LONG_EXT, BED7_LONG_GZIP_EXT, BED7_LONG_BZIP2_EXT, BED8_LONG_EXT, BED8_LONG_GZIP_EXT, BED8_LONG_BZIP2_EXT, BED9_LONG_EXT, BED9_LONG_GZIP_EXT, BED9_LONG_BZIP2_EXT, BED10_LONG_EXT, BED10_LONG_GZIP_EXT, BED10_LONG_BZIP2_EXT, BED11_LONG_EXT, BED11_LONG_GZIP_EXT, BED11_LONG_BZIP2_EXT, BED12_LONG_EXT, BED12_LONG_GZIP_EXT, BED12_LONG_BZIP2_EXT,
    REFERENCE_COORDINATES1_LONG_EXT, REFERENCE_COORDINATES1_LONG_GZIP_EXT, REFERENCE_COORDINATES1_LONG_BZIP2_EXT, REFERENCE_COORDINATES2_LONG_EXT, REFERENCE_COORDINATES2_LONG_GZIP_EXT, REFERENCE_COORDINATES2_LONG_BZIP2_EXT, REFERENCE_COORDINATES3_LONG_EXT, REFERENCE_COORDINATES3_LONG_GZIP_EXT, REFERENCE_COORDINATES3_LONG_BZIP2_EXT, REFERENCE_COORDINATES4_LONG_EXT, REFERENCE_COORDINATES4_LONG_GZIP_EXT, REFERENCE_COORDINATES4_LONG_BZIP2_EXT, REFERENCE_COORDINATES5_LONG_EXT, REFERENCE_COORDINATES5_LONG_GZIP_EXT, REFERENCE_COORDINATES5_LONG_BZIP2_EXT, REFERENCE_COORDINATES6_LONG_EXT, REFERENCE_COORDINATES6_LONG_GZIP_EXT, REFERENCE_COORDINATES6_LONG_BZIP2_EXT,REFERENCE_COORDINATES1_SHORT_EXT, REFERENCE_COORDINATES1_SHORT_GZIP_EXT, REFERENCE_COORDINATES1_SHORT_BZIP2_EXT, REFERENCE_COORDINATES2_SHORT_EXT, REFERENCE_COORDINATES2_SHORT_GZIP_EXT, REFERENCE_COORDINATES2_SHORT_BZIP2_EXT, REFERENCE_COORDINATES3_SHORT_EXT, REFERENCE_COORDINATES3_SHORT_GZIP_EXT, REFERENCE_COORDINATES3_SHORT_BZIP2_EXT, REFERENCE_COORDINATES4_SHORT_EXT, REFERENCE_COORDINATES4_SHORT_GZIP_EXT, REFERENCE_COORDINATES4_SHORT_BZIP2_EXT, REFERENCE_COORDINATES5_SHORT_EXT, REFERENCE_COORDINATES5_SHORT_GZIP_EXT, REFERENCE_COORDINATES5_SHORT_BZIP2_EXT, REFERENCE_COORDINATES6_SHORT_EXT, REFERENCE_COORDINATES6_SHORT_GZIP_EXT, REFERENCE_COORDINATES6_SHORT_BZIP2_EXT, REFERENCE_COORDINATES_LONG_EXT, REFERENCE_COORDINATES_SHORT_EXT, REFERENCE_COORDINATES_LONG_GZIP_EXT, REFERENCE_COORDINATES_SHORT_GZIP_EXT, REFERENCE_COORDINATES_LONG_BZIP2_EXT, REFERENCE_COORDINATES_SHORT_BZIP2_EXT,
    GENEPRED_SHORT_EXT, GENEPRED_LONG_EXT, GENEPRED_SHORT_GZIP_EXT, GENEPRED_LONG_GZIP_EXT, GENEPRED_SHORT_BZIP2_EXT, GENEPRED_LONG_BZIP2_EXT,
    GTF_LONG_EXT, GTF_LONG_GZIP_EXT, GTF_LONG_BZIP2_EXT,
    WIGGLE_SHORT_EXT, WIGGLE_LONG_EXT, WIGGLE_SHORT_GZIP_EXT, WIGGLE_LONG_GZIP_EXT, WIGGLE_SHORT_BZIP2_EXT, WIGGLE_LONG_BZIP2_EXT,
    BIGWIG_SHORT_EXT, BIGWIG_LONG_EXT, BIGWIG_SHORT_GZIP_EXT,BIGWIG_LONG_GZIP_EXT,BIGWIG_SHORT_BZIP2_EXT,BIGWIG_LONG_BZIP2_EXT,
    BIGBED_SHORT_EXT, BIGBED_LONG_EXT, BIGBED_SHORT_GZIP_EXT, BIGBED_LONG_GZIP_EXT, BIGBED_SHORT_BZIP2_EXT, BIGBED_LONG_BZIP2_EXT,
    REFERENCE_ANNOTATIONS_SHORT_EXT, REFERENCE_ANNOTATIONS_SHORT_GZIP_EXT, REFERENCE_ANNOTATIONS_SHORT_BZIP2_EXT,
    REFERENCE_FILTERS_SHORT_EXT, REFERENCE_FILTERS_SHORT_GZIP_EXT, REFERENCE_FILTERS_SHORT_BZIP2_EXT,
};
}
}
#endif
