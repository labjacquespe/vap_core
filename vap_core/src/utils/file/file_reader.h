/**
 * @file    file_extension.h
 * @author  Charles Coulombe
 * @date    26 July 2015, 17:26
 * @version 1.2.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FILE_READER_H
#define FILE_READER_H

#include <ios>
#include <fstream>
#include <string>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include "format_guess.h"

namespace vap
{
namespace io
{

/** Simple wrapper around @c std::istream to read text file.*/
class file_reader
{
private:
    file_reader(const file_reader &fr) = delete; // deactivate evil copy
    void operator=(const file_reader &fr) = delete; // deactivate evil copy

protected:
    std::istream *_stream;
    bool _isopen;
    std::string _fname;

public:

    /** Default constructor. */
    file_reader();

    /**
     * Constructs this reader on @a stream. */
    file_reader(std::istream *stream, const std::string &fname = "");

    /** Constructs this reader on @a fname and opens it. */
    file_reader(const std::string &fname, std::ios_base::openmode mode = std::ios_base::in);

    /** Opens @a fname and associates it with the input stream object. */
    virtual void open(const std::string &fname, std::ios_base::openmode mode = std::ios_base::in);

    virtual ~file_reader();

    /**
     * Check if a file is open.
     * Returns whether the stream is currently associated to a file.
     *
     * @return @c true if a file is open and associated with the input stream object. @c false otherwise.
     */
    virtual bool is_open() const;
    virtual explicit operator bool() const;

    /**
     * Check whether eofbit is set.
     * Returns true if the eofbit error state flag is set for the stream.
     *
     * @return @c true if the stream's eofbit error state flag is set (which signals that the End-of-File has been reached by the last input operation). @c false otherwise.
     */
    virtual bool eof() const;

    virtual int peek() const;

    std::string file() const;

    /**
     * Get line from stream into string.
     * Extracts characters from the stream object and stores them into a std::string until the the newline character is found.
     * The extraction also stops if the end of file is reached in the stream object or if some other error occurs during the input operation.
     *
     * @return The underlayering stream object.
     */
    virtual std::istream &readline(std::string &str, char delim = '\n');

    virtual std::istream &readuntil(std::string &str, char delim);

    /**
     * Get the stream position.
     *
     * @return Current stream position.
     */
    std::streampos streampos() const;

    /**
     * Set the stream position.
     *
     * @param pos New position.
     * @param from From start or end.
     */
    void streampos(const std::streampos pos, std::ios_base::seekdir from = std::ios_base::beg);


    /** Returns a pointer to the internal stream object. */
    virtual std::istream *fstream() const;


    //    bool lookahead(const std::string &prefix)
    //    {
    //        if(eof())
    //            return false;
    //        else
    //        {
    //            std::string str;
    //
    //            size_t pos = _stream->tellg();
    //            std::getline(*_stream, str);
    //            _stream->seekg(pos, std::ios_base::beg);
    //
    //            // compare size and memory block content
    //            return(str.size() >= prefix.size() && memcmp(str.c_str(), prefix.c_str(), prefix.size()) == 0);
    ////            return line.substr(0, (str.empty() ? 0 : str.size() - 1)) == str;
    //        }
    //    }
};

/** Specialized wrapper around @c std::istream and @c boost::iostream::filtering_istream to read gzip files. */
class file_reader_gzip : public file_reader
{
private:
    file_reader_gzip(const file_reader_gzip &fr) = delete; // deactivate evil copy
    void operator=(const file_reader_gzip &fr) = delete; // deactivate evil copy

    boost::iostreams::filtering_istream *_fstream;

    void _open();

public:

    /** Constructs this reader on a @a stream. */
    file_reader_gzip(std::istream *stream, const std::string &fname = "");

    /** Constructs this reader on @a fname and opens it. */
    file_reader_gzip(const std::string &fname);

    ~file_reader_gzip();

    /**
     * Check whether eofbit is set.
     * Returns true if the eofbit error state flag is set for the stream.
     *
     * @return @c true if the stream's eofbit error state flag is set (which signals that the End-of-File has been reached by the last input operation). @c false otherwise.
     */
    bool eof() const;
    int peek() const;

    /**
     * Get line from stream into string.
     * Extracts characters from the stream object and stores them into a std::string until the the newline character is found.
     * The extraction also stops if the end of file is reached in the stream object or if some other error occurs during the input operation.
     *
     * @return The underlayering stream object.
     */
    std::istream &readline(std::string &str, char delim = '\n');

    std::istream &readuntil(std::string &str, char delim);

    /** Returns a pointer to the internal stream object. */
    boost::iostreams::filtering_istream *fstream() const;

};

/** Specialized wrapper around @c std::istream and @c boost::iostream::filtering_istream to read bzip2 files. */
class file_reader_bzip2 : public file_reader
{
private:
    file_reader_bzip2(const file_reader_bzip2 &fr) = delete; // deactivate evil copy
    void operator=(const file_reader_bzip2 &fr) = delete; // deactivate evil copy

    boost::iostreams::filtering_istream *_fstream;

    void _open();

public:

    /** Constructs this reader on a @a stream. */
    file_reader_bzip2(std::istream *stream, const std::string &fname = "");

    /** Constructs this reader on @a fname and opens it. */
    file_reader_bzip2(const std::string &fname);

    ~file_reader_bzip2();

    /**
     * Check whether eofbit is set.
     * Returns true if the eofbit error state flag is set for the stream.
     *
     * @return @c true if the stream's eofbit error state flag is set (which signals that the End-of-File has been reached by the last input operation). @c false otherwise.
     */
    bool eof() const;

    int peek() const;

    /**
     * Get line from stream into string.
     * Extracts characters from the stream object and stores them into a std::string until the the newline character is found.
     * The extraction also stops if the end of file is reached in the stream object or if some other error occurs during the input operation.
     *
     * @return The underlayering stream object.
     */
    std::istream &readline(std::string &str, char delim = '\n');

    std::istream &readuntil(std::string &str, char delim);

    /** Returns a pointer to the internal stream object. */
    boost::iostreams::filtering_istream *fstream() const;

};

/** Helper class to creates appropriate @c file_reader. */
class file_reader_factory
{
public:
    static file_reader *create(const std::string &fname, std::ios_base::openmode mode = std::ios_base::in);

};
}
}
#endif
