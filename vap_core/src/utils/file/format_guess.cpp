/**
 * @file    file_extension.h
 * @author  Charles Coulombe
 * @date    August 02,2015, 20:57
 * @version 1.2.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "format_guess.h"

namespace vap
{
namespace io
{
void format_guess::report_error(std::ostream &out, const std::string &what, const std::string &prefix)
{
    out << (prefix.empty() ? "" : prefix + ": ") << what << "\n";
}

/** Parse at most 200 lines (skipping any blank, comment, track or browser lines).
    To be valid, at least 2 lines must have been read, and 1 of them must be valid. Otherwise,
    false is returned.*/
bool format_guess::_parse(std::istream *stream, const pcre2cpp &re)
{
    using namespace boost::algorithm;

    size_t nlines = 0, valid = 0, invalid = 0;
    std::string line(1024, '\0');

    while(std::getline(*stream, line) && nlines < MAX_LINES)
    {
        if(!line.empty() && line[0] != '#' && !starts_with(line, "track") && !starts_with(line, "browser"))
        {
            if(re.match(line))
                ++valid;
            else
                ++invalid;
            ++nlines;
        }
    }

    return nlines > 0 && valid >= invalid;
}

bool format_guess::_parseBedgraphFormat(std::istream *stream)
{
    pcre2cpp re; re.compile("^[\\x21-\\x7E]+[\\t\\s]+\\d+[\\t\\s]+\\d+[\\t\\s]+[+-]?(?=\\d*[\\d.eE])(?=\\.?\\d)\\d*\\.?\\d*(?:[eE][+-]?\\d+)?.*$");
    return _parse(stream, re);
}

bool format_guess::_parseBedFormat(std::istream *stream, unsigned int n)
{
    // Build the regex according to the number of columns.

    std::string restr = "^"; // begin the regex
    const std::vector<std::string> reparts
    {
        "[\\x21-\\x7E]+" /* chromosome */,
        "[\\t\\s]+\\d+" /* chrom start */,
        "[\\t\\s]+\\d+" /* chrom end */ /* bed3*/,
        "[\\t\\s]+[\\x21-\\x7E]+" /* name */ /* bed4*/,
        "[\\t\\s]+(\\.|[0-9]|[0-9][0-9]|[0-9][0-9][0-9]|1000)" /* score */ /* bed5 */,
        "[\\t\\s]+[\\.\\+\\-]" /* strand */ /* bed6 */,
        "[\\t\\s]+\\d+" /* thick start */ /* bed7 */,
        "[\\t\\s]+\\d+" /* thick end */ /* bed8 */,
        "[\\t\\s]+(0|([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]),([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]),([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]))" /* item rgb */ /* bed9 */,
        "[\\t\\s]+\\d+" /* block count */ /* bed10 */,
        "[\\t\\s]+(\\d+,?)+" /* block sizes */ /* bed11 */,
        "[\\t\\s]+(\\d+,?)+" /* block starts */ /* bed12 */,
    };

    // Construct the regex based on the number of columns
    for(unsigned int i = 0; i < n; ++i) restr += reparts[i];
    restr += ".*$"; // close the regex

    // pcre2cpp re; re.compile("^[\\x21-\\x7E]+[\\t\\s]+\\d+[\\t\\s]+\\d+([\\t\\s]+[\\x21-\\x7E]+)?([\\t\\s]+(\\.|[0-9]|[0-9][0-9]|[0-9][0-9][0-9]|1000))?([\\t\\s]+[\\.\\+\\-])?([\\t\\s]+\\d+)?([\\t\\s]+\\d+)?([\\t\\s]+(0|([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]),([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]),([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])))?([\\t\\s]+\\d+)?([\\t\\s]+(\\d+,?)+)?([\\t\\s]+(\\d+,?)+)?(?:[\\t\\s]*\\#.*)?$");
    pcre2cpp re; re.compile(restr);
    return _parse(stream, re);
}

bool format_guess::_parseBigwigFormat(std::istream *stream)
{
    std::string line;
    if(std::getline(*stream, line) && !line.empty() && line.size() > 4)
        return line[0] == (char) 0x26 && line[1] == (char) 0xFC && line[2] == (char) 0x8F && line[3] == (char) 0x88;
    else
        return false;
}

bool format_guess::_parseBigbedFormat(std::istream *stream)
{
    std::string line;
    if(std::getline(*stream, line) && !line.empty() && line.size() > 4)
        return line[0] == (char) 0xEB && line[1] == (char) 0xF2 && line[2] == (char) 0x89 && line[3] == (char) 0x87;
    else
        return false;
}

bool format_guess::_parseReferenceCoordinatesFormat(std::istream *stream, unsigned int n)
{
    pcre2cpp re; re.compile("^[\\x21-\\x7E]+[\\t\\s]+[\\.\\+\\-](?:[\\t\\s]+-?\\d+){" + std::to_string(n) + "}(?:[\\t\\s]+[\\x21-\\x7E]+)?.*$");
    return _parse(stream, re);
}

bool format_guess::_parseGenepredExtendedFormat(std::istream *stream)
{
    pcre2cpp re; re.compile("^[\\x21-\\x7E]+[\\t\\s]+[\\x21-\\x7E]+[\\t\\s]+[\\.\\+-][\\t\\s]+(?:\\d+[\\t\\s]+){4}\\d+[\\t\\s]+(?:[-\\d,]+[\\t\\s]+){2}\\d+[\\t\\s]+[\\x21-\\x7E]+[\\t\\s]+(?:(?:none|unk|incmpl|cmpl)[\\t\\s]+){2}[0-2\\.]+.*$");
    return _parse(stream, re);
}

bool format_guess::_parseGenepredFormat(std::istream *stream)
{
    pcre2cpp re; re.compile("^[\\x21-\\x7E]+[\\t\\s]+[\\x21-\\x7E]+[\\t\\s]+[\\.\\+-][\\t\\s]+(?:\\d+[\\t\\s]+){4}\\d+[\\t\\s]+[-\\d,]+[\\t\\s]+[-\\d,]+.*$");
    return _parse(stream, re);
}

bool format_guess::_parseGtfFormat(std::istream *stream)
{
    pcre2cpp re; re.compile("^[\\x21-\\x7E]+[\\t\\s]+[\\x21-\\x7E]+[\\t\\s]+[\\x21-\\x7E]+[\\t\\s]+\\d+[\\t\\s]+\\d+[\\t\\s]+(?:\\.|\\d+)[\\t\\s]+[\\.\\+-][\\t\\s]+[0-2\\.]+[\\t\\s]+gene_id\\s[\\x21-\\x7E]+;\\stranscript_id\\s[\\x21-\\x7E]+;.*$");
    return _parse(stream, re);
}

bool format_guess::_parseWigFormat(std::istream *stream)
{
    pcre2cpp re; re.compile("^(?:variableStep\\s+chrom=[\\x21-\\x7E]+(?:\\s+span=\\d+)?)|(?:\\d+[\\t\\s]+[+-]?(?=\\d*[\\d.eE])(?=\\.?\\d)\\d*\\.?\\d*(?:[eE][+-]?\\d+)?)|(?:fixedStep\\s+chrom=[\\x21-\\x7E]+\\s+start=\\d+\\s+step=\\d+(?:\\s+span=\\d+)?)|(?:[+-]?(?=\\d*[\\d.eE])(?=\\.?\\d)\\d*\\.?\\d*(?:[eE][+-]?\\d+)?)$");
    return _parse(stream, re);
}

bool format_guess::_tryGzip(const std::string &file, const std::function<bool(std::istream *)> &func)
{
    if(tryGzipFormat(file))
    {
        using namespace boost::iostreams;

        std::ifstream ifs(file, std::ios_base::binary | std::ios_base::in);
        try
        {
            filtering_istream fis;
            fis.push(gzip_decompressor());
            fis.push(ifs);

            // This functor should be bind to func(_1, ...)
            return func(&fis);
        }
        catch(const gzip_error &e)
        {
            report_error(std::cerr, e.what());
        }
    }

    return false;
}

bool format_guess::_tryBzip2(const std::string &file, const std::function<bool(std::istream *)> &func)
{
    if(tryBzip2Format(file))
    {
        using namespace boost::iostreams;

        std::ifstream ifs(file, std::ios_base::binary | std::ios_base::in);
        try
        {
            filtering_istream fis;
            fis.push(bzip2_decompressor());
            fis.push(ifs);

            // This functor should be bind to func(_1, ...)
            return func(&fis);
        }
        catch(const bzip2_error &e)
        {
            report_error(std::cerr, e.what());
        }
    }

    return false;
}

////////////////////////////////////////////////////////////////////
// Extensions

/**
 * Checks if @a file ends with a ".gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".gz" or ".gzip" extension, @c false otherwise.
 */
bool format_guess::isGzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bz2" extension, @c false otherwise.
 */
bool format_guess::isBzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bg" or ".bedgraph" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bg" or ".bedgraph" extension, @c false otherwise.
 */
bool format_guess::isBedgraphExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BEDGRAPH_LONG_EXT) || boost::algorithm::ends_with(tmp, BEDGRAPH_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".bg.gz" or ".bedgraph.gz" or ".bg.gzip" or ".bedgraph.gzip" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bg" or ".bedgraph" extension, @c false otherwise.
 */
bool format_guess::isBedgraphGzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BEDGRAPH_LONG_GZIP_EXT) || boost::algorithm::ends_with(tmp, BEDGRAPH_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bg" or ".bedgraph" or ".bg.bz2" or ".bedgraph.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bg" or ".bedgraph" extension, @c false otherwise.
 */
bool format_guess::isBedgraphBzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BEDGRAPH_LONG_BZIP2_EXT) || boost::algorithm::ends_with(tmp, BEDGRAPH_SHORT_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bed" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed" extension, @c false otherwise.
 */
bool format_guess::isBedExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED_LONG_EXT);
}

/**
 * Checks if @a file ends with a ".bed.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed.gz" extension, @c false otherwise.
 */
bool format_guess::isBedGzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED_LONG_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bed.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed.bz2" extension, @c false otherwise.
 */
bool format_guess::isBedBzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED_LONG_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bed3" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed3" extension, @c false otherwise.
 */
bool format_guess::isBed3Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED3_LONG_EXT);
}

/**
 * Checks if @a file ends with a ".bed3.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed3.gz" extension, @c false otherwise.
 */
bool format_guess::isBed3GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED3_LONG_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bed3.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed3.bz2" extension, @c false otherwise.
 */
bool format_guess::isBed3Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED3_LONG_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bed4" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed4" extension, @c false otherwise.
 */
bool format_guess::isBed4Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED4_LONG_EXT);
}

/**
 * Checks if @a file ends with a ".bed4.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed4.gz" extension, @c false otherwise.
 */
bool format_guess::isBed4GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED4_LONG_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bed4.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed4.bz2" extension, @c false otherwise.
 */
bool format_guess::isBed4Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED4_LONG_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bed5" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed5" extension, @c false otherwise.
 */
bool format_guess::isBed5Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED5_LONG_EXT);
}

/**
 * Checks if @a file ends with a ".bed5.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed5.gz" extension, @c false otherwise.
 */
bool format_guess::isBed5GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED5_LONG_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bed5.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed5.bz2" extension, @c false otherwise.
 */
bool format_guess::isBed5Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED5_LONG_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bed6" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed6" extension, @c false otherwise.
 */
bool format_guess::isBed6Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED6_LONG_EXT);
}

/**
 * Checks if @a file ends with a ".bed6.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed6.gz" extension, @c false otherwise.
 */
bool format_guess::isBed6GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED6_LONG_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bed6.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed6.bz2" extension, @c false otherwise.
 */
bool format_guess::isBed6Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED6_LONG_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bed7" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed7" extension, @c false otherwise.
 */
bool format_guess::isBed7Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED7_LONG_EXT);
}

/**
 * Checks if @a file ends with a ".bed7.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed7.gz" extension, @c false otherwise.
 */
bool format_guess::isBed7GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED7_LONG_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bed7.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed7.bz2" extension, @c false otherwise.
 */
bool format_guess::isBed7Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED7_LONG_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bed8" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed8" extension, @c false otherwise.
 */
bool format_guess::isBed8Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED8_LONG_EXT);
}

/**
 * Checks if @a file ends with a ".bed8.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed8.gz" extension, @c false otherwise.
 */
bool format_guess::isBed8GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED8_LONG_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bed8.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed8.bz2" extension, @c false otherwise.
 */
bool format_guess::isBed8Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED8_LONG_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bed9" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed9" extension, @c false otherwise.
 */
bool format_guess::isBed9Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED9_LONG_EXT);
}

/**
 * Checks if @a file ends with a ".bed9.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed9.gz" extension, @c false otherwise.
 */
bool format_guess::isBed9GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED9_LONG_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bed9.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed9.bz2" extension, @c false otherwise.
 */
bool format_guess::isBed9Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED9_LONG_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bed10" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed10" extension, @c false otherwise.
 */
bool format_guess::isBed10Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED10_LONG_EXT);
}

/**
 * Checks if @a file ends with a ".bed10.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed10.gz" extension, @c false otherwise.
 */
bool format_guess::isBed10GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED10_LONG_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bed10.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed10.bz2" extension, @c false otherwise.
 */
bool format_guess::isBed10Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED10_LONG_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bed11" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed11" extension, @c false otherwise.
 */
bool format_guess::isBed11Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED11_LONG_EXT);
}

/**
 * Checks if @a file ends with a ".bed11.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed11.gz" extension, @c false otherwise.
 */
bool format_guess::isBed11GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED11_LONG_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bed11.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed11.bz2" extension, @c false otherwise.
 */
bool format_guess::isBed11Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED11_LONG_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bed12" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed12" extension, @c false otherwise.
 */
bool format_guess::isBed12Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED12_LONG_EXT);
}

/**
 * Checks if @a file ends with a ".bed12.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed12.gz" extension, @c false otherwise.
 */
bool format_guess::isBed12GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED12_LONG_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bed12.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bed12.bz2" extension, @c false otherwise.
 */
bool format_guess::isBed12Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BED12_LONG_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".coord" or a ".rc" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord"" ext or a ".rc" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinatesExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES_LONG_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".coord.gz"  or a ".rc.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord.gz"  or a ".rc.gz" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinatesGzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES_LONG_GZIP_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".coord.bz2" or a ".rc.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord.bz2" or a ".rc.bz2" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinatesBzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES_LONG_BZIP2_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES_SHORT_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".coord1" or a ".rc1" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord1"" ext or a ".rc1" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates1Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES1_LONG_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES1_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".coord1.gz"  or a ".rc1.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord1.gz"  or a ".rc1.gz" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates1GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES1_LONG_GZIP_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES1_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".coord1.bz2" or a ".rc1.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord1.bz2" or a ".rc1.bz2" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates1Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES1_LONG_BZIP2_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES1_SHORT_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".coord2" or a ".rc2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord2"" ext or a ".rc2" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES2_LONG_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES2_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".coord2.gz"  or a ".rc2.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord2.gz"  or a ".rc2.gz" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates2GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES2_LONG_GZIP_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES2_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".coord2.bz2" or a ".rc2.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord2.bz2" or a ".rc2.bz2" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates2Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES2_LONG_BZIP2_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES2_SHORT_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".coord3" or a ".rc3" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord3"" ext or a ".rc3" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates3Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES3_LONG_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES3_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".coord3.gz"  or a ".rc3.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord3.gz"  or a ".rc3.gz" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates3GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES3_LONG_GZIP_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES3_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".coord3.bz2" or a ".rc3.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord3.bz2" or a ".rc3.bz2" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates3Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES3_LONG_BZIP2_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES3_SHORT_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".coord4" or a ".rc4" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord4"" ext or a ".rc4" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates4Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES4_LONG_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES4_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".coord4.gz"  or a ".rc4.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord4.gz"  or a ".rc4.gz" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates4GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES4_LONG_GZIP_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES4_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".coord4.bz4" or a ".rc4.bz4" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord4.bz4" or a ".rc4.bz4" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates4Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES4_LONG_BZIP2_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES4_SHORT_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".coord5" or a ".rc5" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord5"" ext or a ".rc5" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates5Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES5_LONG_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES5_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".coord5.gz"  or a ".rc5.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord5.gz"  or a ".rc5.gz" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates5GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES5_LONG_GZIP_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES5_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".coord5.bz5" or a ".rc5.bz5" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord5.bz5" or a ".rc5.bz5" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates5Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES5_LONG_BZIP2_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES5_SHORT_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".coord6" or a ".rc6" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord6"" ext or a ".rc6" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates6Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES6_LONG_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES6_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".coord6.gz"  or a ".rc6.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord6.gz"  or a ".rc6.gz" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates6GzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES6_LONG_GZIP_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES6_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".coord6.bz6" or a ".rc6.bz6" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".coord6.bz6" or a ".rc6.bz6" extension, @c false otherwise.
 */
bool format_guess::isReferenceCoordinates6Bzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES6_LONG_BZIP2_EXT) || boost::algorithm::ends_with(tmp, REFERENCE_COORDINATES6_SHORT_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bw" or ".bigwig" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bw" or ".bigwig" extension, @c false otherwise.
 */
bool format_guess::isBigwigExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BIGWIG_LONG_EXT) || boost::algorithm::ends_with(tmp, BIGWIG_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".bw.gz" or ".bigwig.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bw.gz" or ".bigwig.gz" extension, @c false otherwise.
 */
bool format_guess::isBigwigGzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BIGWIG_LONG_GZIP_EXT) || boost::algorithm::ends_with(tmp, BIGWIG_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bw.bz2" or ".bigwig.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bw.bz2" or ".bigwig.bz2" extension, @c false otherwise.
 */
bool format_guess::isBigwigBzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BIGWIG_LONG_BZIP2_EXT) || boost::algorithm::ends_with(tmp, BIGWIG_SHORT_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".bb" or ".bigbed" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bb" or ".bigbed" extension, @c false otherwise.
 */
bool format_guess::isBigbedExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BIGBED_LONG_EXT) || boost::algorithm::ends_with(tmp, BIGBED_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".bb.gz" or ".bigbed.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bb.gz" or ".bigbed.gz" extension, @c false otherwise.
 */
bool format_guess::isBigbedGzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BIGBED_LONG_GZIP_EXT) || boost::algorithm::ends_with(tmp, BIGBED_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".bb.bz2" or ".bigbed.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".bb.bz2" or ".bigbed.bz2" extension, @c false otherwise.
 */
bool format_guess::isBigbedBzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, BIGBED_LONG_BZIP2_EXT) || boost::algorithm::ends_with(tmp, BIGBED_SHORT_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".gp" or ".genepred" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".gp" or ".genepred" extension, @c false otherwise.
 */
bool format_guess::isGenepredExtendedExtension(const std::string &file)
{
    return isGenepredExtension(file);
}

/**
 * Checks if @a file ends with a ".gp.gz" or ".genepred.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".gp.gz" or ".genepred.gz" extension, @c false otherwise.
 */
bool format_guess::isGenepredExtendedGzipExtension(const std::string &file)
{
    return isGenepredGzipExtension(file);
}

/**
 * Checks if @a file ends with a ".gp.bz2" or ".genepred.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".gp.bz2" or ".genepred.bz2" extension, @c false otherwise.
 */
bool format_guess::isGenepredExtendedBzip2Extension(const std::string &file)
{
    return isGenepredBzip2Extension(file);
}

/**
 * Checks if @a file ends with a ".gp" or ".genepred" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".gp" or ".genepred" extension, @c false otherwise.
 */
bool format_guess::isGenepredExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, GENEPRED_LONG_EXT) || boost::algorithm::ends_with(tmp, GENEPRED_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".gp.gz" or ".genepred.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".gp.gz" or ".genepred.gz" extension, @c false otherwise.
 */
bool format_guess::isGenepredGzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, GENEPRED_LONG_GZIP_EXT) || boost::algorithm::ends_with(tmp, GENEPRED_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".gp.bz2" or ".genepred.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".gp.bz2" or ".genepred.bz2" extension, @c false otherwise.
 */
bool format_guess::isGenepredBzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, GENEPRED_LONG_BZIP2_EXT) || boost::algorithm::ends_with(tmp, GENEPRED_SHORT_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".gtf" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".gtf" extension, @c false otherwise.
 */
bool format_guess::isGtfExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, GTF_LONG_EXT);
}

/**
 * Checks if @a file ends with a .gtf.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".gtf.gz" extension, @c false otherwise.
 */
bool format_guess::isGtfGzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, GTF_LONG_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".gtf.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".gtf.bz2" extension, @c false otherwise.
 */
bool format_guess::isGtfBzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, GTF_LONG_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".wiggle" or ".wig" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a .wiggle" or ".wig" extension, @c false otherwise.
 */
bool format_guess::isWigExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, WIGGLE_LONG_EXT) || boost::algorithm::ends_with(tmp, WIGGLE_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".wiggle.gz" or ".wig.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".wiggle.gz" or ".wig.gz" extension, @c false otherwise.
 */
bool format_guess::isWigGzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, WIGGLE_LONG_GZIP_EXT) || boost::algorithm::ends_with(tmp, WIGGLE_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".wiggle.bz2" or ".wig.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".wiggle.bz2" or ".wig.bz2" extension, @c false otherwise.
 */
bool format_guess::isWigBzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, WIGGLE_LONG_BZIP2_EXT) || boost::algorithm::ends_with(tmp, WIGGLE_SHORT_BZIP2_EXT);
}


/**
 * Checks if @a file ends with a ".ra" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".ra" extension, @c false otherwise.
 */
bool format_guess::isReferenceAnnotationsExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_ANNOTATIONS_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".ra.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".ra.gz" extension, @c false otherwise.
 */
bool format_guess::isReferenceAnnotationsGzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_ANNOTATIONS_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".ra.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".ra.bz2" extension, @c false otherwise.
 */
bool format_guess::isReferenceAnnotationsBzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_ANNOTATIONS_SHORT_BZIP2_EXT);
}

/**
 * Checks if @a file ends with a ".rf" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".rf" extension, @c false otherwise.
 */
bool format_guess::isReferenceFiltersExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_FILTERS_SHORT_EXT);
}

/**
 * Checks if @a file ends with a ".rf.gz" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".rf.gz" extension, @c false otherwise.
 */
bool format_guess::isReferenceFiltersGzipExtension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_FILTERS_SHORT_GZIP_EXT);
}

/**
 * Checks if @a file ends with a ".rf.bz2" extension.
 *
 * @param file File.
 *
 * @return @c true when @a file ends with a ".rf.bz2" extension, @c false otherwise.
 */
bool format_guess::isReferenceFiltersBzip2Extension(const std::string &file)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, REFERENCE_FILTERS_SHORT_BZIP2_EXT);
}

////////////////////////////////////////////////////////////////////
// Discovery

/**
 * Checks if @a file is Gzip compressed by reading its magic number.
 *
 * @param file File.
 *
 * @return @c true when @a file is Gzip compressed, @c false otherwise.
 */
bool format_guess::tryGzipFormat(const std::string &file)
{
    // When file does not exists, `false` is returned.
    std::ifstream ifs(file, std::ios_base::binary | std::ios_base::in);
    std::string line = "";

    if(std::getline(ifs, line) && !line.empty() && line.size() > 2)
        return line[0] == (char) 0x1f && line[1] == (char) 0x8b;

    return false;
}

/**
 * Checks if @a file is Bzip2 compressed by reading its magic number.
 *
 * @param file File.
 *
 * @return @c true when @a file is Bzip2 compressed, @c false otherwise.
 */
bool format_guess::tryBzip2Format(const std::string &file)
{
    // When file does not exists, `false` is returned.
    std::ifstream ifs(file, std::ios_base::binary | std::ios_base::in);
    std::string line = "";

    if(std::getline(ifs, line) && !line.empty() && line.size() > 4)
        return line[0] == 'B' && line[1] == 'Z' && line[2] == 'h' && line[3] >= '1' && line[3] <= '9';

    return false;
}

/**
 * Checks if @a file is of Bedgraph format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bedgraph, @c false otherwise.
 */
bool format_guess::tryBedgraphFormat(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseBedgraphFormat(&ifs) : false;
}

/**
 * Checks if @a file is of Bedgraph Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bedgraph, @c false otherwise.
 */
bool format_guess::tryBedgraphGzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseBedgraphFormat, std::placeholders::_1));
}

/**
 * Checks if @a file is of Bedgraph Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bedgraph, @c false otherwise.
 */
bool format_guess::tryBedgraphBzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseBedgraphFormat, std::placeholders::_1));
}

/**
 * Checks if @a file is of Bed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed, @c false otherwise.
 */
bool format_guess::tryBedFormat(const std::string &file)
{
    std::ifstream ifs(file);
    if(ifs) for(unsigned int n = 3; n <= 12; ++n) if(_parseBedFormat(&ifs, n)) return true;
    return false;
}

/**
 * Checks if @a file is of Bed Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed, @c false otherwise.
 */
bool format_guess::tryBedGzipFormat(const std::string &file)
{
    auto lam_parseBed = [](std::istream *ifs) { for(unsigned int n = 3; n <= 12; ++n) if(_parseBedFormat(ifs, n)) return true; };

    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(lam_parseBed, std::placeholders::_1));
}

/**
 * Checks if @a file is of Bed Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed, @c false otherwise.
 */
bool format_guess::tryBedBzip2Format(const std::string &file)
{
    auto lam_parseBed = [](std::istream *ifs) { for(unsigned int n = 3; n <= 12; ++n) if(_parseBedFormat(ifs, n)) return true; };

    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(lam_parseBed, std::placeholders::_1));
}

/**
 * Checks if @a file is of Bed3 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed3, @c false otherwise.
 */
bool format_guess::tryBed3Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseBedFormat(&ifs, 3) : false;
}

/**
 * Checks if @a file is of Bed3 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed3, @c false otherwise.
 */
bool format_guess::tryBed3GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseBedFormat, std::placeholders::_1, 3));
}

/**
 * Checks if @a file is of Bed3 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed3, @c false otherwise.
 */
bool format_guess::tryBed3Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseBedFormat, std::placeholders::_1, 3));
}

/**
 * Checks if @a file is of Bed4 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed4, @c false otherwise.
 */
bool format_guess::tryBed4Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseBedFormat(&ifs, 4) : false;
}

/**
 * Checks if @a file is of Bed4 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed4, @c false otherwise.
 */
bool format_guess::tryBed4GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseBedFormat, std::placeholders::_1, 4));
}

/**
 * Checks if @a file is of Bed4 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed4, @c false otherwise.
 */
bool format_guess::tryBed4Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseBedFormat, std::placeholders::_1, 4));
}

/**
 * Checks if @a file is of Bed5 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed5, @c false otherwise.
 */
bool format_guess::tryBed5Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseBedFormat(&ifs, 5) : false;
}

/**
 * Checks if @a file is of Bed5 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed5, @c false otherwise.
 */
bool format_guess::tryBed5GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseBedFormat, std::placeholders::_1, 5));
}

/**
 * Checks if @a file is of Bed5 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed5, @c false otherwise.
 */
bool format_guess::tryBed5Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseBedFormat, std::placeholders::_1, 5));
}

/**
 * Checks if @a file is of Bed6 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed6, @c false otherwise.
 */
bool format_guess::tryBed6Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseBedFormat(&ifs, 6) : false;
}

/**
 * Checks if @a file is of Bed6 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed6, @c false otherwise.
 */
bool format_guess::tryBed6GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseBedFormat, std::placeholders::_1, 6));
}

/**
 * Checks if @a file is of Bed6 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed6, @c false otherwise.
 */
bool format_guess::tryBed6Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseBedFormat, std::placeholders::_1, 6));
}

/**
 * Checks if @a file is of Bed7 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed7, @c false otherwise.
 */
bool format_guess::tryBed7Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseBedFormat(&ifs, 7) : false;
}

/**
 * Checks if @a file is of Bed7 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed7, @c false otherwise.
 */
bool format_guess::tryBed7GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseBedFormat, std::placeholders::_1, 7));
}

/**
 * Checks if @a file is of Bed7 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed7, @c false otherwise.
 */
bool format_guess::tryBed7Bzip2Format(const std::string &file)
{
       // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseBedFormat, std::placeholders::_1, 7));
}

/**
 * Checks if @a file is of Bed8 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed8, @c false otherwise.
 */
bool format_guess::tryBed8Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseBedFormat(&ifs, 8) : false;
}

/**
 * Checks if @a file is of Bed8 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed8, @c false otherwise.
 */
bool format_guess::tryBed8GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseBedFormat, std::placeholders::_1, 8));
}

/**
 * Checks if @a file is of Bed8 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed8, @c false otherwise.
 */
bool format_guess::tryBed8Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseBedFormat, std::placeholders::_1, 8));
}

/**
 * Checks if @a file is of Bed9 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed9, @c false otherwise.
 */
bool format_guess::tryBed9Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseBedFormat(&ifs, 9) : false;
}

/**
 * Checks if @a file is of Bed9 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed9, @c false otherwise.
 */
bool format_guess::tryBed9GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseBedFormat, std::placeholders::_1, 9));
}

/**
 * Checks if @a file is of Bed9 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed9, @c false otherwise.
 */
bool format_guess::tryBed9Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseBedFormat, std::placeholders::_1, 9));
}

/**
 * Checks if @a file is of Bed10 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed10, @c false otherwise.
 */
bool format_guess::tryBed10Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseBedFormat(&ifs, 10) : false;
}

/**
 * Checks if @a file is of Bed10 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed10, @c false otherwise.
 */
bool format_guess::tryBed10GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseBedFormat, std::placeholders::_1, 10));
}

/**
 * Checks if @a file is of Bed10 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed10, @c false otherwise.
 */
bool format_guess::tryBed10Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseBedFormat, std::placeholders::_1, 10));
}

/**
 * Checks if @a file is of Bed11 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed11, @c false otherwise.
 */
bool format_guess::tryBed11Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseBedFormat(&ifs, 11) : false;
}

/**
 * Checks if @a file is of Bed11 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed11, @c false otherwise.
 */
bool format_guess::tryBed11GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseBedFormat, std::placeholders::_1, 11));
}

/**
 * Checks if @a file is of Bed11 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed11, @c false otherwise.
 */
bool format_guess::tryBed11Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseBedFormat, std::placeholders::_1, 11));
}

/**
 * Checks if @a file is of Bed12 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed12, @c false otherwise.
 */
bool format_guess::tryBed12Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseBedFormat(&ifs, 12) : false;
}

/**
 * Checks if @a file is of Bed12 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed12, @c false otherwise.
 */
bool format_guess::tryBed12GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseBedFormat, std::placeholders::_1, 12));
}

/**
 * Checks if @a file is of Bed12 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bed12, @c false otherwise.
 */
bool format_guess::tryBed12Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseBedFormat, std::placeholders::_1, 12));
}

/**
 * Checks if @a file is of ReferenceCoordinates format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinatesFormat(const std::string &file)
{
    std::ifstream ifs(file);
    if(ifs) for(unsigned int n = 1; n <= 6; ++n) if(_parseReferenceCoordinatesFormat(&ifs, n)) return true;
    return false;
}

/**
 * Checks if @a file is of ReferenceCoordinates Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinatesGzipFormat(const std::string &file)
{
    auto lam_refcoord= [](std::istream *ifs) { for(unsigned int n = 1; n <= 6; ++n) if(_parseReferenceCoordinatesFormat(ifs, n)) return true; };

    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(lam_refcoord, std::placeholders::_1));
}

/**
 * Checks if @a file is of ReferenceCoordinates Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinatesBzip2Format(const std::string &file)
{
    auto lam_refcoord= [](std::istream *ifs) { for(unsigned int n = 1; n <= 6; ++n) if(_parseReferenceCoordinatesFormat(ifs, n)) return true; };

    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(lam_refcoord, std::placeholders::_1));
}

/**
 * Checks if @a file is of ReferenceCoordinates1 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates1, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates1Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseReferenceCoordinatesFormat(&ifs, 1) : false;
}

/**
 * Checks if @a file is of ReferenceCoordinates1 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates1, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates1GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseReferenceCoordinatesFormat, std::placeholders::_1, 1));
}

/**
 * Checks if @a file is of ReferenceCoordinates1 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates1, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates1Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseReferenceCoordinatesFormat, std::placeholders::_1, 1));
}

/**
 * Checks if @a file is of ReferenceCoordinates2 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates2, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates2Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseReferenceCoordinatesFormat(&ifs, 2) : false;
}

/**
 * Checks if @a file is of ReferenceCoordinates2 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates2, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates2GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseReferenceCoordinatesFormat, std::placeholders::_1, 2));
}

/**
 * Checks if @a file is of ReferenceCoordinates2 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates2, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates2Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseReferenceCoordinatesFormat, std::placeholders::_1, 2));
}

/**
 * Checks if @a file is of ReferenceCoordinates3 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates3, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates3Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseReferenceCoordinatesFormat(&ifs, 3) : false;
}

/**
 * Checks if @a file is of ReferenceCoordinates3 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates3, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates3GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseReferenceCoordinatesFormat, std::placeholders::_1, 3));
}

/**
 * Checks if @a file is of ReferenceCoordinates3 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates3, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates3Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseReferenceCoordinatesFormat, std::placeholders::_1, 3));
}

/**
 * Checks if @a file is of ReferenceCoordinates4 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates4, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates4Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseReferenceCoordinatesFormat(&ifs, 4) : false;
}

/**
 * Checks if @a file is of ReferenceCoordinates4 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates4, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates4GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseReferenceCoordinatesFormat, std::placeholders::_1, 4));
}

/**
 * Checks if @a file is of ReferenceCoordinates4 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates4, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates4Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseReferenceCoordinatesFormat, std::placeholders::_1, 4));
}

/**
 * Checks if @a file is of ReferenceCoordinates5 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates5, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates5Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ?_parseReferenceCoordinatesFormat(&ifs, 5) : false;
}

/**
 * Checks if @a file is of ReferenceCoordinates5 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates5, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates5GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseReferenceCoordinatesFormat, std::placeholders::_1, 5));
}

/**
 * Checks if @a file is of ReferenceCoordinates5 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates5, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates5Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseReferenceCoordinatesFormat, std::placeholders::_1, 5));
}

/**
 * Checks if @a file is of ReferenceCoordinates6 format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates6, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates6Format(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseReferenceCoordinatesFormat(&ifs, 6) : false;
}

/**
 * Checks if @a file is of ReferenceCoordinates6 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates6, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates6GzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseReferenceCoordinatesFormat, std::placeholders::_1, 6));
}

/**
 * Checks if @a file is of ReferenceCoordinates6 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a ReferenceCoordinates6, @c false otherwise.
 */
bool format_guess::tryReferenceCoordinates6Bzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseReferenceCoordinatesFormat, std::placeholders::_1, 6));
}

/**
 * Checks if @a file is of Bigwig format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bigwig, @c false otherwise.
 */
bool format_guess::tryBigwigFormat(const std::string &file)
{
    std::ifstream ifs(file, std::ios_base::binary | std::ios_base::in);
    return ifs ? _parseBigwigFormat(&ifs) : false;
}

/**
 * Checks if @a file is of Bigwig format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bigwig, @c false otherwise.
 */
bool format_guess::tryBigwigGzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseBigwigFormat, std::placeholders::_1));
}

/**
 * Checks if @a file is of Bigwig format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bigwig, @c false otherwise.
 */
bool format_guess::tryBigwigBzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseBigwigFormat, std::placeholders::_1));
}

/**
 * Checks if @a file is of Bigbed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bigbed, @c false otherwise.
 */
bool format_guess::tryBigbedFormat(const std::string &file)
{
    std::ifstream ifs(file, std::ios_base::binary | std::ios_base::in);
    return ifs ? _parseBigbedFormat(&ifs) : false;
}

/**
 * Checks if @a file is of Bigbed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bigbed, @c false otherwise.
 */
bool format_guess::tryBigbedGzipFormat(const std::string &file)
{
        // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseBigbedFormat, std::placeholders::_1));
}

/**
 * Checks if @a file is of Bigbed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Bigbed, @c false otherwise.
 */
bool format_guess::tryBigbedBzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseBigbedFormat, std::placeholders::_1));
}

/**
 * Checks if @a file is of GenepredExtended format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a GenepredExtended, @c false otherwise.
 */
bool format_guess::tryGenepredExtendedFormat(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseGenepredExtendedFormat(&ifs) : false;
}

/**
 * Checks if @a file is of GenepredExtended Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a GenepredExtended, @c false otherwise.
 */
bool format_guess::tryGenepredExtendedGzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseGenepredExtendedFormat, std::placeholders::_1));
}

/**
 * Checks if @a file is of GenepredExtended Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a GenepredExtended, @c false otherwise.
 */
bool format_guess::tryGenepredExtendedBzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseGenepredExtendedFormat, std::placeholders::_1));
}

/**
 * Checks if @a file is of Genepred format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Genepred, @c false otherwise.
 */
bool format_guess::tryGenepredFormat(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseGenepredFormat(&ifs) : false;
}

/**
 * Checks if @a file is of Genepred Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Genepred, @c false otherwise.
 */
bool format_guess::tryGenepredGzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseGenepredFormat, std::placeholders::_1));
}

/**
 * Checks if @a file is of Genepred Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Genepred, @c false otherwise.
 */
bool format_guess::tryGenepredBzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseGenepredFormat, std::placeholders::_1));
}

/**
 * Checks if @a file is of GTF format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a GTF, @c false otherwise.
 */
bool format_guess::tryGtfFormat(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseGtfFormat(&ifs) : false;
}

/**
 * Checks if @a file is of GTF Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a GTF, @c false otherwise.
 */
bool format_guess::tryGtfGzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseGtfFormat, std::placeholders::_1));
}

/**
 * Checks if @a file is of GTF Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a GTF, @c false otherwise.
 */
bool format_guess::tryGtfBzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseGtfFormat, std::placeholders::_1));
}

/**
 * Checks if @a file is of Wig format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Wig, @c false otherwise.
 */
bool format_guess::tryWigFormat(const std::string &file)
{
    std::ifstream ifs(file);
    return ifs ? _parseWigFormat(&ifs) : false;
}

/**
 * Checks if @a file is of Wig Gzip compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Wig, @c false otherwise.
 */
bool format_guess::tryWigGzipFormat(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryGzip(file, std::bind(&_parseWigFormat, std::placeholders::_1));
}

/**
 * Checks if @a file is of Wig Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
 *
 * @param file File.
 *
 * @return @c true when @a file is a Wig, @c false otherwise.
 */
bool format_guess::tryWigBzip2Format(const std::string &file)
{
    // bind to func(_1, ...) where _1 will be the filtering_istream
    return _tryBzip2(file, std::bind(&_parseWigFormat, std::placeholders::_1));
}

////////////////////////////////////////////////////////////////////
// Helper

bool format_guess::hasKnownExtension(const std::string &file)
{
    for(const auto &ext : SUPPORTED_EXTENSIONS)
        if(hasExtension(file, ext))
            return true;

    return false;
}

bool format_guess::hasExtension(const std::string &file, const std::string &extension)
{
    std::string tmp = boost::algorithm::to_lower_copy(file);
    return boost::algorithm::ends_with(tmp, extension);
}

bool format_guess::hasExtension(const std::string &file)
{
    pcre2cpp re; re.compile("\\..+$");
    return re.match(file);
}

/**
 * Checks if @a file is Gzip compressed.
 *
 * @param file File.
 *
 * @return @c true when @a file is Gzip compressed, @c false otherwise.
 */
bool format_guess::isGzipCompressed(const std::string &file)
{
    return isGzipExtension(file) || tryGzipFormat(file);
}

/**
 * Checks if @a file is Bzip2 compressed.
 *
 * @param file File.
 *
 * @return @c true when @a file is Bzip2 compressed, @c false otherwise.
 */
bool format_guess::isBzip2Compressed(const std::string &file)
{
    return isBzip2Extension(file) || tryBzip2Format(file);
}

io::file_format format_guess::discover(const std::string &fname)
{
    // For robustness, do exhaustive checks of extensions and format (even if cumbersome ...)
    // The following format are allowed:
    //         - filename.ext1.ext2
    //         - filename.ext1
    //         - filename

    // WARNING : Conditions order is VERY important. The conditions must be from the most elaborate to the simplier.
    // Per example, Genepred Extended MUST come before Genepred format because both format have the same first ten columns.
    // We assume that the file exists, otherwise, expect undefined results.

    // Note: In order to differentiate Genepred of GenepredExtended, we need to look inside the files.

    // TODO: Use Boost.MultiIndex for beauty and search.
    using namespace io;
    // check extension first
    if(isBedgraphExtension(fname) || isBedgraphGzipExtension(fname) || isBedgraphBzip2Extension(fname)) return file_format::BEDGRAPH;
    else if(isBed3Extension(fname) || isBed3GzipExtension(fname) || isBed3Bzip2Extension(fname)) return file_format::BED3;
    else if(isBed4Extension(fname) || isBed4GzipExtension(fname) || isBed4Bzip2Extension(fname)) return file_format::BED4;
    else if(isBed5Extension(fname) || isBed5GzipExtension(fname) || isBed5Bzip2Extension(fname)) return file_format::BED5;
    else if(isBed6Extension(fname) || isBed6GzipExtension(fname) || isBed6Bzip2Extension(fname)) return file_format::BED6;
    else if(isBed7Extension(fname) || isBed7GzipExtension(fname) || isBed7Bzip2Extension(fname)) return file_format::BED7;
    else if(isBed8Extension(fname) || isBed8GzipExtension(fname) || isBed8Bzip2Extension(fname)) return file_format::BED8;
    else if(isBed9Extension(fname) || isBed9GzipExtension(fname) || isBed9Bzip2Extension(fname)) return file_format::BED9;
    else if(isBed10Extension(fname) || isBed10GzipExtension(fname) || isBed10Bzip2Extension(fname)) return file_format::BED10;
    else if(isBed11Extension(fname) || isBed11GzipExtension(fname) || isBed11Bzip2Extension(fname)) return file_format::BED11;
    else if(isBed12Extension(fname) || isBed12GzipExtension(fname) || isBed12Bzip2Extension(fname)) return file_format::BED12;
    else if(isBedExtension(fname) || isBedGzipExtension(fname) || isBedBzip2Extension(fname))
    {
        // Must discover which format it really is since a generic extension is used.
        if(tryBed12Format(fname) || tryBed12GzipFormat(fname) || tryBed12Bzip2Format(fname)) return file_format::BED12;
        else if(tryBed11Format(fname) || tryBed11GzipFormat(fname) || tryBed11Bzip2Format(fname)) return file_format::BED11;
        else if(tryBed10Format(fname) || tryBed10GzipFormat(fname) || tryBed10Bzip2Format(fname)) return file_format::BED10;
        else if(tryBed9Format(fname) || tryBed9GzipFormat(fname) || tryBed9Bzip2Format(fname)) return file_format::BED9;
        else if(tryBed8Format(fname) || tryBed8GzipFormat(fname) || tryBed8Bzip2Format(fname)) return file_format::BED8;
        else if(tryBed7Format(fname) || tryBed7GzipFormat(fname) || tryBed7Bzip2Format(fname)) return file_format::BED7;
        else if(tryBed6Format(fname) || tryBed6GzipFormat(fname) || tryBed6Bzip2Format(fname)) return file_format::BED6;
        else if(tryBed5Format(fname) || tryBed5GzipFormat(fname) || tryBed5Bzip2Format(fname)) return file_format::BED5;
        else if(tryBed4Format(fname) || tryBed4GzipFormat(fname) || tryBed4Bzip2Format(fname)) return file_format::BED4;
        else if(tryBed3Format(fname) || tryBed3GzipFormat(fname) || tryBed3Bzip2Format(fname)) return file_format::BED3;
    }
    else if(isReferenceCoordinates6Extension(fname) || isReferenceCoordinates6GzipExtension(fname) || isReferenceCoordinates6Bzip2Extension(fname)) return file_format::REFERENCE_COORDINATES6;
    else if(isReferenceCoordinates5Extension(fname) || isReferenceCoordinates5GzipExtension(fname) || isReferenceCoordinates5Bzip2Extension(fname)) return file_format::REFERENCE_COORDINATES5;
    else if(isReferenceCoordinates4Extension(fname) || isReferenceCoordinates4GzipExtension(fname) || isReferenceCoordinates4Bzip2Extension(fname)) return file_format::REFERENCE_COORDINATES4;
    else if(isReferenceCoordinates3Extension(fname) || isReferenceCoordinates3GzipExtension(fname) || isReferenceCoordinates3Bzip2Extension(fname)) return file_format::REFERENCE_COORDINATES3;
    else if(isReferenceCoordinates2Extension(fname) || isReferenceCoordinates2GzipExtension(fname) || isReferenceCoordinates2Bzip2Extension(fname)) return file_format::REFERENCE_COORDINATES2;
    else if(isReferenceCoordinates1Extension(fname) || isReferenceCoordinates1GzipExtension(fname) || isReferenceCoordinates1Bzip2Extension(fname)) return file_format::REFERENCE_COORDINATES1;
    else if(isReferenceCoordinatesExtension(fname) || isReferenceCoordinatesGzipExtension(fname) || isReferenceCoordinatesBzip2Extension(fname))
    {
        // Must discover which format it really is since a generic extension is used.
        if(tryReferenceCoordinates6Format(fname) || tryReferenceCoordinates6GzipFormat(fname) || tryReferenceCoordinates6Bzip2Format(fname)) return file_format::REFERENCE_COORDINATES6;
        else if(tryReferenceCoordinates5Format(fname) || tryReferenceCoordinates5GzipFormat(fname) || tryReferenceCoordinates5Bzip2Format(fname)) return file_format::REFERENCE_COORDINATES5;
        else if(tryReferenceCoordinates4Format(fname) || tryReferenceCoordinates4GzipFormat(fname) || tryReferenceCoordinates4Bzip2Format(fname)) return file_format::REFERENCE_COORDINATES4;
        else if(tryReferenceCoordinates3Format(fname) || tryReferenceCoordinates3GzipFormat(fname) || tryReferenceCoordinates3Bzip2Format(fname)) return file_format::REFERENCE_COORDINATES3;
        else if(tryReferenceCoordinates2Format(fname) || tryReferenceCoordinates2GzipFormat(fname) || tryReferenceCoordinates2Bzip2Format(fname)) return file_format::REFERENCE_COORDINATES2;
        else if(tryReferenceCoordinates1Format(fname) || tryReferenceCoordinates1GzipFormat(fname) || tryReferenceCoordinates1Bzip2Format(fname)) return file_format::REFERENCE_COORDINATES1;
    }
    else if(isBigwigExtension(fname) || isBigwigGzipExtension(fname) || isBigwigBzip2Extension(fname)) return file_format::BIGWIG;
    else if(isBigbedExtension(fname) || isBigbedGzipExtension(fname) || isBigbedBzip2Extension(fname)) return file_format::BIGBED;
    else if(isGtfExtension(fname) || isGtfGzipExtension(fname) || isGtfBzip2Extension(fname)) return file_format::GTF;
    else if(isWigExtension(fname) || isWigGzipExtension(fname) || isWigBzip2Extension(fname)) return file_format::WIG;
    else if((isGenepredExtendedExtension(fname) || isGenepredExtendedGzipExtension(fname) || isGenepredExtendedBzip2Extension(fname)) && (tryGenepredExtendedFormat(fname) || tryGenepredExtendedGzipFormat(fname) || tryGenepredExtendedBzip2Format(fname))) return file_format::GENEPRED_EXTENDED;
    else if((isGenepredExtension(fname) || isGenepredGzipExtension(fname) || isGenepredBzip2Extension(fname)) && (tryGenepredFormat(fname) || tryGenepredGzipFormat(fname) || tryGenepredBzip2Format(fname))) return file_format::GENEPRED;
    else if(isReferenceFiltersExtension(fname) || isReferenceFiltersGzipExtension(fname) || isReferenceFiltersBzip2Extension(fname)) return file_format::REFERENCE_FILTERS;
    else if(isReferenceAnnotationsExtension(fname) || isReferenceAnnotationsGzipExtension(fname) || isReferenceAnnotationsBzip2Extension(fname)) return file_format::REFERENCE_ANNOTATIONS;

    // then check file content
    if(tryBedgraphFormat(fname) || tryBedgraphGzipFormat(fname) || tryBedgraphBzip2Format(fname)) return file_format::BEDGRAPH;
    else if(tryBed12Format(fname) || tryBed12GzipFormat(fname) || tryBed12Bzip2Format(fname)) return file_format::BED12;
    else if(tryBed11Format(fname) || tryBed11GzipFormat(fname) || tryBed11Bzip2Format(fname)) return file_format::BED11;
    else if(tryBed10Format(fname) || tryBed10GzipFormat(fname) || tryBed10Bzip2Format(fname)) return file_format::BED10;
    else if(tryBed9Format(fname) || tryBed9GzipFormat(fname) || tryBed9Bzip2Format(fname)) return file_format::BED9;
    else if(tryBed8Format(fname) || tryBed8GzipFormat(fname) || tryBed8Bzip2Format(fname)) return file_format::BED8;
    else if(tryBed7Format(fname) || tryBed7GzipFormat(fname) || tryBed7Bzip2Format(fname)) return file_format::BED7;
    else if(tryBed6Format(fname) || tryBed6GzipFormat(fname) || tryBed6Bzip2Format(fname)) return file_format::BED6;
    else if(tryBed5Format(fname) || tryBed5GzipFormat(fname) || tryBed5Bzip2Format(fname)) return file_format::BED5;
    else if(tryBed4Format(fname) || tryBed4GzipFormat(fname) || tryBed4Bzip2Format(fname)) return file_format::BED4;
    else if(tryBed3Format(fname) || tryBed3GzipFormat(fname) || tryBed3Bzip2Format(fname)) return file_format::BED3;
    else if(tryReferenceCoordinates6Format(fname) || tryReferenceCoordinates6GzipFormat(fname) || tryReferenceCoordinates6Bzip2Format(fname)) return file_format::REFERENCE_COORDINATES6;
    else if(tryReferenceCoordinates5Format(fname) || tryReferenceCoordinates5GzipFormat(fname) || tryReferenceCoordinates5Bzip2Format(fname)) return file_format::REFERENCE_COORDINATES5;
    else if(tryReferenceCoordinates4Format(fname) || tryReferenceCoordinates4GzipFormat(fname) || tryReferenceCoordinates4Bzip2Format(fname)) return file_format::REFERENCE_COORDINATES4;
    else if(tryReferenceCoordinates3Format(fname) || tryReferenceCoordinates3GzipFormat(fname) || tryReferenceCoordinates3Bzip2Format(fname)) return file_format::REFERENCE_COORDINATES3;
    else if(tryReferenceCoordinates2Format(fname) || tryReferenceCoordinates2GzipFormat(fname) || tryReferenceCoordinates2Bzip2Format(fname)) return file_format::REFERENCE_COORDINATES2;
    else if(tryReferenceCoordinates1Format(fname) || tryReferenceCoordinates1GzipFormat(fname) || tryReferenceCoordinates1Bzip2Format(fname)) return file_format::REFERENCE_COORDINATES1;
    else if(tryBigwigFormat(fname) || tryBigwigGzipFormat(fname) || tryBigwigBzip2Format(fname)) return file_format::BIGWIG;
    else if(tryBigbedFormat(fname) || tryBigbedGzipFormat(fname) || tryBigbedBzip2Format(fname)) return file_format::BIGBED;
    else if(tryGtfFormat(fname) || tryGtfGzipFormat(fname) || tryGtfBzip2Format(fname)) return file_format::GTF;
    else if(tryWigFormat(fname) || tryWigGzipFormat(fname) || tryWigBzip2Format(fname)) return file_format::WIG;
    else if(tryGenepredExtendedFormat(fname) || tryGenepredExtendedGzipFormat(fname) || tryGenepredExtendedBzip2Format(fname)) return file_format::GENEPRED_EXTENDED;
    else if(tryGenepredFormat(fname) || tryGenepredGzipFormat(fname) || tryGenepredBzip2Format(fname)) return file_format::GENEPRED;

    return file_format::UNKNOWN;
}

io::file_format format_guess::discoverDataset(const std::string &fname)
{
    // For robustness, do exhaustive checks of extensions and format (even if cumbersome ...)
    // The following format are allowed:
    //         - filename.ext1.ext2
    //         - filename.ext1
    //         - filename

    // WARNING : Conditions order is VERY important. The conditions must be from the most elaborate to the simplier.
    // For example, Genepred Extended MUST come before Genepred format because both format have the same first ten columns.
    // We assume that the file exists, otherwise, expect undefined results.

    // Note: In order to differentiate Genepred of GenepredExtended, we need to look inside the files.
    using namespace io;

    // check extension first
    if(isBedgraphExtension(fname) || isBedgraphGzipExtension(fname) || isBedgraphBzip2Extension(fname)) return file_format::BEDGRAPH;
    else if(isBigwigExtension(fname) || isBigwigGzipExtension(fname) || isBigwigBzip2Extension(fname)) return file_format::BIGWIG;
    else if(isWigExtension(fname) || isWigGzipExtension(fname) || isWigBzip2Extension(fname)) return file_format::WIG;

    // then check file content
    if(tryBedgraphFormat(fname) || tryBedgraphGzipFormat(fname) || tryBedgraphBzip2Format(fname)) return file_format::BEDGRAPH;
    else if(tryBigwigFormat(fname) || tryBigwigGzipFormat(fname) || tryBigwigBzip2Format(fname)) return file_format::BIGWIG;
    else if(tryWigFormat(fname) || tryWigGzipFormat(fname) || tryWigBzip2Format(fname)) return file_format::WIG;

    return file_format::UNKNOWN;
}

io::file_format format_guess::discoverGenome(const std::string &fname)
{
    // For robustness, do exhaustive checks of extensions and format (even if cumbersome ...)
    // The following format are allowed:
    //         - filename.ext1.ext2
    //         - filename.ext1
    //         - filename

    // WARNING : Conditions order is VERY important. The conditions must be from the most elaborate to the simplier.
    // Per example, Genepred Extended MUST come before Genepred format because both format have the same first ten columns.
    // We assume that the file exists, otherwise, expect undefined results.

    // Note: In order to differentiate Genepred of GenepredExtended, we need to look inside the files.

    using namespace io;

    // check extension first
    if(isGtfExtension(fname) || isGtfGzipExtension(fname) || isGtfBzip2Extension(fname)) return file_format::GTF;
    else if((isGenepredExtendedExtension(fname) || isGenepredExtendedGzipExtension(fname) || isGenepredExtendedBzip2Extension(fname)) && (tryGenepredExtendedFormat(fname) || tryGenepredExtendedGzipFormat(fname) || tryGenepredExtendedBzip2Format(fname))) return file_format::GENEPRED_EXTENDED;
    else if((isGenepredExtension(fname) || isGenepredGzipExtension(fname) || isGenepredBzip2Extension(fname)) && (tryGenepredFormat(fname) || tryGenepredGzipFormat(fname) || tryGenepredBzip2Format(fname))) return file_format::GENEPRED;

    // then check file content
    if(tryGtfFormat(fname) || tryGtfGzipFormat(fname) || tryGtfBzip2Format(fname)) return file_format::GTF;
    else if(tryGenepredExtendedFormat(fname) || tryGenepredExtendedGzipFormat(fname) || tryGenepredExtendedBzip2Format(fname)) return file_format::GENEPRED_EXTENDED;
    else if(tryGenepredFormat(fname) || tryGenepredGzipFormat(fname) || tryGenepredBzip2Format(fname)) return file_format::GENEPRED;

    return file_format::UNKNOWN;
}
}
}
