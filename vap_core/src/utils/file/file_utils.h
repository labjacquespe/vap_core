/**
 * @file    file_extension.h
 * @author  Charles Coulombe
 * @date    3 August 2015, 20:40
 * @version 1.2.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FILE_UTILS_H
#define FILE_UTILS_H

#include <string>
#include <fstream>

namespace vap{
namespace filesystem
{

/**
 * Extracts file's name from path.
 *
 * @param path full path
 *
 * @return file's name
 */
inline std::string extract_filename(const std::string &path)
{
    size_t pos = path.find_last_of("\\/");
    return (pos == path.npos ? path : path.substr(pos + 1));
}

/**
 * Extracts the directory from paths
 *
 * @param path full path
 * @return directory's path
 */
inline std::string extract_directory(const std::string &path)
{
    size_t pos = path.find_last_of("\\/");
    return (pos == path.npos ? "" : path.substr(0, pos + 1));
}

/**
 * Extracts the file's type(extension) from path
 *
 * @param path full path
 * @return file's type
 */
inline std::string extract_extension(const std::string &path, const unsigned int n = 2)
{
    std::string filename = extract_filename(path);
    size_t pos = std::string::npos, cur = std::string::npos;
    for(unsigned int i = 0; i < n; ++i)
    {
       cur = filename.find_last_of('.', cur - 1);
       if(cur != std::string::npos)
           pos = cur;
    }
    return (pos == path.npos ? "" : filename.substr(pos, filename.size()));
}

/**
 * Changes or Adds extension to the path std::string.
 *
 * @param path full path
 * @param ext new extension with the format .ext
 * @return new paths with extension
 */
inline std::string change_extension(const std::string &path, const std::string &extension)
{
    std::string filename = extract_filename(path);
    size_t pos = filename.find_last_of('.');
    std::string directory = extract_directory(path);
    return directory + (pos == path.npos ? filename : filename.substr(0, pos)) + extension;
}

/**
 * Checks if given file exists. Path must be well structured.
 *
 * @param path file's path
 * @return @c True if file exists and can be open, @c False otherwise
 */
inline bool exists(const std::string &path)
{
    std::ifstream ifs(path.c_str());
    return ifs.is_open();
}

inline std::string add_filename_prefix(const std::string &path, const std::string &prefix)
{
    return path.empty() ? path : extract_directory(path) + prefix + extract_filename(path);
}

inline std::string add_filename_suffix(const std::string &path, const std::string &suffix, const size_t n = 2)
{
    if(path.empty())
        return path;

    std::string filename = extract_filename(path);
    size_t pos = std::string::npos, cur = std::string::npos;
    for(size_t i = 0; i < n; ++i)
    {
        cur = filename.find_last_of('.', cur - 1);
        if(cur != std::string::npos)
            pos = cur;
    }
    filename.insert((pos == std::string::npos ? filename.size() : pos), suffix);
    return extract_directory(path) + filename;
}

/**
 * Normalize a path to the current OS path separator.
 *
 * @param path path to be normalized
 * @return a valid path defined by the current OS
 */
inline std::string normalize_path(const std::string &path)
{
    if(path.empty())
        return path;

    std::string str = path;
    std::string::iterator first = str.begin(),
            last = str.end(),
            result = str.begin();

    while(++first != last)
    {
        if(!(*result == *first && (*result == '/' || *result == '\\')))
            *(++result) = *first;
    }

    str.resize(std::distance(str.begin(), ++result));
    return str;
}
}
}
#endif
