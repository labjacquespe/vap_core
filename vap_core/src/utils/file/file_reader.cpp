/**
 * @file    file_extension.h
 * @author  Charles Coulombe
 * @date    26 July 2015, 17:26
 * @version 1.2.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "file_reader.h"

namespace vap
{
namespace io
{

/** Simple wrapper around @c std::istream to read text file.*/
/** Default constructor. */
file_reader::file_reader()
    : _stream(nullptr), _isopen(false), _fname("")
{
}

/**
 * Constructs this reader on @a stream. */
file_reader::file_reader(std::istream *stream, const std::string &fname)
    : _stream(stream), _isopen((stream != nullptr)), _fname(fname)
{
}

/** Constructs this reader on @a fname and opens it. */
file_reader::file_reader(const std::string &fname, std::ios_base::openmode mode)
    : _stream(nullptr), _isopen(false), _fname(fname)
{
    open(fname, mode);
}

/** Opens @a fname and associates it with the input stream object. */
void file_reader::open(const std::string &fname, std::ios_base::openmode mode)
{
    if(!_isopen)
    {
        _fname = fname;
        std::ifstream *stream = new std::ifstream(fname, mode);
        if(stream->is_open())
        {
            _stream = stream;
            _isopen = true;
        }
        else
        {
            stream->close();
            delete stream;
            // std::cerr << "ERROR: \"" << fname << "\" no such file.\n";
        }
    }
}

file_reader::~file_reader()
{
    delete _stream;
    _stream = nullptr;
    _isopen = false;
}

/**
 * Check if a file is open.
 * Returns whether the stream is currently associated to a file.
 *
 * @return @c true if a file is open and associated with the input stream object. @c false otherwise.
 */
bool file_reader::is_open() const
{
    return _isopen;
}

file_reader::operator bool() const
{
    return _stream;
}

/**
 * Check whether eofbit is set.
 * Returns true if the eofbit error state flag is set for the stream.
 *
 * @return @c true if the stream's eofbit error state flag is set (which signals that the End-of-File has been reached by the last input operation). @c false otherwise.
 */
bool file_reader::eof() const
{
    return /*_stream->peek() == EOF || */_stream->eof();
}

int file_reader::peek() const
{
    return _stream->peek();
}

std::string file_reader::file() const
{
    return _fname;
}

/**
 * Get line from stream into string.
 * Extracts characters from the stream object and stores them into a std::string until the the newline character is found.
 * The extraction also stops if the end of file is reached in the stream object or if some other error occurs during the input operation.
 *
 * @return The underlayering stream object.
 */
std::istream &file_reader::readline(std::string &str, char delim)
{
    return std::getline(*_stream, str, delim);
}

std::istream &file_reader::readuntil(std::string &str, char delim)
{

    // std::getline(*_stream, str, delim);
    // _stream->unget();
    // return *_stream;

    char c = '\0';
    str.clear();
    _stream->clear();
    do
    {
        str.push_back(c);
    }
    while(_stream->get(c) && _stream->good() && _stream->peek() != delim);
    return *_stream;
}

/**
 * Get the stream position.
 *
 * @return Current stream position.
 */
std::streampos file_reader::streampos() const
{
    return _stream->tellg();
}

/**
 * Set the stream position.
 *
 * @param pos New position.
 * @param from From start or end.
 */
void file_reader::streampos(const std::streampos pos, std::ios_base::seekdir from)
{
    _stream->seekg(pos, from);
}

/** Returns a pointer to the internal stream object. */
std::istream *file_reader::fstream() const
{
    return _stream;
}

//    bool lookahead(const std::string &prefix)
//    {
//        if(eof())
//            return false;
//        else
//        {
//            std::string str;
//
//            size_t pos = _stream->tellg();
//            std::getline(*_stream, str);
//            _stream->seekg(pos, std::ios_base::beg);
//
//            // compare size and memory block content
//            return(str.size() >= prefix.size() && memcmp(str.c_str(), prefix.c_str(), prefix.size()) == 0);
////            return line.substr(0, (str.empty() ? 0 : str.size() - 1)) == str;
//        }
//    }

/** Specialized wrapper around @c std::istream and @c boost::iostream::filtering_istream to read gzip files. */
void file_reader_gzip::_open()
{
    if(_stream)
    {
        try
        {
            _fstream = new boost::iostreams::filtering_istream();
            _fstream->push(boost::iostreams::gzip_decompressor());
            _fstream->push(*_stream);
            _isopen = true;
        }
        catch(const boost::iostreams::gzip_error &e)
        {
            std::cerr << e.what() << '\n';
        }
    }
}

/** Constructs this reader on a @a stream. */
file_reader_gzip::file_reader_gzip(std::istream *stream, const std::string &fname)
    : file_reader(stream, fname), _fstream(nullptr)
{
    _isopen = false;
    _open();
}

/** Constructs this reader on @a fname and opens it. */
file_reader_gzip::file_reader_gzip(const std::string &fname)
    : file_reader(fname, std::ios::binary | std::ios::in), _fstream(nullptr)
{
    _isopen = false;
    _open();
}

file_reader_gzip::~file_reader_gzip()
{
    delete _fstream;
    _fstream = nullptr;
}

/**
 * Check whether eofbit is set.
 * Returns true if the eofbit error state flag is set for the stream.
 *
 * @return @c true if the stream's eofbit error state flag is set (which signals that the End-of-File has been reached by the last input operation). @c false otherwise.
 */
bool file_reader_gzip::eof() const
{
    return _fstream->peek() == EOF;
}

int file_reader_gzip::peek() const
{
    return _fstream->peek();
}

/**
 * Get line from stream into string.
 * Extracts characters from the stream object and stores them into a std::string until the the newline character is found.
 * The extraction also stops if the end of file is reached in the stream object or if some other error occurs during the input operation.
 *
 * @return The underlayering stream object.
 */
std::istream &file_reader_gzip::readline(std::string &str, char delim)
{
    return std::getline(*_fstream, str, delim);
}

std::istream &file_reader_gzip::readuntil(std::string &str, char delim)
{
    std::getline(*_fstream, str, delim);
    _fstream->unget();
    return *_fstream;
}

/** Returns a pointer to the internal stream object. */
boost::iostreams::filtering_istream *file_reader_gzip::fstream() const
{
    return _fstream;
}

/** Specialized wrapper around @c std::istream and @c boost::iostream::filtering_istream to read bzip2 files. */
void file_reader_bzip2::_open()
{
    if(_stream)
    {
        try
        {
            _fstream = new boost::iostreams::filtering_istream();
            _fstream->push(boost::iostreams::bzip2_decompressor());
            _fstream->push(*_stream);
            _isopen = true;
        }
        catch(const boost::iostreams::bzip2_error &e)
        {
            std::cerr << e.what() << '\n';
        }
    }
}

/** Constructs this reader on a @a stream. */
file_reader_bzip2::file_reader_bzip2(std::istream *stream, const std::string &fname)
    : file_reader(stream, fname), _fstream(nullptr)
{
    _isopen = false;
    _open();
}

/** Constructs this reader on @a fname and opens it. */
file_reader_bzip2::file_reader_bzip2(const std::string &fname)
    : file_reader(fname, std::ios::binary | std::ios::in), _fstream(nullptr)
{
    _isopen = false;
    _open();
}

file_reader_bzip2::~file_reader_bzip2()
{
    delete _fstream;
    _fstream = nullptr;
}

/**
 * Check whether eofbit is set.
 * Returns true if the eofbit error state flag is set for the stream.
 *
 * @return @c true if the stream's eofbit error state flag is set (which signals that the End-of-File has been reached by the last input operation). @c false otherwise.
 */
bool file_reader_bzip2::eof() const
{
    return _fstream->peek() == EOF;
}

int file_reader_bzip2::peek() const
{
    return _fstream->peek();
}

/**
 * Get line from stream into string.
 * Extracts characters from the stream object and stores them into a std::string until the the newline character is found.
 * The extraction also stops if the end of file is reached in the stream object or if some other error occurs during the input operation.
 *
 * @return The underlayering stream object.
 */
std::istream &file_reader_bzip2::readline(std::string &str, char delim)
{
    return std::getline(*_fstream, str, delim);
}

std::istream &file_reader_bzip2::readuntil(std::string &str, char delim)
{
    std::getline(*_fstream, str, delim);
    _fstream->unget();
    return *_fstream;
}

/** Returns a pointer to the internal stream object. */
boost::iostreams::filtering_istream *file_reader_bzip2::fstream() const
{
    return _fstream;
}

/** Helper class to creates appropriate @c file_reader. */
file_reader *file_reader_factory::create(const std::string &fname, std::ios_base::openmode mode)
{
    if(format_guess::isGzipExtension(fname) || format_guess::tryGzipFormat(fname))
        return new file_reader_gzip(fname);
    else if(format_guess::isBzip2Extension(fname) || format_guess::tryBzip2Format(fname))
        return new file_reader_bzip2(fname);
    else // text format
        return new file_reader(fname, mode);
}
}
}
