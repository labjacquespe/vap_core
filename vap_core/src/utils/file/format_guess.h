/**
 * @file    file_extension.h
 * @author  Charles Coulombe
 * @date    August 02, 2015, 20:57
 * @version 1.2.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FORMAT_GUESS_H
#define FORMAT_GUESS_H

#include <string>
#include <iostream>
#include <fstream>
#include <functional>
#include <map>
#include <vector>
#include "pcre2cpp.h"
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include "file_format.h"
#include "file_extension.h"
#include "regex_strings.h"

namespace vap
{
namespace io
{
class format_guess
{
private:
    /** Number of maximum lines to read in order to discover the format. */
    const static unsigned int MAX_LINES = 200;

    static void report_error(std::ostream &out, const std::string &what, const std::string &prefix="");

    /**
     * Parse at most 200 lines (skipping any blank, comment, track or browser lines).
     * To be be discovered, at least 50% of lines must be valid. Otherwise, false is returned.
     */
    static bool _parse(std::istream *stream, const pcre2cpp &re);
    static bool _parseBedgraphFormat(std::istream *stream);
    static bool _parseBedFormat(std::istream *stream, unsigned int n);
    static bool _parseBigwigFormat(std::istream *stream);
    static bool _parseBigbedFormat(std::istream *stream);
    static bool _parseReferenceCoordinatesFormat(std::istream *stream, unsigned int n);
    static bool _parseGenepredExtendedFormat(std::istream *stream);
    static bool _parseGenepredFormat(std::istream *stream);
    static bool _parseGtfFormat(std::istream *stream);
    static bool _parseWigFormat(std::istream *stream);
    static bool _tryGzip(const std::string &file, const std::function<bool(std::istream *)> &func);
    static bool _tryBzip2(const std::string &file, const std::function<bool(std::istream *)> &func);

public:
    ////////////////////////////////////////////////////////////////////
    // Extensions

    /**
     * Checks if @a file ends with a ".gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".gz" or ".gzip" extension, @c false otherwise.
     */
    static bool isGzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bz2" extension, @c false otherwise.
     */
    static bool isBzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bg" or ".bedgraph" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bg" or ".bedgraph" extension, @c false otherwise.
     */
    static bool isBedgraphExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bg.gz" or ".bedgraph.gz" or ".bg.gzip" or ".bedgraph.gzip" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bg" or ".bedgraph" extension, @c false otherwise.
     */
    static bool isBedgraphGzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bg" or ".bedgraph" or ".bg.bz2" or ".bedgraph.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bg" or ".bedgraph" extension, @c false otherwise.
     */
    static bool isBedgraphBzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed" extension, @c false otherwise.
     */
    static bool isBedExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed.gz" extension, @c false otherwise.
     */
    static bool isBedGzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed.bz2" extension, @c false otherwise.
     */
    static bool isBedBzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed3" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed3" extension, @c false otherwise.
     */
    static bool isBed3Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed3.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed3.gz" extension, @c false otherwise.
     */
    static bool isBed3GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed3.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed3.bz2" extension, @c false otherwise.
     */
    static bool isBed3Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed4" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed4" extension, @c false otherwise.
     */
    static bool isBed4Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed4.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed4.gz" extension, @c false otherwise.
     */
    static bool isBed4GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed4.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed4.bz2" extension, @c false otherwise.
     */
    static bool isBed4Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed5" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed5" extension, @c false otherwise.
     */
    static bool isBed5Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed5.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed5.gz" extension, @c false otherwise.
     */
    static bool isBed5GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed5.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed5.bz2" extension, @c false otherwise.
     */
    static bool isBed5Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed6" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed6" extension, @c false otherwise.
     */
    static bool isBed6Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed6.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed6.gz" extension, @c false otherwise.
     */
    static bool isBed6GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed6.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed6.bz2" extension, @c false otherwise.
     */
    static bool isBed6Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed7" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed7" extension, @c false otherwise.
     */
    static bool isBed7Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed7.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed7.gz" extension, @c false otherwise.
     */
    static bool isBed7GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed7.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed7.bz2" extension, @c false otherwise.
     */
    static bool isBed7Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed8" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed8" extension, @c false otherwise.
     */
    static bool isBed8Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed8.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed8.gz" extension, @c false otherwise.
     */
    static bool isBed8GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed8.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed8.bz2" extension, @c false otherwise.
     */
    static bool isBed8Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed9" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed9" extension, @c false otherwise.
     */
    static bool isBed9Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed9.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed9.gz" extension, @c false otherwise.
     */
    static bool isBed9GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed9.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed9.bz2" extension, @c false otherwise.
     */
    static bool isBed9Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed10" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed10" extension, @c false otherwise.
     */
    static bool isBed10Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed10.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed10.gz" extension, @c false otherwise.
     */
    static bool isBed10GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed10.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed10.bz2" extension, @c false otherwise.
     */
    static bool isBed10Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed11" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed11" extension, @c false otherwise.
     */
    static bool isBed11Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed11.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed11.gz" extension, @c false otherwise.
     */
    static bool isBed11GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed11.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed11.bz2" extension, @c false otherwise.
     */
    static bool isBed11Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed12" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed12" extension, @c false otherwise.
     */
    static bool isBed12Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed12.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed12.gz" extension, @c false otherwise.
     */
    static bool isBed12GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bed12.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bed12.bz2" extension, @c false otherwise.
     */
    static bool isBed12Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord" or a ".rc" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord"" ext or a ".rc" extension, @c false otherwise.
     */
    static bool isReferenceCoordinatesExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord.gz"  or a ".rc.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord.gz"  or a ".rc.gz" extension, @c false otherwise.
     */
    static bool isReferenceCoordinatesGzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord.bz2" or a ".rc.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord.bz2" or a ".rc.bz2" extension, @c false otherwise.
     */
    static bool isReferenceCoordinatesBzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord1" or a ".rc1" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord1"" ext or a ".rc1" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates1Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord1.gz"  or a ".rc1.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord1.gz"  or a ".rc1.gz" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates1GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord1.bz2" or a ".rc1.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord1.bz2" or a ".rc1.bz2" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates1Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord2" or a ".rc2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord2"" ext or a ".rc2" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord2.gz"  or a ".rc2.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord2.gz"  or a ".rc2.gz" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates2GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord2.bz2" or a ".rc2.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord2.bz2" or a ".rc2.bz2" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates2Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord3" or a ".rc3" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord3"" ext or a ".rc3" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates3Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord3.gz"  or a ".rc3.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord3.gz"  or a ".rc3.gz" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates3GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord3.bz2" or a ".rc3.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord3.bz2" or a ".rc3.bz2" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates3Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord4" or a ".rc4" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord4"" ext or a ".rc4" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates4Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord4.gz"  or a ".rc4.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord4.gz"  or a ".rc4.gz" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates4GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord4.bz4" or a ".rc4.bz4" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord4.bz4" or a ".rc4.bz4" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates4Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord5" or a ".rc5" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord5"" ext or a ".rc5" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates5Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord5.gz"  or a ".rc5.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord5.gz"  or a ".rc5.gz" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates5GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord5.bz5" or a ".rc5.bz5" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord5.bz5" or a ".rc5.bz5" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates5Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord6" or a ".rc6" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord6"" ext or a ".rc6" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates6Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord6.gz"  or a ".rc6.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord6.gz"  or a ".rc6.gz" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates6GzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".coord6.bz6" or a ".rc6.bz6" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".coord6.bz6" or a ".rc6.bz6" extension, @c false otherwise.
     */
    static bool isReferenceCoordinates6Bzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bw" or ".bigwig" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bw" or ".bigwig" extension, @c false otherwise.
     */
    static bool isBigwigExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bw.gz" or ".bigwig.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bw.gz" or ".bigwig.gz" extension, @c false otherwise.
     */
    static bool isBigwigGzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bw.bz2" or ".bigwig.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bw.bz2" or ".bigwig.bz2" extension, @c false otherwise.
     */
    static bool isBigwigBzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bb" or ".bigbed" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bb" or ".bigbed" extension, @c false otherwise.
     */
    static bool isBigbedExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bb.gz" or ".bigbed.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bb.gz" or ".bigbed.gz" extension, @c false otherwise.
     */
    static bool isBigbedGzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".bb.bz2" or ".bigbed.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".bb.bz2" or ".bigbed.bz2" extension, @c false otherwise.
     */
    static bool isBigbedBzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".gp" or ".genepred" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".gp" or ".genepred" extension, @c false otherwise.
     */
    static bool isGenepredExtendedExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".gp.gz" or ".genepred.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".gp.gz" or ".genepred.gz" extension, @c false otherwise.
     */
    static bool isGenepredExtendedGzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".gp.bz2" or ".genepred.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".gp.bz2" or ".genepred.bz2" extension, @c false otherwise.
     */
    static bool isGenepredExtendedBzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".gp" or ".genepred" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".gp" or ".genepred" extension, @c false otherwise.
     */
    static bool isGenepredExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".gp.gz" or ".genepred.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".gp.gz" or ".genepred.gz" extension, @c false otherwise.
     */
    static bool isGenepredGzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".gp.bz2" or ".genepred.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".gp.bz2" or ".genepred.bz2" extension, @c false otherwise.
     */
    static bool isGenepredBzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".gtf" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".gtf" extension, @c false otherwise.
     */
    static bool isGtfExtension(const std::string &file);
    /**
     * Checks if @a file ends with a .gtf.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".gtf.gz" extension, @c false otherwise.
     */
    static bool isGtfGzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".gtf.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".gtf.bz2" extension, @c false otherwise.
     */
    static bool isGtfBzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".wiggle" or ".wig" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a .wiggle" or ".wig" extension, @c false otherwise.
     */
    static bool isWigExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".wiggle.gz" or ".wig.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".wiggle.gz" or ".wig.gz" extension, @c false otherwise.
     */
    static bool isWigGzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".wiggle.bz2" or ".wig.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".wiggle.bz2" or ".wig.bz2" extension, @c false otherwise.
     */
    static bool isWigBzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".ra" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".ra" extension, @c false otherwise.
     */
    static bool isReferenceAnnotationsExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".ra.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".ra.gz" extension, @c false otherwise.
     */
    static bool isReferenceAnnotationsGzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".ra.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".ra.bz2" extension, @c false otherwise.
     */
    static bool isReferenceAnnotationsBzip2Extension(const std::string &file);
    /**
     * Checks if @a file ends with a ".rf" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".rf" extension, @c false otherwise.
     */
    static bool isReferenceFiltersExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".rf.gz" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".rf.gz" extension, @c false otherwise.
     */
    static bool isReferenceFiltersGzipExtension(const std::string &file);
    /**
     * Checks if @a file ends with a ".rf.bz2" extension.
     *
     * @param file File.
     *
     * @return @c true when @a file ends with a ".rf.bz2" extension, @c false otherwise.
     */
    static bool isReferenceFiltersBzip2Extension(const std::string &file);

    ////////////////////////////////////////////////////////////////////
    // Discovery

    /**
     * Checks if @a file is Gzip compressed by reading its magic number.
     *
     * @param file File.
     *
     * @return @c true when @a file is Gzip compressed, @c false otherwise.
     */
    static bool tryGzipFormat(const std::string &file);
    /**
     * Checks if @a file is Bzip2 compressed by reading its magic number.
     *
     * @param file File.
     *
     * @return @c true when @a file is Bzip2 compressed, @c false otherwise.
     */
    static bool tryBzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bedgraph format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bedgraph, @c false otherwise.
     */
    static bool tryBedgraphFormat(const std::string &file);
    /**
     * Checks if @a file is of Bedgraph Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bedgraph, @c false otherwise.
     */
    static bool tryBedgraphGzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bedgraph Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bedgraph, @c false otherwise.
     */
    static bool tryBedgraphBzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed, @c false otherwise.
     */
    static bool tryBedFormat(const std::string &file);
    /**
     * Checks if @a file is of Bed Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed, @c false otherwise.
     */
    static bool tryBedGzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bed Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed, @c false otherwise.
     */
    static bool tryBedBzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bed3 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed3, @c false otherwise.
     */
    static bool tryBed3Format(const std::string &file);
    /**
     * Checks if @a file is of Bed3 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed3, @c false otherwise.
     */
    static bool tryBed3GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bed3 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed3, @c false otherwise.
     */
    static bool tryBed3Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bed4 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed4, @c false otherwise.
     */
    static bool tryBed4Format(const std::string &file);
    /**
     * Checks if @a file is of Bed4 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed4, @c false otherwise.
     */
    static bool tryBed4GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bed4 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed4, @c false otherwise.
     */
    static bool tryBed4Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bed5 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed5, @c false otherwise.
     */
    static bool tryBed5Format(const std::string &file);
    /**
     * Checks if @a file is of Bed5 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed5, @c false otherwise.
     */
    static bool tryBed5GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bed5 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed5, @c false otherwise.
     */
    static bool tryBed5Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bed6 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed6, @c false otherwise.
     */
    static bool tryBed6Format(const std::string &file);
    /**
     * Checks if @a file is of Bed6 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed6, @c false otherwise.
     */
    static bool tryBed6GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bed6 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed6, @c false otherwise.
     */
    static bool tryBed6Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bed7 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed7, @c false otherwise.
     */
    static bool tryBed7Format(const std::string &file);
    /**
     * Checks if @a file is of Bed7 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed7, @c false otherwise.
     */
    static bool tryBed7GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bed7 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed7, @c false otherwise.
     */
    static bool tryBed7Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bed8 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed8, @c false otherwise.
     */
    static bool tryBed8Format(const std::string &file);
    /**
     * Checks if @a file is of Bed8 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed8, @c false otherwise.
     */
    static bool tryBed8GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bed8 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed8, @c false otherwise.
     */
    static bool tryBed8Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bed9 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed9, @c false otherwise.
     */
    static bool tryBed9Format(const std::string &file);
    /**
     * Checks if @a file is of Bed9 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed9, @c false otherwise.
     */
    static bool tryBed9GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bed9 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed9, @c false otherwise.
     */
    static bool tryBed9Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bed10 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed10, @c false otherwise.
     */
    static bool tryBed10Format(const std::string &file);
    /**
     * Checks if @a file is of Bed10 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed10, @c false otherwise.
     */
    static bool tryBed10GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bed10 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed10, @c false otherwise.
     */
    static bool tryBed10Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bed11 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed11, @c false otherwise.
     */
    static bool tryBed11Format(const std::string &file);
    /**
     * Checks if @a file is of Bed11 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed11, @c false otherwise.
     */
    static bool tryBed11GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bed11 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed11, @c false otherwise.
     */
    static bool tryBed11Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bed12 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed12, @c false otherwise.
     */
    static bool tryBed12Format(const std::string &file);
    /**
     * Checks if @a file is of Bed12 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed12, @c false otherwise.
     */
    static bool tryBed12GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bed12 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bed12, @c false otherwise.
     */
    static bool tryBed12Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates, @c false otherwise.
     */
    static bool tryReferenceCoordinatesFormat(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates, @c false otherwise.
     */
    static bool tryReferenceCoordinatesGzipFormat(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates, @c false otherwise.
     */
    static bool tryReferenceCoordinatesBzip2Format(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates1 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates1, @c false otherwise.
     */
    static bool tryReferenceCoordinates1Format(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates1 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates1, @c false otherwise.
     */
    static bool tryReferenceCoordinates1GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates1 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates1, @c false otherwise.
     */
    static bool tryReferenceCoordinates1Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates2 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates2, @c false otherwise.
     */
    static bool tryReferenceCoordinates2Format(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates2 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates2, @c false otherwise.
     */
    static bool tryReferenceCoordinates2GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates2 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates2, @c false otherwise.
     */
    static bool tryReferenceCoordinates2Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates3 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates3, @c false otherwise.
     */
    static bool tryReferenceCoordinates3Format(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates3 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates3, @c false otherwise.
     */
    static bool tryReferenceCoordinates3GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates3 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates3, @c false otherwise.
     */
    static bool tryReferenceCoordinates3Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates4 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates4, @c false otherwise.
     */
    static bool tryReferenceCoordinates4Format(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates4 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates4, @c false otherwise.
     */
    static bool tryReferenceCoordinates4GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates4 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates4, @c false otherwise.
     */
    static bool tryReferenceCoordinates4Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates5 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates5, @c false otherwise.
     */
    static bool tryReferenceCoordinates5Format(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates5 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates5, @c false otherwise.
     */
    static bool tryReferenceCoordinates5GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates5 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates5, @c false otherwise.
     */
    static bool tryReferenceCoordinates5Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates6 format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates6, @c false otherwise.
     */
    static bool tryReferenceCoordinates6Format(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates6 Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates6, @c false otherwise.
     */
    static bool tryReferenceCoordinates6GzipFormat(const std::string &file);
    /**
     * Checks if @a file is of ReferenceCoordinates6 Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a ReferenceCoordinates6, @c false otherwise.
     */
    static bool tryReferenceCoordinates6Bzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bigwig format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bigwig, @c false otherwise.
     */
    static bool tryBigwigFormat(const std::string &file);
    /**
     * Checks if @a file is of Bigwig format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bigwig, @c false otherwise.
     */
    static bool tryBigwigGzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bigwig format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bigwig, @c false otherwise.
     */
    static bool tryBigwigBzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Bigbed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bigbed, @c false otherwise.
     */
    static bool tryBigbedFormat(const std::string &file);
    /**
     * Checks if @a file is of Bigbed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bigbed, @c false otherwise.
     */
    static bool tryBigbedGzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Bigbed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Bigbed, @c false otherwise.
     */
    static bool tryBigbedBzip2Format(const std::string &file);
    /**
     * Checks if @a file is of GenepredExtended format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a GenepredExtended, @c false otherwise.
     */
    static bool tryGenepredExtendedFormat(const std::string &file);
    /**
     * Checks if @a file is of GenepredExtended Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a GenepredExtended, @c false otherwise.
     */
    static bool tryGenepredExtendedGzipFormat(const std::string &file);
    /**
     * Checks if @a file is of GenepredExtended Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a GenepredExtended, @c false otherwise.
     */
    static bool tryGenepredExtendedBzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Genepred format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Genepred, @c false otherwise.
     */
    static bool tryGenepredFormat(const std::string &file);
    /**
     * Checks if @a file is of Genepred Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Genepred, @c false otherwise.
     */
    static bool tryGenepredGzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Genepred Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Genepred, @c false otherwise.
     */
    static bool tryGenepredBzip2Format(const std::string &file);
    /**
     * Checks if @a file is of GTF format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a GTF, @c false otherwise.
     */
    static bool tryGtfFormat(const std::string &file);
    /**
     * Checks if @a file is of GTF Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a GTF, @c false otherwise.
     */
    static bool tryGtfGzipFormat(const std::string &file);
    /**
     * Checks if @a file is of GTF Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a GTF, @c false otherwise.
     */
    static bool tryGtfBzip2Format(const std::string &file);
    /**
     * Checks if @a file is of Wig format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Wig, @c false otherwise.
     */
    static bool tryWigFormat(const std::string &file);
    /**
     * Checks if @a file is of Wig Gzip compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Wig, @c false otherwise.
     */
    static bool tryWigGzipFormat(const std::string &file);
    /**
     * Checks if @a file is of Wig Bzip2 compressed format by reading the first (non comment nor blank) 200 lines.
     *
     * @param file File.
     *
     * @return @c true when @a file is a Wig, @c false otherwise.
     */
    static bool tryWigBzip2Format(const std::string &file);

    ////////////////////////////////////////////////////////////////////
    // Helper

    static bool hasKnownExtension(const std::string &file);
    static bool hasExtension(const std::string &file, const std::string &extension);
    static bool hasExtension(const std::string &file);
    /**
     * Checks if @a file is Gzip compressed.
     *
     * @param file File.
     *
     * @return @c true when @a file is Gzip compressed, @c false otherwise.
     */
    static bool isGzipCompressed(const std::string &file);
    /**
     * Checks if @a file is Bzip2 compressed.
     *
     * @param file File.
     *
     * @return @c true when @a file is Bzip2 compressed, @c false otherwise.
     */
    static bool isBzip2Compressed(const std::string &file);
    /**
     * Discover the format of a file.
     *
     * @param fname File
     *
     * @return the @c io::file_format or @c UNKNOWN
     */
    static io::file_format discover(const std::string &fname);
    static io::file_format discoverDataset(const std::string &fname);
    static io::file_format discoverGenome(const std::string &fname);
};

/** Possible extensions functors. */
static const std::map<io::file_format, std::vector<std::function<bool(std::string)> > > extensions_functors =
{
    {
        io::file_format::BEDGRAPH,
        {&format_guess::isBedgraphExtension, &format_guess::isBedgraphGzipExtension, &format_guess::isBedgraphBzip2Extension}
    },
    {
        io::file_format::BED,
        {&format_guess::isBedExtension, &format_guess::isBedGzipExtension, &format_guess::isBedBzip2Extension}
    },
    {
        io::file_format::BED3,
        {&format_guess::isBed3Extension, &format_guess::isBed3GzipExtension, &format_guess::isBed3Bzip2Extension}
    },
    {
        io::file_format::BED4,
        {&format_guess::isBed4Extension, &format_guess::isBed4GzipExtension, &format_guess::isBed4Bzip2Extension}
    },
    {
        io::file_format::BED5,
        {&format_guess::isBed5Extension, &format_guess::isBed5GzipExtension, &format_guess::isBed5Bzip2Extension}
    },
    {
        io::file_format::BED6,
        {&format_guess::isBed6Extension, &format_guess::isBed6GzipExtension, &format_guess::isBed6Bzip2Extension}
    },
    {
        io::file_format::BED7,
        {&format_guess::isBed7Extension, &format_guess::isBed7GzipExtension, &format_guess::isBed7Bzip2Extension}
    },
    {
        io::file_format::BED8,
        {&format_guess::isBed8Extension, &format_guess::isBed8GzipExtension, &format_guess::isBed8Bzip2Extension}
    },
    {
        io::file_format::BED9,
        {&format_guess::isBed9Extension, &format_guess::isBed9GzipExtension, &format_guess::isBed9Bzip2Extension}
    },
    {
        io::file_format::BED10,
        {&format_guess::isBed10Extension, &format_guess::isBed10GzipExtension, &format_guess::isBed10Bzip2Extension}
    },
    {
        io::file_format::BED11,
        {&format_guess::isBed11Extension, &format_guess::isBed11GzipExtension, &format_guess::isBed11Bzip2Extension}
    },
    {
        io::file_format::BED12,
        {&format_guess::isBed12Extension, &format_guess::isBed12GzipExtension, &format_guess::isBed12Bzip2Extension}
    },
    {
        io::file_format::REFERENCE_COORDINATES,
        {&format_guess::isReferenceCoordinatesExtension, &format_guess::isReferenceCoordinatesGzipExtension, &format_guess::isReferenceCoordinatesBzip2Extension}
    },
    {
        io::file_format::REFERENCE_COORDINATES1,
        {&format_guess::isReferenceCoordinates1Extension, &format_guess::isReferenceCoordinates1GzipExtension, &format_guess::isReferenceCoordinates1Bzip2Extension}
    },
    {
        io::file_format::REFERENCE_COORDINATES2,
        {&format_guess::isReferenceCoordinates2Extension, &format_guess::isReferenceCoordinates2GzipExtension, &format_guess::isReferenceCoordinates2Bzip2Extension}
    },
    {
        io::file_format::REFERENCE_COORDINATES3,
        {&format_guess::isReferenceCoordinates3Extension, &format_guess::isReferenceCoordinates3GzipExtension, &format_guess::isReferenceCoordinates3Bzip2Extension}
    },
    {
        io::file_format::REFERENCE_COORDINATES4,
        {&format_guess::isReferenceCoordinates4Extension, &format_guess::isReferenceCoordinates4GzipExtension, &format_guess::isReferenceCoordinates4Bzip2Extension}
    },
    {
        io::file_format::REFERENCE_COORDINATES5,
        {&format_guess::isReferenceCoordinates5Extension, &format_guess::isReferenceCoordinates5GzipExtension, &format_guess::isReferenceCoordinates5Bzip2Extension}
    },
    {
        io::file_format::REFERENCE_COORDINATES6,
        {&format_guess::isReferenceCoordinates6Extension, &format_guess::isReferenceCoordinates6GzipExtension, &format_guess::isReferenceCoordinates6Bzip2Extension}
    },
    {
        io::file_format::BIGWIG,
        {&format_guess::isBigwigExtension, &format_guess::isBigwigGzipExtension, &format_guess::isBigwigBzip2Extension}
    },
    {
        io::file_format::BIGBED,
        {&format_guess::isBigbedExtension, &format_guess::isBigbedGzipExtension, &format_guess::isBigbedBzip2Extension}
    },
    {
        io::file_format::GENEPRED,
        {&format_guess::isGenepredExtension, &format_guess::isGenepredGzipExtension, &format_guess::isGenepredBzip2Extension}
    },
    {
        io::file_format::GENEPRED_EXTENDED,
        {&format_guess::isGenepredExtension, &format_guess::isGenepredGzipExtension, &format_guess::isGenepredBzip2Extension}
    },
    {
        io::file_format::GTF,
        {&format_guess::isGtfExtension, &format_guess::isGtfGzipExtension, &format_guess::isGtfBzip2Extension}
    },
    {
        io::file_format::WIG,
        {&format_guess::isWigExtension, &format_guess::isWigGzipExtension, &format_guess::isWigBzip2Extension}
    },
    {
        io::file_format::REFERENCE_FILTERS,
        {&format_guess::isReferenceFiltersExtension, &format_guess::isReferenceFiltersGzipExtension, &format_guess::isReferenceFiltersBzip2Extension}
    },
    {
        io::file_format::REFERENCE_ANNOTATIONS,
        {&format_guess::isReferenceAnnotationsExtension, &format_guess::isReferenceAnnotationsGzipExtension, &format_guess::isReferenceAnnotationsBzip2Extension}
    },
};

/** Possible discoverable format functors. */
static const std::map<io::file_format, std::vector<std::function<bool(std::string)> > > formats_functors =
{
    {
        io::file_format::BEDGRAPH,
        {&format_guess::tryBedgraphFormat, &format_guess::tryBedgraphGzipFormat, &format_guess::tryBedgraphBzip2Format}
    },
    {
        io::file_format::BED,
        {&format_guess::tryBedFormat, &format_guess::tryBedGzipFormat, &format_guess::tryBedBzip2Format}
    },
    {
        io::file_format::BED3,
        {&format_guess::tryBed3Format, &format_guess::tryBed3GzipFormat, &format_guess::tryBed3Bzip2Format}
    },
    {
        io::file_format::BED4,
        {&format_guess::tryBed4Format, &format_guess::tryBed4GzipFormat, &format_guess::tryBed4Bzip2Format}
    },
    {
        io::file_format::BED5,
        {&format_guess::tryBed5Format, &format_guess::tryBed5GzipFormat, &format_guess::tryBed5Bzip2Format}
    },
    {
        io::file_format::BED6,
        {&format_guess::tryBed6Format, &format_guess::tryBed6GzipFormat, &format_guess::tryBed6Bzip2Format}
    },
    {
        io::file_format::BED7,
        {&format_guess::tryBed7Format, &format_guess::tryBed7GzipFormat, &format_guess::tryBed7Bzip2Format}
    },
    {
        io::file_format::BED8,
        {&format_guess::tryBed8Format, &format_guess::tryBed8GzipFormat, &format_guess::tryBed8Bzip2Format}
    },
    {
        io::file_format::BED9,
        {&format_guess::tryBed9Format, &format_guess::tryBed9GzipFormat, &format_guess::tryBed9Bzip2Format}
    },
    {
        io::file_format::BED10,
        {&format_guess::tryBed10Format, &format_guess::tryBed10GzipFormat, &format_guess::tryBed10Bzip2Format}
    },
    {
        io::file_format::BED11,
        {&format_guess::tryBed11Format, &format_guess::tryBed11GzipFormat, &format_guess::tryBed11Bzip2Format}
    },
    {
        io::file_format::BED12,
        {&format_guess::tryBed12Format, &format_guess::tryBed12GzipFormat, &format_guess::tryBed12Bzip2Format}
    },
    {
        io::file_format::REFERENCE_COORDINATES,
        {&format_guess::tryReferenceCoordinatesFormat, &format_guess::tryReferenceCoordinatesGzipFormat, &format_guess::tryReferenceCoordinatesBzip2Format}
    },
    {
        io::file_format::REFERENCE_COORDINATES1,
        {&format_guess::tryReferenceCoordinates1Format, &format_guess::tryReferenceCoordinates1GzipFormat, &format_guess::tryReferenceCoordinates1Bzip2Format}
    },
    {
        io::file_format::REFERENCE_COORDINATES2,
        {&format_guess::tryReferenceCoordinates2Format, &format_guess::tryReferenceCoordinates2GzipFormat, &format_guess::tryReferenceCoordinates2Bzip2Format}
    },
    {
        io::file_format::REFERENCE_COORDINATES3,
        {&format_guess::tryReferenceCoordinates3Format, &format_guess::tryReferenceCoordinates3GzipFormat, &format_guess::tryReferenceCoordinates3Bzip2Format}
    },
    {
        io::file_format::REFERENCE_COORDINATES4,
        {&format_guess::tryReferenceCoordinates4Format, &format_guess::tryReferenceCoordinates4GzipFormat, &format_guess::tryReferenceCoordinates4Bzip2Format}
    },
    {
        io::file_format::REFERENCE_COORDINATES5,
        {&format_guess::tryReferenceCoordinates5Format, &format_guess::tryReferenceCoordinates5GzipFormat, &format_guess::tryReferenceCoordinates5Bzip2Format}
    },
    {
        io::file_format::REFERENCE_COORDINATES6,
        {&format_guess::tryReferenceCoordinates6Format, &format_guess::tryReferenceCoordinates6GzipFormat, &format_guess::tryReferenceCoordinates6Bzip2Format}
    },
    {
        io::file_format::BIGWIG,
        {&format_guess::tryBigwigFormat, &format_guess::tryBigwigGzipFormat, &format_guess::tryBigwigBzip2Format}
    },
    {
        io::file_format::BIGBED,
        {&format_guess::tryBigbedFormat, &format_guess::tryBigbedGzipFormat, &format_guess::tryBigbedBzip2Format}
    },
    {
        io::file_format::GENEPRED,
        {&format_guess::tryGenepredFormat, &format_guess::tryGenepredGzipFormat, &format_guess::tryGenepredBzip2Format}
    },
    {
        io::file_format::GENEPRED_EXTENDED,
        {&format_guess::tryGenepredFormat, &format_guess::tryGenepredGzipFormat, &format_guess::tryGenepredBzip2Format}
    },
    {
        io::file_format::GTF,
        {&format_guess::tryGtfFormat, &format_guess::tryGtfGzipFormat, &format_guess::tryGtfBzip2Format}
    },
    {
        io::file_format::WIG,
        {&format_guess::tryWigFormat, &format_guess::tryWigGzipFormat, &format_guess::tryWigBzip2Format}
    },
};
}
}
#endif
