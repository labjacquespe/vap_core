#ifndef PCRE2CPP_H
#define PCRE2CPP_H

/* This macro must be defined before including pcre2.h. For a program that uses
only one code unit width, it makes it possible to use generic function names
such as pcre2_compile(). */
#ifndef PCRE2_CODE_UNIT_WIDTH
#define PCRE2_CODE_UNIT_WIDTH 8 /* default code unit width */
#endif

/*If you want to statically link this program against a non-dll .a file, you must
define PCRE2_STATIC before including pcre2.h, so in this environment, uncomment
the following line. */
#define PCRE2_STATIC

#include <pcre2.h>
#include <iostream>

class pcre2cpp
{
private:
    pcre2_code *_cre;

    int errornumber;
    PCRE2_SIZE erroroffset;

public:
    pcre2cpp()
    {
    }

    void compile(const std::string &pattern)
    {
        PCRE2_SPTR cpattern = (PCRE2_SPTR)pattern.c_str();
        _cre = pcre2_compile(
                   cpattern,               /* the pattern */
                   PCRE2_ZERO_TERMINATED, /* indicates pattern is zero-terminated */
                   0,                     /* default options */
                   &errornumber,          /* for error number */
                   &erroroffset,          /* for error offset */
                   NULL);                 /* use default compile context */

        if(_cre == NULL)
        {
            PCRE2_UCHAR buffer[256];
            pcre2_get_error_message(errornumber, buffer, sizeof(buffer));
            std::cerr << "PCRE2 compilation failed at offset " << erroroffset << ':' << buffer << "\n";
            // printf("PCRE2 compilation failed at offset %d: %s\en", (int)erroroffset, buffer);
            throw;
        }
    }

    // bool match(const std::string &subject)
    // {
    //     csubject = (PCRE2_SPTR)subject.c_str();

    //     pcre2_match_data *match_data = pcre2_match_data_create(1, NULL);

    //     int rc = pcre2_match(
    //                  _cre,                   /* the compiled pattern */
    //                  csubject,              /* the subject string */
    //                  subject.size(),        /* the length of the subject */
    //                  0,                     /* start at offset 0 in the subject */
    //                  0,                     /* default options */
    //                  match_data,            /* block for storing the result */
    //                  NULL);                 /* use default match context */

    //     pcre2_match_data_free(match_data);   /* Release memory used for the match */
    //     return rc >= 0;
    // }

    bool _match(PCRE2_SPTR csubject, size_t len) const
    {
        pcre2_match_data *match_data = pcre2_match_data_create(1, NULL);

        int rc = pcre2_match(
                     _cre,                   /* the compiled pattern */
                     csubject,              /* the subject string */
                     len,                   /* the length of the subject */
                     0,                     /* start at offset 0 in the subject */
                     0,                     /* default options */
                     match_data,            /* block for storing the result */
                     NULL);                 /* use default match context */

        pcre2_match_data_free(match_data);   /* Release memory used for the match */
        return rc >= 0;
    }

    bool match(const std::string &subject) const
    {
        PCRE2_SPTR csubject = (PCRE2_SPTR)subject.c_str();
        return _match(csubject, subject.size());
    }

    template <class bi_directional_iterator>
    bool match(bi_directional_iterator first, bi_directional_iterator last) const
    {
        PCRE2_SPTR csubject = (PCRE2_SPTR)&(*first);
        return _match(csubject, (last - first));
    }

    ~pcre2cpp()
    {
        pcre2_code_free(_cre);
    }
};

#endif
