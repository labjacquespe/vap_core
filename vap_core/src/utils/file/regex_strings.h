/**
 * @file    file_extension.h
 * @author  Charles Coulombe
 * @date    August 17,2015, 15:25
 * @version 1.2.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef REGEX_STRINGS_H
#define REGEX_STRINGS_H

#include <string>
#include <map>

namespace vap
{
/** Match all printable characters (except space): 33 to 126 in ASCII code. */
const std::string RE_STR = "^[\\x21-\\x7E]+$";

/** Match all printable characters (including space): 32 to 126 in ASCII code. */
const std::string RE_STR_SPACE = "^[\\x20-\\x7E]+$";

/** Match a positive or negative integer value. */
const std::string RE_INT = "^-?\\d+$";

/** Match a positive only integer value. */
const std::string RE_UINT = "^\\d+$";

/** Match a float value with scientific notation. */
const std::string RE_FLOAT = "^[+-]?(?=\\d*[\\d.eE])(?=\\.?\\d)\\d*\\.?\\d*(?:[eE][+-]?\\d+)?$";

/** Match a score (a dot or integer value). */
const std::string RE_SCORE = "^(\\.|[0-9]|[0-9][0-9]|[0-9][0-9][0-9]|1000)$";

/** Match a strand (a dot, plus or minus). */
const std::string RE_STRAND = "^[\\.\\+\\-]$";

/** Match a RGB value (0 or triplet of integers (0-255) separated by comma). */
const std::string RE_RGB = "^([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]),([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]),([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$";

/** Match a comma separated list of integer values. */
const std::string RE_UINT_CSV = "^(\\d+,?)+$";

/** Match the literal 'gene_id' */
const std::string RE_GENEID = "^gene_id$";

/** Match the literal 'transcript_id' */
const std::string RE_TRANSID = "^transcript_id$";

/** Match '-1' or 0' or '1' or '2' or '.' */
const std::string RE_FRAME = "^-1|[0-2\\.]+$";

/** Match GTF attribute value ending with a semi-colon */
const std::string RE_ATTR = "^[\\x21-\\x7E]+;$";

/** Match GTF Feature attribute. */
const std::string RE_FEATURE = "^(?:start_codon|stop_codon|CDS|exon)$";

/** Match Wiggle chrom field */
const std::string RE_CHROM = "^chrom=[\\x21-\\x7E]+$";

/** Match Wiggle span field */
const std::string RE_SPAN = "^span=\\d+$";

/** Match Wiggle step field */
const std::string RE_STEP = "^step=\\d+$";

/** Match Wiggle start field */
const std::string RE_START = "^start=-?\\d+$";

/** Match GenepredExt cds stat field */
const std::string RE_CDS_STAT = "^(?:none|unk|incmpl|cmpl)$";

/** A list of common patterns:
    - RE_STR
    - RE_STR_SPACE
    - RE_INT
    - RE_UINT
    - RE_FLOAT
    - RE_SCORE
    - RE_STRAND
    - RE_RGB
    - RE_UINT_CSV
    - RE_GENEID
    - RE_TRANSID
    - RE_FRAME
    - RE_ATTR
    - RE_FEATURE
    - RE_CHROM
    - RE_SPAN
    - RE_STEP
    - RE_START
    - RE_CDS_STAT
 */
const std::map<std::string, std::string> COMMON_PATTERNS =
{
    {"RE_STR", RE_STR}, /** Match all printable characters (except space): 33 to 126 in ASCII code. */
    {"RE_STR_SPACE", RE_STR_SPACE}, /** Match all printable characters (including space): 32 to 126 in ASCII code. */
    {"RE_INT", RE_INT}, /** Match a positive or negative integer value. */
    {"RE_UINT", RE_UINT}, /** Match a positive only integer value. */
    {"RE_FLOAT", RE_FLOAT}, /** Match a float value with scientific notation. */
    {"RE_SCORE", RE_SCORE}, /** Match a score (a dot or integer value). */
    {"RE_STRAND", RE_STRAND}, /** Match a strand (a dot, plus or minus). */
    {"RE_RGB", RE_RGB}, /** Match a RGB value (0 or triplet of integers (0-255) separated by comma). */
    {"RE_UINT_CSV", RE_UINT_CSV}, /** Match a comma separated list of integer values. */
    {"RE_GENEID", RE_GENEID}, /** Match the literal 'gene_id' */
    {"RE_TRANSID", RE_TRANSID}, /** Match the literal 'transcript_id' */
    {"RE_FRAME", RE_FRAME}, /** Match '0' or '1' or '2' or '.' */
    {"RE_ATTR", RE_ATTR}, /** Match GTF attribute value ending with a semi-colon */
    {"RE_FEATURE", RE_FEATURE}, /** Match GTF Feature attribute. */
    {"RE_CHROM", RE_CHROM}, /** Match Wiggle chrom field */
    {"RE_SPAN", RE_SPAN}, /** Match Wiggle span field */
    {"RE_STEP", RE_STEP}, /** Match Wiggle step field */
    {"RE_START", RE_START}, /** Match Wiggle start field */
    {"RE_CDS_STAT", RE_CDS_STAT}, /** Match GenepredExt cds stat field */
};

/** A list of track line attributes-patterns (case sensitive):
    - name
    - description
    - type
    - visibility
    - color
    - itemRgb
    - colorByStrand
    - useScore
    - group
    - priority
    - db
    - offset
    - maxItems
    - url
    - htmlUrl
    - bigDataUrl
    - altColor
    - autoScale
    - alwaysZero
    - gridDefault
    - maxHeightPixels
    - graphType
    - viewLimits
    - viewLimitsMax
    - yLineMark
    - yLineOnOff
    - windowingFunction
    - smoothingWindow
    - transformFunc

    @see http://genome.ucsc.edu/goldenpath/help/customTrack.html#TRACK
*/
const std::map<std::string, std::string> TRACK_PATTERNS =
{
    {"name", RE_STR_SPACE},
    {"description", RE_STR_SPACE},
    {"type", "^()$"}, /*Must be completed dynamically.*/
    {"visibility", "^(?:[0-4]|hide|dense|full|pack|squish)$"},
    {"color", RE_RGB},
    {"itemRgb", "^(off|on)$"},
    {"colorByStrand", "^([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]),([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]),([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\s([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]),([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]),([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])$"},
    {"useScore", RE_SCORE},
    {"group", RE_STR},
    {"priority", RE_UINT},
    {"db", RE_STR_SPACE},
    {"offset", RE_UINT},
    {"maxItems", RE_UINT},
    {"url", "^[^\\s]+$"},
    {"htmlUrl", "^[^\\s]+$"},
    {"bigDataUrl", "^[^\\s]+$"},
    {"altColor", RE_RGB},
    {"autoScale", "^(off|on)$"},
    {"alwaysZero", "^(off|on)$"},
    {"gridDefault", "^(off|on)$"},
    {"maxHeightPixels", "^\\d+:\\d+:\\d+$"},
    {"graphType", "^(bar|points)$"},
    {"viewLimits", "^([+-]?(?=\\d*[\\d.eE])(?=\\.?\\d)\\d*\\.?\\d*(?:[eE][+-]?\\d+)?:[+-]?(?=\\d*[\\d.eE])(?=\\.?\\d)\\d*\\.?\\d*(?:[eE][+-]?\\d+)?)$"},
    {"viewLimitsMax", "^([+-]?(?=\\d*[\\d.eE])(?=\\.?\\d)\\d*\\.?\\d*(?:[eE][+-]?\\d+)?:[+-]?(?=\\d*[\\d.eE])(?=\\.?\\d)\\d*\\.?\\d*(?:[eE][+-]?\\d+)?)$"},
    {"yLineMark", RE_FLOAT},
    {"yLineOnOff", "^(off|on)$"},
    {"windowingFunction", "^(mean\\+whiskers|maximum|mean|minimum)$"},
    {"smoothingWindow", "^(off|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16)$"},
    {"transformFunc","^(NONE|LOG)$"},
};

/** A list of browser line attributes-patterns (case sensitive):
    - position
    - hide
    - dense
    - pack
    - squish
    - full

    Browser lines consist of the format: browser attribute_name attribute_value(s)
    @see http://genome.ucsc.edu/goldenpath/help/customTrack.html#BROWSER
*/
const std::map<std::string, std::string> BROWSER_PATTERNS =
{
    /** Match str:int-int */
    {"position", "^[\\x21-\\x7E]+:\\d+-\\d+$"},
    /** Match 'all' or a list of multiple track names blank-separated. */
    {"hide", "^(?:all|[\\x21-\\x7E]+(?:[\\t\\s]+[\\x21-\\x7E]+)*)$"},
    /** Match 'all' or a list of multiple track names blank-separated. */
    {"dense", "^(?:all|[\\x21-\\x7E]+(?:[\\t\\s]+[\\x21-\\x7E]+)*)$"},
    /** Match 'all' or a list of multiple track names blank-separated. */
    {"pack", "^(?:all|[\\x21-\\x7E]+(?:[\\t\\s]+[\\x21-\\x7E]+)*)$"},
    /** Match 'all' or a list of multiple track names blank-separated. */
    {"squish","^(?:all|[\\x21-\\x7E]+(?:[\\t\\s]+[\\x21-\\x7E]+)*)$"},
    /** Match 'all' or a list of multiple track names blank-separated. */
    {"full","^(?:all|[\\x21-\\x7E]+(?:[\\t\\s]+[\\x21-\\x7E]+)*)$"}
};
}
#endif
