/**
 * @file    file_format.h
 * @author  Charles Coulombe
 * @date    7 August 2015, 11:55
 * @version 1.2.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FILE_FORMAT_H
#define FILE_FORMAT_H

#include <fstream>
#include "../text/string_utils.h"
namespace vap
{
namespace io
{
enum class file_format
{
    UNKNOWN,
    BEDGRAPH,
    BED, BED3, BED4, BED5, BED6, BED7, BED8, BED9, BED10, BED11, BED12,
    REFERENCE_COORDINATES, REFERENCE_COORDINATES1, REFERENCE_COORDINATES2, REFERENCE_COORDINATES3, REFERENCE_COORDINATES4, REFERENCE_COORDINATES5, REFERENCE_COORDINATES6,
    REFERENCE_ANNOTATIONS,
    REFERENCE_FILTERS,
    BIGWIG,
    BIGBED,
    GENEPRED,
    GENEPRED_EXTENDED,
    GTF,
    WIG,
    CUSTOM,
};

const std::vector<io::file_format> SUPPORTED_FORMATS =
{
    file_format::UNKNOWN,
    file_format::BEDGRAPH,
    file_format::BED, file_format::BED3, file_format::BED4, file_format::BED5, file_format::BED6, file_format::BED7, file_format::BED8, file_format::BED9, file_format::BED10, file_format::BED11, file_format::BED12,
    file_format::REFERENCE_COORDINATES, file_format::REFERENCE_COORDINATES1, file_format::REFERENCE_COORDINATES2, file_format::REFERENCE_COORDINATES3, file_format::REFERENCE_COORDINATES4, file_format::REFERENCE_COORDINATES5, file_format::REFERENCE_COORDINATES6,
    file_format::REFERENCE_ANNOTATIONS,
    file_format::REFERENCE_FILTERS,
    file_format::BIGWIG,
    file_format::BIGBED,
    file_format::GENEPRED,
    file_format::GENEPRED_EXTENDED,
    file_format::GTF,
    file_format::WIG,
};

inline std::string to_string(const io::file_format &ff)
{
    switch(ff)
    {
        case file_format::BEDGRAPH: return "bedgraph";
        case file_format::BED: return "bed";
        case file_format::BED3: return "bed3";
        case file_format::BED4: return "bed4";
        case file_format::BED5: return "bed5";
        case file_format::BED6: return "bed6";
        case file_format::BED7: return "bed7";
        case file_format::BED8: return "bed8";
        case file_format::BED9: return "bed9";
        case file_format::BED10: return "bed10";
        case file_format::BED11: return "bed11";
        case file_format::BED12: return "bed12";
        case file_format::REFERENCE_COORDINATES: return "reference_coordinates";
        case file_format::REFERENCE_COORDINATES1: return "reference_coordinates1";
        case file_format::REFERENCE_COORDINATES2: return "reference_coordinates2";
        case file_format::REFERENCE_COORDINATES3: return "reference_coordinates3";
        case file_format::REFERENCE_COORDINATES4: return "reference_coordinates4";
        case file_format::REFERENCE_COORDINATES5: return "reference_coordinates5";
        case file_format::REFERENCE_COORDINATES6: return "reference_coordinates6";
        case file_format::REFERENCE_ANNOTATIONS: return "reference_annotations";
        case file_format::REFERENCE_FILTERS: return "reference_filters";
        case file_format::BIGWIG: return "bigwig";
        case file_format::BIGBED: return "bigbed";
        case file_format::GENEPRED: return "genepred";
        case file_format::GENEPRED_EXTENDED: return "genepred_extended";
        case file_format::GTF: return "gtf";
        case file_format::WIG: return "wig";
        case file_format::CUSTOM: return "custom";
        case file_format::UNKNOWN:
        default: return "unknown";
    }
}

inline io::file_format to_file_format(const std::string &str)
{
    if(util::tolower(str) == "bedgraph") return file_format::BEDGRAPH;
    else if(util::tolower(str) == "bed") return file_format::BED;
    else if(util::tolower(str) == "bed3") return file_format::BED3;
    else if(util::tolower(str) == "bed4") return file_format::BED4;
    else if(util::tolower(str) == "bed5") return file_format::BED5;
    else if(util::tolower(str) == "bed6") return file_format::BED6;
    else if(util::tolower(str) == "bed7") return file_format::BED7;
    else if(util::tolower(str) == "bed8") return file_format::BED8;
    else if(util::tolower(str) == "bed9") return file_format::BED9;
    else if(util::tolower(str) == "bed10") return file_format::BED10;
    else if(util::tolower(str) == "bed11") return file_format::BED11;
    else if(util::tolower(str) == "bed12") return file_format::BED12;
    else if(util::tolower(str) == "reference_coordinates") return file_format::REFERENCE_COORDINATES;
    else if(util::tolower(str) == "reference_coordinates1") return file_format::REFERENCE_COORDINATES1;
    else if(util::tolower(str) == "reference_coordinates2") return file_format::REFERENCE_COORDINATES2;
    else if(util::tolower(str) == "reference_coordinates3") return file_format::REFERENCE_COORDINATES3;
    else if(util::tolower(str) == "reference_coordinates4") return file_format::REFERENCE_COORDINATES4;
    else if(util::tolower(str) == "reference_coordinates5") return file_format::REFERENCE_COORDINATES5;
    else if(util::tolower(str) == "reference_coordinates6") return file_format::REFERENCE_COORDINATES6;
    else if(util::tolower(str) == "reference_annotations") return file_format::REFERENCE_ANNOTATIONS;
    else if(util::tolower(str) == "reference_filters") return file_format::REFERENCE_FILTERS;
    else if(util::tolower(str) == "bigwig") return file_format::BIGWIG;
    else if(util::tolower(str) == "bigbed") return file_format::BIGBED;
    else if(util::tolower(str) == "genepred") return file_format::GENEPRED;
    else if(util::tolower(str) == "genepred_extended") return file_format::GENEPRED_EXTENDED;
    else if(util::tolower(str) == "gtf") return file_format::GTF;
    else if(util::tolower(str) == "wig") return file_format::WIG;
    else if(util::tolower(str) == "custom") return file_format::CUSTOM;
    else return file_format::UNKNOWN;
}

inline std::ostream &operator<<(std::ostream &out, const io::file_format &ff)
{
    out << to_string(ff);
    return out;
}

inline std::istream &operator>>(std::istream &in, io::file_format &ff)
{
    std::string str;
    in >> str;
    ff = to_file_format(str);
    return in;
}
}
}
#endif
