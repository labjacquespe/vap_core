/**
 * @file   convert.h
 * @author Charles Coulombe
 *
 * @date 30 October 2012, 12:23
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013,2014 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @ingroup utils
 */

#ifndef CONVERT_H
#define CONVERT_H

#include <string>
#include <stdexcept>
#include <typeinfo>
#include <sstream>
#include <cmath>
#include <algorithm>
#include <cstdlib>
#include <limits>

namespace vap
{
namespace util
{

/**
 * Converter utility
 */
class convert
{
public:
    template <typename TYPE>
    static std::string toString(const TYPE &value);

    template <typename TYPE>
    static TYPE toType(const std::string &value);
};

/**
 * Converts a source type to string.
 * @warning slow conversion
 *
 * @param value value to convert
 * @return a string of source value
 */
template <class SOURCE>
std::string convert::toString(const SOURCE &value)
{
    std::stringstream ss;
    ss << value;
    if(ss.fail())
        throw std::logic_error("Value cannot be read.");

    return ss.str();
}

/**
 * Converts a string to target type
 * @warning slow conversion
 *
 * @note  Target type must be writtable by stream operator>>.
 * @param value string value to convert
 * @return target type if conversion is successful.
 */
template <class TARGET>
TARGET convert::toType(const std::string &value)
{
    std::stringstream ss;
    TARGET out;

    ss << value; // try to read the value
    if(ss.fail())
        throw std::logic_error("Value cannot be read.");

    ss >> out; // try to convert the value
    if(ss.fail())
        throw std::logic_error("Value cannot be converted.");

    return out;
}

/**
 * Fastest and naive way to transform a string into an signed integer.
 * @note Conversion will stop at @b first non numerical character.
 * @param str @c c-string to convert
 * @return @c integer value if conversion succeed, @c 0 in all others cases
 */
inline int stoi(const char *str)
{
    int x = 0;
    bool neg = false;

    // checks if integer is negative
    if(*str == '-')
    {
        neg = true;
        ++str; // eat sign
    }

    // convert character to numerical value until
    // a non numerical character is found or string is exhausted
    while(*str >= '0' && *str <= '9')
    {
        x = (x * 10) + (*str - '0');
        ++str;
    }

    // return negative or positive integer
    return neg ? -x : x;

}

/**
 * Fastest and naive way to transform a string into an signed integer.
 * @note Conversion will stop at @b first non numerical character.
 * @param first Iterator at beginning
 * @param last Iterator at end
 * @return @c integer value if conversion succeed, @c 0 in all others cases
 */
inline int stoi(std::string::const_iterator first, std::string::const_iterator last)
{
    int x = 0;
    bool neg = false;

    // checks if integer is negative
    if(*first == '-')
    {
        neg = true;
        ++first; // eat sign
    }

    // convert character to numerical value until
    // a non numerical character is found or firsting is exhausted
    while(*first >= '0' && *first <= '9' && first != last)
    {
        x = (x * 10) + (*first - '0');
        ++first;
    }

    // return negative or positive integer
    return neg ? -x : x;
}

/**
 * Fastest and naive way to transform a string into an unsigned integer.
 * @note Conversion will stop at @b first non numerical character.
 * @param str @c c-string to convert
 * @return @c integer value if conversion succeed, @c 0 in all others cases
 */
inline unsigned int stoui(const char *str)
{
    unsigned int x = 0;

    // convert character to numerical value until
    // a non numerical character is found or string is exhausted
    while(*str >= '0' && *str <= '9')
    {
        x = (x * 10) + (*str - '0');
        ++str;
    }

    // return positive integer
    return x;
}

/**
 * Fastest and naive way to transform a string into an unsigned integer.
 * @note Conversion will stop at @b first non numerical character.
 * @param first Iterator at beginning
 * @param last Iterator at end
 * @return @c integer value if conversion succeed, @c 0 in all others cases
 */
inline unsigned int stoui(std::string::const_iterator first, std::string::const_iterator last)
{
    unsigned int x = 0;

    // convert character to numerical value until
    // a non numerical character is found or string is exhausted
    while(*first >= '0' && *first <= '9' && first != last)
    {
        x = (x * 10) + (*first - '0');
        ++first;
    }

    // return positive integer
    return x;
}

/**
 * Fastest and naive way to transform a string into a double.
 * @note Conversion will stop at @b first non numerical character.
 * @param str @c c-string to convert
 * @return @c double value if conversion succeed, @c 0 in all others cases
 */
// inline double stod2(const char *str)
// {
//     // 2.0×10^2
//     // 2.0e2

//     double r = 0.0;
//     bool neg = false;
//     bool expneg = false;

//     if(*str == '-')
//     {
//         neg = true;
//         ++str;
//     }

//     // convert character to numerical value until a non numerical character is found or string is exhausted
//     while(*str >= '0' && *str <= '9')
//     {
//         r = (r*10.0) + (*str - '0');
//         ++str;
//     }

//     if(*str == '.')
//     {
//         double f = 0.0;
//         int n = 0;
//         ++str;

//         // convert character to numerical value until a non numerical character is found or string is exhausted
//         while(*str >= '0' && *str <= '9')
//         {
//             f = (f*10.0) + (*str - '0');
//             ++str;
//             ++n;
//         }

// /*********/
//         if(*str == 'e' || *str == 'E') // Scientific exponent
//         {
//             ++str;

//             if(*str == '-') {expneg = true; ++str;}
//             else if(*str == '+') {expneg = false; ++str;}

//             // convert character to numerical value until a non numerical character is found or string is exhausted
//             while(*str >= '0' && *str <= '9')
//             {
//                 f = (f*10.0) + (*str - '0');
//                 ++str;
//                 ++n;
//             }
//         }else
//         /*********/
//         {r += f / std::pow(10.0, n);}
//     }

//     if(neg)
//     {
//         r = -r;
//     }

//     return r;
// }

/**
 * Fastest and naive way to transform a string into a double.
 * @note Conversion will stop at @b first non numerical character.
 * @param str @c c-string to convert
 * @return @c double value if conversion succeed, @c 0 in all others cases
 */
inline double stod(const char *str)
{
    return std::atof(str);
}

/**
 * Fastest and naive way to transform a string into a double.
 * @note Conversion will stop at @b first non numerical character.
 * @param first Iterator at beginning
 * @param last Iterator at end
 * @return @c double value if conversion succeed, @c 0 in all others cases
 */
inline double stod(std::string::const_iterator first, std::string::const_iterator last)
{
    return std::atof(std::string(first, last).c_str());
}

/**
 * Fastest and naive way to transform a string into a float.
 * @note Conversion will stop at @b first non numerical character.
 * @param str @c c-string to convert
 * @return @c float value if conversion succeed, @c 0 in all others cases
 */
inline float stof(const char *str)
{
    return static_cast<float>(std::atof(str));
}

/**
 * Fastest and naive way to transform a string into a float.
 * @note Conversion will stop at @b first non numerical character.
 * @param first Iterator at beginning
 * @param last Iterator at end
 * @return @c float value if conversion succeed, @c 0 in all others cases
 */
inline double stof(std::string::const_iterator first, std::string::const_iterator last)
{
    return static_cast<float>(std::atof(std::string(first, last).c_str()));
}

/**
 * Fastest and naive way to transform a string into a boolean.
 * @note Conversion will stop at @b first non numerical character.
 * @param str @c c-string to convert
 * @return @c bool value if conversion succeed, @c 0 in all others cases
 */
inline bool stob(const char *str)
{
    return str[0] != '0';
}

/**
 * Fastest and naive way to transform a string into a boolean.
 * @note Conversion will stop at @b first non numerical character.
 * @param first Iterator at beginning
 * @param last Iterator at end
 * @return @c bool value if conversion succeed, @c 0 in all others cases
 */
inline bool stob(std::string::const_iterator first, std::string::const_iterator last)
{
    return *first != '0';
}

/**
 * Fastest and naive way to transform an integer into a string.
 *
 * @note Conversion will stop at @b first non numerical character.
 *
 * @param value integer value
 *
 * @return @c integer value if conversion succeed, @c 0 in all others cases
 */
template<class INTEGER>
std::string itos(INTEGER value)
{
    std::string rv(std::numeric_limits<INTEGER>::digits10 + 2, 0);
    size_t i = 0;

    if(!value)
    {
        rv[i++] = '0';
    }
    else
    {
        size_t ro = 0;
        if(value < 0)
        {
            rv[i++] = '-';
            value = std::abs(static_cast<double>(value));
            ro = 1;
        }
        for(INTEGER b; value; value = b)
        {
            b = value / 10;
            INTEGER c = value % 10;
            rv[i++] = static_cast<char>('0' + c);
        }
        std::reverse(&rv[ro], &rv[i]);
    }

    rv.resize(i);
    return rv;
}
}
}
#endif  /* CONVERT_H */
