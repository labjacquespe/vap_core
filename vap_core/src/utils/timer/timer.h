/**
 * @file   timer.h
 * @author Charles Coulombe
 *
 * @date 10 October 2012, 20:45
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @ingroup utils
 */

#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <fstream>

namespace vap
{
namespace util
{

/**
 * Timer utility.
 * Compute elapsed time.
 */
class timer
{
private:
    // ----------------------- attributes
    typedef std::chrono::high_resolution_clock hr_clock;
    typedef std::chrono::duration<double, std::ratio<1> > seconds;
    typedef std::chrono::time_point<hr_clock> time_point;
    time_point _start;

public:
    // ----------------------- attributes

    // ----------------------- constructor
    explicit timer(bool run = false)
        : _start(time_point())
    {
        if(run) start();
    }

    // ----------------------- accessors

    // ----------------------- modifiers

    // ----------------------- methods
    void start()
    {
        _start = hr_clock::now();
    }
    void restart()
    {
        start();
    }
    double elapsed() const
    {
        return std::chrono::duration_cast<seconds>(hr_clock::now() - _start).count();
    }
}; /* timer class */

std::ostream &operator<<(std::ostream &out, const timer &tmr)
{
    return out << tmr.elapsed();
}
} /* util namespace end */
} /* vap namespace end */
#endif  /* TIMER_H */
