/**
 * @file   iterator_utils.h
 * @author Charles Coulombe
 *
 * @date 20 November 2012, 14:26
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @ingroup utils
 */

#ifndef ITERATOR_UTILS_H
#define	ITERATOR_UTILS_H

namespace vap
{
namespace util
{

/**
 * Computes in constant time distance between two iterators.
 * @note @c BiDirectional iterator only.
 *
 * @param second upper bound
 * @param first lower bound
 * @return distance
 */
template <class ITERATOR>
inline int distance(ITERATOR second, ITERATOR first)
{
    return second - first;
}
} /* util namespace end */
} /* vap namespace end */
#endif	/* ITERATOR_UTILS_H */

