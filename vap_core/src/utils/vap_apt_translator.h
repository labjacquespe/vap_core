/*
 * File:   vap_apt_translator.h
 * Author: charles
 *
 * Created on February 20, 2016, 10:59 AM
 */

#ifndef VAP_APT_TRANSLATOR_H
#define VAP_APT_TRANSLATOR_H

#include <string>
#include <fstream>
#include <sstream>
#include <boost/algorithm//string/predicate.hpp>
#include <boost/algorithm//string.hpp>

class vap_apt_translator
{
private:

    /**
     * Translate the APT line to standard simple INI line.
     * Converts '~~@' to standard key value line.
     * Converts all others lines to comments by inserting '#' in front.
     * @remark Does not insert sections.
     *
     * @param line APT line
     * @return INI converted line
     */
    static std::string _transform(const std::string line)
    {
        std::string str = "";

        if(boost::starts_with(line, "~~@")) str = line.substr(3);
        else if(!line.empty()) str = "#" + line;

        str += "\n";
        return str;
    }

public:
    /**
     * Translate the APT file to standard simple INI file.
     * @remark Does not insert sections.
     * @param in Input file.
     * @param out Output file.
     */
    static void to_ini_file(std::istream &in, std::ostream &out)
    {
        std::string line = "";
        while(std::getline(in, line) && out)
            out << _transform(line);
    }

    /**
     * Translate the APT file to standard simple INI file.
     * @remark Does not insert sections.
     * @param in Input file name.
     * @param out Output file name.
     */
    static void to_ini_file(const std::string &from, const std::string &to)
    {
        std::ifstream in(from);
        std::ofstream out(to);
        to_ini_file(in, out);
    }

    /**
     * Translate the APT file to standard simple INI file.
     * @remark Does not insert sections.
     * @param in Input file.
     * @param out Output stream.
     */
    static void to_ini_stream(std::ifstream &in, std::stringstream &out)
    {
        std::string line = "";
        while(std::getline(in, line) && out)
            out << _transform(line);
    }

        /**
     * Translate the APT file to standard simple INI file.
     * @remark Does not insert sections.
     * @param in Input file.
     * @param out Output stream.
     */
    static void to_ini_stream(std::stringstream &in, std::stringstream &out)
    {
        std::string line = "";
        while(std::getline(in, line) && out)
            out << _transform(line);
    }

    /**
     * Translate the APT file to standa
     rd simple INI file.
     * @remark Does not insert sections.
     * @param from Input file name.
     * @param to Output stream.
     */
    static void to_ini_stream(const std::string &from, std::stringstream &to)
    {
        std::ifstream in(from);
        to_ini_stream(in, to);
    }
};

#endif /* VAP_APT_TRANSLATOR_H */
