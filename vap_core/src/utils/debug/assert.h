/**
 * @file    assert.h
 * @author  Charles Coulombe
 * @date    28 November 2012, 20:27
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASSERT_H
#define	ASSERT_H

#ifndef NDEBUG

#include <cstdlib>
#include <iostream>

namespace vap
{
#define ASSERT(condition, message) \
        do{ if (! (condition)) { \
            std::cerr << "Assertion \"" #condition "\" failed in " << __FILE__ \
                      << " line " << __LINE__ << " : \n\t" << message << std::endl; \
            std::exit(EXIT_FAILURE); \
        }}while (false)
#else
namespace vap
{
#define ASSERT(condition, message) do{ ((void)0); }while(false)
#endif
} /* vap namespace end */

#endif	/* ASSERT_H */

