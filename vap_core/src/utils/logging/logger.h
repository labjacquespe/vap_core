/**
 * @file   logger.h
 * @author Charles Coulombe
 *
 * @date 6 September 2012, 21:38
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOGGER_H
#define	LOGGER_H

// pre processor directives
#include <fstream>
#include <sstream>
#include <string>
#include <cassert>

#include "./record.h"

#include "../../defs.h"
#include "../type/convert.h"

namespace vap
{
// constants
#define DEFAULT_LOG_FILENAME "logfile"
#define DEFAULT_LOG_FILE_EXTENSION ".log"

#define LOGGER_STACKTRACE(line, file) "\n\tOn line " + util::itos(line) + " of file \"" + file + "\"."

// type definition
typedef unsigned char options_t;

/**
 * Logging options
 */
struct loggingOptions
{

    /**
     * Logging options :
     *  - None : Nothing can be logged
     *  - All : Everything can be logged
     *  - Informations : Only record of type "Information" can be logged
     *  - Warnings : Only record of type "Warning" can be logged
     *  - Errors : Only record of type "Error" can be logged
     */
    enum options
    {
        NONE = 0x00,
        ALL = 0x01,
        INFORMATIONS = 0x02,
        WARNINGS = 0x04,
        ERRORS = 0x08
    };
};

/**
 * Logging engine use for informative utilities
 * Log all transactions.
 * Log start time and end time.
 * Log transaction level and errors.
 */
class logger
{
  private:

    uint_t _errorsCount; // number of error in this logger
    uint_t _warningCount; //number of warning in this logger

    std::ofstream _writer; //writer to write in the stream
    std::string _path; //path of log file
    record _header; //log file header
    record _tail; //log file tail
    std::string _sep;

    options_t _options;

    logger(const logger &l); //{NOT IMPLEMENTED} deactivate copy
    void operator=(const logger &l); //{NOT IMPLEMENTED} deactivate affectation operator

    void _init(const std::string &filename, options_t options); //initialize this
    bool _canLog(recordLevel::level lvl);
    bool _log(const record &rec);
    bool _log(const record &rec, unsigned int line, const std::string &file);
    std::string _separator();
    bool _isFlagSet(unsigned value, unsigned index);
    static void _clean();

    static logger *_instance; // unique instance

    logger();
    logger(const char *filename, options_t op);
    logger(const std::string &filename, options_t op);

  public:

    // ---------------------------- constructors
    //    logger();
    //    logger(const char *filename);
    //    logger(const std::string &filename);
    //    logger(const char *filename, levelOptions::options options);
    //    logger(const std::string &filename, levelOptions::options options);

    static logger &create();
    static logger &create(const char *filename, options_t op);
    static logger &create(const std::string &filename, options_t op);
    static logger &instance();

    // ---------------------------- desctructor
    ~logger();

    // ---------------------------- accessors
    const std::string &path() const;
    uint_t errors() const;
    options_t loggingOptions() const;

    // ---------------------------- modifiers
    void setLoggingOptions(options_t newOptions);

    // ---------------------------- methods
    bool log(const record &rec);
    bool log(const record &rec, unsigned int line, const std::string &file);

    bool log(recordLevel::level lvl, const std::string &text);
    bool log(recordLevel::level lvl, const std::string &text, unsigned int line, const std::string &file);

    bool log(recordLevel::level lvl, const char *text);
    bool log(recordLevel::level lvl, const char *text, unsigned int line, const std::string &file);

    void head(const std::string &h);
    void head(const char *h = NULL);

    void tail(const std::string &t);
    void tail(const char *t = NULL);

    void close();
};
} /* vap namespace end */

#endif	/* LOGGER_H */
