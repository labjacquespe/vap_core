/**
 * @file   record.h
 * @author Charles Coulombe
 *
 * @date 6 September 2012, 21:49
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RECORD_H
#define	RECORD_H

#include <string>
#include <time.h>

namespace vap
{
struct recordLevel
{

    /**
     *  Record levels
     */
    enum level
    {
        /**Information record*/
        INFORMATION = 0,
        /**Warning record*/
        WARNING,
        /**Error record*/
        ERROR
    };

    /**
     * Gets the std::string representation of the enum value
     * @param value enum value
     * @return name as a std::string
     */
    static std::string toString(level value)
    {
        switch(value)
        {
            case INFORMATION:
                return "Information";

            case WARNING:
                return "Warning";

            case ERROR:
                return "Error";

            default:
                return "Unknown";
        }
    }
};

class record
{
private:
    std::string _text; // record internal text
    std::string _timestamp; // record creation timestamp
    recordLevel::level _level; // current record level
    time_t _timeStart;
    struct tm * _timeInfo;

    void _format();
    void _init(const char *s, recordLevel::level lvl);
public:
    // ------------------------- constructors
    record();
    record(const std::string &s);
    record(const char *s);
    record(const std::string &s, recordLevel::level lvl);
    record(const char *s, recordLevel::level lvl);
    record(const record &r);
    ~ record();

    // ------------------------- accessors
    recordLevel::level level() const;
    const std::string &content() const;
    const std::string &getTimeStamp() const;

    // ------------------------- modifiers
    void setLevel(recordLevel::level lvl);
    void setContent(const std::string &s);
    void setContent(const char *s);

    // ------------------------- methods
    void appendContent(const std::string &s);
    void appendContent(const char *s);


    record &operator=(const record &r);
};
} /* vap namespace end */

#endif	/* RECORD_H */

