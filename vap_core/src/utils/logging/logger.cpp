/**
 * @file   logger.cpp
 * @author Charles Coulombe
 *
 * @date 6 September 2012, 21:38
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./logger.h"

namespace vap
{
// set instance
logger *logger::_instance = NULL;

// =============================================================================
//                              PRIVATE
// =============================================================================

/**
 * Construct logger with default filename : LogFile.log
 */
logger::logger()
{
    std::string s = APPNAME;
    s += "_";
    s += DEFAULT_LOG_FILENAME;
    s += DEFAULT_LOG_FILE_EXTENSION;
    _init(s, loggingOptions::ALL);
}

/**
 * Construct with a specific filename and logging options
 *
 * @param filename log filename
 * @param options logging options
 */
logger::logger(const char* filename, options_t op)
{
    _init(filename, op);
}

/**
 * Construct with a specific filename and logging options
 *
 * @param filename log filename
 * @param options logging options
 */
logger::logger(const std::string& filename, options_t op)
{
    _init(filename.c_str(), op);
}

logger::~logger() { }

/**
 * Initialize the logger. Opens the file in append mode.
 *
 * @param filename file to open/create
 * @param options logging options
 */
void logger::_init(const std::string &filename, options_t op)
{
    assert(filename.c_str() != NULL);

    atexit(&_clean); // register for clean up

    _options = op;
    _path = filename;
    _errorsCount = 0;
    _warningCount = 0;

    _sep = _separator();

    // init writer
    _writer.open(_path.c_str(), std::ios_base::out | std::ios_base::app/*ALWAYS append*/);
}

/**
 * Verify if a bit flag is set
 *
 * @param value flags value
 * @param flag flag
 * @return @c True if bit is set, @c False otherwise
 */
bool logger::_isFlagSet(unsigned value, unsigned flag)
{
    return(value & flag) != 0;
    //    return (value & (1 << index)) != 0;
}

void logger::_clean()
{
    delete _instance;
    _instance = NULL;
}

/**
 * Verify that record has the correct level to be logged
 *
 * @param lvl current level
 * @return @c True if can be logged, @c False otherwise
 */
bool logger::_canLog(recordLevel::level lvl)
{
    bool doLogging = false;

    if(_isFlagSet(_options, 0)) // none
        doLogging = false;
    else
    {
        if(lvl == recordLevel::ERROR && (_isFlagSet(_options, loggingOptions::ALL) || _isFlagSet(_options, loggingOptions::ERRORS)))
            doLogging = true;

        else if(lvl == recordLevel::WARNING && (_isFlagSet(_options, loggingOptions::ALL) || _isFlagSet(_options, loggingOptions::WARNINGS)))
            doLogging = true;

        else if(lvl == recordLevel::INFORMATION && (_isFlagSet(_options, loggingOptions::ALL) || _isFlagSet(_options, loggingOptions::INFORMATIONS)))
            doLogging = true;
    }

    return doLogging;
}

/**
 * Writes the record to the underlayer stream.
 * @param rec record to be written
 * @return @c True when log is successful, @c False otherwise
 */
bool logger::_log(const record &rec)
{
    // TODO : make logger thread-safe

    // check if current can be logged
    if(_canLog(rec.level()))
    {
        // set good bit
        _writer.clear(std::ios_base::goodbit);

        // count logged errors
        if(rec.level() == recordLevel::ERROR)
            _errorsCount++;

        if(rec.level() == recordLevel::WARNING)
            _warningCount++;

        _writer << rec.content() << std::endl;
        _writer.flush();
    }

    // checks if io state is still good...
    return _writer.good();
}

std::string logger::_separator()
{
    return std::string(80, '-');
}

// =============================================================================
//                              PUBLIC
// =============================================================================

/**
 * Gets the unique instance
 *
 * @return reference to the unique instance
 */
logger &logger::instance()
{
    return *_instance;
}

/**
 * Creates an instance of the logger with default filename and options
 *
 * @return a reference to the logger instance
 */
logger &logger::create()
{
    if(_instance == NULL)
        _instance = new logger(); // creates the new instance

    return *_instance; // return the instance
}

/**
 * Creates an instance of the logger
 *
 * @param filename log filename
 * @param op log options
 *
 * @return a reference to the logger instance
 */
logger &logger::create(const char *filename, options_t op)
{
    if(_instance == NULL)
        _instance = new logger(filename, op); // creates the new instance

    return *_instance; // return the instance
}

/**
 * Creates an instance of the logger
 *
 * @param filename log filename
 * @param op log options
 *
 * @return a reference to the logger instance
 */
logger &logger::create(const std::string &filename, options_t op)
{
    if(_instance == NULL)
        _instance = new logger(filename, op); // creates the new instance

    return *_instance; // return the instance
}

/**
 * Gets the current path for this logger
 *
 * @return current logging file path
 */
const std::string &logger::path() const
{
    return _path;
}

/**
 * Gets the number of errors logged
 *
 * @return number of logged errors
 */
uint_t logger::errors() const
{
    return _errorsCount;
}

/**
 * Gets the logging options
 *
 * @return options
 */
options_t logger::loggingOptions() const
{
    return _options;
}

/**
 * Sets the current options
 * Ex : Information | Errors
 *
 * @param newOptions new logging options
 */
void logger::setLoggingOptions(options_t newOptions)
{
    _options = newOptions;
}

/**
 * Logs a record, data is flushed afterwards
 * @param rec record to log
 * @return @c True on success, @c False otherwise
 */
bool logger::log(const record &rec)
{
    return _log(rec);
}

/**
 * Logs a record, data is flushed afterwards
 * @param lvl logging level
 * @param text text to log
 * @return @c True on success, @c False otherwise
 */
bool logger::log(recordLevel::level lvl, const std::string &text)
{
    return _log(record(text, lvl));
}

/**
 * Logs a record, data is flushed afterwards
 *
 * @param lvl record level
 * @param text text to log
 * @return @c True on success, @c False otherwise
 */
bool logger::log(recordLevel::level lvl, const char *text)
{
    return _log(record(text, lvl));
}

/**
 * Logs a record, data is flushed afterwards.
 *
 * @param rec record to log
 * @param line line number
 * @param file filename
 * @return @c True on success, @c False otherwise
 */
bool logger::log(const record &rec, unsigned int line, const std::string &file)
{
    record r(rec);
    r.appendContent(LOGGER_STACKTRACE(line, file));
    return _log(r);
}

/**
 * Logs a record, data is flushed afterwards
 * @param lvl logging level
 * @param text text to log
 * @param line line number
 * @param file filename
 * @return @c True on success, @c False otherwise
 */
bool logger::log(recordLevel::level lvl, const std::string &text, unsigned int line, const std::string &file)
{
    return log(record(text, lvl), line, file);
}

/**
 * Logs a record, data is flushed afterwards
 *
 * @param lvl logging level
 * @param text text to log
 * @param line line number
 * @param file filename
 * @return @c True on success, @c False otherwise
 */
bool logger::log(recordLevel::level lvl, const char *text, unsigned int line, const std::string &file)
{
    return log(lvl, std::string(text), line, file);
}

/**
 * Writes head of log file
 *
 * @param h head content
 */
void logger::head(const std::string &h)
{
    head(h.c_str());
}

/**
 * Writes head of log file
 *
 * @param h head content
 */
void logger::head(const char *h)
{
    record r;
    std::string str;

    if(h == NULL)
        str = "\n" + _sep + "\nApplication started at " + r.getTimeStamp();
    else
        str = h;

    _header = record(str);
    _log(_header);
}

/**
 * Writes tail of log file
 *
 * @param t tail content
 */
void logger::tail(const std::string& t)
{
    tail(t.c_str());
}

/**
 * Writes tail of log file
 *
 * @param t tail content
 */
void logger::tail(const char* t)
{
    record r;
    std::stringstream str;
    str << (t == NULL ? "Application finished at " + r.getTimeStamp() : t);

    if(_warningCount != 0 && _errorsCount != 0)
    {
        str << "\nWarning";
        str << (_warningCount > 1 ? "s" : "");
        str << " : " << _warningCount;

        str << "\nError";
        str << (_errorsCount > 1 ? "s" : "");
        str << " : " << _errorsCount;
    }
        // if any warning occured
    else if(_warningCount != 0)
    {
        str << "\nWarning";
        str << (_warningCount > 1 ? "s" : "");
        str << " : " << _warningCount;
    }
        // if any errors occured
    else if(_errorsCount != 0)
    {
        str << "\nError";
        str << (_errorsCount > 1 ? "s" : "");
        str << " : " << _errorsCount;
    }

    // init the log file header
    _tail = record(str.str());
    _log(_tail);
}

/**
 * Closes the logger and the underlying stream
 */
void logger::close()
{
    _writer.close();
}
} /* vap namespace end */