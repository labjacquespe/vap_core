/**
 * @file   record.cpp
 * @author Charles Coulombe
 *
 * @date 6 September 2012, 21:49
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./record.h"
#include "./logger.h"

namespace vap
{
/**
 * Construct an empty record
 * @note  By default, the level information is @c INFORMATION
 */
record::record()
{
    _init("", recordLevel::INFORMATION);
}

/**
 * Construct a record from a std::string.
 * @note  By default, the level information is @c INFORMATION
 * @param s record content
 */
record::record(const std::string &s)
{
    _init(s.c_str(), recordLevel::INFORMATION);
}

/**
 * Construct a record from a std::string.
 * @note  By default, the level information is @c INFORMATION
 *
 * @param s record content
 */
record::record(const char* s)
{
    _init(s, recordLevel::INFORMATION);
}

/**
 * Construct a record from a std::string and a level
 *
 * @param s record content
 * @param lvl record level
 */
record::record(const std::string& s, recordLevel::level lvl)
{
    _init(s.c_str(), lvl);
}

/**
 * Construct a record from a std::string and a level
 *
 * @param s record content
 * @param lvl record level
 */
record::record(const char * s, recordLevel::level lvl)
{
    _init(s, lvl);
}

/**
 * Construct a copy from a record
 *
 * @param r record to build from
 */
record::record(const record& r)
{
    _text = r._text;
    _timestamp = r._timestamp;
    _level = r._level;
}

record::~record()
{
}

/**
 * Init this record
 *
 * @param s std::string content
 * @param lvl record level
 */
void record::_init(const char *s, recordLevel::level lvl)
{
    char buffer[80];

    // init text
    _text = s;
    _level = lvl;

    // compute time stamp
    time(&_timeStart);
    _timeInfo = localtime(&_timeStart);

    // format date to YYYY-mm-DD HH:MM:ss
    strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", _timeInfo);
    _timestamp = buffer;

    // format record text
    _format();
}

/**
 * Format the current record to :
 * [Timestamp    State : Text ]
 * Note : If the record level is different then Error or Warning,
 *        this will do nothing.
 */
void record::_format()
{
    if(_level == recordLevel::ERROR || _level == recordLevel::WARNING)
    {
        std::string str = ""; // tmp std::string

        str = "[" + _timestamp + "] ";
        str += recordLevel::toString(_level);// ? "Error" : "Warning";
        str += ": " + _text;

        // set the record text to newly formatted text
        _text = str;
    }
}

/**
 * Gets the current level of this record
 * @return record's level
 */
recordLevel::level record::level() const
{
    return _level;
}

/**
 * Gets the content of record
 *
 * @return content
 */
const std::string &record::content() const
{
    return _text;
}

/**
 * Gets the current time stamp of this record
 *
 * @return timestamp
 */
const std::string &record::getTimeStamp() const
{
    return _timestamp;
}

/**
 * Sets the current level of this record
 *
 * @param lvl new level
 */
void record::setLevel(recordLevel::level lvl)
{
    _level = lvl;
}

/**
 * Sets the content of the record
 *
 * @param s new content
 */
void record::setContent(const std::string& s)
{
    _text = s;
}

/**
 * Sets the content of the record
 *
 * @param s new content
 */
void record::setContent(const char* s)
{
    _text = s;
}

/**
 * Appends the content to the record
 *
 * @param s new content
 */
void record::appendContent(const std::string& s)
{
    _text += s;
}

/**
 * Appends the content to the record
 *
 * @param s new content
 */

void record::appendContent(const char* s)
{
    _text += s;
}
record &record::operator=(const record &r)
{
    if(this != &r)
    {
        _text = r._text;
        _timestamp = r._timestamp;
        _level = r._level;
        _timeStart = r._timeStart;
        _timeInfo = r._timeInfo;
    }
    return *this;
}
} /* vap namespace end */
