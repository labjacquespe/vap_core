/**
 * @file   gtf_utils.h
 * @author Charles Coulombe
 *
 * @date July 09 2015 09:45
 * @version 1.1.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @ingroup utils
 */

#ifndef GTF_UTILS_H
#define GTF_UTILS_H

#include <deque>
#include <map>
#include "../dataset/gtf_entry.h"
#include "../dataset/genepred_entry.h"
#include "text/string_utils.h"

namespace vap
{
namespace util
{
namespace internal
{
inline bool gtf_gene_id_entry_lesser_compare(const gtf_entry *l, const gtf_entry *r)
{
    if(l->feature < r->feature)
        return true;

    if(l->feature == r->feature)
    {
        if(l->start < r->start)
            return true;

        else if(l->start == r->start && l->end > r->end)
            return true;
    }

    return false;
}
}

/**
 * Builds a Genepred genome from a GTF genome format.
 *
 * @param gtf Input GTF genome.
 * @param genepred Output Genepred genome.
 */
inline void convertGtfToGenepred(const std::deque<gtf_entry> &gtf, std::deque<genepred_entry> &genepred)
{
    std::map< std::string, std::map< std::string, std::deque< const gtf_entry * > > > tranform;

    std::string name = "";
    std::string chromosome = "";
    char strand = '.';
    int txStart = -1;
    int txEnd = -1;
    int cdsStart = -1;
    int cdsEnd = -1;
    int exonsCount = 0;
    std::vector<int> exonStarts;
    std::vector<int> exonEnds;
    int score = 0;
    std::string alias = "";
    std::string extra = "";
    bool haveCDS = false;
    bool haveExon = false;

    // Constructs transition structure. Map feature to their transcript_id and insert pointer to the GTF line.
    // The transcript_id and feature will be sorted alpha.
    for(std::deque<gtf_entry>::const_iterator i = gtf.begin(); i != gtf.end(); ++i)
        tranform[i->attributes.at("transcript_id")][util::tolower(i->feature)].push_back(&(*i));

    // Loop for all transcript_id
    for(std::map< std::string, std::map< std::string, std::deque< const gtf_entry * > > >::iterator i = tranform.begin(); i != tranform.end(); ++i)
    {
        name = "";
        chromosome = "";
        strand = '.';
        txStart = -1;
        txEnd = -1;
        cdsStart = -1;
        cdsEnd = -1;
        exonsCount = 0;
        exonStarts.clear();
        exonEnds.clear();
        score = 0;
        alias = "";
        extra = "";
        haveCDS = false;
        haveExon = false;

        // Loop for all feature of transcript_id, such as cds, exon, stop_codon and ignore start_codon or other feature values
        // First is the transcript_id
        // Second is the features mapped to the transcript_id
        for(std::map< std::string, std::deque< const gtf_entry * > >::iterator j = i->second.begin(); j != i->second.end(); ++j)
        {
            // sort pointers of feature
            std::sort(j->second.begin(), j->second.end(), internal::gtf_gene_id_entry_lesser_compare);

            // IMPORTANT : General case should not put here as it might segfault.
            // There's no way we can be sure there will be information, unless
            // we have a CDS or EXON feature.

            // If CDS feature is present use the first and the last element
            // in order to set the cdsStart and cdsEnd.
            if(j->first == "cds") // CDS
            {
                name = i->first;
                chromosome = j->second.front()->chromosome;
                score = j->second.front()->score;
                strand = j->second.front()->strand;
                alias = j->second.front()->attributes.at("gene_id");
                cdsStart = j->second.front()->start - 1; // Transform to genepred which is zero-based.
                cdsEnd = j->second.back()->end; // No need to transform in half-open because of zero-based prior transformation (end + 1 - 1).

                haveCDS = true;
            }

            // If EXON feature is present use the first and last element
            // in order to set the txStart and txEnd.
            else if(j->first == "exon") // Exon
            {
                // Set the general available information.
                name = i->first;
                chromosome = j->second.front()->chromosome;
                score = j->second.front()->score;
                strand = j->second.front()->strand;
                alias = j->second.front()->attributes.at("gene_id");
                txStart = j->second.front()->start - 1; // Transform to genepred which is zero-based.
                txEnd = j->second.back()->end; // No need to transform in half-open because of zero-based prior transformation (end + 1 - 1).
                exonsCount = j->second.size();

                // set all exons ranges
                for(std::deque<const gtf_entry *>::const_iterator k = j->second.begin(); k != j->second.end(); ++k)
                {
                    exonStarts.push_back((*k)->start - 1); // Transform to genepred which is zero-based.
                    exonEnds.push_back((*k)->end); // No need to transform in half-open because of zero-based prior transformation (end + 1 - 1).
                }

                haveExon = true;
            }

            // Modify cdsStart or cdsEnd in function of the strand.
            else if(j->first == "stop_codon")
            {
                if(strand == '-')
                {
                    // Transform to genepred which is zero-based.
                    cdsStart = j->second.front()->start - 1;
                }
                else
                    cdsEnd = j->second.back()->end; // No need to transform in half-open because of zero-based prior transformation (end + 1 - 1).
            }
        }

        // When there's no CDS feature, the default is the transcription end position.
        if(!haveCDS)
        {
            cdsStart = txEnd;
            cdsEnd = txEnd;
        }

        // When there's CDS but no EXON, the exons become the CDS positions.
        else if(haveCDS && !haveExon)
        {
            const std::deque<const gtf_entry *> *j = &i->second.at("cds");

            txStart = cdsStart;
            txEnd = cdsEnd;
            exonsCount = j->size();

            for(std::deque<const gtf_entry *>::const_iterator k = j->begin(); k != j->end(); ++k)
            {
                exonStarts.push_back((*k)->start - 1); // Transform to genepred which is zero-based.
                exonEnds.push_back((*k)->end); // No need to transform in half-open because of zero-based prior transformation (end + 1 - 1).
            }

            exonStarts.front() = txStart;
            exonEnds.back() = txEnd; // Add 3 bp to include the stop codon in the cds range to the last exon.
        }

        genepred.push_back(genepred_entry(name, chromosome, strand, txStart, txEnd, cdsStart, cdsEnd, exonsCount, exonStarts, exonEnds, score, alias, extra));
    }
}
} /* util end namespace */
} /* vap end namespace */
#endif
