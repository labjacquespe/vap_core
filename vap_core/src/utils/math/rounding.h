/**
 * @file   rounding.h
 * @author Charles Coulombe
 *
 * @date 29 November 2012, 14:53
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @ingroup utils
 */

#ifndef ROUNDING_H
#define	ROUNDING_H

#include <cmath>

namespace vap
{
namespace util
{

namespace math
{

/**
 * Common rounding : rounds number up
 * @note rounds towards +Infinity
 *
 * @param value value to be rounded
 * @return round value
 */
inline float roundhalfup(float value)
{
    return std::floor(value + 0.5);
}

/**
 * Common rounding : rounds number up
 * @note rounds towards +Infinity
 *
 * @param value value to be rounded
 * @return round value
 */
inline double roundhalfup(double value)
{
    return std::floor(value + 0.5);
}

/**
 * Common rounding : rounds number down
 * @note rounds towards -Infinity
 *
 * @param value value to be rounded
 * @return round value
 */
inline float roundhalfdown(float value)
{
    return std::ceil(value - 0.5);
}

/**
 * Common rounding : rounds number down
 * @note rounds towards -Infinity
 *
 * @param value value to be rounded
 * @return round value
 */
inline double roundhalfdown(double value)
{
    return std::ceil(value - 0.5);
}

/**
 * Rounds a number base on half digit
 *
 * @param value value to round
 * @return round value
 */
inline float cint(float value)
{
    float intPart = 0.0f;
    if(std::modf(value, &intPart) >= .5)
        return value >= 0 ? ceil(value) : floor(value);
    else
        return value < 0 ? ceil(value) : floor(value);
}

/**
 * Rounds a number base on half digit
 *
 * @param value value to round
 * @return round value
 */
inline double cint(double value)
{
    float intPart = 0.0f;
    if(std::modf(value, &intPart) >= .5)
        return value >= 0 ? ceil(value) : floor(value);
    else
        return value < 0 ? ceil(value) : floor(value);
}

/**
 * Rounds a number base on number of places
 *
 * @param value value to round
 * @param places digits to consider
 * @return round value
 */
inline float round(float value, int places)
{
    float off = pow(10.0f, places);
    return cint(value * off) / off;
}

/**
 * Rounds a number base on number of places
 *
 * @param value value to round
 * @param places digits to consider
 * @return round value
 */
inline double round(double value, int places)
{
    double off = pow(10.0f, places);
    return cint(value * off) / off;
}
} /* math */
} /* util namespace end */
} /* vap namespace end */
#endif	/* ROUNDING_H */

