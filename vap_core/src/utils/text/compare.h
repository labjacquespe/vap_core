/**
 * @file   compare.h
 * @author Charles Coulombe
 *
 * @date 30 October 2012, 12:53
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @ingroup utils
 */

#ifndef COMPARE_H
#define	COMPARE_H

#include <string>

#include "./string_utils.h"

namespace vap
{
namespace util
{

/**
 * Case insensitive @c std::string comparison.
 * Compare every character in their upper version.
 *
 * @warning This function is @b not like @c strcmp() or @c std::string.compare()
 * which are case sensitive comparison.
 *
 * @param left left string argument
 * @param right right string argument
 * @return @c True when @a left equals @a right, @c False otherwise
 */
inline bool str_insensitive_equal(const std::string &left, const std::string &right)
{
    // make non const copy
    std::string l = left;
    std::string r = right;

    return util::toupper(l) == util::toupper(r);
}

/**
 * Case insensitive string comparison.
 * Compare every character in their upper version.
 *
 * @warning This function is @b not like @c strcmp() or @c string.compare()
 * which are case sensitive comparison.
 *
 * @param left left string argument
 * @param right right string argument
 * @return @c True when left is smaller then right, @c False otherwise
 */
inline bool str_insensitive_less_compare(const std::string &left, const std::string &right)
{
    // make non const copy
    std::string l = left;
    std::string r = right;

    return util::toupper(l) < util::toupper(r);
}

/**
 * Checks @a str begins with particular @a prefix.
 *
 * @param str base string
 * @param prefix string prefix
 * @return @c True when @a prefix was found at begining, @c False otherwise
 */
inline bool starts_with(const std::string &str, const std::string &prefix)
{
    // compare size and memory block content
    return(str.size() >= prefix.size() && memcmp(str.c_str(), prefix.c_str(), prefix.size()) == 0);
}

/**
 * Checks @a str begins with particular @a prefix.
 *
 * @param str base string
 * @param prefix character prefix
 * @return @c True when @a prefix was found at begining, @c False otherwise
 */
inline bool starts_with(const std::string &str, const char prefix)
{
    // compare size and memory block content
    return(str.size() >= 1 && memcmp(str.c_str(), &prefix, 1) == 0);
}

/**
 * Checks @a str ends with particular @a suffix.
 *
 * @param str base string
 * @param suffix string suffix
 * @return @c True when @a suffix was found at the end, @c False otherwise
 */
inline bool ends_with(const std::string &str, const std::string &suffix)
{
    // compare size and memory block content
    return(str.size() >= suffix.size() && memcmp(str.c_str() + (str.size() - suffix.size()), suffix.c_str(), suffix.size()) == 0);
}

/**
 * Checks @a str ends with particular @a suffix.
 *
 * @param str base string
 * @param suffix character suffix
 * @return @c True when @a suffix was found at the end, @c False otherwise
 */
inline bool ends_with(const std::string &str, const char suffix)
{
    // compare size and memory block content
    return(str.size() >= 1 && memcmp(str.c_str() + (str.size() - 1), &suffix, 1) == 0);
}
} /* util namespace end */
} /* vap namespace end */
#endif	/* INVARIANT_CMP_H */
