/**
 * @file   string_utils.h
 * @author Charles Coulombe
 *
 * @date 30 October 2012, 12:58
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @ingroup utils
 */

#ifndef STRING_UTILS_H
#define	STRING_UTILS_H

#include <string>
#include <locale>
#include <sstream>
#include <cstdarg>
#include <cstring>

namespace vap
{
namespace util
{

/**
 * Trims leading characters.
 *
 * @param str string to be trimmed
 * @param characters characters to trim. By default, the character is a whitespace.
 */
inline std::string &ltrim(std::string &str, const std::string characters = " ")
{
    return str.erase(0, str.find_first_not_of(characters)); //prefixing spaces
}

/**
 * Trims trailing characters.
 *
 * @param str string to be trimmed
 * @param characters characters to trim. By default, the character is a whitespace.
 */
inline std::string &rtrim(std::string &str, const std::string characters = " ")
{
    return str.erase(str.find_last_not_of(characters) + 1); //sufixing spaces
}

/**
 * Trims both trailing and leading characters.
 *
 * @param str string to be trimmed
 * @param characters characters to trim. By default, the character is a whitespace.
 */
inline std::string &trim(std::string &str, const std::string characters = " ")
{
    return ltrim(rtrim(str, characters));
}

/**
 * Trims both trailing and leading characters.
 *
 * @param str string to be trimmed
 * @param character character to trim. By default, the character is a whitespace.
 */
inline std::string &trim(std::string &str, const char character)
{
    return trim(str, std::string(1, character));
}

/**
 * Trims leading characters.
 *
 * @param str string to be trimmed
 * @param character character to trim. By default, the character is a whitespace.
 */
inline std::string &ltrim(std::string &str, const char character)
{
    return ltrim(str, std::string(1, character));
}

/**
 * Trims trailing characters.
 *
 * @param str string to be trimmed
 * @param character character to trim. By default, the character is a whitespace.
 */
inline std::string &rtrim(std::string &str, const char character)
{
    return rtrim(str, std::string(1, character));
}

/**
 * Convert all characters within @c std::string to upper case.
 *
 * @param s string to be converted
 * @param loc locale, by default the current system locale is used.
 *
 * @return upper case @c std::string
 */
inline std::string &toupper(std::string& s, const std::locale loc = std::locale())
{
    for(unsigned int i = 0; i < s.length(); ++i)
        s[i] = toupper(s[i], loc);

    return s;
}

/**
 * Convert all characters within @c std::string to upper case.
 *
 * @param s string to be converted
 * @param loc locale, by default the current system locale is used.
 *
 * @return upper case @c std::string
 */
inline std::string toupper(const std::string& s, const std::locale loc = std::locale())
{
    std::string cp = s;
    for(unsigned int i = 0; i < s.length(); ++i)
        cp[i] = toupper(cp[i], loc);

    return cp;
}

/**
 * Convert all characters within @c std::string to lower case.
 *
 * @param s string to be converted
 * @param loc locale, by default the current system locale is used.
 *
 * @return lower case @c std::string
 */
inline std::string &tolower(std::string& s, const std::locale loc = std::locale())
{
    for(unsigned int i = 0; i < s.length(); ++i)
        s[i] = tolower(s[i], loc);

    return s;
}

/**
 * Convert all characters within @c std::string to lower case.
 *
 * @param s string to be converted
 * @param loc locale, by default the current system locale is used.
 *
 * @return lower case @c std::string
 */
inline std::string tolower(const std::string& s, const std::locale loc = std::locale())
{
    std::string cp = s;
    for(unsigned int i = 0; i < s.length(); ++i)
        cp[i] = tolower(cp[i], loc);

    return cp;
}

/**
 * Removes @c EOL characters.
 * @note This method is compliant with Windows, Unix, Mac systems.
 *
 * @param str @c std::string to be trimmed
 */
inline std::string &cut_eol(std::string &str)
{
    return str.erase(str.find_last_not_of("\n\r") + 1); // removes sufixing EOL characters
}

/**
 * Checks if @c std::string is a number.
 * @warning Every character must be a digit. The only exception is '-'
 * that represent a negative number, in the latter case, it must come first in the string.
 *
 * @param str numerical string
 * @return @c True when all characters have been successfully checked, @c False otherwise.
 */
inline bool isNumber(const std::string &str)
{
    std::string::const_iterator it = str.begin();

    if(*it == '-')
        ++it;

    // tests all characters
    while(it != str.end() && isdigit(*it))
        ++it;

    // if string was empty or we did not reached the end, hence it is not a number
    return !str.empty() && it == str.end();
}

/**
 *
 * Checks if @c std::string begin with a number.
 * Only the first two characters are checked. If the first character is a digit,
 * or if the two first characters are a minus and a digit.
 *
 * @param str string to be tested
 * @return @c True when string begin with a number, @c False otherwise.
 */
inline bool starts_with_digit(const std::string &str)
{
    // if we have more than a '-' character.
    // we need at least 1 digits or a minus and a digit

    return(str.size() >= 1 && isdigit(str[0])) || (str.size() >= 2 && (str[0] == '-' && isdigit(str[1])));
}
} /* util namespace end */
} /* vap namespace end */
#endif	/* STRING_UTILS_H */
