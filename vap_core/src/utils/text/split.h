/**
 * @file   split.h
 * @author Charles Coulombe
 *
 * @date 3 December 2012, 22:33
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @ingroup utils
 */

#ifndef SPLIT_H
#define	SPLIT_H

#include <string>
#include <vector>

namespace vap
{
namespace util
{
typedef std::vector<std::string> tokens_t;

/**
 * Tokenizes a @c std::string against delimiters.
 * @note By default, empty tokens are discarded.
 *
 * @param tokens tokenized string
 * @param str input string
 * @param delimiters delimiters of tokens
 * @param emptyTokens @c True to keep empty tokens, @c False otherwise.
 */
inline void split(tokens_t &tokens, const std::string &str, const std::string &delimiters, bool emptyTokens = false)
{
    size_t start = 0,
            end = 0;

    std::string token;
    token.reserve(1024); // reserve enough memory

    // parse all the string
    while(end != std::string::npos/*not found*/)
    {
        // find the end of a token
        end = str.find_first_of(delimiters, start);

        // if at end, use length = maxLength, otherwise use length = end - start
        token = str.substr(start, (end == std::string::npos) ? std::string::npos : end - start);

        // if we need to discard an empty token
        if(!(token.size() == 0 && !emptyTokens))
            tokens.push_back(token);

        // if at end, use start = maxSize, otherwise use start = end + delimiter
        start = ((end > (std::string::npos - 1)) ? std::string::npos : end + 1);
    }
}
} /* util namespace end */
} /* vap namespace end */
#endif	/* SPLIT_H */

