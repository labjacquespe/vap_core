/**
 * @file    vap_core.cpp
 * @author  Charles Coulombe
 * @date    12 November 2012, 09:21
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if __cplusplus < 201103L
#error A C++11 compliant compiler is required.
#endif

// Pre-processor directives
#include <cstdlib>
#include <vector>
#include <deque>
#include <string>
#include <iostream>
#include <bitset>
#include <algorithm>
#include <fstream>

#include "./defs.h"

#include "./dataset/reference_coordinates.h"
#include "./dataset/reference_annotation.h"
#include "./dataset/wig_dataset.h"
#include "./dataset/genome_dataset.h"
#include "./dataset/bedgraph_dataset.h"
#include "./dataset/bigwig_dataset.h"
#include "./utils/file/format_guess.h"

#include "./feature/reference_feature.h"
#include "./feature/group_feature.h"
#include "./feature/aggregate_reference_feature.h"

#include "./utils/memory/smart_pointer.h"
#include "./utils/timer/timer.h"
#include "./utils/iterator_utils.h"
#include "./utils/debug/assert.h"
#include "./utils/type/convert.h"
#include "./utils/file/file_reader.h"
#include "./utils/coordinates/coordinates_utils.h"
#include "./utils/text/split.h"
#include "./utils/math/rounding.h"

#include "./output/output_builder.h"

#include "./parameters/parameters.h"
#include "parameters/parser_parameters.h"

#include "./exception/error.h"

#include "./filter/reference_filter.h"

////////////////////////////////////////////////////////////////////////////////
// Functions prototypes
////////////////////////////////////////////////////////////////////////////////

using namespace vap;

#ifdef __DEBUG__
void printFeatures(const group_feature<reference_feature> &referenceFeatures, const parameters &param, const std::string &datasetName);
void printGenes(const group_feature<reference_feature> &referenceFeatures, const parameters &param, const std::string &datasetName);
#endif

bool validateTrackType(const std::string &path, const io::file_format format);
template <class ENTRY_TYPE> void updateRange(const abstract_dataset<ENTRY_TYPE> *dataset, group_feature<reference_feature> &referenceFeatures, bool includeZeros);
bool validateLineFormat(const std::string &path, const io::file_format fileFormat, const uint_t referencePoints, const std::string &coordType = "");
void parseFileHeader(std::map< std::string, std::string > &properties, const std::string &path);
std::string loadGroupOfFeatures(std::pair< std::string, std::deque<reference_annotation> > &groupAnnotations, const std::string &path);
io::file_format loadGroupOfCoordinates(std::pair< std::string, std::deque<reference_coordinates> > &groupCoordinates, const std::string &path, uint_t referencePoints);
void loadGroupOfCoordinatesTypeBed(std::pair< std::string, std::deque<reference_coordinates> > &groupCoordinates, const std::unique_ptr<io::file_reader> &reader);
void loadGroupOfCoordinatesTypeCoord(std::pair< std::string, std::deque<reference_coordinates> > &groupCoordinates, const std::unique_ptr<io::file_reader> &reader, uint_t referencePoints, const std::string &type);
void loadAnnotationsFilter(std::deque<reference_annotation> &sequence, const std::string &path);
bool loadAndProcessDataset(const std::string &datasetPath, group_feature<reference_feature> &referenceFeatures, const unsigned long long datasetChunkSize, std::string &datasetName, bool includeZeros);
bool loadGenome(util::scalar_smart_pointer<genome>::type &genomeData, const std::string &annotationsPath, const analysis_mode::type &analysisMode, const annotation_coordinates_type::type &annotationCoordinatesType);
uint_t readGroupsList(std::map< uint_t, std::deque<std::string> > &refGroupPaths, std::map< uint_t, std::deque<std::string> > &datasetPaths, const std::string &path);
void linkAnnotationsOrExons(const analysis_mode::type type, std::deque<std::string> &refGroupPaths, group_feature<reference_feature> &referenceFeatures, group_feature<aggregate_reference_feature> &aggregateFeatures, const std::vector<genome_entry_pointer> &sortedGenome, const positive_filter &selected, const negative_filter &excluded, const parameters &param, const io::file_format &genomeFmt);
void linkCoordinates(std::deque<std::string> &refGroupPaths, group_feature<reference_feature> &referenceFeatures, group_feature<aggregate_reference_feature> &aggregateFeatures, const positive_filter &selected, const negative_filter &excluded, const parameters &param);
std::string ensureNameUniqueness(std::string &value, std::map< std::string, uint_t > &table);
void addDummiesEntry(genome *dataset);
void removeUnlinkedFeatures(group_feature<reference_feature> &referenceFeatures);
void buildSortedGenome(std::vector<genome_entry_pointer> &sortedGenome, genome *dataset);
void updateAggregate(group_feature<reference_feature> &referenceFeatures, group_feature<aggregate_reference_feature> &aggregateFeatures, const std::bitset < N_ORI_SUBGROUPS > &orientationSubgroups, const aggregate_data_type::type aggregateDataType, const mean_dispersion_value::type meanDispersionValue, const bool includeZeros);
void updateWeightedMean(group_feature<reference_feature> &referenceFeatures, bool includeZeros);
void prefillUpdateTable(group_feature<reference_feature> &referenceFeatures);
void determineFeatureOrientationType(const annotation_strand::strand &priorStrand, reference_feature &feature, const annotation_strand::strand &nextStrand);
template <class ENTRY_TYPE> void clean(abstract_dataset<ENTRY_TYPE> *dataset, const group_feature<reference_feature> &referenceFeatures = group_feature<reference_feature>(), bool filter = false);
void setExonsFeatures(group_feature<reference_feature> &referenceFeatures, std::vector<genome_entry_pointer> sortedGenome, const parameters &param);
void setAnnotationsFeatures(group_feature<reference_feature> &referenceFeatures, std::vector<genome_entry_pointer> sortedGenome, const parameters &param);
void setCoordinatesFeatures(group_feature<reference_feature> &referenceFeatures, std::deque< std::pair<std::string, std::deque<reference_coordinates> > > &groupCoordinates, const parameters &param);
template <class ENTRY_TYPE> void createReferenceFeatures(const std::deque< std::pair< std::string, std::deque<ENTRY_TYPE> > > &groupFeatures, group_feature<reference_feature> &referenceFeatures, const parameters &param, const std::vector<bool> &convertOneBased, const std::vector<bool> &convert);
template <class ENTRY_TYPE> void createAggregateFeatures(const std::deque< std::pair< std::string, std::deque<ENTRY_TYPE> > > &groupFeatures, group_feature<aggregate_reference_feature> &aggregateFeatures, const parameters &param);
template <class ENTRY_TYPE> void processDataset(abstract_dataset<ENTRY_TYPE> *dataset, group_feature<reference_feature> &referenceFeatures, bool includeZeros);
void copyToOutputDirectory(const parameters &param, const boost::program_options::variables_map &vm);
void alterNonSenseParameters(parameters &param);
void resetFeaturesData(group_feature<reference_feature> &referenceFeatures, group_feature<aggregate_reference_feature> &aggregateFeatures);
void clearFeaturesData(group_feature<reference_feature> &referenceFeatures, group_feature<aggregate_reference_feature> &aggregateFeatures);

/**
 * VAP main entry point.
 *
 * This file contains all definitions and algorithms that make VAP an efficient
 * tool for genome-wide data representation and discovery.
 *
 * The following pseudo-code is self-explanatory :
 * -#   READ parameters
 * -#   IF type of reference is A or E
 * -#       READ genome
 * -#       READ filters
 * -#   READ list of groups and stores dataset names
 * -#   IF type of reference is A or E
 * -#      APPLY filters
 * -#      ADD dummies annotation
 * -#   ELSE IF type of reference is C
 * -#      READ and LOAD coordinates
 * -#  FILL object cross linking genome and feature
 * -#  READ one stored dataset type accordingly
 * -#  FILL object cross linking dataset and feature
 * -#  FILL aggregate
 * -#  OUTPUT data
 */
int main(int argc, char **argv)
{
    // Do not use smart pointer here as pointer is a local variable and no "new"
    // is performed, variable will be released on exit.
    logger *logger = &logger::create(); // use default filename and log all
    logger->head(); // log header as application started

    parser_parameters cmd;
    group_feature< reference_feature > referenceFeatures;
    group_feature< aggregate_reference_feature > aggregateFeatures;
    positive_filter selected;
    negative_filter excluded;
    output_builder outBuilder;
    std::string datasetName;
    std::map< std::string, std::deque< std::string > > graphMap;
    util::timer runTmr, roundTmr;
    std::string round = "";
    std::map< std::string, std::deque<std::string> > refGroupsPaths;
    uint_t nProcessedDatasets;

    runTmr.start(); // start process timer

    // Read and parse parameters file then alter parameter that do not make
    // any sense e.g. : upstream region split 25%.
    parameters param = cmd.parse(argc, argv);

    LOG(recordLevel::INFORMATION, MSG_LOG_HEADER(cmd.command(argc, argv), param.outputDirectory));
    PRINT_AND_LOG(MSG_APP_RUNNING(param.parametersPath));

    alterNonSenseParameters(param); // TODO : Remove this as it is non intuitive!!
    refGroupsPaths = param.refGroupsPaths; // clone the reference groups paths to update the value with the alias if present

    // Copy parameter file and reference groups and datasets file to the output directory if we do not read from it already
    // but also tests that we can read/write in the output directory
    copyToOutputDirectory(param, cmd.variablesMap());

    // read list of annotations/coordinates names or ranges to be used as filters
    PRINT_AND_LOG(MSG_READING_FILTERS);

    selected.load(param.selectionPath); // entries are automatically sorted
    excluded.load(param.exclusionPath); // entries are automatically sorted

    // std::vector to keep all genome entries sorted alphabetically.
    // This will be useful to iterate over each entry and link each feature
    // along the way, this is SAFE to keep genome_entry addresses as they
    // will not change once loaded. This is actually only filled in annotations or exon mode.
    // Hence, this it is empty for coordinates mode.
    std::vector<genome_entry_pointer> sortedGenome; // just store addresses, no new memory allocated
    util::scalar_smart_pointer<genome>::type genomeData; // swap container to preserve genome entries through the different rounds since sortedGenome points to this data

    if(param.analysisMode == analysis_mode::type::ANNOTATION || param.analysisMode == analysis_mode::type::EXON)
    {
        // read genome and parse it
        PRINT_AND_LOG(MSG_READING_GENOME(param.annotationsPath));
        if(loadGenome(genomeData, param.annotationsPath, param.analysisMode, param.annotationCoordinatesType))
            // make the alphabetically sorted genome out of the genome dataset
            buildSortedGenome(sortedGenome, genomeData.get()/*get raw pointer*/);
        else
            ERROR(false, MSG_INVALID_GENOME(param.annotationsPath));

        // Convert ranges filter to annotations names using the reference genome.
        // This actually clears any ranges filters.
        selected.mapRangesToNames(sortedGenome, genome_ptr_reference_filter_range_lesser_compare, genome_ptr_reference_filter_range_greater_compare);
        excluded.mapRangesToNames(sortedGenome, genome_ptr_reference_filter_range_lesser_compare, genome_ptr_reference_filter_range_greater_compare);
    }

    // iterate for all rounds
    for(std::deque<std::string>::iterator roundIt = param.rounds.begin(); roundIt != param.rounds.end(); ++roundIt)
    {
        nProcessedDatasets = 0;
        round = *roundIt; // retrieve round name

        if(!round.empty())
            PRINT_AND_LOG(MSG_ROUND(round));

        roundTmr.start();

        // process features by their analysis mode
        switch(param.analysisMode)
        {
            case analysis_mode::type::ANNOTATION:
            case analysis_mode::type::EXON:
                {
                    // apply filters then link features with annotation or exons
                    linkAnnotationsOrExons(param.analysisMode, refGroupsPaths[round], referenceFeatures, aggregateFeatures, sortedGenome, selected, excluded, param, genomeData->format());
                    break;
                }

            case analysis_mode::type::COORDINATE:
                {
                    // link features with coordinates
                    linkCoordinates(refGroupsPaths[round], referenceFeatures, aggregateFeatures, selected, excluded, param);
                    break;
                }

            case analysis_mode::type::UNKNOWN: // do nothing..should never happen
            default:
                break;
        }

        // sort features by chromosomes, begin(asc), end(desc)
        std::sort(referenceFeatures.begin(), referenceFeatures.end(), features_less_compare);

        // Prefill the update table AFTER sorting the reference features so their
        // elements are not moved in memory, thus keeping valid pointers to the
        // windows.
        if(param.processMissingData)
            prefillUpdateTable(referenceFeatures);

        // Removes features who were not linked : no chromosome, begin = 0, end = -1
        // as we do not want them to interfere in the population.
        removeUnlinkedFeatures(referenceFeatures);

        // Do not process datasets if there's no more reference features.
        if(!referenceFeatures.empty())
        {
            // loop on each dataset file and process it, dataset name may change on load
            for(std::deque<std::string>::iterator datasetIt = param.datasetsPaths[round].begin(); datasetIt != param.datasetsPaths[round].end(); ++datasetIt)
            {
                PRINT_AND_LOG(MSG_PROCESS_DATASET(*datasetIt));

                // Try to load data if we recognize it, if it is readable then process it
                // and update reference features with data. Afterwards we can update
                // the aggregate features and write on disk the results.
                if(loadAndProcessDataset(*datasetIt, referenceFeatures, param.datasetChunkSize, datasetName, param.processMissingData))
                {
                    ++nProcessedDatasets;
                    updateWeightedMean(referenceFeatures, param.processMissingData);
                    updateAggregate(referenceFeatures, aggregateFeatures, param.orientationSubgroups, param.aggregateDataType, param.meanDispersionValue, param.processMissingData);
                    outBuilder.outputResults(datasetName, referenceFeatures, aggregateFeatures, param);

#ifdef __DEBUG__
                    //printFeatures(referenceFeatures, param, datasetName);
                    //                printGenes(referenceFeatures, param, datasetName);
#endif

                    // flush features data, this does not affect features and windows coordinates
                    resetFeaturesData(referenceFeatures, aggregateFeatures);
                }
            }
        }
        else
            PRINT_AND_LOG_WARNING(MSG_EMPTY_REFERENCE_FEATURES);

        // print time in seconds to the console
        LOG(recordLevel::INFORMATION, MSG_PROCESSED_DATASETS(nProcessedDatasets));
        PRINT_AND_LOG(MSG_APP_ROUND_DONE(util::math::round(roundTmr.elapsed(), 3)));

        // clears features from their data, all elements are destroyed and memory is freed
        clearFeaturesData(referenceFeatures, aggregateFeatures);
    }

    // output graph mappings and heat map files
    if(param.generateAggregateGraphs)
        outBuilder.writeListAggGraphs(param);
    if(param.generateHeatmaps)
        outBuilder.writeListIndHeatmaps(param);

    // print time in seconds to the console
    PRINT_AND_LOG(MSG_APP_DONE(util::math::round(runTmr.elapsed(),3)));

    logger->tail(); // log application success
    logger->close();
    return EXIT_SUCCESS;
}

/**
 * Clears data structures.
 *
 * @see group_feature
 *
 * @param referenceFeatures A group of reference features.
 * @param aggregateFeatures A group of aggregate features.
 */
void clearFeaturesData(group_feature<reference_feature> &referenceFeatures, group_feature<aggregate_reference_feature> &aggregateFeatures)
{
    referenceFeatures.clean();
    aggregateFeatures.clean();
}

/**
 * Resets all windows to initial state.
 *
 * @param referenceFeatures A group of reference features.
 * @param aggregateFeatures A group of aggregate features.
 */
void resetFeaturesData(group_feature<reference_feature> &referenceFeatures, group_feature<aggregate_reference_feature> &aggregateFeatures)
{
    // resets all reference feature windows
    for(group_feature<reference_feature>::iterator featureIt = referenceFeatures.begin(); featureIt != referenceFeatures.end(); ++featureIt)
        featureIt->resetData();

    // resets all aggregate feature windows
    for(group_feature<aggregate_reference_feature>::iterator aggFeatureIt = aggregateFeatures.begin(); aggFeatureIt != aggregateFeatures.end(); ++aggFeatureIt)
        aggFeatureIt->resetData();
}

/**
 * Fix parameters value that make incorrect combination.
 *
 * When block_alignment is different than block_alignment::SPLIT or the analysis_method
 * is analysis_method::type::RELATIVE, the following parameters are subject to changes :
 *      - block_split_type will equal block_split_type::PERCENTAGE
 *      - block_split_value will equal 100.
 *      - block_split_alignment will equal block_split_alignment::LEFT
 *      - block_alignment will equal block_alignment::LEFT (only applicable in relative mode)
 *
 * When analysis_mode is different than analysis_mode::ANNOTATION,
 * the following parameters are subject to changes :
 *      - orientations_subgroups will have only graph_orientation::orientation::APA set to @c True;
 *      - one_orientation_per_group will be set to @c True.
 *
 * @param param list of parameters
 */
void alterNonSenseParameters(parameters &param)
{
    // TODO : Move into parameter class.

    // TODO : Add a warning if the user has selected 0% and left, because we overwrite(assume) it to 100%
    for(uint_t i = 0; i < param.referencePoints + 1; i++)
    {
        if(param.blockAlignment[i] != block_alignment::alignment::SPLIT || param.analysisMethod == analysis_method::type::RELATIVE)
        {
            param.blockSplitType[i] = block_split_type::type::PERCENTAGE;
            param.blockSplitValue[i] = 100;
            param.blockSplitAlignment[i] = block_split_alignment::type::LEFT;

            if(param.analysisMethod == analysis_method::type::RELATIVE)
                param.blockAlignment[i] = block_alignment::alignment::LEFT;
        }
    }

    // only APA can be considered when reference is a coordinates file or exons
    if(param.analysisMode != analysis_mode::type::ANNOTATION)
    {
        param.orientationSubgroups.reset();
        param.orientationSubgroups.set(static_cast<unsigned int>(graph_orientation::orientation::APA), true); // only APA
        param.oneOrientationPerGraph = true;
    }

    // make sure we process all data if chunk processing is not selected
    if(!param.processDataByChunk)
        param.datasetChunkSize = PROCESS_ALL_DATA;
}

/**
 * Copies parameters file to the directory provided by the parameter output_directory.
 *
 * @warning If the @c prefix_filename has an invalid character, the parameter file will
 * not be copied and an error will be raised.
 *
 * @param param VAP parameters
 * @param vm variables map of parameters
 */
void copyToOutputDirectory(const parameters &param, const boost::program_options::variables_map &vm)
{
    // determine prefix and extract file names
    std::string prefix = (param.prefixFilename.empty() ? "" : param.prefixFilename + "_");
    std::string paramFilename = filesystem::extract_filename(param.parametersPath);

    // if files are already prefixed with the same prefix, then do not concatenate prefix
    if(!util::starts_with(paramFilename, prefix))
        paramFilename = paramFilename.insert(0, prefix);

    std::string outFile = param.outputDirectory + paramFilename;

    std::ofstream ofs(outFile);
    ERROR(ofs, MSG_FILE_NOTOPEN_NOTFOUND(outFile));

    // Copy all the parameters
    ofs << vm;
}

bool validateTrackType(const std::string &path, const io::file_format format)
{
    // Just try to read the magic number for the bigwig format
    // since they don't have a track line.
    if(format == io::file_format::BIGWIG)
        return io::format_guess::tryBigwigFormat(path);
    else
    {
        std::unique_ptr<io::file_reader> reader(io::file_reader_factory::create(path));
        ERROR(reader->is_open(), MSG_FILE_NOTOPEN_NOTFOUND(path));

        std::string line = "";
        if(reader->readline(line) && util::starts_with(line, "track"))
        {
            size_t pos = line.find("type");
            if(pos != std::string::npos)
            {
                std::string value = "";

                util::parser_string parser(&line, " ");
                parser.advance(pos + 5); // advance beyond 'track='

                parser >> value;
                value = util::tolower(value);

                switch(format)
                {
                    case io::file_format::BEDGRAPH:
                        // For historic reasons we must accept wiggle_0 as a valid
                        // bedgraph track type.
                        return value == "bedgraph" || value == "wiggle_0";

                    case io::file_format::WIG:
                        // The wig format can have an option (wiggle_0) for controlling display on the UCSC browser,
                        // we don't want to include this option in the comparison, thus we only compare the 'wiggle' part.
                        return value == "wiggle_0";

                    case io::file_format::UNKNOWN:
                    default:
                        break;
                }
            }
        }
    }

    return false;
}

/**
 * Loads a dataset and then process it.
 *
 * Process of a dataset consists of cleaning it and then updating all features
 * with its data.
 * @see dataset_format
 *
 * @note Header is mandatory for datasets, the track line is cross-validated with format.
 *
 * @param datasetPath A dataset file.
 * @param referenceFeatures A group of reference features to process.
 * @param datasetChunkSize Size of chunk to read.
 * @param datasetName The dataset name.
 * @param includeZeros Process missing data (include zeros into mean computation).
 *
 * @return @c True when a dataset format is known, @c False otherwise.
 */
bool loadAndProcessDataset(const std::string &datasetPath, group_feature<reference_feature> &referenceFeatures, const unsigned long long datasetChunkSize, std::string &datasetName, bool includeZeros)
{
    // TODO: Add message of number of lines read from dataset
    // TODO : add condition to not loop twice when the dataset is fully read the first time, for all formats
    bool isValid = false;
    size_t size = 0;

    // instanciate the factory to allow dataset creation
    io::file_format format = io::format_guess::discoverDataset(datasetPath);

    // cross-validate with track line
    if(format == io::file_format::BEDGRAPH || format == io::file_format::WIG || format == io::file_format::BIGWIG)
    {
        if(!validateTrackType(datasetPath, format))
        {
            PRINT_AND_LOG_WARNING(MSG_TRACKTYPE_NOMATCH(datasetPath, io::to_string(format)));
            format = io::file_format::UNKNOWN; // reset erroneous format
        }
    }

    // determine dataset format if known
    switch(format)
    {
        case io::file_format::BEDGRAPH:
            {
                // creates appropriate pointer and the bedgraph dataset
                util::scalar_smart_pointer<bedgraph>::type dataset(new bedgraph(datasetPath, datasetChunkSize)); // self-destruct scalar pointer

                // load a sized-chunk of the dataset and then process it if we have read something
                while(dataset->load())
                {
                    size += dataset->size();

                    // process its data with the group of features
                    processDataset(dataset.get(), referenceFeatures, includeZeros);

                    // clear memory for the next chunk
                    dataset->clear();
                }

                datasetName = dataset->propertyValue("name");
                isValid = size > 0; // return true if it is a valid dataset
                break;
            }
        case io::file_format::WIG:
            {
                // creates appropriate pointer and the wig dataset
                util::scalar_smart_pointer<wig>::type dataset(new wig(datasetPath, datasetChunkSize)); // self-destruct scalar pointer

                // load a sized-chunk of the dataset and then process it
                while(dataset->load())
                {
                    size += dataset->size();

                    // process its data with the group of features
                    processDataset(dataset.get(), referenceFeatures, includeZeros);

                    // clear memory for the next chunk
                    dataset->clear();
                }

                datasetName = dataset->propertyValue("name");
                isValid = size > 0; // return true if it is a valid dataset
                break;
            }

        case io::file_format::BIGWIG:
            {
                // creates appropriate pointer and the BW dataset, pass a pointer of the references feature for feature queuing within the BW reader.
                util::scalar_smart_pointer<bigwig>::type dataset(new bigwig(datasetPath, &referenceFeatures, datasetChunkSize)); // self-destruct scalar pointer

                // load a sized-chunk of the dataset and then process it
                while(dataset->load())
                {
                    size += dataset->size();

                    // process its data with the group of features
                    processDataset(dataset.get(), referenceFeatures, includeZeros);

                    // clear memory for the next chunk
                    dataset->clear();
                }

                datasetName = dataset->propertyValue("name");
                isValid = size > 0; // return true if it is a valid dataset
                break;
            }

        case io::file_format::UNKNOWN:
        default:
            PRINT_AND_LOG_WARNING(MSG_UNKNOWN_DATASET(datasetPath)); // log a warning for this dataset
            isValid = false;
            break;
    }

    if(size == 0)
        PRINT_AND_LOG_WARNING(MSG_DATASET_NOT_READ(datasetPath));

    return isValid;
}

/**
 * Loads a reference genome file.
 *
 * @note Header is optionnal for GTF and Genepred format, so no cross validation is done.
 *
 * @param genomeData Pointer to the genome data.
 * @param annotationsPath Genome's file path.
 * @param analysisMode Which type of reference features to use (annotations or exons).
 * @param annotationCoordinatesType Use either the transcription (tx) or coding (cds) coordinate columns for annotations.
 *
 * @return @c True when a dataset format is known, @c False otherwise.
 */
bool loadGenome(util::scalar_smart_pointer<genome>::type &genomeData, const std::string &annotationsPath, const analysis_mode::type &analysisMode, const annotation_coordinates_type::type &annotationCoordinatesType)
{
    bool isValid = false;
    size_t size = 0;

    io::file_format format = io::format_guess::discoverGenome(annotationsPath);

    // determine dataset format if known
    switch(format)
    {
        case io::file_format::GENEPRED_EXTENDED:
        case io::file_format::GENEPRED:
        case io::file_format::GTF:
            {
                util::scalar_smart_pointer<genome>::type genomeTmp = util::scalar_smart_pointer<genome>::type(new genome(annotationsPath, format, analysisMode, annotationCoordinatesType)); // self-destruct scalar pointer

                genomeTmp->load();
                size = genomeTmp->size();

                // add dummies entry to each chromosome entries(begin and end)
                addDummiesEntry(genomeTmp.get());

                // removes useless entries(ones beginning with a comment tag (#))
                clean(genomeTmp.get());

                genomeTmp.swap(genomeData); // exchange pointers to preserve entries

                isValid = size > 0; // return true if it is a valid dataset
                break;
            }

        case io::file_format::UNKNOWN:
        default:
            PRINT_AND_LOG_WARNING(MSG_UNKNOWN_GENOME(annotationsPath)); // log a warning for this dataset
            isValid = false;
            break;
    }

    if(size == 0)
        PRINT_AND_LOG_WARNING(MSG_GENOME_NOT_READ(annotationsPath));

    return isValid;
}

/**
 * Processes a dataset.
 *
 * The dataset is cleaned then is used to update the group of reference_feature.
 * @see @c clean()
 *
 * @param dataset A dataset to be processed.
 * @param referenceFeatures A group of reference features.
 * @param includeZeros Process missing data (include zeros into mean computation).
 */
template <class ENTRY_TYPE>
void processDataset(abstract_dataset<ENTRY_TYPE> *dataset, group_feature<reference_feature> &referenceFeatures, bool includeZeros)
{
    clean(dataset, referenceFeatures, true); // remove filtered or invalid chromosomes

    updateRange(dataset, referenceFeatures, includeZeros);
}

/**
 * Builds a sorted @c std::vector of genome entry pointer.
 *
 * The genome is built in a way where all its entries are in lexicographical order.
 *
 * @param sortedGenome
 * @param dataset
 */
void buildSortedGenome(std::vector<genome_entry_pointer> &sortedGenome, genome *dataset)
{
    uint_t memory = 0;

    // pre-process chromosomes to calculate total memory amount required
    for(abstract_dataset<genome_entry>::const_iterator grpIt = dataset->begin(); grpIt != dataset->end(); ++grpIt)
        memory += grpIt->second.size();

    // reserve just enough memory for all chr among the different groups, hence avoiding copy of element on growth
    sortedGenome.reserve(memory);

    // insert genome pointers, loop for all chromosomes
    for(abstract_dataset<genome_entry>::const_iterator chrIt = dataset->begin(); chrIt != dataset->end(); ++chrIt)
    {
        // loop for all entries of the current chromosome
        for(std::deque<genome_entry>::const_iterator entryIt = chrIt->second.begin() + 1/*begin dummy*/; entryIt != chrIt->second.end() - 1/*end dummy*/; ++entryIt)
            sortedGenome.push_back(genome_entry_pointer(&(*(entryIt - 1))/*prior*/, &(*entryIt)/*main annot*/, &(*(entryIt + 1)) /*next*/)); // insert addresses
    }

    // sort alphabetically
    std::sort(sortedGenome.begin(), sortedGenome.end(), genome_name_compare);
}

/**
 * Reads a list of annotations which will be used to filter further groups of features.
 *
 * @note If file cannot be open or do not exists, nothing is done.
 * @param sequence sequence of annotations used for filtering
 * @param path file that contains list of annotations
 */
void loadAnnotationsFilter(std::deque<reference_annotation> &sequence, const std::string &path)
{
    std::unique_ptr<io::file_reader> reader(io::file_reader_factory::create(path));

    // if file exists and we can read it otherwise nothing is done
    if(reader->is_open())
    {
        std::string name, line;
        util::parser_string parser(&line, DEFAULT_DELIMITERS);

        while(reader->readline(line))
        {
            parser >> name;
            if(!name.empty()) sequence.push_back(reference_annotation(name));
            parser.resetPosition(); // reset buffer read position
        }
    }
}

/**
 * Add dummies entry to the @c genome.
 *
 * The dummies entry fulfill the condition of having
 * at least one prior annotation to the first annotation of the genome
 * and at least one following annotation to the last annotation of the genome.
 * This way, no annotations are discarded.
 *
 * @param dataset genome dataset
 */
void addDummiesEntry(genome *dataset)
{
    // define iterator type
    typedef abstract_dataset<genome_entry>::iterator it_t;

    std::string dummy_begin = "dummy_begin_";
    std::string dummy_end = "dummy_end_";

    exons_t exons;
    exons.push_back(exon(0, -1)); // push empty exon

    // TRICKY PART :
    //
    // For each chromosome, add two dummy entries, one at the front and the other at the back.
    //
    // front dummy entry :
    //      Must be Watson oriented so there's no promoters in front and its coordinates must be reversed so that
    //      begin point is the same as the real annotation that follows, and the end point is 1 bp less.
    //
    // back dummy entry :
    //      Must be Crick oriented so there's no terminator after and its coordinates must be reversed so that
    //      end point is the same as the real annotation that follows, and the end point is 1 bp less.
    //
    // To simulate the above scenario, the values are hard coded.
    for(it_t it = dataset->begin(); it != dataset->end(); ++it)
    {
        it->second.insert(it->second.begin(), genome_entry(dummy_begin + it->first, it->first, annotation_strand::strand::POSITIVE, 0, -1, 1, exons));
        it->second.push_back(genome_entry(dummy_end + it->first, it->first, annotation_strand::strand::NEGATIVE, 99999999, 99999998, 1, exons));
    }
}

/**
 * Initializes all reference_feature among all groups.
 *
 * References features are created from a group of annotations. The group name
 * is stored and the reference features are created. Newly created reference features
 * are initialized with invalid coordinates (0, -1). This way, unlinked reference feature
 * are not mixed with linked ones. Further, in the process the reference features
 * will be set with the proper coordinates according the genome entries.
 *
 * @param groupAnnotations All groups of annotations.
 * @param referenceFeatures A group of newly created reference features.
 * @param param VAP parameters.
 * @param convertOneBased Array of groups to be converted one based.
 * @param convertHalfOpen Array of groups to be converted half open.
 */
template <class ENTRY_TYPE>
void createReferenceFeatures(const std::deque< std::pair< std::string, std::deque<ENTRY_TYPE> > > &groupAnnotations, group_feature<reference_feature> &referenceFeatures, const parameters &param, const std::vector<bool> &convertOneBased, const std::vector<bool> &convertHalfOpen)
{
    int groupIndex = 0;
    bool isAbsolute = param.analysisMethod == analysis_method::type::ABSOLUTE;

    // loop for all groups
    for(typename std::deque< std::pair< std::string, std::deque<ENTRY_TYPE> > >::const_iterator grpIt = groupAnnotations.begin(); grpIt != groupAnnotations.end(); ++grpIt)
    {
        // add the group name as a new group of features into the global set
        referenceFeatures.addGroupName(grpIt->first);
        groupIndex = referenceFeatures.groupIndex(grpIt->first);

        referenceFeatures.setOneBased(groupIndex, convertOneBased[groupIndex]);
        referenceFeatures.setHalfOpen(groupIndex, convertHalfOpen[groupIndex]);

        // create features out of group annotations
        for(typename std::deque<ENTRY_TYPE>::const_iterator it = grpIt->second.begin(); it != grpIt->second.end(); ++it)
        {
            // Initializes reference feature with invalid coordinates.
            // Coordinates will be set further in the process.
            referenceFeatures.push_back(reference_feature(0, -1, param.referencePoints, groupIndex, it->index(), it->name));
            referenceFeatures[referenceFeatures.size() - 1].buildBlocks(param.referencePoints, param.windowSize, param.windowsPerBlock, param.blockAlignment, isAbsolute);
        }
    }
    // TODO : Complete summary.
}

/**
 * Initializes all aggregate_reference_feature among all groups
 * of reference features.
 *
 * @param groupFeatures All groups of reference features.
 * @param aggregateFeatures Newly created group of aggregate reference features
 * @param param VAP parameters
 */
template <class ENTRY_TYPE>
void createAggregateFeatures(const std::deque< std::pair< std::string, std::deque<ENTRY_TYPE> > > &groupFeatures, group_feature<aggregate_reference_feature> &aggregateFeatures, const parameters &param)
{
    int groupIndex = 0;

    bool isAbsolute = param.analysisMethod == analysis_method::type::ABSOLUTE;

    // override parameters to ensure we display windows in the correct order
    // and not relatively to the blocks alignments, etc.
    std::vector<block_split_type::type> absolute(param.referencePoints + 1, block_split_type::type::NOT_APPLICABLE);
    std::vector<block_split_alignment::type> leftAlign(param.referencePoints + 1, block_split_alignment::type::LEFT);
    std::vector<uint_t> props(param.referencePoints + 1, 100);
    std::vector<block_alignment::alignment> alignments(param.referencePoints + 1, block_alignment::alignment::LEFT);

    // loop for all groups
    for(typename std::deque< std::pair< std::string, std::deque<ENTRY_TYPE> > >::const_iterator grpIt = groupFeatures.begin(); grpIt != groupFeatures.end(); ++grpIt)
    {
        // add the group name as a new group of features into the global set
        aggregateFeatures.addGroupName(grpIt->first);
        groupIndex = aggregateFeatures.groupIndex(grpIt->first);

        // make features out of group annotations
        for(uint_t i = 0; i < N_ORI_SUBGROUPS; i++)
        {
            // push the aggregate features, even if not asked by the user
            // The entry index is required for the reference feature but not for the aggregate one,
            // therefore we can set it to -1.
            aggregateFeatures.push_back(aggregate_reference_feature(output_builder::firstRelativeWindow(param), output_builder::lastRelativeWindow(param), param.referencePoints, groupIndex, -1, graph_orientation::toString(static_cast<graph_orientation::orientation>(i))));

            if(param.orientationSubgroups[i])
            {
                aggregateFeatures[aggregateFeatures.size() - 1].buildBlocks(param.referencePoints, param.windowSize, param.windowsPerBlock, alignments, isAbsolute);
                aggregateFeatures[aggregateFeatures.size() - 1].initializePopulation();
                aggregateFeatures[aggregateFeatures.size() - 1].fillBlocks(absolute, leftAlign, props, param.windowSize, param.analysisMode, isAbsolute);
            }
        }
    }
    // TODO : Complete summary.
}

/**
 * Links all annotations or exons with the genome.
 *
 * Reference groups are read and the filtered.
 * With the filtered annotations, the groups of reference features and aggregate
 * reference features are created.
 * The groups are sorted in lexicographical order. Then, regarding the analysis mode,
 * the reference features are linked with the sorted genome.
 *
 * @note Reference groups path will be updated with the alias if it is present in
 * the file.
 *
 * @param type Annotation or exon type.
 * @param refGroupPaths Reference groups files.
 * @param referenceFeatures Created reference features.
 * @param aggregateFeatures Created aggregate reference features
 * @param sortedGenome Genome
 * @param selected Positive annotations filter.
 * @param excluded Negative annotations filter.
 * @param param VAP parameters.
 * @param genomeFmt Genome file's format.
 */
void linkAnnotationsOrExons(const analysis_mode::type type,
                            std::deque<std::string> &refGroupPaths,
                            group_feature<reference_feature> &referenceFeatures,
                            group_feature<aggregate_reference_feature> &aggregateFeatures,
                            const std::vector<genome_entry_pointer> &sortedGenome,
                            const positive_filter &selected,
                            const negative_filter &excluded,
                            const parameters &param,
                            const io::file_format &genomeFmt)
{
    std::deque< std::pair< std::string, std::deque<reference_annotation> > > groupAnnotations;
    std::map< std::string, uint_t > pathsTable;
    uint_t grpIdx = 0;
    std::vector<bool> convertOneBased, convertHalfOpen;
    bool convert = genomeFmt != io::file_format::GENEPRED;

    // initialize table
    for(std::deque<std::string>::iterator pathIt = refGroupPaths.begin(); pathIt != refGroupPaths.end(); ++pathIt)
        pathsTable[*pathIt] = 0;

    for(std::deque<std::string>::iterator pathIt = refGroupPaths.begin(); pathIt != refGroupPaths.end(); ++pathIt, ++grpIdx)
    {
        PRINT_AND_LOG(MSG_READING_GROUP(*pathIt));
        groupAnnotations.push_back(std::make_pair(std::string(), std::deque<reference_annotation > ()));
        loadGroupOfFeatures(groupAnnotations[grpIdx], *pathIt);
        *pathIt = ensureNameUniqueness(groupAnnotations[grpIdx].first, pathsTable); // update path with the alias if it was present or the path

        // TODO : Add validation of expected file format here? (For annot and coords too?)
        // Avoid filtering if there's no filter.
        if(!selected.isEmpty() || !excluded.isEmpty())
        {
            // sort alphabetically annotations name
            std::sort(groupAnnotations[grpIdx].second.begin(), groupAnnotations[grpIdx].second.end(), annotations_name_lesser_compare);

            PRINT_AND_LOG(MSG_APPLYING_FILTER(selected.size(), excluded.size()));

            // filter annotation for this group
            applyNameFilters(groupAnnotations[grpIdx].second, selected, excluded, annotations_reference_filter_name_lesser_compare, annotations_reference_filter_name_greater_compare);
        }

        convertOneBased.push_back(convert);
        convertHalfOpen.push_back(!convert);
    }

    createReferenceFeatures(groupAnnotations, referenceFeatures, param, convertOneBased, convertHalfOpen);
    createAggregateFeatures(groupAnnotations, aggregateFeatures, param);

    // sort alpha
    std::sort(referenceFeatures.begin(), referenceFeatures.end(), features_name_less_compare);

    PRINT_AND_LOG(MSG_LINKING_FEATURES(referenceFeatures.size()));

    // link reference features
    switch(type)
    {
        case analysis_mode::type::ANNOTATION:
            setAnnotationsFeatures(referenceFeatures, sortedGenome, param);
            break;

        case analysis_mode::type::EXON:
            setExonsFeatures(referenceFeatures, sortedGenome, param);
            break;

        default:
            break;
    }
}

/**
 * Links all coordinates.
 *
 * Reference groups are read, then the groups of reference features and aggregate
 * reference features are created. Afterward the reference features are linked
 * with the reference groups.
 *
 * @note Reference groups path will be updated with the alias if it is present in
 * the file.
 *
 * @param refGroupPaths Paths of reference groups files.
 * @param referenceFeatures A group of reference features.
 * @param aggregateFeatures A group of aggregate reference features.
 * @param selected List of selected annotations.
 * @param excluded List of excluded annotations
 * @param param VAP parameters.
 */
void linkCoordinates(std::deque<std::string> &refGroupPaths, group_feature<reference_feature> &referenceFeatures, group_feature<aggregate_reference_feature> &aggregateFeatures, const positive_filter &selected, const negative_filter &excluded, const parameters &param)
{
    std::deque< std::pair< std::string, std::deque<reference_coordinates> > > groupCoordinates;
    std::map< std::string, uint_t > pathsTable;
    uint_t grpIdx = 0;
    io::file_format fileType = io::file_format::UNKNOWN;
    std::vector<bool> convertOneBased, convertHalfOpen;
    positive_filter selected2;
    negative_filter excluded2;

    // initialize table
    for(std::deque<std::string>::iterator pathIt = refGroupPaths.begin(); pathIt != refGroupPaths.end(); ++pathIt)
        pathsTable[*pathIt] = 0;

    for(std::deque<std::string>::iterator pathIt = refGroupPaths.begin(); pathIt != refGroupPaths.end(); ++pathIt, ++grpIdx)
    {
        PRINT_AND_LOG(MSG_READING_GROUP(*pathIt));
        groupCoordinates.push_back(std::make_pair(std::string(), std::deque<reference_coordinates > ()));
        fileType = loadGroupOfCoordinates(groupCoordinates[grpIdx], *pathIt, param.referencePoints);
        *pathIt = ensureNameUniqueness(groupCoordinates[grpIdx].first, pathsTable); // update path with the alias if it was present or the path

        // Avoid filtering if there's no filter.
        if(!selected.isEmpty() || !excluded.isEmpty())
        {
            // Make a backup copy of filters for every group of coordinates so we does not
            // loose the original filters.
            selected2 = selected;
            excluded2 = excluded;

            // Sort entries according to the chromosomic order using the coordinates of interest of each reference_coordinates.
            // This sort is needed in order to map correctly the ranges to names.
            std::sort(groupCoordinates[grpIdx].second.begin(), groupCoordinates[grpIdx].second.end(), coordinates_interest_range_chromosomic_lesser_compare);

            // Map all ranges to their associated names. This actually clears any ranges.
            selected2.mapRangesToNames(groupCoordinates[grpIdx].second, coordinates_interest_filter_entry_range_lesser_compare, coordinates_interest_filter_entry_range_greater_compare);
            excluded2.mapRangesToNames(groupCoordinates[grpIdx].second, coordinates_interest_filter_entry_range_lesser_compare, coordinates_interest_filter_entry_range_greater_compare);

            // Sort alphabetically coordinates according to their name in order to apply filters.
            std::sort(groupCoordinates[grpIdx].second.begin(), groupCoordinates[grpIdx].second.end(), coordinates_name_lesser_compare);

            PRINT_AND_LOG(MSG_APPLYING_FILTER(selected2.size(), excluded2.size()));

            applyNameFilters(groupCoordinates[grpIdx].second, selected2, excluded2, coordinates_filter_entry_name_lesser_compare, coordinates_filter_entry_name_greater_compare);
        }

        convertOneBased.push_back(false);
        convertHalfOpen.push_back(fileType == io::file_format::BED);
    }

    createReferenceFeatures(groupCoordinates, referenceFeatures, param, convertOneBased, convertHalfOpen);
    createAggregateFeatures(groupCoordinates, aggregateFeatures, param);

    PRINT_AND_LOG(MSG_LINKING_FEATURES(referenceFeatures.size()));
    setCoordinatesFeatures(referenceFeatures, groupCoordinates, param);
}

/**
 * Ensures reference group paths are unique.
 *
 * This has the effect of adding an increment on the @a value when it is not
 * unique, otherwise the @a value is simply returned.
 * The returned value will either be the original value or a value of the following
 * format : value(increment)
 *
 * @param value Reference group path.
 * @param table Reference group paths table.
 *
 * @return unique value
 */
std::string ensureNameUniqueness(std::string &value, std::map< std::string, uint_t > &table)
{
    table[value]++;

    if(table[value] > 1)
        value += "(" + util::itos(table[value] - 1) + ")";

    return value;
}

/**
 * Removes references features which were not linked.
 *
 * The unlinked entries would have bubbled up with the sort, therefore all the unlinked
 * entries are together at the front of the group of reference features. An unlinked
 * featured is characterized by an empty chromosome and (0,-1) coordinates.
 *
 * Each removed entry are logged.
 *
 * @warning Reference features must be sorted by chromosomes otherwise there will
 * be remaining unlinked reference features.
 *
 * @param referenceFeatures A group of reference features.
 */
void removeUnlinkedFeatures(group_feature<reference_feature> &referenceFeatures)
{
    // ASSUMPTION : features are sorted by chromosomes

    // loop for all unlinked entries which find themselves to be at front
    unsigned int i = 0;
    for(group_feature<reference_feature>::iterator featureIt = referenceFeatures.begin(); featureIt != referenceFeatures.end(); ++featureIt)
    {
        if(featureIt->chromosome().empty() && featureIt->begin() == 0 && featureIt->end() == -1)
        {
            LOG(recordLevel::WARNING, MSG_REMOVED_FEATURE(featureIt->name()));
            referenceFeatures.pop_front();
        }
        else
            break; // no more features to removed
        ++i;
    }
}
// TODO : Add line format validation for all input file, for now there's only coord that has it.

/**
 * Validates the first non-comment line of a file to check if its content is
 * in the valid format.
 *
 * @param path file
 * @param format file format
 * @param referencePoints number of reference points
 * @param coordType (optional) specifies the number of reference points in the coordinates file
 *
 * @return @c True when line is in the valid format, @c False otherwise
 */
bool validateLineFormat(const std::string &path, const io::file_format format, const uint_t referencePoints, const std::string &coordType)
{
    std::unique_ptr<io::file_reader> reader(io::file_reader_factory::create(path));
    ERROR(reader->is_open(), MSG_FILE_NOTOPEN_NOTFOUND(path));

    std::string line;
    util::tokens_t tokens;
    bool isValid = false;
    uint_t refPts = 0, typeRefPts = 0;

    // ignore first comment lines
    while(reader->readline(line) && util::starts_with(line, COMMENT_TAG));

    // split line into tokens
    util::split(tokens, line, " \t");

    switch(format)
    {
        case io::file_format::REFERENCE_COORDINATES:

            // cross validate type property with extension
            if(!util::ends_with(util::tolower(path), coordType))
                return false;

            typeRefPts = util::stoui(coordType.substr(coordType.size() - 1).c_str());

            // coord format says that all fields are mandatory except the alias
            // string char int..int name [alias]

            // first token is the chr and is a string so no need to validate it
            // the same logic applies for the second token which is the strand
            // third to n(which would be the number of reference points) token must be a number
            // the name and the alias must be a string so no need to validate them

            // only validate coordinates
            for(uint_t index = 2; index < tokens.size() && refPts < referencePoints && util::isNumber(tokens[index]); ++index, ++refPts);
            // TODO : Better error message in case of mismatch between the number of RF points and the typeRF points from the coordinates file.

            // validate if line is in the right format
            isValid = tokens.size() >= 3 && refPts == referencePoints && typeRefPts == referencePoints;
            break;

        case io::file_format::BED:
            // bed format says that the three first tokens are mandatory
            // string int int

            // first token is the chr and is a string so no need to validate it
            // second and third token must be a number

            // validate if line is in the right format
            isValid = tokens.size() >= 3 && util::isNumber(tokens[1]) && util::isNumber(tokens[2]);
            break;

        default:
            break;
    }

    return isValid;
}

/**
 * Reads and parses coordinates entries.
 * File header is parsed to determine file type.
 *
 * @see @c parseFileHeader()
 *
 * @param groupCoordinates coordinates for a reference group
 * @param path file containing coordinates
 * @param referencePoints number of reference points
 *
 * @return file alias if present, otherwise the file name is used
 */
io::file_format loadGroupOfCoordinates(std::pair< std::string, std::deque<reference_coordinates> > &groupCoordinates, const std::string &path, uint_t referencePoints)
{
    std::unique_ptr<io::file_reader> reader(io::file_reader_factory::create(path));
    ERROR(reader->is_open(), MSG_FILE_NOTOPEN_NOTFOUND(path));

    std::map< std::string, std::string > fileProperties;
    io::file_format format = io::file_format::UNKNOWN;

    std::string tmpPath = util::tolower(path);
    if
    (
        (io::format_guess::isBedExtension(tmpPath) || io::format_guess::isBedGzipExtension(tmpPath) || io::format_guess::isBedBzip2Extension(tmpPath)) ||
        (io::format_guess::isReferenceCoordinates6Extension(tmpPath) || io::format_guess::isReferenceCoordinates6GzipExtension(tmpPath) || io::format_guess::isReferenceCoordinates6Bzip2Extension(tmpPath)) ||
        (io::format_guess::isReferenceCoordinates5Extension(tmpPath) || io::format_guess::isReferenceCoordinates5GzipExtension(tmpPath) || io::format_guess::isReferenceCoordinates5Bzip2Extension(tmpPath)) ||
        (io::format_guess::isReferenceCoordinates4Extension(tmpPath) || io::format_guess::isReferenceCoordinates4GzipExtension(tmpPath) || io::format_guess::isReferenceCoordinates4Bzip2Extension(tmpPath)) ||
        (io::format_guess::isReferenceCoordinates3Extension(tmpPath) || io::format_guess::isReferenceCoordinates3GzipExtension(tmpPath) || io::format_guess::isReferenceCoordinates3Bzip2Extension(tmpPath)) ||
        (io::format_guess::isReferenceCoordinates2Extension(tmpPath) || io::format_guess::isReferenceCoordinates2GzipExtension(tmpPath) || io::format_guess::isReferenceCoordinates2Bzip2Extension(tmpPath)) ||
        (io::format_guess::isReferenceCoordinates1Extension(tmpPath) || io::format_guess::isReferenceCoordinates1GzipExtension(tmpPath) || io::format_guess::isReferenceCoordinates1Bzip2Extension(tmpPath)) ||
        (io::format_guess::isReferenceCoordinatesExtension(tmpPath)  || io::format_guess::isReferenceCoordinatesGzipExtension(tmpPath)  || io::format_guess::isReferenceCoordinatesBzip2Extension(tmpPath))
    )
    {
        // parse header and retrieve properties if present
        parseFileHeader(fileProperties, path);

        // gets the alias if present, if not the file name is used as the alias
        groupCoordinates.first = fileProperties[FILE_PROPERTY_NAME];

        // TODO : Update warning message with format information.
        // if 'type' property is present and equal 'coord', else, try 'bed' format,
        // otherwise we have an unknown format
        if(util::starts_with(fileProperties[FILE_PROPERTY_TYPE], FILE_PROPERTY_TYPE_COORD) && validateLineFormat(path, io::file_format::REFERENCE_COORDINATES, referencePoints, fileProperties[FILE_PROPERTY_TYPE]))
        {
            loadGroupOfCoordinatesTypeCoord(groupCoordinates, reader, referencePoints, fileProperties[FILE_PROPERTY_TYPE]);
            format = io::file_format::REFERENCE_COORDINATES;
        }
        else if(referencePoints == 2 && validateLineFormat(path, io::file_format::BED, referencePoints))
        {
            // BED format is always 2 reference points so there's no need to pass the number of reference points as an argument
            loadGroupOfCoordinatesTypeBed(groupCoordinates, reader);
            format = io::file_format::BED;
        }
        // TODO : test an unknown file what happens with the number of groups, group index,  etc.
    }

    if(format == io::file_format::UNKNOWN)
        PRINT_AND_LOG_WARNING(MSG_UNKNOWN_FILE_FORMAT(path));

    return format;
}

/**
 * Reads and parses [Bed](http://genome.ucsc.edu/FAQ/FAQformat.html#format1) coordinates entries.
 * Line is split on space and tabulation.
 *
 * @warning lines with less than three columns are discarded
 * @note comment line(lines starting with #) are ignored
 *
 * For information on the file header :
 * @see @c parseFileHeader()
 *
 * @param groupCoordinates coordinates for a reference group
 * @param reader file reader
 */
void loadGroupOfCoordinatesTypeBed(std::pair< std::string, std::deque<reference_coordinates> > &groupCoordinates, const std::unique_ptr<io::file_reader> &reader)
{
    std::string chromosome("", 100), alias("", 100), line;
    int remainingTokens = 0;
    uint_t index = 0, entryIndex = 0;
    char strand = '.';
    std::vector<int> coords;
    util::tokens_t tokens;
    tokens.reserve(10);

    // read entire file
    while(reader->readline(line))
    {
        // pass to the next line if its begin with a comment tag
        if(util::starts_with(line, COMMENT_TAG))
            continue;

        // split tokens and discard empty tokens (multiples delimiters
        // creates multiples empty tokens which are discarded)
        util::split(tokens, line, DEFAULT_DELIMITERS);

        remainingTokens = index = 0;
        alias = chromosome = "";
        strand = '.';

        // three first fields are required
        if(tokens.size() >= 3)
        {
            // first two tokens are std::stringbit
            chromosome = tokens[index++];

            // next tokens are coordinates
            coords.push_back(util::stoi(tokens[index++].c_str()));
            coords.push_back(util::stoi(tokens[index++].c_str()));

            // The BED format uses half-open coordinates system, thus the need to
            // transform to an inclusive range for our internal representation.
            // Half-Open : [1-10] -> [1-11[ -> +1
            // Closed    : [1-11[ -> [1-10] -> -1
            util::toClosedRange(coords, 2);

            // the other fields are optional

            // get the number of remaining fields
            remainingTokens = tokens.size() - index;

            if(remainingTokens > 0)
            {
                // we have at least 1 optional field which is the alias
                alias = tokens[index++];

                // ignore score in all cases
                index++;

                // get strand
                if(index < tokens.size())
                    strand = tokens[index++][0]; // get only the first character
            }

            // add entry by accessing or inserting group name
            groupCoordinates.second.push_back(reference_coordinates(chromosome, annotation_strand::toStrand(strand), coords, util::cut_eol(alias), entryIndex));

            ++entryIndex;
            coords.clear();
        }

        // clear parsed tokens
        tokens.clear();
    }
}

/**
 * Reads and parses coordinates entries.
 * Line is split on tabulation.
 *
 * @warning lines with less than three columns or a number of columns different
 * then the number of reference points are discarded
 * @note comment line(lines starting with #) are ignored
 *
 * For information on the file header :
 * @see @c parseFileHeader()
 *
 * @param groupCoordinates coordinates for a reference group
 * @param reader file reader
 * @param referencePoints number of reference points
 * @param type type property from file header which specifies the number of reference points for the coordinates file
 */
void loadGroupOfCoordinatesTypeCoord(std::pair< std::string, std::deque<reference_coordinates> > &groupCoordinates, const std::unique_ptr<io::file_reader> &reader, uint_t referencePoints, const std::string &type)
{
    std::string chromosome("", 100), alias("", 100), line;
    uint_t index = 0, refPts = 0, typeRefPts = util::stoui(type.substr(type.size() - 1).c_str()), entryIndex = 0;
    char strand = '.';
    std::vector<int> coords;
    util::tokens_t tokens;
    tokens.reserve(10);

    // do not process file if we do not have the same number of reference points
    if(typeRefPts != referencePoints)
        return;

    // read entire file
    while(reader->readline(line))
    {
        // pass to the next line if its begin with a comment tag
        if(util::starts_with(line, COMMENT_TAG))
            continue;

        util::split(tokens, line, "\t");
        index = refPts = 0;
        alias = chromosome = "";
        strand = '.';

        // "if" can be removed once a validation step has been implemented
        if(tokens.size() >= 3)
        {
            // first two tokens are std::string
            chromosome = tokens[index++];
            strand = tokens[index++][0]/*std::string is a char*/;

            // next tokens are coordinates, read only coordinates token
            // until a string or the number of ref points is reached, hence
            // avoiding reading region name per example
            for(; index < tokens.size() && refPts < referencePoints && util::isNumber(tokens[index]); ++index, ++refPts)
                coords.push_back(util::stoi(tokens[index].c_str()));

            // alias tag is present
            if(index < tokens.size())
                alias = tokens[index++];

            // add entry by accessing or inserting group name and reference position in file
            groupCoordinates.second.push_back(reference_coordinates(chromosome, annotation_strand::toStrand(strand), coords, util::cut_eol(alias), entryIndex));

            ++entryIndex;
            coords.clear();
        }

        tokens.clear();
    }
}

/**
 * Reads and parses an annotations file.
 *
 * For information on the file header :
 * @see @c parseFileHeader()
 *
 * @param groupAnnotations annotations for a reference group
 * @param path file containing the annotations
 *
 * @return file alias if present, otherwise the file name is used
 */
std::string loadGroupOfFeatures(std::pair< std::string, std::deque<reference_annotation> > &groupAnnotations, const std::string &path)
{
    std::unique_ptr<io::file_reader> reader(io::file_reader_factory::create(path));
    ERROR(reader->is_open(), MSG_FILE_NOTOPEN_NOTFOUND(path));

    std::string line;

    // make buffer reader stops at end of line regardless of operating system
    util::parser_string parser(&line, "\t\r\n");

    std::map< std::string, std::string > fileProperties;
    std::string name = "";
    uint_t entryIndex = 0;

    // parse header and retrieve properties if present
    parseFileHeader(fileProperties, path);

    // gets the alias if present, if not the file name is used as the alias
    groupAnnotations.first = fileProperties[FILE_PROPERTY_NAME];

    // read entire file
    while(reader->readline(line))
    {
        // pass to the next line if its begin with a comment tag
        if(util::starts_with(line, COMMENT_TAG))
            continue;

        // while there's characters, parse chunk
        if(!parser.eob())
        {
            // read annotation and ignores rest of line thus ignores \r or \n to
            // be fully compliant with Windows or Unix or Mac.

            parser >> name;
            parser.ignoreUntilEndl(); // discard rest of line including EOL characters

            // add name and entry position in file
            groupAnnotations.second.push_back(reference_annotation(name, entryIndex));
            ++entryIndex;
        }

        parser.resetPosition();
    }

    return fileProperties[FILE_PROPERTY_NAME];
}

/**
 * Parses header of reference groups files.
 *
 * Parses the following properties :
 * - name : file alias.
 * - type : file type (optional). Irrelevant for groups of features.
 * - desc : file description.
 *
 * @note For coordinates files : if 'type' property is present its value must be one
 * of the following : coord1,...,coord6. Otherwise, the 'bed' type is assumed.
 *
 * @param properties file properties
 * @param path file's path
 */
void parseFileHeader(std::map< std::string, std::string> &properties, const std::string &path)
{
    /*
     * Parse a file header which has the following structure :
     *  # name=["]file_alias["] type=["]file_type["] desc=["]file_description["]
     */
    std::unique_ptr<io::file_reader> reader(io::file_reader_factory::create(path));
    ERROR(reader->is_open(), MSG_FILE_NOTOPEN_NOTFOUND(path));

    std::string groupName = filesystem::extract_filename(path), line;

    // clean
    util::cut_eol(groupName);
    util::trim(groupName, ' ');

    // at least insert the file name as the file group name.
    properties[FILE_PROPERTY_NAME] = filesystem::change_extension(groupName, "");

    // peek first character to parse file properties
    if(reader->peek() == COMMENT_TAG && reader->readline(line))
    {
        util::parser_string hreader(&line, "\" =");
        std::string key = "", value = "";
        char equal = '\0';

        hreader.ignore(1); // ignores comment character (#)

        // parse entire line
        while(!hreader.eob())
        {
            // parse property
            hreader.skip(); // skip spaces
            hreader >> key >> equal;

            if(hreader.peek() == '"')
                hreader.readFromTo(value, '"', '"');
            else
                hreader >> value;

            // insert token
            properties[key] = value;
        }
    }
}

/**
 * Cleans a dataset of its invalid(beginning with a #) chromosomes and ones
 * which are not referenced by the grouped of reference features.
 *
 * @param dataset A dataset.
 * @param referenceFeatures A group of reference features.
 * @param filter @c True to remove not referenced chromosomes, @c False to ignore.
 */
template <class ENTRY_TYPE>
void clean(abstract_dataset<ENTRY_TYPE> *dataset, const group_feature<reference_feature> &referenceFeatures, bool filter)
{
    // TODO : Add a validation that chr must be in the features group or coord file, otherwise flush it
    // TODO : Add warning when a genome chr entry is not found in a dataset
    // TODO : If no more chr :( -> ERROR...

    std::deque<typename abstract_dataset<ENTRY_TYPE>::iterator> invalidIts;
    std::deque<typename abstract_dataset<ENTRY_TYPE>::iterator> filteredIts;

    // loop and flagged invalid chromosomes + chromosomes which are not part of the group of reference feature
    for(typename abstract_dataset<ENTRY_TYPE>::iterator it = dataset->begin(); it != dataset->end(); ++it)
    {
        if(util::starts_with(it->first, COMMENT_TAG))
            invalidIts.push_back(it);
        else if(filter && !referenceFeatures.containsChr(it->first))
            filteredIts.push_back(it);
    }

    // erase all flagged chromosome entry that are ugly and useless as we have no reason to keep them
    for(typename std::deque<typename abstract_dataset<ENTRY_TYPE>::iterator>::iterator invalidChrIt = invalidIts.begin(); invalidChrIt != invalidIts.end(); ++invalidChrIt)
    {
        LOG(recordLevel::WARNING, MSG_INVALID_CHROMOSOME((*invalidChrIt)->first));
        dataset->erase(*invalidChrIt);
    }

    // erase all filtered chromosome entry
    for(typename std::deque<typename abstract_dataset<ENTRY_TYPE>::iterator>::iterator filteredChrIt = filteredIts.begin(); filteredChrIt != filteredIts.end(); ++filteredChrIt)
    {
        LOG(recordLevel::WARNING, MSG_FILTERED_CHROMOSOME((*filteredChrIt)->first));
        dataset->erase(*filteredChrIt);
    }
}

// TODO : Update summary where a warning is logged.

/**
 * Links all exon typed reference features.
 *
 * For each reference feature the following is done :
 * - chromosome is set;
 * - strand is set;
 * - chromosome is added as a filter for the group;
 * - extra information is set;
 * - alias is set;
 * - coordinates of interest are set;
 * - blocks are sets and filled with the proper windows;
 *
 * In case of multiple genome entry with the same name, only the first one is
 * used to set the reference features.
 *
 * @warning Reference features and genome must be in lexicographical order.
 *
 * @param referenceFeatures A group of reference features where its entries are in lexicographical order.
 * @param sortedGenome The genome where its entries are in lexicographical order.
 * @param param VAP parameters.
 */
void setExonsFeatures(group_feature<reference_feature> &referenceFeatures, std::vector<genome_entry_pointer> sortedGenome, const parameters &param)
{
    // ASSUMPTION : features and genome are alphabetically sorted

    bool isAbsolute = param.analysisMethod == analysis_method::type::ABSOLUTE;
    std::vector<genome_entry_pointer>::const_iterator genome_entryIt = sortedGenome.begin();

    // loop for all features to link them with genome entries
    for(group_feature<reference_feature>::iterator featureIt = referenceFeatures.begin(); featureIt != referenceFeatures.end(); ++featureIt)
    {
        // advance genome entry to the next one that can match feature
        while(genome_entryIt != sortedGenome.end() && genome_entryIt->main->name < featureIt->name())
            genome_entryIt++;

        // sets the feature with the first equivalent genome entry only
        if(genome_entryIt != sortedGenome.end() && featureIt->name() == genome_entryIt->main->name)
        {
            featureIt->setChromosome(genome_entryIt->main->chromosome);
            featureIt->setStrand(genome_entryIt->main->strand);
            referenceFeatures.addChrToFilter(genome_entryIt->main->chromosome);

            // sets the coordinates of the main annotation for further reference
            featureIt->setExtra(genome_entryIt->main->extra);
            featureIt->setAlias(genome_entryIt->main->alias);
            featureIt->setFeatureOfInterestCoordinates(genome_entryIt->main->begin, genome_entryIt->main->end);
            featureIt->setBlocksCoordinates(genome_entryIt->main->exons, param.blockAlignment[3]/*always 6 ref pts*/, param.windowsPerBlock[3], param.mergeMiddleIntrons);
            featureIt->fillBlocks(param.blockSplitType, param.blockSplitAlignment, param.blockSplitValue, param.windowSize, param.analysisMode, isAbsolute);
        }
    }
}

/**
 * Links all annotations typed reference features.
 *
 * For each reference feature the following is done :
 * - chromosome is set;
 * - strand is set;
 * - chromosome is added as a filter for the group;
 * - extra information is set;
 * - alias is set;
 * - coordinates of interest are set;
 * - blocks are sets and filled with the proper windows;
 *
 * In case of multiple genome entry with the same name, only the first one is
 * used to set the reference features.
 *
 * @warning Reference features and genome must be in lexicographical order.
 *
 * @param referenceFeatures A group of reference features where its entries are in lexicographical order.
 * @param sortedGenome The genome where its entries are in lexicographical order.
 * @param param VAP parameters.
 */
void setAnnotationsFeatures(group_feature<reference_feature> &referenceFeatures, std::vector<genome_entry_pointer> sortedGenome, const parameters &param)
{
    // ASSUMPTION : features and genome are alphabetically sorted

    bool is5p = param.oneRefPtBoundary == oneRefPt_boundary::type::FIVE_PRIME;
    bool isAbsolute = param.analysisMethod == analysis_method::type::ABSOLUTE;

    std::vector<coordinates> coords;
    std::vector<genome_entry_pointer>::const_iterator genome_entryIt = sortedGenome.begin();

    // loop for all features to link them with genome entries
    for(group_feature<reference_feature>::iterator featureIt = referenceFeatures.begin(); featureIt != referenceFeatures.end(); ++featureIt)
    {
        // advance genome entry to the next one that can match feature
        while(genome_entryIt != sortedGenome.end() && genome_entryIt->main->name < featureIt->name())
            genome_entryIt++;

        // sets the feature with the first equivalent genome entry only
        if(genome_entryIt != sortedGenome.end() && featureIt->name() == genome_entryIt->main->name)
        {
            featureIt->setChromosome(genome_entryIt->main->chromosome);
            featureIt->setStrand(genome_entryIt->main->strand);
            referenceFeatures.addChrToFilter(genome_entryIt->main->chromosome);

            // Regarding the neighbors annotations we can determine
            // the reference feature orientation(APA, PPP,...).
            determineFeatureOrientationType(genome_entryIt->prior->strand, *featureIt, genome_entryIt->next->strand);

            /*
             * Regarding the number of reference point of the reference feature,
             * we add the number of required coordinates.
             *
             * In case of 1 or 2 reference points, only the first coordinates, the
             * gene of interest, is required.
             *
             * In case of 3,4,5,6 reference points, only three coordinates (the gene
             * from left, gene of interest, right gene) are required.
             */
            switch(featureIt->referencePoints())
            {
                case 1:
                case 2:
                    coords.push_back(coordinates(genome_entryIt->main->begin, genome_entryIt->main->end)); // annotation of interest
                    break;

                case 3:
                case 4:
                case 5:
                case 6:
                    coords.push_back(coordinates(genome_entryIt->prior->begin, genome_entryIt->prior->end)); // prior annotation
                    coords.push_back(coordinates(genome_entryIt->main->begin, genome_entryIt->main->end)); // annotation of interest
                    coords.push_back(coordinates(genome_entryIt->next->begin, genome_entryIt->next->end)); // next annotation
                    break;

                default:
                    // WARNING :
                    LOG(recordLevel::WARNING, MSG_UNKNOWN_NUMBER_REFPTS(featureIt->name()));
                    ASSERT(false, MSG_UNKNOWN_NUMBER_REFPTS(featureIt->name())); // should NEVER happen
            }

            // sets the coordinates of the main annotation for further reference
            // sets the extra information from the genome
            featureIt->setExtra(genome_entryIt->main->extra);
            featureIt->setAlias(genome_entryIt->main->alias);
            featureIt->setFeatureOfInterestCoordinates(genome_entryIt->main->begin, genome_entryIt->main->end);
            featureIt->setBlocksCoordinates(coords, is5p);
            featureIt->fillBlocks(param.blockSplitType, param.blockSplitAlignment, param.blockSplitValue, param.windowSize, param.analysisMode, isAbsolute);

            //            ++genome_entryIt;
            coords.clear();
        }
    }
}

/**
 * Links all annotations typed reference features.
 *
 * For each reference feature the following is done :
 * - chromosome is set;
 * - strand is set;
 * - chromosome is added as a filter for the group;
 * - alias is set;
 * - coordinates of interest are set;
 * - blocks are sets and filled with the proper windows;
 *
 * @param referenceFeatures A group of reference features.
 * @param groupCoordinates A group of reference coordinates.
 * @param param VAP parameters.
 */
void setCoordinatesFeatures(group_feature<reference_feature> &referenceFeatures, std::deque< std::pair< std::string, std::deque<reference_coordinates> > > &groupCoordinates, const parameters &param)
{
    bool isAbsolute = param.analysisMethod == analysis_method::type::ABSOLUTE;

    std::vector<coordinates> coords;
    group_feature<reference_feature>::iterator featureIt = referenceFeatures.begin();

    // loop for all coordinates of reference
    for(std::deque< std::pair< std::string, std::deque<reference_coordinates> > >::const_iterator grpIt = groupCoordinates.begin(); grpIt != groupCoordinates.end(); ++grpIt)
    {
        // loop for all coordinates reference feature to set them
        for(std::deque<reference_coordinates>::const_iterator it = grpIt->second.begin(); it != grpIt->second.end() && featureIt != referenceFeatures.end(); ++it, ++featureIt)
        {
            featureIt->setChromosome(it->chromosome);
            featureIt->setStrand(it->strand);
            referenceFeatures.addChrToFilter(it->chromosome);

            /*
             * Regarding the number of reference point of the reference feature,
             * we add the number of required coordinates.
             *
             * 1rf : Only the first coordinate is required.
             *
             * 2rf : Only the first two coordinates are required.
             *
             * 3rf : The first coordinate is used twice when the strand
             *       is Crick because the annotation of interest is on the extreme
             *       left of the gene. Thus, corresponding to the downstream intergenic region(3')
             *       which is also on the border of the annotation of interest.
             *       On the other hand, when the strand is Watson, the last coordinate is used twice
             *       because it is on the extreme right of the gene. Thus, corresponding to the
             *       upstream intergenic region(5') which is also on the border of the
             *       annotation of interest.
             *
             * 4rf : Only the left intergenic region end point and the right intergenic begin point
             *       is required with the coordinates of the annotation of interest.
             *
             * 5rf : Same scenario as the 3rf.
             *
             * 6rf : All coordinates are required.
             */
            switch(featureIt->referencePoints())
            {
                case 1:
                    // sets the coordinates of the main annotation for further reference
                    coords.push_back(coordinates(it->coordinates[0], it->coordinates[0])); // annotation of interest
                    featureIt->setFeatureOfInterestCoordinates(it->coordinates[0], it->coordinates[0]);
                    break;

                case 2:
                    // sets the coordinates of the main annotation for further reference
                    coords.push_back(coordinates(it->coordinates[0], it->coordinates[1])); // annotation of interest
                    featureIt->setFeatureOfInterestCoordinates(it->coordinates[0], it->coordinates[1]);
                    break;

                case 3:
                    coords.push_back(coordinates(it->coordinates[0], it->coordinates[0])); // begin annotation

                    if(featureIt->isCrickOriented())
                    {
                        // sets the coordinates of the main annotation for further reference
                        coords.push_back(coordinates(it->coordinates[0], it->coordinates[1])); // annotation of interest
                        featureIt->setFeatureOfInterestCoordinates(it->coordinates[0], it->coordinates[1]);
                    }
                    else
                    {
                        // sets the coordinates of the main annotation for further reference
                        coords.push_back(coordinates(it->coordinates[1], it->coordinates[2])); // annotation of interest
                        featureIt->setFeatureOfInterestCoordinates(it->coordinates[1], it->coordinates[2]);
                    }

                    coords.push_back(coordinates(it->coordinates[2], it->coordinates[2])); // end annotation or next annotation begin
                    break;

                case 4:
                    coords.push_back(coordinates(it->coordinates[0], it->coordinates[0])); // prior annotation, only the end point since this the only coordinates that is used
                    coords.push_back(coordinates(it->coordinates[1], it->coordinates[2])); // annotation of interest
                    coords.push_back(coordinates(it->coordinates[3], it->coordinates[3])); // next annotation, only the begin point since this the only coordinates that is used

                    // sets the coordinates of the main annotation for further reference
                    featureIt->setFeatureOfInterestCoordinates(it->coordinates[1], it->coordinates[2]);
                    break;

                case 5:
                    if(featureIt->isCrickOriented())
                    {
                        coords.push_back(coordinates(it->coordinates[0], it->coordinates[0])); // prior annotation
                        coords.push_back(coordinates(it->coordinates[1], it->coordinates[2])); // annotation of interest
                        coords.push_back(coordinates(it->coordinates[3], it->coordinates[4])); // next annotation, only the begin point since this the only coordinates that is used

                        // sets the coordinates of the main annotation for further reference
                        featureIt->setFeatureOfInterestCoordinates(it->coordinates[1], it->coordinates[2]);
                    }
                    else
                    {
                        coords.push_back(coordinates(it->coordinates[0], it->coordinates[1])); // prior annotation
                        coords.push_back(coordinates(it->coordinates[2], it->coordinates[3])); // annotation of interest
                        coords.push_back(coordinates(it->coordinates[4], it->coordinates[4])); // next annotation, only the begin point since this the only coordinates that is used

                        // sets the coordinates of the main annotation for further reference
                        featureIt->setFeatureOfInterestCoordinates(it->coordinates[2], it->coordinates[3]);
                    }
                    break;

                case 6:
                    coords.push_back(coordinates(it->coordinates[0], it->coordinates[1])); // prior annotation
                    coords.push_back(coordinates(it->coordinates[2], it->coordinates[3])); // annotation of interest
                    coords.push_back(coordinates(it->coordinates[4], it->coordinates[5])); // next annotation

                    // sets the coordinates of the main annotation for further reference
                    featureIt->setFeatureOfInterestCoordinates(it->coordinates[2], it->coordinates[3]);
                    break;

                default:
                    // WARNING :
                    LOG(recordLevel::WARNING, MSG_UNKNOWN_NUMBER_REFPTS(featureIt->name()));
                    ASSERT(false, MSG_UNKNOWN_NUMBER_REFPTS(featureIt->name())); // should NEVER happen
            }

            // sets the alias
            featureIt->setAlias(it->alias);
            featureIt->setBlocksCoordinates(coords, true/*always true for 1 ref points*/);
            featureIt->fillBlocks(param.blockSplitType, param.blockSplitAlignment, param.blockSplitValue, param.windowSize, param.analysisMode, isAbsolute);
            coords.clear();
        }
    }
}


// TODO : Check possibilities to make set_feature a map, to be able to map only chr nodes together
// and not pass all chr entries until the next node...

/**
 * Update the group of reference features value within a range.
 *
 * For each dataset entry, we update the reference feature if it's in its coordinates range.
 *
 * @param dataset The current dataset.
 * @param referenceFeatures A group of reference features.
 * @param includeZeros Process missing data (include zeros into mean computation).
 */
template <class ENTRY_TYPE>
void updateRange(const abstract_dataset<ENTRY_TYPE> *dataset, group_feature<reference_feature> &referenceFeatures, bool includeZeros)
{
    // ASSUMPTION : entry type defines a member "value" which should be part of all dataset type

    typedef typename abstract_dataset<ENTRY_TYPE>::const_iterator chrIt_t;
    typedef typename std::deque<ENTRY_TYPE>::const_iterator entryIt_t;
    typedef group_feature<reference_feature>::iterator featureIt_t;

    featureIt_t featurePtr = referenceFeatures.begin(),
                featureIt = featurePtr;
    bool flag = false;

    // loop for all chromosome node
    for(chrIt_t chrIt = dataset->begin(); chrIt != dataset->end(); ++chrIt)
    {
        // TODO : TEST IF IT INTRODUCES A BUG -> optimization
        //        if(chrIt->first < featurePtr->chromosome())
        //            continue;

        // loop for all entries associated to the current chromosome and see if we can find a match
        for(entryIt_t entryIt = chrIt->second.begin(); entryIt != chrIt->second.end(); ++entryIt)
        {
            // advance to the next feature that has the same chromosome value
            while(featurePtr != referenceFeatures.end() && featurePtr->chromosome() < chrIt->first/*chromosome*/)
                featurePtr++;

            // update range for all reference feature that are on the current chromosome

            // reminder pointer of which feature we were on as there might be
            // multiple feature that fits the current chromosome
            featureIt = featurePtr;
            while(featureIt != referenceFeatures.end() && featureIt->chromosome() == chrIt->first/*chromosome*/)
            {
                if(entryIt->end < featureIt->begin())
                    break;

                if(featureIt->isInRange(entryIt->begin, entryIt->end))
                {
                    if(!flag)
                    {
                        flag = true;
                        featurePtr = featureIt; // store the current feature for further reference
                    }

                    // update its value
                    featureIt->updateRange(entryIt->begin, entryIt->end, entryIt->value, includeZeros);
                }

                ++featureIt;
            }

            // reset flag
            flag = false;
        }
    }
}

/**
 * Updates the weighted mean of all the references features.
 *
 * @param referenceFeatures Set of reference features.
 * @param includeZeros Process missing data (include zeros into mean computation).
 */
void updateWeightedMean(group_feature<reference_feature> &referenceFeatures, bool includeZeros)
{
    // TODO : Is it possible to do this loop while updating range?
    // Loop for all the reference features and update its weighted mean.
    if(includeZeros)
        for(group_feature<reference_feature>::iterator featureIt = referenceFeatures.begin(); featureIt != referenceFeatures.end(); ++featureIt)
            featureIt->updateWeigthedMeanZeros();
    else
        for(group_feature<reference_feature>::iterator featureIt = referenceFeatures.begin(); featureIt != referenceFeatures.end(); ++featureIt)
            featureIt->updateWeigthedMean();
}

/**
 * Fill the @c update_table of all reference features in advance to consider
 * all windows even if they were not hit by a data point.
 *
 * @param referenceFeatures Set of reference features.
 */
void prefillUpdateTable(group_feature<reference_feature> &referenceFeatures)
{
    // In case we need to process the missing data, we need to consider all windows
    // as if they were hit by a data point.
    for(group_feature<reference_feature>::iterator featureIt = referenceFeatures.begin(); featureIt != referenceFeatures.end(); ++featureIt)
        featureIt->prefillUpdateTable();
}

/**
 * Updates the @c aggregate_reference_feature in order to display the results.
 *
 * @note On the way, the update_table is cleaned.
 *
 * @param referenceFeatures The group of reference features to used as source.
 * @param aggregateFeatures The group of aggregate reference feature to update.
 * @param orientationSubgroups The list of subgroups to update.
 * @param aggregateDataType The aggregate type.
 * @param meanDispersionValue The mean dispersion value to use.
 * @param includeZeros Process missing data (include zeros into mean computation).
 */
void updateAggregate(group_feature<reference_feature> &referenceFeatures, group_feature<aggregate_reference_feature> &aggregateFeatures, const std::bitset < N_ORI_SUBGROUPS > &orientationSubgroups, const aggregate_data_type::type aggregateDataType, const mean_dispersion_value::type meanDispersionValue, const bool includeZeros)
{
    uint_t grpIdx = 0;
    for(group_feature<reference_feature>::iterator featureIt = referenceFeatures.begin(); featureIt != referenceFeatures.end(); ++featureIt)
    {
        // retrieve group index from the feature
        grpIdx = featureIt->groupIndex();

        if(orientationSubgroups[/*graph_orientation::orientation::APA)*/0])
            aggregateFeatures[/*graph_orientation::orientation::APA)*/0 + (grpIdx * N_ORI_SUBGROUPS)].updateAggregatePhase1(*featureIt, aggregateDataType, meanDispersionValue); // also update population

        if(orientationSubgroups[/*graph_orientation::orientation::APN)*/1] &&
                (featureIt->orientation() == orientation_type::type::PPN/*2*/ || featureIt->orientation() == orientation_type::type::PNN/*-2*/ ||
                 featureIt->orientation() == orientation_type::type::NPN/*4*/ || featureIt->orientation() == orientation_type::type::PNP/*-4*/))
        {
            aggregateFeatures[/*graph_orientation::orientation::APN)*/1 + (grpIdx * N_ORI_SUBGROUPS)].updateAggregatePhase1(*featureIt, aggregateDataType, meanDispersionValue);
        }

        if(orientationSubgroups[/*graph_orientation::orientation::NPA)*/2] &&
                (featureIt->orientation() == orientation_type::type::NPP/*3*/ || featureIt->orientation() == orientation_type::type::NNP/*-3*/ ||
                 featureIt->orientation() == orientation_type::type::NPN/*4*/ || featureIt->orientation() == orientation_type::type::PNP/*-4*/))
        {
            aggregateFeatures[/*graph_orientation::orientation::NPA)*/2 + (grpIdx * N_ORI_SUBGROUPS)].updateAggregatePhase1(*featureIt, aggregateDataType, meanDispersionValue);
        }

        if(orientationSubgroups[/*graph_orientation::orientation::APP)*/3] &&
                (featureIt->orientation() == orientation_type::type::PPP/*1*/ || featureIt->orientation() == orientation_type::type::NNN/*-1*/ ||
                 featureIt->orientation() == orientation_type::type::NPP/*3*/ || featureIt->orientation() == orientation_type::type::NNP/*-3*/))
        {
            aggregateFeatures[/*graph_orientation::orientation::APP)*/3 + (grpIdx * N_ORI_SUBGROUPS)].updateAggregatePhase1(*featureIt, aggregateDataType, meanDispersionValue);
        }

        if(orientationSubgroups[/*graph_orientation::orientation::PPA)*/4] &&
                (featureIt->orientation() == orientation_type::type::PPP/*1*/ || featureIt->orientation() == orientation_type::type::NNN/*-1*/ ||
                 featureIt->orientation() == orientation_type::type::PPN/*2*/ || featureIt->orientation() == orientation_type::type::PNN/*-2*/))
        {
            aggregateFeatures[/*graph_orientation::orientation::PPA)*/4 + (grpIdx * N_ORI_SUBGROUPS)].updateAggregatePhase1(*featureIt, aggregateDataType, meanDispersionValue);
        }

        switch(featureIt->orientation())
        {
            case orientation_type::type::PPP/*1*/:
            case orientation_type::type::NNN/*-1*/:
                if(orientationSubgroups[/*graph_orientation::orientation::PPP)*/5])
                {
                    aggregateFeatures[/*graph_orientation::orientation::PPP)*/5 + (grpIdx * N_ORI_SUBGROUPS)].updateAggregatePhase1(*featureIt, aggregateDataType, meanDispersionValue);
                }
                break;

            case orientation_type::type::PPN/*2*/:
            case orientation_type::type::PNN/*-2*/:
                if(orientationSubgroups[/*graph_orientation::orientation::PPN)*/6])
                {
                    aggregateFeatures[/*graph_orientation::orientation::PPN)*/6 + (grpIdx * N_ORI_SUBGROUPS)].updateAggregatePhase1(*featureIt, aggregateDataType, meanDispersionValue);
                }
                break;

            case orientation_type::type::NPP/*3*/:
            case orientation_type::type::NNP/*-3*/:
                if(orientationSubgroups[/*graph_orientation::orientation::NPP)*/7])
                {
                    aggregateFeatures[/*graph_orientation::orientation::NPP)*/7 + (grpIdx * N_ORI_SUBGROUPS)].updateAggregatePhase1(*featureIt, aggregateDataType, meanDispersionValue);
                }
                break;

            case orientation_type::type::NPN/*4*/:
            case orientation_type::type::PNP/*-4*/:
                if(orientationSubgroups[/*graph_orientation::orientation::NPN)*/8])
                {
                    aggregateFeatures[/*graph_orientation::orientation::NPN)*/8 + (grpIdx * N_ORI_SUBGROUPS)].updateAggregatePhase1(*featureIt, aggregateDataType, meanDispersionValue);
                }
                break;
            default:
                break;
        }

        // clean up update table in prevision of another dataset
        // only when we do not want to process the missing data.
        // In case we process the missing data, we want to keep
        // the update_table as it is : with all windows.
        if(!includeZeros)
            featureIt->cleanUpdateTable();
    }

    // do phase 2 to compute the median
    if(aggregateDataType == aggregate_data_type::type::MEDIAN)
    {
        for(group_feature<aggregate_reference_feature>::iterator aggFeatureIt = aggregateFeatures.begin(); aggFeatureIt != aggregateFeatures.end(); ++aggFeatureIt)
            aggFeatureIt->updateAggregatePhase2(aggregateDataType);
    }
}

/**
 * Determines the reference feature orientation base on neighbors annotation.
 *
 * A warning is logged when the reference feature orientation could not be determined.
 *
 * @param priorStrand Previous annotation strand.
 * @param feature Current reference feature to update.
 * @param nextStrand Next annotation strand.
 */
void determineFeatureOrientationType(const annotation_strand::strand &priorStrand, reference_feature &feature, const annotation_strand::strand &nextStrand)
{
    switch(feature.strand())
    {
        case annotation_strand::strand::POSITIVE:
            if(priorStrand == annotation_strand::strand::POSITIVE && nextStrand == annotation_strand::strand::POSITIVE)
                feature.setOrientation(orientation_type::type::PPP); // PPP -> 1

            else if(priorStrand == annotation_strand::strand::POSITIVE && nextStrand == annotation_strand::strand::NEGATIVE)
                feature.setOrientation(orientation_type::type::PPN); //PPN -> 2

            else if(priorStrand == annotation_strand::strand::NEGATIVE && nextStrand == annotation_strand::strand::POSITIVE)
                feature.setOrientation(orientation_type::type::NPP); // NPP -> 3

            else if(priorStrand == annotation_strand::strand::NEGATIVE && nextStrand == annotation_strand::strand::NEGATIVE)
                feature.setOrientation(orientation_type::type::NPN); // NPN -> 4

            else
            {
                LOG(recordLevel::WARNING, MSG_UNDEFINED_ANNOTATION_STRAND(feature.name()));
                ASSERT(false, MSG_UNDEFINED_ANNOTATION_STRAND(feature.name()));
            }
            break;

        case annotation_strand::strand::NEGATIVE:
            if(priorStrand == annotation_strand::strand::NEGATIVE && nextStrand == annotation_strand::strand::NEGATIVE)
                feature.setOrientation(orientation_type::type::NNN); // NNN -> -1

            else if(priorStrand == annotation_strand::strand::POSITIVE && nextStrand == annotation_strand::strand::NEGATIVE)
                feature.setOrientation(orientation_type::type::PNN); // PNN -> -2;

            else if(priorStrand == annotation_strand::strand::NEGATIVE && nextStrand == annotation_strand::strand::POSITIVE)
                feature.setOrientation(orientation_type::type::NNP); // NNP -> -3

            else if(priorStrand == annotation_strand::strand::POSITIVE && nextStrand == annotation_strand::strand::POSITIVE)
                feature.setOrientation(orientation_type::type::PNP); // PNP -> -4

            else
            {
                LOG(recordLevel::WARNING, MSG_UNDEFINED_ANNOTATION_STRAND(feature.name()));
                ASSERT(false, MSG_UNDEFINED_ANNOTATION_STRAND(feature.name()));
            }
            break;
        default:
            break;
    }
}

#ifdef __DEBUG__

void printFeatures(const group_feature<reference_feature> &referenceFeatures, const parameters &param, const std::string &datasetName)
{
    std::ofstream out(param.outputDirectory + (param.prefixFilename.empty() ? "" : param.prefixFilename + "_") + "dbg_dump_" + datasetName + ".txt", std::ios::out | std::ios::trunc);

    if(out.is_open())
        out << std::endl << "Features : " << std::endl << referenceFeatures;
    //    for(set_feature::const_iterator i = referenceFeatures.begin(); i != referenceFeatures.end(); ++i, idx++)
    //        out << idx << "-" << *i << std::endl;

    out.close();
}

void printGenes(const group_feature<reference_feature> &referenceFeatures, const parameters &param, const std::string &datasetName)
{
    std::ofstream outGenes(param.outputDirectory + (param.prefixFilename.empty() ? "" : param.prefixFilename + "_") + "dbg_genes" + datasetName + ".txt", std::ios::out | std::ios::trunc);
    std::vector< std::pair<int, std::string> > genes;

    uint_t totalNumberOfWindows = (output_builder::lastRelativeWindow(param) + (output_builder::firstRelativeWindow(param) *-1)) / param.windowSize;

    genes.clear();
    genes.resize(totalNumberOfWindows, std::make_pair(0, ""));

    int relativeWindow = output_builder::firstRelativeWindow(param);
    for(uint_t windowIdx = 0; windowIdx < totalNumberOfWindows; windowIdx++, relativeWindow += param.windowSize)
        genes[windowIdx].first = relativeWindow;

    int w = 0;
    for(uint_t g = 0; g < referenceFeatures.size(); g++)
    {
        w = 0;
        // for all blocks
        for(uint_t i = 0; i < referenceFeatures[g].blocks().size(); i++)
        {
            //                if(referenceFeatures[g].type() == annotation_type::NPP){
            // for all windows of block
            for(uint_t l = 0; l < referenceFeatures[g].blocks()[i].windows().size(); l++)
            {
                // if value of current gene is not BAD_NUM
                if(!referenceFeatures[g].blocks()[i].windows()[l].isNotPopulated())
                    genes[w + (referenceFeatures[g].blocks()[i].windows()[l]).index()].second += referenceFeatures[g].name() + ',';
            }

            w += param.windowsPerBlock[i];
        }
    }

    for(uint_t windowIdx = 0; windowIdx < totalNumberOfWindows; windowIdx++)
        outGenes << genes[windowIdx].first << "\t" << genes[windowIdx].second << std::endl;
}
#endif
