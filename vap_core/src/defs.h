/**
 * @file   defs.h
 * @author Charles Coulombe
 *
 * @date 4 October 2012, 23:36
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEFS_H
#define	DEFS_H

#include <string>
#include <limits>

namespace vap
{
// -----------------------------------------------------------------------------
//                            TYPES
// -----------------------------------------------------------------------------

/** Defines an <c>unsigned int</c> */
typedef unsigned int uint_t;

// -----------------------------------------------------------------------------
//                            CONSTANTS
// -----------------------------------------------------------------------------

/** Uninitialized mean */
#define BAD_NUM -999.0f

/** Process all data */
#define PROCESS_ALL_DATA -1

/** Value is not available */
#define NOT_AVAILABLE "NA"

/** Window is invalid */
#define INVALID_WINDOW "#INV"

/** Dataset "browser" tag */
#define BROWSER_TAG "browser"

/** Dataset "Comment" tag */
#define COMMENT_TAG '#'

/** Tabulation character */
#define TABULATION '\t'

/** Character to replace empty string character */
#define EMPTY_STRING_CHAR '_'

/** Dataset "Track" tag */
#define TRACK_TAG "track"

/** Parameters multiple value delimiter */
#define MULTIPLE_VALUE_DELIMITER ";"

/** Graph files prefix */
#define GRAPH_PREFIX "graph"

/** Heatmap files prefix */
#define HEATMAP_PREFIX "heatmap"

/** Aggregate files prefix */
#define OUT_AGG_PREFIX_FILENAME "agg_data"

/** Individual features file prefix*/
#define OUT_IND_PREFIX_FILENAME "ind_data"

/** not a position */
#define NPOS std::numeric_limits<int>::min()

/** Defines application name */
#define APPNAME "vap_core"

/** Defines application version */
#define APPVERSION "1.2.0"

/** Defines file 'name' property */
#define FILE_PROPERTY_NAME "name"

/** Defines file 'type' property */
#define FILE_PROPERTY_TYPE "type"

/** Defines file 'desc' property */
#define FILE_PROPERTY_DESC "desc"

/** Defines a coordinates file type */
#define FILE_PROPERTY_TYPE_COORD "coord"

/** Number of orientation subgroups */
#define N_ORI_SUBGROUPS 9

/** Reference feature gap threshold. */
#define RF_GAP_THRESHOLD 1

/** Default columns delimiters. */
const std::string DEFAULT_DELIMITERS = " \t";

// -----------------------------------------------------------------------------
//                            ENUMARATIONS
// -----------------------------------------------------------------------------

/** Orientation subgroups contained and displayed on a graph */
struct graph_orientation
{

    /** Graphs orientations. */
    enum class orientation
    {
        /**
         * Numerical value : 0
         * Internal value : Any-Positive-Any
         * Outputted value : Any-Any
         */
        APA = 0,
        /**
         * Numerical value : 1
         * Internal value : Any-Positive-Negative
         * Outputted value : Any-Convergent
         */
        APN,
        /**
         * Numerical value : 2
         * Internal value : Negative-Positive-Any
         * Outputted value : Divergent-Any
         */
        NPA,
        /**
         * Numerical value : 3
         * Internal value : Any-Positive-Positive
         * Outputted value : Any-Tandem
         */
        APP,
        /**
         * Numerical value : 4
         * Internal value : Positive-Positive-Any
         * Outputted value : Tandem-Any
         */
        PPA,
        /**
         * Numerical value : 5
         * Internal value : Positive-Positive-Positive
         * Outputted value : Tandem-Tandem
         */
        PPP,
        /**
         * Numerical value : 6
         * Internal value : Positive-Positive-Negative
         * Outputted value : Tandem-Convergent
         */
        PPN,
        /**
         * Numerical value : 7
         * Internal value : Negative-Positive-Positive
         * Outputted value : Divergent-Tandem
         */
        NPP,
        /**
         * Numerical value : 8
         * Internal value : Negative-Positive-Negative
         * Outputted value : Divergent-Convergent
         */
        NPN
    };

    /**
     * Gets the @c std::string representation of the enum value.
     *
     * @param value orientation subgroup value
     * @return orientation subgroup as a string
     */
    static std::string toString(orientation value)
    {
        switch(value)
        {
            case orientation::APA:
            {
                /**Any-Any*/
                return "AA";
            }
            case orientation::APN:
            {
                /**Any-Convergent*/
                return "AC";
            }
            case orientation::PPN:
            {
                /**Tandem-Convergent*/
                return "TC";
            }
            case orientation::PPP:
            {
                /**Tandem-Tandem*/
                return "TT";
            }
            case orientation::NPN:
            {
                /**Divergent-Convergent*/
                return "DC";
            }
            case orientation::NPA:
            {
                /**Divergent-Any*/
                return "DA";
            }
            case orientation::NPP:
            {
                /**Divergent-Tandem*/
                return "DT";
            }
            case orientation::APP:
            {
                /**Any-Tandem*/
                return "AT";
            }
            case orientation::PPA:
            {
                /**Tandem-Any*/
                return "TA";
            }
            default:
            {
                return "Unknown";
            }
        }
    }
};

/**
 * Reference feature orientation type.
 *
 * @warning This should not be used as an index in enumerable values.
 */
struct orientation_type
{

    /** Reference feature type. */
    enum class type
    {
        /**
         * Numerical value : 1
         * Internal value : Positive-Positive-Positive
         * Outputted value : Tandem-Tandem
         */
        PPP = 1,
        /**
         * Numerical value : -1
         * Internal value : Negative-Negative-Negative
         * Outputted value : Tandem-Tandem
         */
        NNN = -1,
        /**
         * Numerical value : 2
         * Internal value : Positive-Positive-Negative
         * Outputted value : Tandem-Convergent
         */
        PPN = 2,
        /**
         * Numerical value : -2
         * Internal value : Positive-Negative-Negative
         * Outputted value : Tandem-Convergent
         */
        PNN = -2,
        /**
         * Numerical value : 3
         * Internal value : Negative-Positive-Positive
         * Outputted value : Divergent-Tandem
         */
        NPP = 3,
        /**
         * Numerical value : -3
         * Internal value : Negative-Negative-Positive
         * Outputted value : Divergent-Tandem
         */
        NNP = -3,
        /**
         * Numerical value : 4
         * Internal value : Negative-Positive-Negative
         * Outputted value : Divergent-Convergent
         */
        NPN = 4,
        /**
         * Numerical value : -4
         * Internal value : Positive-Negative-Positive
         * Outputted value : Divergent-Convergent
         */
        PNP = -4
    };

    /**
     * Gets a @c std::string representation of enumerable value
     *
     * @param value enum value
     * @return a string of enum value
     */
    static std::string toString(type value)
    {
        switch(value)
        {
            case type::PPP:
            {
                /**Tandem-Tandem*/
                return "TT";
            }

            case type::PPN:
            {
                /**Tandem-Convergent*/
                return "TC";
            }

            case type::NNN:
            {
                /**Tandem-Tandem*/
                return "TT";
            }

            case type::PNN:
            {
                /**Tandem-Convergent*/
                return "TC";
            }

            case type::NPP:
            {
                /**Divergent-Tandem*/
                return "DT";
            }

            case type::NNP:
            {
                /**Divergent-Tandem*/
                return "DT";
            }

            case type::NPN:
            {
                /**Divergent-Convergent*/
                return "DC";
            }

            case type::PNP:
            {
                /**Divergent-Convergent*/
                return "DC";
            }

            default:
            {
                return "Unknown";
            }
        }
    }
};

/** Reference feature strand */
struct annotation_strand
{

    /** Reference feature strand. */
    enum class strand
    {
        /** Positive sense : 0 */
        POSITIVE = 0,
        /** Negative sense : 1 */
        NEGATIVE
    };

    /**
     * Gets a @c std::string representation of enumerable value
     *
     * @param value enum value
     * @return a string of enum value
     */
    static std::string toString(strand value)
    {
        switch(value)
        {
            case strand::POSITIVE:
            {
                return "Positive";
            }

            case strand::NEGATIVE:
            {
                return "Negative";
            }

            default:
            {
                return "Unknown";
            }
        }
    }

    /**
     * Gets the literal symbol for the strand.
     *
     * @param value strand value
     * @return strand symbol
     */
    static char toSymbol(strand value)
    {
        switch(value)
        {
            case strand::POSITIVE:
            {
                return '+';
            }

            case strand::NEGATIVE:
            {
                return '-';
            }

            default:
            {
                return '.';
            }
        }
    }

    /**
     * Gets a @c strand representation of the value.
     * @note default value is @c WATSON
     *
     * @param value strand symbol
     * @return @c strand value
     */
    static strand toStrand(char value)
    {
        switch(value)
        {
            case '-':
                return strand::NEGATIVE;

            case '+':
            default:
                return strand::POSITIVE;
        }
    }
};

/**
 * This parameter is used with the parameters file.
 *
 * This parameter indicates the alignment desired for each block individually.
 * A gap is introduced at the split point inside a block or between consecutive
 * blocks representing regions in the genome not necessarily contiguous.
 *
 * @note
 * This parameter is not used in the @c analysis_method relative representation mode.
 * There are @c reference_points +1 values, each value must be delimited by a semi-colon (;).
 *
 * @remark @c UNKNOWN value represents an error or unknown value, thus this value
 * is only used for validation purpose.
 */
struct block_alignment
{

    /** Block split alignment. */
    enum class alignment
    {
        /** Unknown or error.. */
        UNKNOWN = -1,
        /** Block's content is aligned to the left. */
        LEFT,
        /** Block's content is aligned to the right. */
        RIGHT,
        /** Block's content is split and aligned at both boundaries based on the
         * @c block_split_type, @c block_split_value and @c block_split_alignment
         * parameters. */
        SPLIT
    };

    /**
     * Gets a @c std::string representation of enumerable value
     *
     * @param value enum value
     * @return a string of enum value
     */
    static std::string toString(alignment value)
    {
        switch(value)
        {
            case alignment::LEFT:
                return "Left";

            case alignment::RIGHT:
                return "Right";

            case alignment::SPLIT:
                return "Split";

            case alignment::UNKNOWN:
            default:
                return "Unknown";
        }
    }

    /**
     * Gets a @c type representation of the value.
     * @note Default is @c UNKNOWN
     *
     * @param value raw character
     * @return @c alignment of value
     */
    static alignment toType(char value)
    {
        switch(value)
        {
            case 'S':
                return alignment::SPLIT;

            case 'R':
                return alignment::RIGHT;

            case 'L':
                return alignment::LEFT;

            default:
                return alignment::UNKNOWN;
        }
    }
};

/**
 * Parameter : @c analysis_mode definition.
 *
 * This parameter indicates which type of reference features to use.
 */
struct analysis_mode
{

    /** Types of analysis mode. */
    enum class type
    {
        /** Unknown or error. */
        UNKNOWN = -1,

        /**
         * The reference features contained in the reference group(s)
         * are region coordinates directly containing the @c reference_points to
         * use. The first two columns of this tab-delimited file are the
         * chromosome and strand, followed by the required number of
         * @c reference points, and optionally followed by a region name or
         * description.
         */
        COORDINATE,

        /**
         * The reference features contained in the reference group(s) are
         * annotation name (unique identifier) from a provided annotations file,
         * and the @c reference_points are derived from the exon boundaries.
         */
        EXON,

        /**
         * The reference features contained in the reference group(s) are
         * annotation name (unique identifier) from a provided annotations file,
         * and the @c reference_points are derived from the annotation boundaries.
         */
        ANNOTATION
    };

    /**
     * Gets a @c std::string representation of enumerable value
     *
     * @param value enum value
     * @return a string of enum value
     */
    static std::string toString(type value)
    {
        switch(value)
        {
            case type::COORDINATE:
                return "Coordinates";

            case type::EXON:
                return "Exon";

            case type::ANNOTATION:
                return "Annotations";

            case type::UNKNOWN:
            default:
                return "Unknown";
        }

    }

    /**
     * Gets a @c type representation of the value
     * @note default value is @c UNKNOWN
     *
     * @param value raw value
     * @return @c type of value
     */
    static type toType(char value)
    {
        switch(value)
        {
            case 'C':
                return type::COORDINATE;

            case 'E':
                return type::EXON;

            case 'A':
                return type::ANNOTATION;

            default:
                return type::UNKNOWN;
        }
    }

    /**
     * Gets a @c char representation of enumerable value.
     *
     * @param value enum value
     * @return a char of enum value
     */
    static char toChar(type value)
    {
        switch(value)
        {
            case type::COORDINATE:
                return 'C';

            case type::EXON:
                return 'E';

            case type::ANNOTATION:
                return 'A';

            case type::UNKNOWN:
            default:
                return '\0';
        }
    }
};

/**
 * Parameter : @c annotation_coordinates_type definition.
 *
 * This parameter indicates to use either the transcription (tx) or coding (cds)
 * coordinate columns (start and stop) from the annotations file when reference
 * features are annotations.
 *
 * @remark @c UNKNOWN value represents an error or unknown value, thus this value
 * is only used for validation purpose.
 */
struct annotation_coordinates_type
{

    /** Types of coordinates. */
    enum class type
    {
        /** Unknown or error. */
        UNKNOWN = -1,

        /** Corresponds to the @e cdsStart and @e cdsStop. (CDS) */
        CODING_SEQUENCE,

        /** Corresponds to the @e txStart and @e txStop. (TX) */
        TRANSCRIPTION,

        /**
         * Not Applicable to this parameter. In other words, when reference
         * features are different than annotations.
         */
        NOT_APPLICABLE
    };

    /**
     * Gets a @c std::string representation of enumerable value
     *
     * @param value enum value
     * @return a string of enum value
     */
    static std::string toString(type value)
    {
        switch(value)
        {
            case type::CODING_SEQUENCE:
                return "Coding sequence";

            case type::TRANSCRIPTION:
                return "Transcription";

            case type::NOT_APPLICABLE:
                return "Not applicable";

            case type::UNKNOWN:
            default:
                return "Unknown";
        }
    }

    /**
     * Gets a @c type representation of the value.
     * @note default case is @c UNKNOWN
     *
     * @param value raw value
     * @return @c type of value
     */
    static type toType(char value)
    {
        switch(value)
        {
            case 'C':
                return type::CODING_SEQUENCE;

            case 'T':
                return type::TRANSCRIPTION;

            case 'N':
                return type::NOT_APPLICABLE;

            default:
                return type::UNKNOWN;
        }
    }
};

/**
 * Parameter : @c analysis_method definition.
 *
 * This parameter indicates how to process the data by segmenting each block
 * (representing the feature or inter feature regions) either by windows of
 * constant size (absolute mode) (meaning that longer feature will contains more
 * windows), or in a constant number of windows (relative mode) (meaning that
 * longer feature will contains longer windows).
 *
 * @remark @c UNKNOWN value represents an error or unknown value, thus this value
 * is only used for validation purpose.
 */
struct analysis_method
{

    /** Type of representations. */
    enum class type
    {
        /** Unknown or error. */
        UNKNOWN = -1,

        /** Each feature (e.g. gene) is divided in windows of constant size. */
        ABSOLUTE,

        /** Each feature (e.g. gene) is divided in a constant number of windows. */
        RELATIVE
    };

    /**
     * Gets a @c std::string representation of enumerable value
     *
     * @param value enum value
     * @return a string of enum value
     */
    static std::string toString(type value)
    {
        switch(value)
        {
            case type::ABSOLUTE:
                return "Absolute";

            case type::RELATIVE:
                return "Relative";

            case type::UNKNOWN:
            default:
                return "Unknown";
        }

    }

    /**
     * Gets a @c type representation of the value
     * @note default case is @c UNKNOWN
     *
     * @param value raw value
     * @return @c type of value
     */
    static type toType(char value)
    {
        switch(value)
        {
            case 'A':
                return type::ABSOLUTE;

            case 'R':
                return type::RELATIVE;

            default:
                return type::UNKNOWN;
        }
    }

};

/**
 * Parameter : @c block_split_type definition.
 *
 * This parameter indicates the type of split desired for each relevant block
 * individually.
 *
 * @note This parameter is not used in the relative representation mode.
 *
 * There are @c reference_points + 1 values, each value must be delimited by a
 * semi-colon (;).
 *
 * @remark @c UNKNOWN value represents an error or unknown value, thus this value
 * is only used for validation purpose.
 */
struct block_split_type
{

    /**
     *  Types of block split. */
    enum class type
    {
        /** Unknown or error. */
        UNKNOWN = -1,

        /**
         * Indicates that the @c block_split_value represent an absolute number
         * of windows before the split of the block.
         */
        ABSOLUTE,

        /**
         * Indicates that the @c block_split_value represent a percentage of the
         * feature before the split of the block.
         */
        PERCENTAGE,

        /**
         * Not applicable for this parameter. In other words, when
         * @c block_alignment is not split.
         */
        NOT_APPLICABLE
    };

    /**
     * Gets a @c std::string representation of enumerable value
     *
     * @param value enum value
     * @return a string of enum value
     */
    static std::string toString(type value)
    {
        switch(value)
        {
            case type::ABSOLUTE:
                return "Absolute";

            case type::PERCENTAGE:
                return "Percentage";

            case type::NOT_APPLICABLE:
                return "Not applicable";

            case type::UNKNOWN:
            default:
                return "Unknown";
        }
    }

    /**
     * Gets a @c type representation of the value
     * @note default case is @c UNKOWN
     *
     * @param value raw value
     * @return @c type of value
     */
    static type toType(char value)
    {
        switch(value)
        {
            case 'A':
                return type::ABSOLUTE;

            case 'P':
                return type::PERCENTAGE;

            case 'N':
                return type::NOT_APPLICABLE;

            default:
                return type::UNKNOWN;
        }
    }

};

/**
 * Parameter : @c block_split_alignment definition.
 *
 * This parameter indicates on which side of the block (left or right) the number
 * of windows before the split should be aligned.
 *
 * @note This parameter is not used in the relative representation mode.
 *
 * There are @c reference_points + 1 values, each value must be delimited by a
 * semi-colon (;).
 *
 * @remark @c UNKNOWN value represents an error or unknown value, thus this value
 * is only used for validation purpose.
 */
struct block_split_alignment
{

    /** Type of alignments. */
    enum class type
    {
        /** Unknown or error. */
        UNKNOWN = -1,

        /**
         * The desired number of windows is aligned to the left side of the block,
         * the remaining number of windows being aligned to the right side.
         */
        LEFT,

        /**
         * The desired number of windows is aligned to the right side of the block,
         * the remaining number of windows being aligned to the left side.
         */
        RIGHT,

        /**
         * Not applicable for this parameter. In other words, when @c
         * block_alignment is not split.
         */
        NOT_APPLICABLE
    };

    /**
     * Gets a @c std::string representation of enumerable value
     *
     * @param value enum value
     * @return a string of enum value
     */
    static std::string toString(type value)
    {
        switch(value)
        {
            case type::LEFT:
                return "Left";

            case type::RIGHT:
                return "Right";

            case type::NOT_APPLICABLE:
                return "Not applicable";

            case type::UNKNOWN:
            default:
                return "Unknown";
        }
    }

    /**
     * Gets a @c type representation of the value.
     * @note default case is @c UNKNOWN
     *
     * @param value raw value
     * @return @c type of value
     */
    static type toType(char value)
    {
        switch(value)
        {
            case 'R':
                return type::RIGHT;

            case 'L':
                return type::LEFT;

            case 'N':
                return type::NOT_APPLICABLE;

            default:
                return type::UNKNOWN;
        }
    }
};

/**
 * Parameter : @c merge_mid_introns definition.
 *
 * This parameter indicates to merge (or not) the signal of the middle introns
 * to one of the existing intron block.
 *
 * @note This parameter is therefore only used in exon mode.
 *
 * @remark @c UNKNOWN value represents an error or unknown value, thus this value
 * is only used for validation purpose.
 */
struct merge_mid_introns
{

    /** Type of merge. */
    enum class type
    {
        /** Unknown or error. */
        UNKNOWN = -1,

        /** Ignore the signal of the middle introns. */
        NONE,

        /** Merge the signal of the middle introns with the signal of the first intron. */
        FIRST,

        /** Merge the signal of the middle introns with the signal of the last intron. */
        LAST
    };

    /**
     * Gets a @c std::string representation of enumerable value
     *
     * @param value enum value
     * @return a string of enum value
     */
    static std::string toString(type value)
    {
        switch(value)
        {
            case type::NONE:
                return "None";

            case type::FIRST:
                return "First";

            case type::LAST:
                return "Last";

            case type::UNKNOWN:
            default:
                return "Unknown";
        }
    }

    /**
     * Gets a @c type representation of the value
     * @note default value is @c UNKNOWN
     *
     * @param value raw value
     * @return @c type of value
     */
    static type toType(char value)
    {
        switch(value)
        {
            case 'F':
                return type::FIRST;

            case 'L':
                return type::LAST;

            case 'N':
                return type::NONE;

            default:
                return type::UNKNOWN;
        }
    }
};

/**
 * Parameter : @c aggregate_data_type definition.
 *
 * This parameter indicates to report in the aggregate output files either the
 * mean, the median, the max or the min of the data values across corresponding
 * windows of the reference features of a given group.
 *
 * @remark @c UNKNOWN value represents an error or unknown value, thus this value
 * is only used for validation purpose.
 */
struct aggregate_data_type
{

    /** Type of aggregate. */
    enum class type
    {
        /** Unknown or error. */
        UNKNOWN = -1,

        /**
         * The aggregate value reported is the mean of the values across
         * corresponding windows.
         */
        MEAN,

        /**
         * The aggregate value reported is the median of the values across
         * corresponding windows.
         */
        MEDIAN,

        /**
         * The aggregate value reported is the max of the values across
         * corresponding windows.
         */
        MAX,

        /**
         * The aggregate value reported is the min of the values across
         * corresponding windows.
         */
        MIN
    };

    /**
     * Gets a @c std::string representation of enumerable value
     *
     * @param value enum value
     * @return a string of enum value
     */
    static std::string toString(type value)
    {
        switch(value)
        {
            case type::MEAN:
                return "Mean";

            case type::MEDIAN:
                return "Median";

            case type::MAX:
                return "Max";

            case type::MIN:
                return "Min";

            case type::UNKNOWN:
            default:
                return "Unknown";
        }
    }

    /**
     * Gets a @c type representation of the value
     * @note default value is @c UNKNOWN
     *
     * @param value raw value
     * @return @c type of value
     */
    static type toType(char value)
    {
        switch(value)
        {
            case 'E':
                return type::MEAN;

            case 'D':
                return type::MEDIAN;

            case 'A':
                return type::MAX;

            case 'I':
                return type::MIN;

            default:
                return type::UNKNOWN;
        }
    }
};

/**
 * Parameter : @c mean_dispersion_value definition.
 *
 * This parameter indicates to report the dispersion of the aggregate data in
 * each window, either as standard error of the mean (SEM) or as standard
 * deviation (SD).
 *
 * @remark @c UNKNOWN value represents an error or unknown value, thus this value
 * is only used for validation purpose.
 */
struct mean_dispersion_value
{

    /** Type of dispersion. */
    enum class type
    {
        /** Unknown or error. */
        UNKNOWN = -1,

        /**
         * The aggregate value reported is the mean of the values across
         * corresponding windows.
         */
        SEM,

        /**
         * The aggregate value reported is the median of the values across
         * corresponding windows.
         */
        SD

        /**
         * NOT YET IMPLEMENTED
         *
         * The aggregate value reported is the confidence interval of the values
         * across corresponding windows.
         */
        //        CI
    };

    /**
     * Gets a @c std::string representation of enumerable value
     *
     * @param value enum value
     * @return a string of enum value
     */
    static std::string toString(type value)
    {
        switch(value)
        {
            case type::SEM:
                return "SEM";

            case type::SD:
                return "SD";

                // NOT YET IMPLEMENTED
                //            case type::CI:
                //                return "CI";

            case type::UNKNOWN:
            default:
                return "Unknown";
        }
    }

    /**
     * Gets a @c type representation of the value
     * @note default value is @c UNKNOWN
     *
     * @param value raw value
     * @return @c type of value
     */
    static type toType(char value)
    {
        switch(value)
        {
            case 'E':
                return type::SEM;

            case 'D':
                return type::SD;

                // NOT YET IMPLEMENTED
                //            case 'I':
                //                return type::CI;

            default:
                return type::UNKNOWN;
        }
    }
};

/**
 * Parameter : @c 1pt_boundary definition.
 *
 * This parameter indicates which boundary of the feature (5' or 3') to use when
 * @c reference_points = 1 and @c analysis_mode is annotation.
 *
 * @remark @c UNKNOWN value represents an error or unknown value, thus this value
 * is only used for validation purpose.
 */
struct oneRefPt_boundary
{

    /** Type of boundary. */
    enum class type
    {
        /** Unknown or error. */
        UNKNOWN = -1,

        /** 3' boundary. */
        THREE_PRIME,

        /** 5' boundary. */
        FIVE_PRIME,

        /**
         * Not applicable for this parameter. In other words, when
         * @c reference_point is greater then one.
         */
        NOT_APPLICABLE
    };

    /**
     * Gets a @c std::string representation of enumerable value
     *
     * @param value enum value
     * @return a string of enum value
     */
    static std::string toString(type value)
    {
        switch(value)
        {
            case type::NOT_APPLICABLE:
                return "Not applicable";

            case type::THREE_PRIME:
                return "3 prime";

            case type::FIVE_PRIME:
                return "5 prime";

            case type::UNKNOWN:
            default:
                return "Unknown";
        }
    }

    /**
     * Gets a @c type representation of the value
     * @note default value is @c UNKNOWN
     *
     * @param value raw value
     * @return @c type of value
     */
    static type toType(char value)
    {
        switch(value)
        {
            case '3':
                return type::THREE_PRIME;

            case '5':
                return type::FIVE_PRIME;

            case 'N':
                return type::NOT_APPLICABLE;

            default:
                return type::UNKNOWN;
        }
    }
};
} /* vap namespace end */
#endif	/* DEFS_H */
