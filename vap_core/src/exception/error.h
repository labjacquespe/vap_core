/**
 * @file   error.h
 * @author Charles Coulombe
 *
 * @date 1 mars 2013, 15:39
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ERROR_H
#define	ERROR_H

#include "../messages/messages.h"

namespace vap
{
/**
 * Error barrier.
 * Halt program and produce an error message that will be logged and print.
 */
#define ERROR(condition, message) \
        do{ if(! (condition)) \
        { \
            PRINT_AND_LOG_ERROR(message); \
            PRINT(MSG_EXITNOW); \
            logger::instance().tail(); \
            logger::instance().close(); \
            exit(EXIT_FAILURE); \
        };}while(false)
} /* vap namespace end */
#endif	/* ERROR_H */

