/**
 * @file   parser_parameters_h.h
 * @author Charles Coulombe
 *
 * @date 1 March 2016, 20:12
 * @version 1.2.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PARSER_PARAMETERS_H
#define PARSER_PARAMETERS_H

#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>
#include <sstream>
#include <iostream>
#include <numeric>
#include <fstream>
#include <iterator>
#include <vector>
#include <limits>
#include "../utils/memory/parser_string.h"
#include "../utils/logging/logger.h"
#include "../utils/file/file_extension.h"
#include "../utils/file/file_format.h"
#include "../utils/file/file_utils.h"
#include "../utils/vap_apt_translator.h"
#include "../utils/text/string_utils.h"
#include "../messages/messages.h"
#include "../defs.h"
#include "parameters.h"

namespace vap
{

#define PARAMETER_TAG "~~@"
#define ROUND_TAG ":=:"

#define ANALYSIS_MODE "analysis_mode"
#define DATASET_PATH "dataset_path"
#define REFGROUP_PATH "refgroup_path"
#define ANALYSIS_METHOD "analysis_method"
#define PROCESS_DATA_BY_CHUNK "process_data_by_chunk"
#define DATASET_CHUNKSIZE "dataset_chunk_size"
#define ANNOTATIONS_PATH "annotations_path"
#define SELECTION_PATH "selection_path"
#define EXCLUSION_PATH "exclusion_path"
#define ANNOTATION_COORDINATES_TYPE "annotation_coordinates_type"
#define REFERENCE_POINTS "reference_points"
#define ONEREFPT_BOUNDARY "1pt_boundary"
#define WINDOW_SIZE "window_size"
#define WINDOWS_PER_BLOCK "windows_per_block"
#define BLOCK_ALIGNMENT "block_alignment"
#define BLOCK_SPLIT_TYPE "block_split_type"
#define BLOCK_SPLIT_ALIGNMENT "block_split_alignment"
#define BLOCK_SPLIT_VALUE "block_split_value"
#define SMOOTHING_WINDOWS "smoothing_windows"
#define OUTPUT_DIRECTORY "output_directory"
#define PREFIX_FILENAME "prefix_filename"
#define AGGREGATE_DATA_TYPE "aggregate_data_type"
#define ORIENTATION_SUBGROUPS "orientation_subgroups"
#define WRITE_INDIVIDUAL_REFERENCES "write_individual_references"
#define GENERATE_HEATMAPS "generate_heatmaps"
#define GENERATE_AGGREGATE_GRAPHS "generate_aggregate_graphs"
#define ONE_GRAPH_PER_GROUP "one_graph_per_group"
#define ONE_GRAPH_PER_DATASET "one_graph_per_dataset"
#define ONE_GRAPH_PER_ORIENTATION "one_graph_per_orientation"
#define MEAN_DISPERSION_VALUE "mean_dispersion_value"
#define MERGE_MID_INTRONS "merge_mid_introns"
#define PROCESS_MISSING_DATA "process_missing_data"
#define DISPLAY_DISPERSION_VALUES "display_dispersion_values"
#define Y_AXIS_SCALE "Y_axis_scale"

// A helper function to simplify the main part.
template<class T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &v)
{
    std::copy(v.begin(), v.end(), std::ostream_iterator<T>(os, " "));
    return os;
}

class parser_parameters
{
private:
    boost::program_options::variables_map _vm;
    boost::program_options::options_description _visible;
    boost::program_options::options_description _cmd_options;
    boost::program_options::options_description _cfg_options;
    boost::program_options::positional_options_description _pos_options;

    // Workaround: Boost PO has a bug that does not display option value, so do it for the library
    template <class T>
    std::string invalid_option_value_str(const std::string &option_name, const T &option_value, const std::string &context)
    {
        std::string msg = "the argument '"+boost::lexical_cast<std::string>(option_value)+"' for the option '"+option_name+"' is invalid";

        if(context.empty())
            return msg;
        else
            return msg.append(" (").append(context).append(")");
    }

    std::string required_option_missing_str(const std::string &option_name)
    {
        return "the option '"+option_name+"' is required but missing";
    }

public:

    parser_parameters()
    : _vm(boost::program_options::variables_map()), _visible(boost::program_options::options_description(81)), _cmd_options(boost::program_options::options_description()), _cfg_options(boost::program_options::options_description()), _pos_options(boost::program_options::positional_options_description())
    {
        namespace po = boost::program_options;

        // Declare a group of options that will be allowed only on the command line.
        po::options_description generic_desc("Generic options", 81);
        generic_desc.add_options()
        ("help,h", "show help message")
        ("version,v", "show version")
        ("show-extensions", "Show all supported extensions.")
        ("show-formats", "Show all supported formats.")
        ("parameters,p", po::value<std::string>()->default_value("-"), "set parameters file")
        ;

        // Declare a group of options that will be allowed both on command line and in
        // the parameters file
        po::options_description params_desc("Configuration", 81);
        params_desc.add_options()

        // IMPORTANT: In order to have more flexibility over validation,
        // options type MUST be string or bool (for on/off options).
        (ANALYSIS_MODE, po::value<std::string>()->required(), "type of reference features; where arg is [(A)nnotation,(E)xon,(C)oordinate]")
        (DATASET_PATH, po::value< std::vector<std::string> >()->multitoken()->required(), "path(s) of dataset file(s)")
        (REFGROUP_PATH, po::value< std::vector<std::string> >()->multitoken()->required(), "path(s) of reference group file(s)")
        (ANNOTATIONS_PATH, po::value<std::string>(), "path of the annotations file")
        (SELECTION_PATH, po::value<std::string>(), "path of the file containing a list of annotations to select (positive filter)")
        (EXCLUSION_PATH, po::value<std::string>(), "path of the file containing a list of annotations to exclude (negative filter)")
        (ANALYSIS_METHOD, po::value<std::string>()->default_value("A")->required(), "how to process the data by segmenting each block; where arg is [(A)bsolute,(R)elative]")
        (ANNOTATION_COORDINATES_TYPE, po::value<std::string>()->default_value("T")->required(), "use either the transcription (tx) or coding (cds) coordinate columns (start and end); where arg is [(T)ranscription, (C)oding sequence, (N)ot applicable]")
        (REFERENCE_POINTS, po::value<std::string>()->default_value("2")->required(), "number of reference points; where arg is between 1 and 6")
        (ONEREFPT_BOUNDARY, po::value<std::string>()->default_value("N"), "annotation boundary (5' or 3') to use with 1 reference points in annotation mode; where arg is [5,3,(N)ot applicable]")
        (WINDOW_SIZE, po::value<std::string>()->default_value("50")->required(), "size of the window used to segment each feature")
        (WINDOWS_PER_BLOCK, po::value<std::string>()->default_value("50;150;50")->required(), "number of windows to use in the representation of each block; where arg is integer")
        (BLOCK_ALIGNMENT, po::value<std::string>()->default_value("R;S;L")->required(), "alignment desired for each block; where arg is [(L)eft,(R)ight,(S)plit]")
        (BLOCK_SPLIT_TYPE, po::value<std::string>()->default_value("N;P;N")->required(), "type of split desired for each relevant block; where arg is [(A)bsolute,(P)ercentage,(N)ot applicable]")
        (BLOCK_SPLIT_VALUE, po::value<std::string>()->default_value("100;50;100")->required(), "value (absolute number of windows or percentage of the feature length) desired for each relevant block; where arg is integer")
        (BLOCK_SPLIT_ALIGNMENT, po::value<std::string>()->default_value("N;L;N")->required(), "side of the block (left or rigth) on which the number of windows before the split should be aligned; where arg is [(L)eft,(R)ight,(N)ot applicable]")
        (MERGE_MID_INTRONS, po::value<std::string>()->default_value("N"), "merge (or not) the signal of the middle introns to one of the existing intron block; where arg is [(F)irst,(L)ast,(N)one]")
        (AGGREGATE_DATA_TYPE, po::value<std::string>()->default_value("E"), "aggregate value to report in the aggregate output files; where arg is [m(E)an,me(D)ian,m(A)x,m(I)n]")
        (SMOOTHING_WINDOWS, po::value<std::string>()->default_value("6"), "number of consecutive windows to average in order to smooth the data")
        (MEAN_DISPERSION_VALUE, po::value<std::string>()->default_value("E"), "the dispersion value of the aggregate data in each window; where arg is [S(E)M,S(D)]")
        (PROCESS_MISSING_DATA, po::value<bool>()->default_value(true), "process missing data")
        (PROCESS_DATA_BY_CHUNK, po::value<bool>()->default_value(false), "process data by chunk")
        (DATASET_CHUNKSIZE, po::value<std::string>()->default_value("-1"), "dataset chunk size")
        (OUTPUT_DIRECTORY, po::value<std::string>()->required(), "path of the output directory")
        (PREFIX_FILENAME, po::value<std::string>(), "prefix to generated files")
        (WRITE_INDIVIDUAL_REFERENCES, po::value<bool>()->default_value(false), "write individual references features")
        (GENERATE_HEATMAPS, po::value<bool>()->default_value(false), "generate heatmaps")
        (GENERATE_AGGREGATE_GRAPHS, po::value<bool>()->default_value(true), "generate aggregate graphs")
        (ONE_GRAPH_PER_DATASET, po::value<bool>()->default_value(true), "combine all the datasets in one graph")
        (ONE_GRAPH_PER_GROUP, po::value<bool>()->default_value(false), "combine all the group of reference features in one graph")
        (ONE_GRAPH_PER_ORIENTATION, po::value<bool>()->default_value(false), "combine all the selected orientation subgroups in one graph")
        (ORIENTATION_SUBGROUPS, po::value<std::string>()->default_value("1;0;0;0;0;0;0;0;0"), "subgroup(s) of a group of reference features to use, based on the orientation of the flanking annotations; where arg is a semi-colon ")
        ;

        // Declare a group of options that will be allowed only for the GUI.
        po::options_description gui_desc("GUI", 81);
        gui_desc.add_options()
        (DISPLAY_DISPERSION_VALUES, po::value<bool>()->default_value(false), "display dispersion values")
        (Y_AXIS_SCALE, po::value<std::string>(), "select the visible portion of the Y axis of the graph(s)")
        ;

        // Combine the set of options that will be allowed only on the command line.
        _cmd_options.add(generic_desc).add(params_desc).add(gui_desc);

        // Combine the set of options that will be allowed only in the configuration file.
        _cfg_options.add(params_desc).add(gui_desc);

        // Combine the set of options that will be visible for the help message.
        _visible.add(generic_desc).add(params_desc).add(gui_desc);

        // Add the parameters option as a positionnal option to the cmd line.
        _pos_options.add("parameters", -1);
    }

    const boost::program_options::variables_map &variablesMap() const
    {
        return _vm;
    }

    std::string command(int ac, char *av[])
    {
        std::ostringstream command;
        command << av[0];
        for(int i = 1; i < ac; ++i) command << " " << (std::strlen(av[i]) == 0 ? "''" : av[i]);
        return command.str();
    }

    // TODO: wpb and ba and bst and bsv bsa must be completed if the defaut value was
    //       used and the reference points is not 2(default value). Compare referene points and default value to determine if the default value was used.
    parameters parse(int ac, char *av[])
    {
        namespace po = boost::program_options;

        parameters params;
        std::stringstream ini_stream;

        // Parse command line options first.
        try
        {
            store(po::command_line_parser(ac, av).options(_cmd_options).positional(_pos_options).run(), _vm);

            if(_vm.count("help"))
            {
                std::cout << APPNAME << " " << APPVERSION << "\n"
                          << "Usage: " << APPNAME << " [OPTION]... [FILE]" << "\n"
                          << std::boolalpha << _visible << "\n\n"
                          << "'arg' means the options has an argument and (=) defines the default value for this option.\n"
                          << "The required options are: analysis_mode, dataset_path, refgroup_path, annotations_path (A and E mode only) and the output_directory.\n"
                          << "For more informations on the options and their values, see the parameters file at <https://bitbucket.org/labjacquespe/vap_core/>.\n"
                          << "When FILE is - or no FILE is given, read standard input.\n"
                          << "\n"
                          << "Examples:\n"
                          << "\tvap_core vap_params.txt\n"
                          << "\tvap_core -p[parameters] vap_params.txt --smoothing_windows 6\n"
                          << "\tvap_core --analysis_mode A --dataset_path data/dataset1 data/dataset2 --refgroup_path=data/group1\n"
                          << "\tvap_core --analysis_mode A --dataset_path data/dataset1 --dataset_path data/dataset2 --refgroup_path=data/group1 vap_params.txt\n"
                          << "\tvap_core --window_size 100 < vap_params.txt\n"
                          << "\nReport bugs to: <https://bitbucket.org/labjacquespe/vap_core/issues>\n"
                          << "For support: <vap_support@usherbrooke.ca>\n"
                          << APPNAME << " home page: <http://lab-jacques.recherche.usherbrooke.ca/software_en/vap/home/>\n";
                std::exit(EXIT_SUCCESS);
            }

            if(_vm.count("version"))
            {
                std::cout << APPNAME << " " << APPVERSION << "\n"
                          << "License GPLv3: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>." << "\n"
                          << MSG_COPYRIGHT << "\n";
                std::exit(EXIT_SUCCESS);
            }

            if(_vm.count("show-extensions"))
            {
                for(const auto &e : io::SUPPORTED_EXTENSIONS) std::cout << e << '\n';
                std::exit(EXIT_SUCCESS);
            }

            if(_vm.count("show-formats"))
            {
                for(const auto &f : io::SUPPORTED_FORMATS) std::cout << f << '\n';
                std::exit(EXIT_SUCCESS);
            }

            if(_vm.count("parameters") && _vm["parameters"].as<std::string>() != "-") // Read file if filename was given, if - was given we need to scan stdin
            {
                // Parse configuration file if present
                std::string file = _vm["parameters"].as<std::string>();
                std::ifstream apt_ifs(file);
                if(!apt_ifs)
                    throw std::runtime_error(invalid_option_value_str("parameters", file, "no such file"));
                else
                    vap_apt_translator::to_ini_stream(apt_ifs, ini_stream);
                params.parametersPath = file;
            }
            else // Scan stdin
            {
                std::stringstream apt_stream;
                std::cin >> std::noskipws;
                std::copy(std::istream_iterator<char>(std::cin), std::istream_iterator<char>(), std::ostream_iterator<char>(apt_stream));
                vap_apt_translator::to_ini_stream(apt_stream, ini_stream);
                params.parametersPath = _vm["parameters"].as<std::string>();
            }

            store(parse_config_file(ini_stream, _cfg_options), _vm);
            notify(_vm);

            if(_vm.count(ANALYSIS_MODE))
            {
#ifdef __DEBUG__
                std::cout << "analysis_mode:" << _vm[ANALYSIS_MODE].as<std::string>() << "\n";
#endif

                std::string value = _vm[ANALYSIS_MODE].as<std::string>();

                if(value != "A" && value != "E" && value != "C")
                    throw std::runtime_error(invalid_option_value_str(ANALYSIS_MODE, value, "must be either 'A|E|C'"));

                params.analysisMode = analysis_mode::toType(value.front());
            }

            if(_vm.count(DATASET_PATH))
            {
#ifdef __DEBUG__
                std::cout << "dataset_path:" << _vm[DATASET_PATH].as< std::vector<std::string> >() << "\n";
#endif

                size_t pos = std::string::npos;
                std::string round = "";

                std::vector<std::string> values = _vm[DATASET_PATH].as< std::vector<std::string>>();
                for(auto value : values)
                {
                    round.clear();

                    if((pos = value.find(ROUND_TAG)) < value.size())
                    {
                        round = value.substr(0, pos); // retrieve round name
                        value.erase(0, pos + 3); // remove round and round tag from value
                    }

                    // Check that file actually exists
                    if(!filesystem::exists(value))
                        throw std::runtime_error(invalid_option_value_str(DATASET_PATH, value, "no such file"));

                    params.datasetsPaths[round].push_back(value);
                }
            }

            if(_vm.count(REFGROUP_PATH))
            {
#ifdef __DEBUG__
                std::cout << "refgroup_path:" << _vm[REFGROUP_PATH].as< std::vector<std::string> >() << "\n";
#endif

                size_t pos = std::string::npos;
                std::string round = "";

                std::vector<std::string> values = _vm[REFGROUP_PATH].as< std::vector<std::string> >();
                for(auto value : values)
                {
                    round.clear();

                    if((pos = value.find(ROUND_TAG)) < value.size())
                    {
                        round = value.substr(0, pos); // retrieve round name
                        value.erase(0, pos + 3); // remove round and round tag from value
                    }

                    // Check that file actually exists
                    if(!filesystem::exists(value))
                        throw std::runtime_error(invalid_option_value_str(REFGROUP_PATH, value, "no such file"));

                    params.refGroupsPaths[round].push_back(value);
                }
            }

            if(_vm.count(ANNOTATIONS_PATH))
            {
#ifdef __DEBUG__
                std::cout << "annotations_path:" << _vm[ANNOTATIONS_PATH].as<std::string>() << "\n";
#endif
                std::string value = _vm[ANNOTATIONS_PATH].as<std::string>();

                if(params.analysisMode != analysis_mode::type::COORDINATE && value.empty())
                    throw std::runtime_error(invalid_option_value_str(ANNOTATIONS_PATH, value, "annotation and exon mode requires an annotations file"));

                // Check that file actually exists
                if((!value.empty() && !filesystem::exists(value)))
                    throw std::runtime_error(invalid_option_value_str(ANNOTATIONS_PATH, value, "no such file"));

                params.annotationsPath = value;
            }
            else if(params.analysisMode != analysis_mode::type::COORDINATE)
                throw std::runtime_error(required_option_missing_str(ANNOTATIONS_PATH));

            if(_vm.count(SELECTION_PATH))
            {
#ifdef __DEBUG__
                std::cout << "selection_path:" << _vm[SELECTION_PATH].as<std::string>() << "\n";
#endif
                std::string value = _vm[SELECTION_PATH].as<std::string>();

                // Check that file actually exists
                if(!value.empty() && !filesystem::exists(value))
                    throw std::runtime_error(invalid_option_value_str(SELECTION_PATH, value, "no such file"));

                params.selectionPath = value;
            }

            if(_vm.count(EXCLUSION_PATH))
            {
#ifdef __DEBUG__
                std::cout << "exclusion_path:" << _vm[EXCLUSION_PATH].as<std::string>() << "\n";
#endif
                std::string value = _vm[EXCLUSION_PATH].as<std::string>();

                // Check that file actually exists
                if(!value.empty() && !filesystem::exists(value))
                    throw std::runtime_error(invalid_option_value_str(EXCLUSION_PATH, value, "no such file"));

                params.exclusionPath = value;
            }

            if(_vm.count(ANALYSIS_METHOD))
            {
#ifdef __DEBUG__
                std::cout << "analysis_method:" << _vm[ANALYSIS_METHOD].as<std::string>() << "\n";
#endif

                std::string value = _vm[ANALYSIS_METHOD].as<std::string>();

                if(value!="A" && value!="R")
                    throw std::runtime_error(invalid_option_value_str(ANALYSIS_METHOD, value, "must be either 'A|R'"));

                params.analysisMethod = analysis_method::toType(value.front());
            }

            if(_vm.count(ANNOTATION_COORDINATES_TYPE))
            {
#ifdef __DEBUG__
                std::cout << "annotation_coordinates_type:" << _vm[ANNOTATION_COORDINATES_TYPE].as<std::string>() << "\n";
#endif

                std::string value = _vm[ANNOTATION_COORDINATES_TYPE].as<std::string>();

                if(value != "T" && value != "C" && value != "N")
                    throw std::runtime_error(invalid_option_value_str(ANNOTATION_COORDINATES_TYPE, value, "must be either 'T|C|N'"));

                params.annotationCoordinatesType = annotation_coordinates_type::toType(value.front());
            }

            if(_vm.count(REFERENCE_POINTS))
            {
#ifdef __DEBUG__
                std::cout << "reference_points:" << _vm[REFERENCE_POINTS].as<std::string>() << "\n";
#endif
                std::string svalue = _vm[REFERENCE_POINTS].as<std::string>();
                unsigned int value;

                if(!util::isNumber(svalue))
                    throw std::runtime_error(invalid_option_value_str(REFERENCE_POINTS, svalue, "must be an integer"));

                value = boost::lexical_cast<unsigned int>(svalue);

                // Check that reference points is between 1 and 6
                if(value < 1 || value > 6)
                    throw std::runtime_error(invalid_option_value_str(REFERENCE_POINTS, value, "must be between 1 and 6"));

                // Check that in exon mode, the number of reference points is 6
                if(params.analysisMode == analysis_mode::type::EXON && value != 6)
                    throw std::runtime_error(invalid_option_value_str(REFERENCE_POINTS, value, "must be 6 in exon mode"));

                params.referencePoints = value;
            }

            if(_vm.count(ONEREFPT_BOUNDARY))
            {
#ifdef __DEBUG__
                std::cout << "1refpt_boundary:" << _vm[ONEREFPT_BOUNDARY].as<std::string>() << "\n";
#endif

                std::string value = _vm[ONEREFPT_BOUNDARY].as<std::string>();

                if(params.analysisMode == analysis_mode::type::ANNOTATION && params.referencePoints == 1 && value == "N")
                    throw std::runtime_error(invalid_option_value_str(ONEREFPT_BOUNDARY, value, "when in 1 reference_point, value must be different then none, must be either '5|3'"));

                if(value != "5" && value != "3" && value != "N")
                    throw std::runtime_error(invalid_option_value_str(ONEREFPT_BOUNDARY, value, "must be either '5|3|N'"));

                params.oneRefPtBoundary = oneRefPt_boundary::toType(value.front());
            }

            if(_vm.count(WINDOW_SIZE))
            {
#ifdef __DEBUG__
                std::cout << "window_size:" << _vm[WINDOW_SIZE].as<std::string>() << "\n";
#endif

                std::string svalue = _vm[WINDOW_SIZE].as<std::string>();
                unsigned int value;

                if(!util::isNumber(svalue))
                    throw std::runtime_error(invalid_option_value_str(WINDOW_SIZE, svalue, "must be an integer"));

                value = boost::lexical_cast<unsigned int>(svalue);

                if(value > 500000000)
                    throw std::runtime_error(invalid_option_value_str(WINDOW_SIZE, value, "must be between 0 and 500,000,000"));

                params.windowSize = value;
            }

            if(_vm.count(WINDOWS_PER_BLOCK))
            {
#ifdef __DEBUG__
                std::cout << "windows_per_block:" << _vm[WINDOWS_PER_BLOCK].as<std::string>() << "\n";
#endif

                unsigned int value; std::string svalue; size_t i;
                std::string str = _vm[WINDOWS_PER_BLOCK].as<std::string>();

                util::parser_string parser(&str, ";");
                for(i = 0; !parser.eob(); ++i)
                {
                    // TODO: Let wpb have a zero as value?
                    parser >> svalue;
                    parser.skip();

                    if(!util::isNumber(svalue))
                        throw std::runtime_error(invalid_option_value_str(WINDOWS_PER_BLOCK, svalue, "must be a list of integer"));

                    value = boost::lexical_cast<unsigned int>(svalue);

                    if(value < 1 || value > 10000)
                        throw std::runtime_error(invalid_option_value_str(WINDOWS_PER_BLOCK, value, "must be between 1 and 10000"));

                    params.windowsPerBlock.push_back(value);
                }

                // There must be as many wpb values as the number of reference points + 1
                if(i != params.referencePoints+1)
                    throw std::runtime_error(invalid_option_value_str(WINDOWS_PER_BLOCK, str, "must have " +boost::lexical_cast<std::string>(params.referencePoints+1)+" values"));
            }

            if(_vm.count(BLOCK_ALIGNMENT))
            {
#ifdef __DEBUG__
                std::cout << "block_alignment:" << _vm[BLOCK_ALIGNMENT].as<std::string>() << "\n";
#endif

                char c; size_t i;
                std::string str = _vm[BLOCK_ALIGNMENT].as<std::string>();

                util::parser_string parser(&str, ";");
                for(i = 0; !parser.eob(); ++i)
                {
                    parser >> c;
                    parser.skip();

                    if(c != 'L' && c != 'R' && c != 'S')
                        throw std::runtime_error(invalid_option_value_str(BLOCK_ALIGNMENT, c, "must be either 'L|R|S'"));

                    params.blockAlignment.push_back(block_alignment::toType(c));
                }

                // There must be as many ba values as the number of reference points + 1
                if(i != params.referencePoints+1)
                    throw std::runtime_error(invalid_option_value_str(BLOCK_ALIGNMENT, str, "must have " +boost::lexical_cast<std::string>(params.referencePoints+1)+" values"));
            }

            if(_vm.count(BLOCK_SPLIT_TYPE))
            {
#ifdef __DEBUG__
                std::cout << "block_split_type:" << _vm[BLOCK_SPLIT_TYPE].as<std::string>() << "\n";
#endif

                char c; size_t i;
                std::string str = _vm[BLOCK_SPLIT_TYPE].as<std::string>();

                util::parser_string parser(&str, ";");
                for(i = 0; !parser.eob(); ++i)
                {
                    parser >> c;
                    parser.skip();

                    if(c!='A' && c!='P' && c!='N')
                        throw std::runtime_error(invalid_option_value_str(BLOCK_SPLIT_TYPE, c, "must be either 'A|P|N'"));

                    params.blockSplitType.push_back(block_split_type::toType(c));
                }

                // There must be as many bst values as the number of reference points + 1
                if(i != params.referencePoints+1)
                    throw std::runtime_error(invalid_option_value_str(BLOCK_SPLIT_TYPE, str, "must have " +boost::lexical_cast<std::string>(params.referencePoints+1)+" values"));
            }

            if(_vm.count(BLOCK_SPLIT_VALUE))
            {
#ifdef __DEBUG__
                std::cout << "block_split_value:" << _vm[BLOCK_SPLIT_VALUE].as<std::string>() << "\n";
#endif

                unsigned int value; std::string svalue; size_t i;
                std::string str = _vm[BLOCK_SPLIT_VALUE].as<std::string>();

                util::parser_string parser(&str, ";");
                for(i = 0; !parser.eob(); ++i)
                {
                    parser >> svalue;
                    parser.skip();

                    if(!util::isNumber(svalue))
                        throw std::runtime_error(invalid_option_value_str(WINDOWS_PER_BLOCK, svalue, "must be an integer"));

                    value = boost::lexical_cast<unsigned int>(svalue);

                    // Validate value range for the absolute or percentage proportion type
                    switch(params.blockSplitType[i])
                    {
                        case block_split_type::type::ABSOLUTE:
                            if(value > params.windowsPerBlock[i])
                                throw std::runtime_error(invalid_option_value_str(BLOCK_SPLIT_VALUE, value, "must be between 0 and "+boost::lexical_cast<std::string>(params.windowsPerBlock[i])+ "(windows_per_block)"));
                            break;

                        case block_split_type::type::PERCENTAGE:
                            if(value > 100)
                                throw std::runtime_error(invalid_option_value_str(BLOCK_SPLIT_VALUE, value, "must be between 0 and 100"));
                            break;

                        default:
                            break;
                    }

                    params.blockSplitValue.push_back(value);
                }

                // There must be as many bsv values as the number of reference points + 1
                if(i != params.referencePoints+1)
                    throw std::runtime_error(invalid_option_value_str(BLOCK_SPLIT_VALUE, str, "must have " +boost::lexical_cast<std::string>(params.referencePoints+1)+" values"));
            }

            if(_vm.count(BLOCK_SPLIT_ALIGNMENT))
            {
#ifdef __DEBUG__
                std::cout << "block_split_alignment:" << _vm[BLOCK_SPLIT_ALIGNMENT].as<std::string>() << "\n";
#endif

                char c; size_t i;
                std::string str = _vm[BLOCK_SPLIT_ALIGNMENT].as<std::string>();

                util::parser_string parser(&str, ";");
                for(i = 0; !parser.eob(); ++i)
                {
                    parser >> c;
                    parser.skip();

                    if(c!='L' && c!='R' && c!='N')
                        throw std::runtime_error(invalid_option_value_str(BLOCK_SPLIT_ALIGNMENT, c, "must be either 'L|R|N'"));

                    params.blockSplitAlignment.push_back(block_split_alignment::toType(c));
                }

                // There must be as many bsa values as the number of reference points + 1
                if(i != params.referencePoints+1)
                    throw std::runtime_error(invalid_option_value_str(BLOCK_SPLIT_ALIGNMENT, str, "must have " +boost::lexical_cast<std::string>(params.referencePoints+1)+" values"));
            }

            if(_vm.count(MERGE_MID_INTRONS))
            {
#ifdef __DEBUG__
                std::cout << "merge_mid_introns:" << _vm[MERGE_MID_INTRONS].as<std::string>() << "\n";
#endif

                std::string value = _vm[MERGE_MID_INTRONS].as<std::string>();

                if(value!="F" && value!="L" && value!="N")
                    throw std::runtime_error(invalid_option_value_str(MERGE_MID_INTRONS, value, "must be either 'F|L|N'"));

                params.mergeMiddleIntrons = merge_mid_introns::toType(value.front());
            }

            if(_vm.count(AGGREGATE_DATA_TYPE))
            {
#ifdef __DEBUG__
                std::cout << "aggregate_data_type:" << _vm[AGGREGATE_DATA_TYPE].as<std::string>() << "\n";
#endif
                std::string value = _vm[AGGREGATE_DATA_TYPE].as<std::string>();

                if(value!="E" && value!="D" && value!="A" && value!="I")
                    throw std::runtime_error(invalid_option_value_str(AGGREGATE_DATA_TYPE, value, "must be either 'E|D|A|I'"));

                params.aggregateDataType = aggregate_data_type::toType(value.front());
            }

            if(_vm.count(SMOOTHING_WINDOWS))
            {
#ifdef __DEBUG__
                std::cout << "smoothing_windows:" << _vm[SMOOTHING_WINDOWS].as<std::string>() << "\n";
#endif

                std::string svalue = _vm[SMOOTHING_WINDOWS].as<std::string>();
                unsigned int value, sum = std::accumulate(params.windowsPerBlock.begin(), params.windowsPerBlock.end(), 0);

                if(!util::isNumber(svalue))
                    throw std::runtime_error(invalid_option_value_str(SMOOTHING_WINDOWS, svalue, "must be an integer"));

                value = boost::lexical_cast<unsigned int>(svalue);

                if(value > sum)
                    throw std::runtime_error(invalid_option_value_str(SMOOTHING_WINDOWS, value, "must be less than "+boost::lexical_cast<std::string>(sum)+", the sum of windows_per_block"));

                if(value % 2 != 0)
                    throw std::runtime_error(invalid_option_value_str(SMOOTHING_WINDOWS, value, "must be an even value"));

                params.smoothingWindows = value;
            }

            if(_vm.count(MEAN_DISPERSION_VALUE))
            {
#ifdef __DEBUG__
                std::cout << "mean_dispersion_value:" << _vm[MEAN_DISPERSION_VALUE].as<std::string>() << "\n";
#endif
                std::string value = _vm[MEAN_DISPERSION_VALUE].as<std::string>();

                if(value!="E" && value!="D")
                    throw std::runtime_error(invalid_option_value_str(MEAN_DISPERSION_VALUE, value, "must be either 'E|D'"));

                params.meanDispersionValue = mean_dispersion_value::toType(value.front());
            }

            if(_vm.count(PROCESS_MISSING_DATA))
            {
#ifdef __DEBUG__
                std::cout << std::boolalpha <<"process_missing_data:" << _vm[PROCESS_MISSING_DATA].as<bool>() << "\n";
#endif
                params.processMissingData = _vm[PROCESS_MISSING_DATA].as<bool>();
            }

            if(_vm.count(PROCESS_DATA_BY_CHUNK))
            {
#ifdef __DEBUG__
                std::cout << std::boolalpha << "process_data_by_chunk:" << _vm[PROCESS_DATA_BY_CHUNK].as<bool>() << "\n";
#endif
                params.processDataByChunk = _vm[PROCESS_DATA_BY_CHUNK].as<bool>();
            }

            if(_vm.count(DATASET_CHUNKSIZE))
            {
#ifdef __DEBUG__
                std::cout << "dataset_chunksize:" << _vm[DATASET_CHUNKSIZE].as<unsigned long long>() << "\n";
#endif
                std::string svalue = _vm[DATASET_CHUNKSIZE].as<std::string>();
                unsigned long long value;

                if(!util::isNumber(svalue))
                    throw std::runtime_error(invalid_option_value_str(DATASET_CHUNKSIZE, svalue, "must be an integer"));

                value = boost::lexical_cast<unsigned long long>(svalue);

                params.datasetChunkSize = value;
            }

            if(_vm.count(OUTPUT_DIRECTORY))
            {
#ifdef __DEBUG__
                std::cout << "output_directory:" << _vm[OUTPUT_DIRECTORY].as<std::string>() << "\n";
#endif
                std::string value = _vm[OUTPUT_DIRECTORY].as<std::string>();

                // Add '/' if only the directory name was given
                if(!value.empty() && value.back() != '/')
                    value += '/';

                // Make sure the directory exists
                if(!filesystem::exists(value))
                    throw std::runtime_error(invalid_option_value_str(OUTPUT_DIRECTORY, value, "no such directory"));

                params.outputDirectory = value;
            }

            if(_vm.count(PREFIX_FILENAME))
            {
#ifdef __DEBUG__
                std::cout << "prefix_filename:" << _vm[PREFIX_FILENAME].as<std::string>() << "\n";
#endif
                params.prefixFilename = _vm[PREFIX_FILENAME].as<std::string>();
            }

            if(_vm.count(WRITE_INDIVIDUAL_REFERENCES))
            {
#ifdef __DEBUG__
                std::cout << std::boolalpha << "write_individual_references:" << _vm[WRITE_INDIVIDUAL_REFERENCES].as<bool>() << "\n";
#endif
                params.writeIndividualReferences = _vm[WRITE_INDIVIDUAL_REFERENCES].as<bool>();
            }

            if(_vm.count(GENERATE_HEATMAPS))
            {
#ifdef __DEBUG__
                std::cout << std::boolalpha << "generate_heatmaps:" << _vm[GENERATE_HEATMAPS].as<bool>() << "\n";
#endif

                bool value = _vm[GENERATE_HEATMAPS].as<bool>();

                // 'writeIndividualReferences' must be true in order to generate heatmaps
                if(!params.writeIndividualReferences && value)
                    throw std::runtime_error(invalid_option_value_str(GENERATE_HEATMAPS, value, "must have 'write_individual_references' true in order to generate heatmaps"));

                params.generateHeatmaps = value;
            }

            if(_vm.count(GENERATE_AGGREGATE_GRAPHS))
            {
#ifdef __DEBUG__
                std::cout << std::boolalpha << "generate_aggregate_graphs:" << _vm[GENERATE_AGGREGATE_GRAPHS].as<bool>() << "\n";
#endif
                params.generateAggregateGraphs = _vm[GENERATE_AGGREGATE_GRAPHS].as<bool>();
            }

            if(_vm.count(ONE_GRAPH_PER_DATASET))
            {
#ifdef __DEBUG__
                std::cout << std::boolalpha << "one_graph_per_dataset:" << _vm[ONE_GRAPH_PER_DATASET].as<bool>() << "\n";
#endif
                params.oneGraphPerDataset = _vm[ONE_GRAPH_PER_DATASET].as<bool>();
            }

            if(_vm.count(ONE_GRAPH_PER_GROUP))
            {
#ifdef __DEBUG__
                std::cout << std::boolalpha << "one_graph_per_group:" << _vm[ONE_GRAPH_PER_GROUP].as<bool>() << "\n";
#endif
                params.oneGraphPerGroup = _vm[ONE_GRAPH_PER_GROUP].as<bool>();
            }

            if(_vm.count(ORIENTATION_SUBGROUPS))
            {
#ifdef __DEBUG__
                std::cout << "orientation_subgroups:" << _vm[ORIENTATION_SUBGROUPS].as<std::string>() << "\n";
#endif

                char c; size_t i;
                std::string value = _vm[ORIENTATION_SUBGROUPS].as<std::string>();

                util::parser_string parser(&value, ";");
                for(i = 0; !parser.eob() && i < N_ORI_SUBGROUPS; ++i)
                {
                    parser >> c;
                    parser.skip();
                    if(c != '0' && c != '1')
                        throw std::runtime_error(invalid_option_value_str(ORIENTATION_SUBGROUPS, c, "must be either '0' or '1'"));

                    params.orientationSubgroups.set(i, (c == '1'));
                }

                // There must be as many ba values as the number of orientation subgroups
                if(i != N_ORI_SUBGROUPS)
                    throw std::runtime_error(invalid_option_value_str(ORIENTATION_SUBGROUPS, value, "must have " +boost::lexical_cast<std::string>(N_ORI_SUBGROUPS)+" values"));

                // Check that there at least one subgroup is set when 'writeIndividualReferences' is true
                if(params.writeIndividualReferences && params.orientationSubgroups.count() < 1)
                    throw std::runtime_error(invalid_option_value_str(ORIENTATION_SUBGROUPS, value, "must have at least one orientation subgroups when 'write_individual_references' is true"));
            }

            if(_vm.count(ONE_GRAPH_PER_ORIENTATION))
            {
#ifdef __DEBUG__
                std::cout << std::boolalpha << "one_graph_per_orientation:" << _vm[ONE_GRAPH_PER_ORIENTATION].as<bool>() << "\n";
#endif
                params.oneOrientationPerGraph = _vm[ONE_GRAPH_PER_ORIENTATION].as<bool>();
            }

            if(params.datasetsPaths.size() - params.refGroupsPaths.size() != 0)
                throw std::runtime_error("the number of rounds is not the same for the 'dataset_path' and 'refgroup_path' options.");

            auto it1 = params.datasetsPaths.begin();
            auto it2 = params.refGroupsPaths.begin();
            while(it1 != params.datasetsPaths.end() && it2 != params.refGroupsPaths.end())
            {
                if(it1->first != it2->first)
                    throw std::runtime_error("the 'dataset_path' round does not match the 'refgroup_path' round");

                ++it1; ++it2;
            }

            // Internal parameter. Help to iterate over all the rounds
            for(const auto &v : params.refGroupsPaths)
                params.rounds.push_back(v.first);

            // TODO: Remove alterNonSenseParameters or put logic here!
        }
        catch(std::exception &e)
        {
            // std::cerr << "error: " << e.what() << "\n";
            PRINT_AND_LOG_ERROR(e.what());
            std::exit(EXIT_FAILURE);
        }

        return params;
    }
};

std::ostream &operator<<(std::ostream &out, const boost::program_options::variables_map &vm)
{
    // if(vm.count("parameters")) out << "parameters:"<< vm["parameters"].as<std::string>() << "\n";
    if(vm.count(ANALYSIS_MODE)) out << "~~@" << "analysis_mode=" << vm[ANALYSIS_MODE].as<std::string>() << "\n";
    if(vm.count(DATASET_PATH))
    {
        const auto &paths = vm[DATASET_PATH].as< std::vector<std::string> >();
        for(const auto &v : paths)  out << "~~@" << "dataset_path=" << v << "\n";
    }
    if(vm.count(REFGROUP_PATH))
    {
        const auto &paths = vm[REFGROUP_PATH].as< std::vector<std::string> >();
        for(const auto &v : paths) out << "~~@" << "refgroup_path=" << v << "\n";
    }
    if(vm.count(ANNOTATIONS_PATH)) out << "~~@" << ANNOTATIONS_PATH << "=" << vm[ANNOTATIONS_PATH].as<std::string>() << "\n";
    if(vm.count(SELECTION_PATH)) out << "~~@" << SELECTION_PATH << "=" << vm[SELECTION_PATH].as<std::string>() << "\n";
    if(vm.count(EXCLUSION_PATH)) out << "~~@" << EXCLUSION_PATH << "=" << vm[EXCLUSION_PATH].as<std::string>() << "\n";
    if(vm.count(ANALYSIS_METHOD)) out << "~~@" << ANALYSIS_METHOD << "=" << vm[ANALYSIS_METHOD].as<std::string>() << "\n";
    if(vm.count(ANNOTATION_COORDINATES_TYPE)) out << "~~@" << ANNOTATION_COORDINATES_TYPE << "=" << vm[ANNOTATION_COORDINATES_TYPE].as<std::string>() << "\n";
    if(vm.count(REFERENCE_POINTS)) out << "~~@" << REFERENCE_POINTS << "=" << vm[REFERENCE_POINTS].as<std::string>() << "\n";
    if(vm.count(ONEREFPT_BOUNDARY)) out << "~~@" << ONEREFPT_BOUNDARY << "=" << vm[ONEREFPT_BOUNDARY].as<std::string>() << "\n";
    if(vm.count(WINDOW_SIZE)) out << "~~@" << WINDOW_SIZE << "=" << vm[WINDOW_SIZE].as<std::string>() << "\n";
    if(vm.count(WINDOWS_PER_BLOCK)) out << "~~@" << WINDOWS_PER_BLOCK << "=" << vm[WINDOWS_PER_BLOCK].as<std::string>() << "\n";
    if(vm.count(BLOCK_ALIGNMENT)) out << "~~@" << BLOCK_ALIGNMENT << "=" << vm[BLOCK_ALIGNMENT].as<std::string>() << "\n";
    if(vm.count(BLOCK_SPLIT_TYPE)) out << "~~@" << BLOCK_SPLIT_TYPE << "=" << vm[BLOCK_SPLIT_TYPE].as<std::string>() << "\n";
    if(vm.count(BLOCK_SPLIT_VALUE)) out << "~~@" << BLOCK_SPLIT_VALUE << "=" << vm[BLOCK_SPLIT_VALUE].as<std::string>() << "\n";
    if(vm.count(BLOCK_SPLIT_ALIGNMENT)) out << "~~@" << BLOCK_SPLIT_ALIGNMENT << "=" << vm[BLOCK_SPLIT_ALIGNMENT].as<std::string>() << "\n";
    if(vm.count(MERGE_MID_INTRONS)) out << "~~@" << MERGE_MID_INTRONS << "=" << vm[MERGE_MID_INTRONS].as<std::string>() << "\n";
    if(vm.count(AGGREGATE_DATA_TYPE)) out << "~~@" << AGGREGATE_DATA_TYPE << "=" << vm[AGGREGATE_DATA_TYPE].as<std::string>() << "\n";
    if(vm.count(SMOOTHING_WINDOWS)) out << "~~@" << SMOOTHING_WINDOWS << "=" << vm[SMOOTHING_WINDOWS].as<std::string>() << "\n";
    if(vm.count(MEAN_DISPERSION_VALUE)) out << "~~@" << MEAN_DISPERSION_VALUE << "=" << vm[MEAN_DISPERSION_VALUE].as<std::string>() << "\n";
    if(vm.count(PROCESS_MISSING_DATA)) out << "~~@" << PROCESS_MISSING_DATA << "=" << vm[PROCESS_MISSING_DATA].as<bool>() << "\n";
    if(vm.count(PROCESS_DATA_BY_CHUNK)) out << "~~@" << PROCESS_DATA_BY_CHUNK << "=" << vm[PROCESS_DATA_BY_CHUNK].as<bool>() << "\n";
    if(vm.count(DATASET_CHUNKSIZE)) out << "~~@" << DATASET_CHUNKSIZE << "=" << vm[DATASET_CHUNKSIZE].as<std::string>() << "\n";
    if(vm.count(OUTPUT_DIRECTORY)) out << "~~@" << OUTPUT_DIRECTORY << "=" << vm[OUTPUT_DIRECTORY].as<std::string>() << "\n";
    if(vm.count(PREFIX_FILENAME)) out << "~~@" << PREFIX_FILENAME << "=" << vm[PREFIX_FILENAME].as<std::string>() << "\n";
    if(vm.count(WRITE_INDIVIDUAL_REFERENCES)) out << "~~@" << WRITE_INDIVIDUAL_REFERENCES << "=" << vm[WRITE_INDIVIDUAL_REFERENCES].as<bool>() << "\n";
    if(vm.count(GENERATE_HEATMAPS)) out << "~~@" << GENERATE_HEATMAPS << "=" << vm[GENERATE_HEATMAPS].as<bool>() << "\n";
    if(vm.count(GENERATE_AGGREGATE_GRAPHS)) out << "~~@" << GENERATE_AGGREGATE_GRAPHS << "=" << vm[GENERATE_AGGREGATE_GRAPHS].as<bool>() << "\n";
    if(vm.count(DISPLAY_DISPERSION_VALUES)) out << "~~@" << DISPLAY_DISPERSION_VALUES << "=" << vm[DISPLAY_DISPERSION_VALUES].as<bool>() << "\n";
    if(vm.count(ONE_GRAPH_PER_DATASET)) out << "~~@" << ONE_GRAPH_PER_DATASET << "=" << vm[ONE_GRAPH_PER_DATASET].as<bool>() << "\n";
    if(vm.count(ONE_GRAPH_PER_GROUP)) out << "~~@" << ONE_GRAPH_PER_GROUP << "=" << vm[ONE_GRAPH_PER_GROUP].as<bool>() << "\n";
    if(vm.count(ORIENTATION_SUBGROUPS)) out << "~~@" << ORIENTATION_SUBGROUPS << "=" << vm[ORIENTATION_SUBGROUPS].as<std::string>() << "\n";
    if(vm.count(ONE_GRAPH_PER_ORIENTATION)) out << "~~@" << ONE_GRAPH_PER_ORIENTATION << "=" << vm[ONE_GRAPH_PER_ORIENTATION].as<bool>() << "\n";
    if(vm.count(Y_AXIS_SCALE)) out << "~~@" << Y_AXIS_SCALE << "=" << vm[Y_AXIS_SCALE].as<std::string>() << "\n";
    return out;
}

std::ostream &operator<<(std::ostream &out, const parser_parameters &opts)
{
    return out << opts.variablesMap();
}
}
#endif
