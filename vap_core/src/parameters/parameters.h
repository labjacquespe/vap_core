/**
 * @file   parameters.h
 * @author Charles Coulombe
 *
 * @date 12 December 2012, 23:44
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARAMETERS_H
#define	PARAMETERS_H

#include <vector>
#include <deque>
#include <bitset>
#include <string>
#include <map>

#include "../defs.h"

namespace vap
{

/** Definition for parameters found in the parameters file. */
struct parameters
{
    /**
     * @brief Parameter @c output_directory : [ path ]
     *
     * This parameter contains the path(full or relative) of the output directory where all the
     * output and log files will be centralized.
     *
     * @warning The folder must exist and the path must not be empty.
     */
    std::string outputDirectory;

    /**
     * Parameter @c analysis_mode : [ A, E, C ]
     *
     * This parameter indicates which type of reference features to use.
     *
     * - (A)nnotation: the reference features contained in the reference group(s)
     * are annotation name (unique identifier) from a provided annotations file,
     * and the [reference points](@ref referencePoints) are derived from the annotation boundaries.
     * The orientation of the flanking annotations are used to create
     * @c orientation_subgroups if desired.
     *
     * - (E)xon: the reference features contained in the reference
     * group(s) are annotation name (unique identifier) from a provided annotations
     * file, and the [reference points](@ref referencePoints) are derived from the exon boundaries.
     * The orientation of the flanking annotations is ignored.
     *
     * - (C)oordinate: the reference features contained in the reference
     * group(s) are region coordinates directly containing the [reference points](@ref referencePoints)
     * to use. The first two columns of this tab-delimited file are the
     * chromosome and strand, followed by the required number of [reference points](@ref referencePoints),
     * and optionally followed by a region name or description.
     * The orientation of the flanking annotations is ignored.
     */
    analysis_mode::type analysisMode;

    /**
     * Parameter @c dataset_path : [ path ]
     *
     * This parameter contains the path of the file containing the information
     * about the datasets to analyze.
     *
     * @note This parameter may be used multiple times for multiple datasets.
     *
     * Each instance of this parameter contains a path to a dataset.
     *
     * The supported format of dataset file are :
     * - [Bedgrap](https://genome.ucsc.edu/goldenPath/help/bedgraph.html);
     * - [Wig](https://genome.ucsc.edu/goldenPath/help/wiggle.html).
     *
     * @note When the "track" line of a dataset file contains the field "name",
     * this information is used as dataset name, otherwise the file name is used
     * as dataset name in the output files.
     *
     * Because it is possible to have multiple datasets, you can also regroup
     * datasets together by specifying an alias(which is optional). If present
     * it must be followed by ':=:' and the parameter value.
     * @code dataset_path=[alias:=:]path @endcode
     */
    std::map< std::string, std::deque<std::string> > datasetsPaths;

    /**
     * Parameter @c refgroup_path : [ path ]
     *
     * This parameter contains the path of the file containing the information
     * about the reference groups (either annotation name or coordinates based
     * on the @c [analysis mode](analysisMode) to analyze.
     *
     * @note This parameter may be used multiple times for multiple datasets.
     *
     * Each instance of this parameter contains a path to a reference group.
     *
     * The format of the reference group file is simply a list of one annotation
     * name per line (must come from the first column of the annotations file)
     * in annotation or exon type of reference, or must directly contains the
     * coordinates in coordinates mode.
     *
     * @note When the first line of a reference group file start by a "#", it means
     * it is a header, otherwise the file name is used as group name in the output files.@n
     * The following properties can be set :
     * - name : name of the group;
     * - desc : description of the group;
     *
     * Because it is possible to have multiple reference groups, you can also regroup
     * reference groups together by specifying an alias(which is optional). If present
     * it must be followed by ':=:' and the parameter value.
     * @code refgroup_path=[alias:=:]path @endcode
     */
    std::map< std::string, std::deque<std::string> > refGroupsPaths;

    /**
     * Parameter @c annotations_path : [ path ]
     *
     * This parameter contains the path(full or relative) of the annotations file which must be in
     * [GenePred](https://genome.ucsc.edu/FAQ/FAQformat.html#format9) tab-delimited
     * format (exon coordinates must be flanked by a comma (e.g. "200,500,") and
     * columns after the 10th are considered as extra information).
     *
     * @note The path can be empty when reference features are coordinates.
     */
    std::string annotationsPath;

    /**
     * Parameter @c selection_path : [ path ]
     *
     * This parameter contains the path(full or relative) of the file containing
     * a list of annotations to select (positive filter).
     *
     * If not empty, only annotations part of this file will be selected from
     * the reference group(s).
     *
     * @note The path can be empty if no positive filtering is required, or when
     * reference features are coordinates.
     */
    std::string selectionPath;

    /**
     * Parameter @c exclusion_path : [ path ]
     *
     * This parameter contains the path(full or relative) of the file containing
     * a list of annotations to exclude (negative filter).
     *
     * If not empty, annotations part of this file will be excluded from the
     * reference group(s).
     *
     * @note The path can be empty if no negative filtering is required, or when
     * reference features are coordinates.
     */
    std::string exclusionPath;

    /**
     * Parameter @c annotation_coordinates_type : [ T, C, N ]
     *
     * This parameter indicates to use either the transcription (tx) or coding
     * (cds) coordinate columns (start and stop) from the annotations file when
     * reference features are [annotations](@ref analysisMode).
     *
     * Values :
     * - @c (T)ranscription : corresponds to the @e txStart and @e txStop;
     * - @c (C)oding sequence : corresponds to the @e cdsStart and @e cdsStop;
     * - @c (N)ot Applicable : when reference features are different than annotations.
     *
     * @note This parameter is irrelevant when [analysis mode](analysisMode) is <c>(C)oding sequence</c>
     */
    annotation_coordinates_type::type annotationCoordinatesType;

    /**
     * Parameter @c analysis_method : [ A, R ]
     *
     * This parameter indicates how to process the data by segmenting each block
     * (representing the feature or inter feature regions) either by windows of
     * constant size (absolute mode) (meaning that longer feature will contains
     * more windows), or in a constant number of windows (relative mode) (meaning
     * that longer feature will contains longer windows). The number of
     * @c windows_per_block represented in the final graph has to be determined
     * for each block individually.
     *
     * Values :
     * - @c (A)bsolute : each feature (e.g. gene) is divided in windows of constant size.
     * - @c (R)elative : each feature (e.g. gene) is divided in a constant number windows.
     */
    analysis_method::type analysisMethod;

    /**
     * Parameter @c process_data_by_chunk : [ 1, 0 ]
     *
     * This parameter indicates to process each datasets by chunk of data.
     *
     * Values :
     * - @c 1 : @c True means to use the parameter [dataset chunk size](datasetChunkSize) to process the data by sized chunks.
     * - @c 0 : @c False means to ignore the parameter [dataset chunk size](datasetChunkSize).
     */
    bool processDataByChunk;

    /**
     * Parameter @c dataset_chunk_size : [ integer ]
     *
     * This parameter allows to limit the number of lines of a dataset processed
     * at a time in order to control the memory footprint (and the swapping).
     *
     * @note The value @c -1 means no limit.
     *
     * Value : an integer value between 0 and <c>std::numeric_limits<long long>::max()</c>.
     */
    unsigned long long datasetChunkSize;

    /**
     * Parameter @c reference_points : [ integer ]
     *
     * This parameter indicates the number of reference points (and by extension
     * the  number of block being the number of @c reference_points + 1) desired.
     *
     * Value : an integer value between 1 and 6.
     */
    uint_t referencePoints;

    /**
     * Parameter @c 1pt_boundary : [ 3, 5, N ]
     *
     * This parameter indicates which boundary of the feature (5' or 3') to use
     * when [reference points](referencePoints) = 1 and @c [analysis_mode](analysisMode) is annotation.
     *
     * Values :
     * - 5: means to use the 5 prime of the reference feature;
     * - 3: means to use the 3 prime of the reference feature;
     * - N: mean that this parameter does not applies in the current context.
     *
     * @note This parameter is irrelevant when number of @c referencePoints is different than 1.
     */
    oneRefPt_boundary::type oneRefPtBoundary;

    /**
     * Parameter @c merge_mid_introns : [ F, L, N ]
     *
     * This parameter indicates to merge (or not) the signal of the middle introns
     * to one of the existing intron block.
     *
     * @note This parameter is only used in exon mode.
     *
     * Values :
     * - @c (F)irst : merge the signal of the middle introns with the signal of the first intron;
     * - @c (L)ast : merge the signal of the middle introns with the signal of the last intron;
     * - @c (N)one : ignore the signal of the middle introns.
     */
    merge_mid_introns::type mergeMiddleIntrons;
    ////////////////////////////////////////////////////////
    /**
     * Parameter @c window_size : [ integer ]
     *
     * This parameter indicates the size of the window used to segment each
     * feature in the [absolute](@ref analysis_method) mode.
     *
     * @note This parameter is also used for the upstream and downstream blocks
     * in the [relative](@ref analysis_method) mode.
     *
     * Value : an integer value between 0 and 250000000.
     */
    uint_t windowSize;

    /**
     * Parameter @c windows_per_block : [ integer ]
     *
     * This parameter indicates the number of windows to use in the
     * representation of each block individually. There are
     * [reference points](@ref referencePoints) + 1 values, each value must be
     * delimited by a semi-colon (;).
     *
     * Value : integers values between 1 and 10000.
     */
    std::vector<uint_t> windowsPerBlock;

    /**
     * Parameter @c block_alignment : [ L, R, S ]
     *
     * This parameter indicates the alignment desired for each block individually.
     * A gap is introduced at the split point inside a block or between
     * consecutive blocks representing regions in the genome not necessarily
     * contiguous.
     *
     * @note This parameter is not used in the [relative](@ref analysis_method)
     * representation mode.
     *
     * There are [reference points](@ref referencePoints) + 1 values, each value
     * must be delimited by a semi-colon (;).
     *
     * Values :
     * - @c (L)eft: the content of the block is aligned to the left;
     * - @c (R)ight: the content of the block is aligned to the left;
     * - @c (S)plit: the content of the block is split and aligned at both
     * boundaries based on the [block split type](@ref block_split_type),
     * [block split value](@ref blockSplitValue) and [block split alignment]
     * (@ref block_split_alignment) parameters.
     */
    std::vector<block_alignment::alignment> blockAlignment;

    /**
     * Parameter @c block_split_type : [ A, P, N ]
     *
     * This parameter indicates the type of [split](@ref block_alignment) desired
     * for each relevant block individually.
     *
     * @note This parameter is not used in the [relative](@ref analysis_method)
     * representation mode.
     *
     * There are [reference points](@ref referencePoints)+1 values, each value
     * must be delimited by a semi-colon (;).
     *
     * Values :
     * - @c (A)bsolute: indicates that the [block split value](@ref blockSplitValue) represent an absolute number of windows before the [split](@ref block_alignment) of the block.
     * - @c (P)ercentage: indicates that the [block split value](@ref blockSplitValue) represent a percentage of the feature before the [split](@ref block_alignment) of the block.
     * - @c (N)ot Applicable: when [block alignment](@ref block_alignment) is not split.
     */
    std::vector<block_split_type::type> blockSplitType;

    /**
     * Parameter @c block_split_value : [ integer ]
     *
     * This parameter indicates the value (absolute number of windows or
     * percentage of feature based on [block split type](@ref block_split_type)
     * desired for each relevant block individually.
     *
     * @note This parameter is not used in the [relative](@ref analysis_method)
     * representation mode.
     *
     * There are [reference points](@ref referencePoints)+1 values, each value
     * must be delimited by a semi-colon (;).
     *
     * Value : integers between 0 and 100 when block_split_type
     * is set to percentage, or a value between 0 and the [number of window in a block]
     * (windows_per_block) when block_split_type)
     * is set to absolute (set to 100 when [block alignment](@ref block_alignment)
     * is not split).
     */
    std::vector<uint_t> blockSplitValue;

    /**
     * Parameter @c block_split_alignment : [ L, R, N ]
     *
     * This parameter indicates on which side of the block (left or rigth) the
     * [number of windows before the split](@ref blockSplitValue) should be aligned.
     *
     * @note This parameter is not used in the [relative](@ref analysis_method)
     * representation mode.
     *
     * There are [reference points](@ref referencePoints)+1 values, each value
     * must be delimited by a semi-colon (;).
     *
     * Values :
     * - @c (L)eft: the desired number of windows is aligned to the left side of the block, the remaining number of windows being aligned to the rigth side.
     * - @c (R)ight: the desired number of windows is aligned to the right side of the block, the remaining number of windows being aligned to the left side.
     * - @c (N)ot Applicable: when [block alignment](@ref block_alignment) is not split.
     */
    std::vector<block_split_alignment::type> blockSplitAlignment;


    /**
     * Parameter @c smoothing_windows : [ integer ]
     *
     * This parameter indicates the number of consecutive windows to average in
     * order to smooth the data.
     *
     * @warning This value must be an even integer because the average is
     * calculated using smoothing_windows/2 window(s) on each side of a particular
     * window, therefore creating a gap of smoothing_windows/2 window(s) at a
     * split point or between consecutive block not necessarily contiguous in
     * the genome based on the [block alignment](@ref block_alignment).
     *
     * Value : an even integer value between 0 and the sum of the [windows in all blocks](@ref windowsPerBlock).
     */
    uint_t smoothingWindows;

    /**
     * Parameter @c aggregate_data_type : [ E, D, A, I ]
     *
     * This parameter indicates to report in the aggregate output files either
     * the mean, the median, the max or the min of the data values across
     * corresponding windows of the reference features of a given group.
     *
     * Values :
     * - @c M(E)an: the aggregate value reported is the mean of the values across corresponding windows.
     * - @c Me(D)ian: the aggregate value reported is the median of the values across corresponding windows.
     * - @c M(A)x: the aggregate value reported is the max of the values across corresponding windows.
     * - @c M(I)n: the aggregate value reported is the min of the values across corresponding windows.
     */
    aggregate_data_type::type aggregateDataType;

    /**
     * Parameter @c mean_dispersion_value : [ E, D]
     *
     * This parameter indicates to report the dispersion of the aggregate data
     * in each window, either as standard error of the mean (SEM) or as standard
     * deviation (SD). The dispersion of the data values inside an individual
     * reference feature window is not computed.
     *
     * Values :
     * - @c S(E)M : The value reported is the standard error of the mean of the values across corresponding windows.
     * - @c S(D) : The value reported is the standard deviation of the values across corresponding windows.
     */
    mean_dispersion_value::type meanDispersionValue;

    /**
     * Parameter @c process_missing_data : [ 1, 0 ]
     *
     * This parameter indicates to process the missing data of the datasets, by this mean the zeros.
     *
     * Values :
     * - @c 1 : @c True means to includes the zeros in the computation of the mean.
     * - @c 0 : @c False means to ignore the zeros in the computation of the mean.
     */
    bool processMissingData;

    /**
     * Parameter @c prefix_filename : [ string ]
     *
     * This parameter contains a prefix(which can be empty) to add to ALL the files copied/created
     * in the [output directory](@ref outputDirectory).
     */
    std::string prefixFilename;

    /**
     * Parameter @c write_individual_references : [ 1, 0 ]
     *
     * This parameter indicates to output (or not) one file per group of
     * reference features, where the lines contain the features and the columns
     * contain the average of each window (when more than one data value are
     * included in a window). This file could be useful for a heat map
     * representation of all reference features of a group to complement the
     * aggregate profile.
     *
     * The name of this file start by "ind_" and contains the name of the group of reference features.
     *
     * Values :
     * - 1 : @c True means to generate this output file.
     * - 0 : @c False means to not generate this output file.
     */
    bool writeIndividualReferences;

    /**
     * Parameter @c generate_heatmaps : [ 1, 0 ]
     *
     * This parameter indicates to the interface to generate (or not) one heat map
     * file per individual reference features.
     *
     * Values :
     * - 1 : @c True means to generate this output file.
     * - 0 : @c False means to not generate this output file.
     */
    bool generateHeatmaps;

    /**
     * Parameter @c generate_aggregate_graphs : [ 1, 0 ]
     *
     * This parameter indicates to the interface to generate (or not) the aggregate graphs.
     *
     * Values :
     * - 1 : @c True means to generate this output file.
     * - 0 : @c False means to not generate this output file.
     */
    bool generateAggregateGraphs;

    /**
     * Parameter @c one_graph_per_group : [ 1, 0 ]
     *
     * This parameter indicates to combine (or not) all the group of reference
     * features in one graph.
     *
     * Values :
     * - 1 : @c True means to create one graph for each group of reference features (the file name contains the group name).
     * - 0 : @c False means to combine all the groups of reference features as different curves of the same graph.
     */
    bool oneGraphPerGroup;

    /**
     * Parameter @c one_graph_per_dataset : [ 1, 0 ]
     *
     * This parameter indicates to combine (or not) all the datasets in one graph.
     *
     * Values :
     * - 1 : @c True means to create one graph for each dataset (the name of file contains the dataset name).
     * - 0 : @c False means to combine all the dataset as different curves of the same graph.
     */
    bool oneGraphPerDataset;

    /**
     * Parameter @c one_graph_per_orientation : [ 1, 0 ]
     *
     * This parameter indicates to combine (or not) all the selected
     * [subgroups of orientation](orientation_subgroups) in one graph.
     *
     * Values :
     * - 1 : @c True means to create one graph for each selected subset of orientation (the name of file contains the orientation_subset).
     * - 0 : @c False means to combine all the selected subset of orientation as different curves of the same graph.
     */
    bool oneOrientationPerGraph;

    /**
     * Parameter @c orientation_subgroups : [ 1, 0 ]
     *
     * This parameter indicates which subgroup(s) of a group of reference
     * features to use, based on the orientation of the flanking annotations.
     *
     * @note This parameter is relevant only in [annotation](@ref analysisMode)
     * mode and could be really important to correctly interpret signal in proximal
     * regions that could be affected by the orientation of the flanking genes
     * in compact genomes.
     *
     * There are always nine values representing one of the automatic orientation
     * subgroups named :
     * - AA;
     * - AC;
     * - DA;
     * - AT;
     * - TA;
     * - TT;
     * - TC;
     * - DT;
     * - DC
     *
     * where A stands for Any, C stands for Convergent (tail-tail), D stands for
     * Divergent (head-head), and T stands for Tandem (tail-head); each value
     * must be delimited by a semi-colon (;).
     *
     * Since all the reference features are represented from left to right
     * (5' to 3') in an aggregate (and individual) profile, those standing on
     * the negative strand are virtually flipped (along with their flanking
     * annotations) such that it is impossible to get an upstream orientation
     * Convergent. In other words, the reference features are always represented
     *  on the positive (P) strand, while the upstream and downstream annotations
     *  can be on the positive or negative (N) strand.
     *
     * The biological meaning of each subgroup is:
     * - @c AA: all reference features (no subgroup) (their flanking annotations can be in Any orientation) (TT+TC+DT+DC)
     * - @c AC: the subgroup of reference features where the upstream annotation is on Any strand, and the downstream annotation is on the opposite strand (Convergent orientation) (tail to tail) (TC+DC)
     * - @c DA: the subgroup of reference features where the upstream annotation is on the opposite strand (Divergent orientation) (head to head), and the downstream annotation is on Any strand (DT+DC)
     * - @c AT: the subgroup of reference features where the upstream annotation is on Any strand, and the downstream annotation is on the same strand (Tandem orientation) (tail to head) (TT+DT)
     * - @c TA: the subgroup of reference features where the upstream annotation is on the same strand (Tandem orientation) (tail to head), and the downstream annotation is on Any strand (TT+TC)
     * - @c TT: the subgroup of reference features in Tandem with their flanking annotations (the three annotations are on the same strand)
     * - @c TC: the subgroup of reference features in Tandem with the upstream annotation (same strand) and Convergent with the downstream annotation (opposite strand)
     * - @c DT: the subgroup of reference features Divergent with the upstream annotation (opposite strand) and in Tandem with the downstream annotation (same strand)
     * - @c DC: the subgroup of reference features Divergent with the upstream annotation (opposite strand) and Convergent with the downstream annotation (opposite strand)
     *
     * @warning This parameter must have the following @b strict order : AA,AC,DA,AT,TA,TT,TC,DT,DC.
     */
    std::bitset < N_ORI_SUBGROUPS > orientationSubgroups;

    /** Internal parameter to ease the rounds processing. */
    std::deque< std::string > rounds;

    /** Parameters file path (if given)*/
    std::string parametersPath;
};
} /* vap namespace end */
#endif	/* PARAMETERS_H */
