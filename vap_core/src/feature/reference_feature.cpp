/**
 * @file   reference_feature.cpp
 * @author Charles Coulombe
 *
 * @date 9 November 2012, 16:38
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./reference_feature.h"

namespace vap
{
////////////////////////////////////////////////////////////////////////////////
//                          RF PRIVATE
////////////////////////////////////////////////////////////////////////////////

/**
 * Fill the @c blocks of this @c reference_feature with coordinates in @a args.
 *
 * This method preserve the feature strand by filling the blocks decrementally or
 * incrementally.
 *
 * @note number of arguments passed must be consequent with the number of blocks
 * @warning arguments must have been filled in the right order
 *
 * @param args arguments array containing the coordinates relatively to the blocks to
 * be filled.
 */
void reference_feature::_setReferencePoints(const std::vector<coordinates> &args)
{
    // make sure we have enough arguments
    ASSERT(args.size() == _blocks.size(), "Not enough arguments to fill correctly the blocks");

    typedef std::vector<coordinates>::const_iterator cit_t;

    // set block index to points either to the last one when strand is Crick
    // otherwise set it to the beginning.
    // with this logic, the strand orientation is preserved and we can
    // fill correctly the underlaying blocks
    uint_t idx = isCrickOriented() ? _blocks.size() - 1 : 0;

    // loop for all reference points and set coordinates from arguments array
    // the index is decremented when strand is negative, otherwise we increment it
    for(cit_t it = args.begin(); it != args.end(); ++it)
        _blocks[isCrickOriented() ? idx-- : idx++].setCoordinates(it->begin(), it->end());
}

/**
 * Sets the @c block coordinates for 1 reference point.
 *
 * The coordinates are set accordingly to the strand.
 *
 * @param coords Coordinates sequence.
 * @param is5p Is 5' oriented.
 */
void reference_feature::_set_1referencePoint(const std::vector<coordinates> &coords, bool is5p)
{
    ASSERT(coords.size() == 1, "Too many arguments, only one is required"); // exactly 1 coordinates is required

    // get the only coordinates for the gene of interest
    coordinates geneOfInterest = coords.front();

    /*
     * crick
     * ==============================
     * |            |               |
     * |     B1     |       B0      |
     * |            |               |
     * ==============================
     * B0 : InterGenic
     * B1 : Annotation
     */

    /*
     * watson
     * ==============================
     * |            |               |
     * |     B0     |       B1      |
     * |            |               |
     * ==============================
     * B0 : InterGenic
     * B1 : Annotation
     */

    std::vector<coordinates> args;

    /*
     * Truth table
     *
     * 1 : true
     * 0 : false
     *
     *         Crick && !5p ## !(Crick && !5p)
     * 0 0  ##       0      ##          1
     * 0 1  ##       0      ##          1
     * 1 0  ##       1      ##          0
     * 1 1  ##       0      ##          1
     *
     */

    if(is5p)
    {
        // set point of reference regarding strand
        int point = isCrickOriented() ? geneOfInterest.end() : geneOfInterest.begin();

        // set begin and end point relatively to point of reference
        // coordinates must be inclusive for the annotation block, thus we need to adjust the coordinates regarding the strand (+/- 1)
        int begin = point - _blocks[isCrickOriented() ? _referencePoints : 0].width() + (isCrickOriented() ? 1 : 0)/*inclusive*/;
        int end = point + _blocks[isCrickOriented() ? 0 : _referencePoints].width() - (isCrickOriented() ? 0 : 1)/*inclusive*/;

        // set the RF coordinates accordingly to the strand
        args.push_back(coordinates(begin, point - (isCrickOriented() ? 0 : 1))); // IR - 1
        args.push_back(coordinates(point + (isCrickOriented() ? 1 : 0), end)); // Annotation
    }
    else
    {
        // 3 prime

        // set point of reference regarding strand
        int point = isCrickOriented() ? geneOfInterest.begin() : geneOfInterest.end();

        // set begin and end point relatively to point of reference
        // coordinates must be inclusive for the annotation block, thus we need to adjust the coordinates regarding the strand (+/- 1)
        // ***begin point of B1(end of gene + 1) is canceled by the inclusive coordinates(-1)***
        int begin = point - _blocks[isCrickOriented() ? _referencePoints : 0].width() + (isCrickOriented() ? 0 : 1)/*inclusive*/;
        int end = point + _blocks[isCrickOriented() ? 0 : _referencePoints].width() - (isCrickOriented() ? 1 : 0)/*inclusive*/;

        // set the RF coordinates accordingly to the strand
        args.push_back(coordinates(begin, point - (isCrickOriented() ? 1 : 0))); //Annotation
        args.push_back(coordinates(point + (isCrickOriented() ? 0 : 1), end)); //IR + 1
    }

    // fill gene with coordinates
    _setReferencePoints(args);
}

/**
 * Sets the @c block coordinates for 2 reference point.
 *
 * The coordinates are set accordingly to the strand.
 *
 * @param coords Coordinates sequence.
 */
void reference_feature::_set_2referencePoint(const std::vector<coordinates> &coords)
{
    ASSERT(coords.size() == 1, "Too many or too few arguments, exactly one is required"); // exactly 1 coordinates is required

    coordinates geneOfInterest = coords.front(); // get the only one argument

    /*
     * crick
     * ==============================================
     * |            |               |               |
     * |     B2     |       B1      |       B0      |
     * |            |               |               |
     * ==============================================
     * B0 : InterGenic
     * B1 : Annotation
     * B2 : InterGenic
     */

    /*
     * watson
     * ==============================================
     * |            |               |               |
     * |     B0     |       B1      |       B2      |
     * |            |               |               |
     * ==============================================
     * B0 : InterGenic
     * B1 : Annotation
     * B2 : InterGenic
     */

    // ******************
    // coordinates are already included from the 2 reference points
    // ******************

    int begin = 0, end = 0;

    //RF will starts at left coord - 1(end of intergenic region) minus the requested distance (stored in bloc 2 or 0 )
    //RF will ends at right coord + 1(begin of intergenic region) plus the requested distance (stored in bloc 0 or 2)
    begin = geneOfInterest.begin() - 1/*IR -1 end*/ - _blocks[isCrickOriented() ? _referencePoints : 0].width() + 1/*inclusive*/;
    end = geneOfInterest.end() + 1 /*IR + 1 begin*/ + _blocks[isCrickOriented() ? 0 : _referencePoints].width() - 1/*inclusive*/;

    std::vector<coordinates> args;
    args.push_back(coordinates(begin, geneOfInterest.begin() - 1)); // IR - 1
    args.push_back(coordinates(geneOfInterest.begin(), geneOfInterest.end())); // Annotation
    args.push_back(coordinates(geneOfInterest.end() + 1, end)); // IR + 1

    _setReferencePoints(args);
}

/**
 * Sets the @c block coordinates for 3 reference point.
 *
 * The coordinates are set accordingly to the strand.
 *
 * @param coords Coordinates sequence.
 */
void reference_feature::_set_3referencePoint(const std::vector<coordinates> &coords)
{
    ASSERT(coords.size() == 3, "Too many arguments, only three are required"); // exactly 2 coordinates are required

    coordinates leftGene = coords.front(),
            geneInterest = *(coords.begin() + 1), // middle argument
            rightGene = coords.back();

    /*
     * crick
     * ==========================================
     * |        |       |       |       |       |
     * |    |   |   B3  |   B2  |   B1  |   B0  |
     * |        |       |       |       |       |
     * ==========================================
     * B0 : Annotation - 1
     * B1 : InterGenic - 1
     * B2 : Annotation
     * B3 : InterGenic + 1
     */

    /*
     * watson
     * ==========================================
     * |        |       |       |       |       |
     * |   B0   |   B1  |   B2  |   B3  |   |   |
     * |        |       |       |       |       |
     * ==========================================
     * B0 : Upstream
     * B1 : InterGenic - 1
     * B2 : Annotation
     * B3 : Downstream
     */

    // *********************
    // correction is needed on both edge since "B0" and "B4" have both inclusive
    // reference point (right & left respectively),
    // *********************
    int begin = 0, end = 0;
    std::vector<coordinates> args;

    if(isCrickOriented())
    {
        //RF will starts at left coord -1(end of intergenic region) - the requested distance (stored in bloc 3 or 0 )
        //RF will ends at right coord + the requested distance (stored in bloc 0 or 3)
        begin = geneInterest.begin() - 1/*IR -1 end*/ - _blocks[_referencePoints].width() + 1/*inclusive*/;
        end = rightGene.begin() + _blocks[0].width() - 1/*inclusive*/;

        args.push_back(coordinates(begin, geneInterest.begin() - 1)); // upstream region
        args.push_back(coordinates(geneInterest.begin(), geneInterest.end())); // Annotation
        args.push_back(coordinates(geneInterest.end() + 1, rightGene.begin() - 1)); // IR - 1
        args.push_back(coordinates(rightGene.begin(), end)); // downstream region
    }
    else
    {
        //RF will starts at left coord - the requested distance (stored in bloc 3 or 0 )
        //RF will ends at right coord +1(begin of intergenic region) the requested distance (stored in bloc 0 or 3)
        begin = leftGene.end() - _blocks[0].width() + 1/*inclusive*/;
        end = geneInterest.end() + 1/*IR +1 begin*/ + _blocks[_referencePoints].width() - 1/*inclusive*/;

        args.push_back(coordinates(begin, leftGene.end())); // upstream region
        args.push_back(coordinates(leftGene.end() + 1, geneInterest.begin() - 1)); // IR - 1
        args.push_back(coordinates(geneInterest.begin(), geneInterest.end())); // Annotation
        args.push_back(coordinates(geneInterest.end() + 1, end)); // downstream region
    }

    _setReferencePoints(args);
}

/**
 * Sets the @c block coordinates for 4 reference point.
 *
 * The coordinates are set accordingly to the strand.
 *
 * @param coords Coordinates sequence.
 */
void reference_feature::_set_4referencePoint(const std::vector<coordinates> &coords)
{
    ASSERT(coords.size() == 3, "Too many arguments, only three are required"); // exactly 3 coordinates are required

    coordinates leftGene = coords.front(),
            geneOfInterest = *(coords.begin() + 1), // middle argument
            rightGene = coords.back();

    /*
     * crick
     * ==========================================
     * |        |       |       |       |       |
     * |   B4   |   B3  |   B2  |   B1  |   B0  |
     * |        |       |       |       |       |
     * ==========================================
     * B0 : Annotation - 1
     * B1 : InterGenic - 1
     * B2 : Annotation
     * B3 : InterGenic + 1
     * B4 : Annotation + 1
     */

    /*
     * watson
     * ==========================================
     * |        |       |       |       |       |
     * |   B0   |   B1  |   B2  |   B3  |   B4  |
     * |        |       |       |       |       |
     * ==========================================
     * B0 : Annotation - 1
     * B1 : InterGenic - 1
     * B2 : Annotation
     * B3 : InterGenic + 1
     * B4 : Annotation + 1
     */

    // *********************
    // correction is NOT needed on both edge since "B0" and "B4" have both inclusive
    // reference point (right & left respectivly), and the width already take care of it
    // *********************
    int begin = 0, end = 0;

    //RF will starts at left coord - the requested distance (stored in bloc 4 or 0 )
    //RF will ends at right coord + the requested distance (stored in bloc 0 or 4)
    begin = leftGene.end() - _blocks[isCrickOriented() ? _referencePoints : 0].width() + 1/*inclusive*/;
    end = rightGene.begin() + _blocks[isCrickOriented() ? 0 : _referencePoints].width() - 1/*inclusive*/;

    std::vector<coordinates> args;
    args.push_back(coordinates(begin, leftGene.end())); // Annotation - 1
    args.push_back(coordinates(leftGene.end() + 1, geneOfInterest.begin() - 1)); // IR - 1
    args.push_back(coordinates(geneOfInterest.begin(), geneOfInterest.end())); // Annotation
    args.push_back(coordinates(geneOfInterest.end() + 1, rightGene.begin() - 1)); // IR + 1
    args.push_back(coordinates(rightGene.begin(), end)); // Annotation + 1

    _setReferencePoints(args);
}

/**
 * Sets the @c block coordinates for 5 reference point.
 *
 * The coordinates are set accordingly to the strand.
 *
 * @param coords Coordinates sequence.
 */
void reference_feature::_set_5referencePoint(const std::vector<coordinates> &coords)
{
    ASSERT(coords.size() == 3, "Too many arguments, only three are required"); // exactly six/2 coordinates are required

    coordinates leftGene = coords.front(),
            geneOfInterest = *(coords.begin() + 1), // middle arguments
            rightGene = coords.back();

    /*
     * crick
     * ==========================================================
     * |        |       |       |       |       |       |       |
     * |    |   |   B5  |   B4  |   B3  |   B2  |   B1  |   B0  |
     * |        |       |       |       |       |       |       |
     * ==========================================================
     * B5 : Annotation - 1
     * B4 : InterGenic - 1
     * B3 : Annotation
     * B2 : InterGenic + 1
     * B1 : Annotation + 1
     * B0 : InterGenic + 2
     */

    /*
     * watson
     * ==========================================================
     * |        |       |       |       |       |       |       |
     * |   B0   |   B1  |   B2  |   B3  |   B4  |   B5  |   |   |
     * |        |       |       |       |       |       |       |
     * ==========================================================
     * B0 : InterGenic - 2
     * B1 : Annotation - 1
     * B2 : InterGenic - 1
     * B3 : Annotation
     * B4 : InterGenic + 1
     * B5 : Annotation + 1
     */

    // ******************
    // coordinates are already included from the 3 reference points
    // ******************

    std::vector<coordinates> args;
    int begin = 0, end = 0;

    if(isCrickOriented())
    {
        //RF will starts at left coord -1(end of intergenic region) - the requested distance (stored in bloc 3 or 0 )
        //RF will ends at right coord + the requested distance (stored in bloc 0 or 3)
        begin = leftGene.end() - _blocks[_referencePoints].width() + 1/*inclusive*/;
        end = rightGene.end() + 1/*IR -1 begin*/ + _blocks[0].width() - 1/*inclusive*/;

        args.push_back(coordinates(begin, leftGene.end())); // IR - 2
        args.push_back(coordinates(leftGene.end() + 1, geneOfInterest.begin() - 1)); // Annotation - 1
        args.push_back(coordinates(geneOfInterest.begin(), geneOfInterest.end())); // Annotation
        args.push_back(coordinates(geneOfInterest.end() + 1, rightGene.begin() - 1)); // IR - 1
        args.push_back(coordinates(rightGene.begin(), rightGene.end())); // IR + 1
        args.push_back(coordinates(rightGene.end() + 1, end)); // Annotation + 1
    }
    else
    {
        //RF will starts at left coord -1(end of intergenic region) minus the requested distance (stored in bloc 6 or 0)
        //RF will ends at right coord plus the requested distance (stored in bloc 0 or 6)
        begin = leftGene.begin() - 1/*IR end*/ - _blocks[0].width() + 1/*inclusive*/;
        end = rightGene.begin() + _blocks[_referencePoints].width() - 1/*inclusive*/;

        args.push_back(coordinates(begin, leftGene.begin() - 1)); // IR - 2
        args.push_back(coordinates(leftGene.begin(), leftGene.end())); // Annotation - 1
        args.push_back(coordinates(leftGene.end() + 1, geneOfInterest.begin() - 1)); // IR - 1
        args.push_back(coordinates(geneOfInterest.begin(), geneOfInterest.end())); // Annotation
        args.push_back(coordinates(geneOfInterest.end() + 1, rightGene.begin() - 1)); // IR + 1
        args.push_back(coordinates(rightGene.begin(), end)); // Annotation + 1
    }

    _setReferencePoints(args);
}

/**
 * Sets the @c block coordinates for 6 reference point.
 *
 * The coordinates are set accordingly to the strand.
 *
 * @param coords Coordinates sequence.
 */
void reference_feature::_set_6referencePoint(const std::vector<coordinates> &coords)
{
    ASSERT(coords.size() == 3, "Too many arguments, only three are required"); // exactly six/2 coordinates are required

    coordinates leftGene = coords.front(),
            geneOfInterest = *(coords.begin() + 1), // middle arguments
            rightGene = coords.back();

    /*
     * crick
     * ==========================================================
     * |        |       |       |       |       |       |       |
     * |   B6   |   B5  |   B4  |   B3  |   B2  |   B1  |   B0  |
     * |        |       |       |       |       |       |       |
     * ==========================================================
     * B6 : InterGenic - 2
     * B5 : Annotation - 1
     * B4 : InterGenic - 1
     * B3 : Annotation
     * B2 : InterGenic + 1
     * B1 : Annotation + 1
     * B0 : InterGenic + 2
     */

    /*
     * watson
     * ==========================================================
     * |        |       |       |       |       |       |       |
     * |   B0   |   B1  |   B2  |   B3  |   B4  |   B5  |   B6  |
     * |        |       |       |       |       |       |       |
     * ==========================================================
     * B0 : InterGenic - 2
     * B1 : Annotation - 1
     * B2 : InterGenic - 1
     * B3 : Annotation
     * B4 : InterGenic + 1
     * B5 : Annotation + 1
     * B6 : InterGenic + 2
     */

    // ******************
    // coordinates are already included from the 3 reference points
    // ******************
    int begin = 0, end = 0;

    //RF will starts at left coord - 1(end of intergenic region) minus the requested distance (stored in bloc 6 or 0)
    //RF will ends at right coord + 1(begin of intergenic region) plus the requested distance (stored in bloc 0 or 6)
    begin = leftGene.begin() - 1 - _blocks[isCrickOriented() ? _referencePoints : 0].width() + 1/*inclusive*/;
    end = rightGene.end() + 1 + _blocks[isCrickOriented() ? 0 : _referencePoints].width() - 1/*inclusive*/;

    std::vector<coordinates> args;
    args.push_back(coordinates(begin, leftGene.begin() - 1)); // IR - 2
    args.push_back(coordinates(leftGene.begin(), leftGene.end())); // Annotation - 1
    args.push_back(coordinates(leftGene.end() + 1, geneOfInterest.begin() - 1)); // IR - 1
    args.push_back(coordinates(geneOfInterest.begin(), geneOfInterest.end())); // Annotation
    args.push_back(coordinates(geneOfInterest.end() + 1, rightGene.begin() - 1)); // IR + 1
    args.push_back(coordinates(rightGene.begin(), rightGene.end())); // Annotation + 1
    args.push_back(coordinates(rightGene.end() + 1, end)); // IR + 2

    _setReferencePoints(args);
}

/**
 * Updates window's mean and keeps track of the updated data in an update table.
 *
 * @note Contrary to its counterpart @c _updateDataZeros(), this method does not
 * include zeros in the computation of the mean.
 *
 * @param item Item to update.
 * @param blockIndex Block index in which the item was found.
 * @param value New value.
 * @param begin Data start value.
 * @param end Data end value.
 */
void reference_feature::_updateData(found_item item, const uint_t blockIndex, const float value, const int begin, const int end)
{
    // Compute the base pair intersection between the data range and window coordinates
    // then compute the coverage value from this intersection.
    coordinates overlap = util::range_intersection(item.window->begin(), item.window->end(), begin, end);
    int coverage = overlap.end() - overlap.begin() + 1/*inclusive range*/;

    // Update window with new value multiplied by the coverage to accumulate
    // the value coverage for the current window.
    item.window->accumulateValue(value * coverage);
    item.window->accumulateCoverage(coverage);

    // Push index for further reference by inserting or accessing(via operator[])
    // block index. Update the table only when we are not including missing data.
    // The update table was already prefilled to simulate the missing data.
    _updateTable[blockIndex].insert(std::make_pair(item.index, item.window));
}

/**
 * Updates window's mean and keeps track of the updated data in an update table.
 *
 * @note Contrary to its counterpart @c _updateDataZeros(), this method does
 * include zeros in the computation of the mean.
 *
 * @param item Item to update.
 * @param blockIndex Block index in which the item was found.
 * @param value New value.
 * @param begin Data start value.
 * @param end Data end value.
 */
void reference_feature::_updateDataZeros(found_item item, const uint_t blockIndex, const float value, const int begin, const int end)
{
    // Compute the base pair intersection between the data range and window coordinates
    // then compute the coverage value from this intersection.
    coordinates overlap = util::range_intersection(item.window->begin(), item.window->end(), begin, end);
    int coverage = overlap.end() - overlap.begin() + 1/*inclusive range*/;

    // Update window with new value multiplied by the coverage to accumulate
    // the value coverage for the current window.
    item.window->accumulateValue(value * coverage);
    item.window->accumulateCoverage(coverage);
}

/**
 * Prefills the @c update_table with all @c window from
 * this reference feature.
 *
 * @remark This is only done in case of missing data.
 * @see processMissingData
 */
void reference_feature::prefillUpdateTable()
{
    /*
     * IMPORTANT : There's a bias that is introduce by pushing ALL windows
     * into the update_table. Some windows may be outside chromosome boundaries
     * and because we don't have the chromosomes boundaries we cannot check for
     * out of range windows. Thus, we count these out of range windows as valid
     * ones and count their value as correct. The bias introduced is really
     * small, by the order of 0.0001 for the mean.
     */
    uint_t bIdx = 0;

    // loop for all blocks of this feature
    for(blocks_t::iterator bIt = _blocks.begin(); bIt != _blocks.end(); ++bIt, ++bIdx)
    {
        // loop for all windows of this block
        for(windows_t::iterator wIt = bIt->windows().begin(); wIt != bIt->windows().end(); ++wIt)
        {
            // access the block and insert the window in the update table
            _updateTable[bIdx].insert(std::make_pair(wIt->index(), &(*wIt)));
        }
    }
}
////////////////////////////////////////////////////////////////////////////////
//                          RF PUBLIC
////////////////////////////////////////////////////////////////////////////////

/**
 * Initializes a reference feature.
 *
 * @param begin Begin point.
 * @param end End point.
 * @param rfPoints Number of reference points.
 * @param groupIndex Group reference index.
 * @param entryIndex Feature position in file.
 * @param name Reference feature name.
 * @param chromosome Reference feature chromosome.
 * @param strand Reference feature strand.
 */
reference_feature::reference_feature(int begin, int end, uint_t rfPoints, uint_t groupIndex, uint_t entryIndex, const std::string &name, const std::string &chromosome, annotation_strand::strand strand)
: coordinates(begin, end), reference_entry(entryIndex), _name(name), _chromosome(chromosome), _strand(strand), _type((orientation_type::type)0), _alias("_"), _blocks(blocks_t()), _referencePoints(rfPoints), _groupIndex(groupIndex), _featureOfInterest(coordinates(0, -1)), _extra(""), _updateTable(util::update_table())
{ }

/**
 * Initializes a reference feature.
 *
 * @param coords Reference feature coordinates.
 * @param rfPoints Number of reference points.
 * @param groupIndex Group reference index.
 * @param entryIndex Feature position in file.
 * @param name Reference feature name.
 * @param chromosome Reference feature chromosome.
 * @param strand Reference feature strand.
 */
reference_feature::reference_feature(const coordinates &coords, uint_t rfPoints, uint_t groupIndex, uint_t entryIndex, const std::string &name, const std::string &chromosome, annotation_strand::strand strand)
: coordinates(coords), reference_entry(entryIndex), _name(name), _chromosome(chromosome), _strand(strand), _type((orientation_type::type)0), _alias("_"), _blocks(blocks_t()), _referencePoints(rfPoints), _groupIndex(groupIndex), _featureOfInterest(coordinates(0, -1)), _extra(""), _updateTable(util::update_table())
{ }

/** Clear this. */
reference_feature::~reference_feature()
{
    clear();
}

/**
 * Sets the chromosome name for this feature.
 *
 * @param chromosome new chromosome name
 */
void reference_feature::setChromosome(const std::string & chromosome)
{
    _chromosome = chromosome;
}

/**
 * Sets the strand for this feature.
 *
 * @see annotation_strand
 * @param strand new strand value
 */
void reference_feature::setStrand(annotation_strand::strand strand)
{
    _strand = strand;
}

/**
 * Sets the name of this feature.
 *
 * @param name new name
 */
void reference_feature::setName(const std::string & name)
{
    _name = name;
}

/**
 * Sets the type of this feature.
 *
 * @see annotation_type
 * @param type new type
 */
void reference_feature::setOrientation(orientation_type::type type)
{
    _type = type;
}

/**
 * Gets the chromosome of this feature.
 *
 * @return chromosome name
 */
std::string reference_feature::chromosome() const
{
    return _chromosome;
}

/**
 * Gets the name of this feature.
 *
 * @return name
 */
std::string reference_feature::name() const
{
    return _name;
}

/**
 * Gets the strand for this feature.
 *
 * @see annotation_strand
 * @return strand
 */
annotation_strand::strand reference_feature::strand() const
{
    return _strand;
}

/**
 * Gets the type for this feature.
 *
 * @see annotation_type
 * @return type
 */
orientation_type::type reference_feature::orientation() const
{
    return _type;
}

/**
 * Gets the number of blocks in this feature.
 *
 * @return number of blocks
 */
uint_t reference_feature::count() const
{
    return _blocks.size();
}

/**
 * Gets the alias for this feature.
 *
 * @return alias
 */
std::string reference_feature::alias() const
{
    return _alias;
}

/**
 * Sets the alias for this feature.
 *
 * @param alias new alias
 */
void reference_feature::setAlias(const std::string &alias)
{
    _alias = alias;
}

/**
 * Gets the number of reference points of this feature.
 *
 * @return  number of reference points
 */
uint_t reference_feature::referencePoints() const
{
    return _referencePoints;
}

/**
 * Checks if the @c reference_feature strand is <i>Crick</i>.
 *
 * @see annotation_type
 * @return @c True when strand is <i>Crick</i>, @c False otherwise
 */
bool reference_feature::isCrickOriented() const
{
    return _strand == annotation_strand::strand::NEGATIVE;
}

/**
 * Gets group reference index.
 *
 * @return index
 */
uint_t reference_feature::groupIndex() const
{
    return _groupIndex;
}

/**
 * Gets the @c block sequence.
 *
 * @return blocks
 */
const blocks_t &reference_feature::blocks() const
{
    return _blocks;
}

/**
 * Gets the @c block sequence.
 *
 * @return blocks
 */
blocks_t & reference_feature::blocks()
{
    return _blocks;
}

/**
 * Gets the @c utils::update_table .
 *
 * @return the table
 */
const util::update_table &reference_feature::updateTable() const
{
    return _updateTable;
}

/**
 * Clear all feature contents.
 *
 * All underlayed blocks are cleared of their contents, the reference points
 * stays unchanged.
 */
void reference_feature::clear()
{
    // clear block content but keeps all block instanciated
    for(blocks_t::iterator i = _blocks.begin(); i != _blocks.end(); i++)
        i->clear();
}

/**
 * Clean inner update table.
 *
 * @note Required after each processed dataset.
 */
void reference_feature::cleanUpdateTable()
{
    _updateTable.clear();
}

/**
 * Constructs and initializes a block.
 *
 * @param rfPoints  Number of reference points.
 * @param windowSize Window size.
 * @param capacities Max number of windows.
 * @param alignments Block alignments.
 * @param isAbsolute Is absolute mode.
 */
void reference_feature::buildBlocks(uint_t rfPoints, uint_t windowSize, const std::vector<uint_t> & capacities, const std::vector<block_alignment::alignment> &alignments, bool isAbsolute)
{
    ASSERT(alignments.size() == rfPoints + 1, "Alignements array size does not match with the number of reference points. Size : " << alignments.size());

    // +1 to have the right number of block
    _blocks.reserve(rfPoints + 1); // reserve memory

    uint_t preferredWidth = 0;
    int begin = _begin;

    // build the required number of blocks
    for(uint_t i = 0; i < rfPoints + 1; i++)
    {
        // if we are not the first, or last and in relative mode we
        // set the preferred width to be the capacity of windows set by the user
        // TODO : Detail more...
        if(!isAbsolute && i != 0 && i != rfPoints)
            preferredWidth = capacities[i];
        else
            preferredWidth = capacities[i] * windowSize;

        // set to maximal width and coordinates will be overridden with fillCoordinates() method.
        // thus changing width if not the full width.
        // this is needed to be sure that we fill the block, and that we have a full span
        // when calculation means(APA,APN...)
        _blocks.push_back(block(begin, begin + preferredWidth - 1/*inclusive*/, capacities[i], alignments[i]));
        begin += preferredWidth;
    }
}

/**
 * Sets the blocks coordinates regarding the number of reference points.
 *
 * @note @c is5p only apply for 1 reference point.
 *
 * @param coords Coordinates sequence.
 * @param is5p Is 5' oriented.
 */
void reference_feature::setBlocksCoordinates(const std::vector<coordinates>& coords, bool is5p)
{
    // sets blocks coordinates
    switch(_referencePoints)
    {
        case 1:
            _set_1referencePoint(coords, is5p);
            break;

        case 2:
            _set_2referencePoint(coords);
            break;

        case 3:
            _set_3referencePoint(coords);
            break;

        case 4:
            _set_4referencePoint(coords);
            break;

        case 5:
            _set_5referencePoint(coords);
            break;

        case 6:
            _set_6referencePoint(coords);
            break;

        default:
            ASSERT(false, MSG_UNKNOWN_NUMBER_REFPTS(_name)); // should NEVER happen
    }

    // set RF coordinates
    setCoordinates(std::min_element(_blocks.begin(), _blocks.end(), smallest_coordinates_compare)->begin(),
                   std::max_element(_blocks.begin(), _blocks.end(), largest_coordinates_compare)->end());

    ASSERT(_begin < _end, "Begin is over end : " << this->name() << " Begin:" << _begin << " End:" << _end);
}

/**
 * Sets the blocks coordinates for exons.
 *
 * @param exons Sequence of exons.
 * @param middleAlignment Alignment of middle block.
 * @param exonCapacity Maximum number of windows.
 * @param mergeMiddleIntrons Merge introns in the centered block.
 */
void reference_feature::setBlocksCoordinates(const std::vector<exon> &exons, block_alignment::alignment middleAlignment, uint_t exonCapacity, merge_mid_introns::type mergeMiddleIntrons)
{
    ASSERT(!exons.empty(), "Exons sequence is empty");

    exon leftExon(exons.front()), rightExon(exons.back());

    /*
     * crick orientation
     * ==========================================================
     * |        |       |       |       |       |       |       |
     * |   B6   |   B5  |   B4  |   B3  |   B2  |   B1  |   B0  |
     * |        |       |       |       |       |       |       |
     * ==========================================================
     * B0 : InterGenic
     * B1 : First exon
     * B2 : Intron
     * B3 : Other exons
     * B4 : Intron
     * B5 : Last exon
     * B6 : InterGenic
     */

    /*
     * watson orientation
     * ==========================================================
     * |        |       |       |       |       |       |       |
     * |   B0   |   B1  |   B2  |   B3  |   B4  |   B5  |   B6  |
     * |        |       |       |       |       |       |       |
     * ==========================================================
     * B0 : InterGenic
     * B1 : First exon
     * B2 : Intron
     * B3 : Other exons
     * B4 : Intron
     * B5 : Last exon
     * B6 : InterGenic
     */


    //////////////
    // In case of 1 or 2 exons, some blocks won't be initialized(remains to -1)
    //////////////

    //RF will starts at the first exon(begin) - the requested distance (stored in bloc 6 or 0)
    //RF will ends at the last exon(end) - the requested distance (stored in bloc 6 or 0)
    // ***end  and begin point of RF is cancelled by the inclusive coordinates(-1)***
    coordinates::setCoordinates(leftExon.begin() - _blocks[isCrickOriented() ? _referencePoints : 0].width(),
                                rightExon.end() + _blocks[isCrickOriented() ? 0 : _referencePoints].width());

    // InterGenic (B0)
    _blocks[isCrickOriented() ? 6 : 0].setCoordinates(_begin, leftExon.begin() - 1);

    // First exon (B1)
    if(isCrickOriented())
        _blocks[1].setCoordinates(rightExon.begin(), rightExon.end());
    else
        _blocks[1].setCoordinates(leftExon.begin(), leftExon.end());

    // InterGenic (B6)
    _blocks[isCrickOriented() ? 0 : 6].setCoordinates(rightExon.end() + 1, _end);

    switch(exons.size())
    {
        case 1:
        {
            // Because we only have 1 exon, we will be able to
            // fill only 1 block, which by default is the B1. Still, the
            // intergenics blocks can be set.

            // B1 was previously set.

            // other blocks are set to empty(negative width)
            _blocks[2].setCoordinates(0, -1);
            _blocks[3].setCoordinates(0, -1);
            _blocks[4].setCoordinates(0, -1);
            _blocks[5].setCoordinates(0, -1);

            break;
        }
        case 2:
        {
            // Because we only have 2 exons, we will be able to
            // fill only 2 blocks, which are the first and last block(B1 & B5).
            // In this case, the gap is set as of the intron and set to B2.

            // B1 was previoulsy set.

            // Intron (B2) which is the gap between both exons, only one intron regardless of the strand
            _blocks[2].setCoordinates(leftExon.end() + 1, rightExon.begin() - 1);

            // Last exon (B5)
            if(isCrickOriented())
                _blocks[5].setCoordinates(leftExon.begin(), leftExon.end());
            else
                _blocks[5].setCoordinates(rightExon.begin(), rightExon.end());

            // other blocks are set to empty(negative width)
            _blocks[3].setCoordinates(0, -1);
            _blocks[4].setCoordinates(0, -1);
            break;
        }

        default: // the middle block will contain at least 1 exon
        {
            exon leftMiddleExon(exons[1]/*first centered exon*/);

            // Balance of exons (B3)
            _blocks[3].setCoordinates(leftMiddleExon.begin(), leftMiddleExon.end());
            // args[3] = coordinates(leftMiddleExon.begin(), leftMiddleExon.end());

            // accumulates the balance
            std::vector<exon>::const_iterator it = exons.begin() + 1/*start from the first centered exon*/;
            std::vector<exon>::const_iterator end = exons.end() - 1; // last Exon
            while(++it != end) // does nothing when we have only 3 exons because we already are at the end
            {
                _blocks[3].extrons().push_back(exon(it->begin(), it->end(), exonCapacity, middleAlignment));

                // push an intron
                switch(mergeMiddleIntrons)
                {
                    case merge_mid_introns::type::NONE:
                    case merge_mid_introns::type::UNKNOWN:
                    default:
                        break;

                    case merge_mid_introns::type::FIRST:
                        _blocks[2].extrons().push_back(exon((it - 1)->end() + 1, it->begin() - 1));
                        break;

                    case merge_mid_introns::type::LAST:
                        _blocks[4].extrons().push_back(exon((it - 1)->end() + 1, it->begin() - 1));
                        break;
                }
            }

            // Intron (B2) which is the gap between both exons
            if(isCrickOriented())
                _blocks[2].setCoordinates((_blocks[3].extrons().size() > 0 ? _blocks[3].extrons().back().end()/*access last stacked block*/ : leftMiddleExon.end()) + 1, rightExon.begin() - 1);
            else
                _blocks[2].setCoordinates(leftExon.end() + 1, leftMiddleExon.begin() - 1);

            // Introns (B4)
            if(isCrickOriented())
                _blocks[4].setCoordinates(leftExon.end() + 1, leftMiddleExon.begin() - 1);
            else
                _blocks[4].setCoordinates((_blocks[3].extrons().size() > 0 ? _blocks[3].extrons().back().end()/*access last stacked block*/ : leftMiddleExon.end()) + 1, rightExon.begin() - 1);

            // Last exon (B5)
            if(isCrickOriented())
                _blocks[5].setCoordinates(leftExon.begin(), leftExon.end());
            else
                _blocks[5].setCoordinates(rightExon.begin(), rightExon.end());
            break;
        }
    }

    ASSERT(_begin < _end, "Begin is greater than end" << " Name:" << _name << " B:" << _begin << " E:" << _end); //	if overlapping annotations, no data will be linked
}

/**
 * Fills the block with windows.
 *
 * All windows are set with the proper coordinates.
 *
 * @param proportionType Split is absolute or percentage.
 * @param leftAlign
 * @param proportion
 * @param windowSize
 * @param type
 * @param isAbsolute
 */
void reference_feature::fillBlocks(const std::vector<block_split_type::type> &proportionType, const std::vector<block_split_alignment::type> &leftAlign, const std::vector<uint_t> &proportion, uint_t windowSize, const analysis_mode::type type, bool isAbsolute)
{
    // TODO : Detail more...
    float windowProportion = 0.0f;
    uint_t i = 0;
    blocks_t::iterator it;
    uint_t wSize = windowSize;

    // loop for all blocks of this feature
    for(i = 0, it = _blocks.begin(); it != _blocks.end(); ++it, i++)
    {
        // if windowSize is required to be relative.
        // set the windowSize only when it's not the first and last block as
        // they have fixed width which is base upon the capacity and the windowSize
        // set by the user.
        if(!isAbsolute && i != 0 && i != _blocks.size() - 1)
        {
            wSize = std::floor((it->width() / (float) it->capacity()));

            // adjust window size if zero(happens when capacity is greater then the width)
            // we then need a window size of 1 to fill the gene.
            // TODO : Better way to do this?
            if(wSize == 0)
                wSize = 1;
        }
        else
            wSize = windowSize;

        // if we require an absolute number of windows
        // then calculates the proportion out of the number of windows
        // otherwise take the left of right proportion
        if(proportionType[i] == block_split_type::type::ABSOLUTE)
            windowProportion = it->proportion(proportion[i], it->width(), it->capacity(), wSize, leftAlign[i] == block_split_alignment::type::LEFT);
        else
            windowProportion = ((leftAlign[i] == block_split_alignment::type::LEFT ? proportion[i] : 100 - proportion[i]) / 100.0f);

        // fill windows consequently
        it->fillWindows(it->begin(), it->end(), windowProportion, wSize, isCrickOriented(), isAbsolute);

        if(type != analysis_mode::type::EXON || (type == analysis_mode::type::EXON && !it->isEmpty()))
            it->count++;

        // do stacked exons when there's some
        if(!it->extrons().empty())
        {
            // do stacked exons, this only happens on centered exon when there is stacked exons
            for(exons_t::iterator exonIt = it->extrons().begin(); exonIt != it->extrons().end(); exonIt++)
            {
                exonIt->fillWindows(exonIt->begin(), exonIt->end(), windowProportion, wSize, isCrickOriented(), isAbsolute);

                if(!exonIt->isEmpty())
                    it->count++;
            }
        }
    }
}

/**
 * Check if the @a begin and the @a end point
 * are in the current range of this reference feature
 * coordinates.
 *
 * @note  the range is inclusive.
 *
 * @param begin Begin point.
 * @param end End point.
 * @return @c True when is between the range, @c False otherwise.
 */
bool reference_feature::isInRange(int begin, int end) const
{
    return end >= _begin && begin <= _end;
}

/**
 * Updates the range for all the blocks.
 *
 * Finds the range of windows and updates them.
 *
 * @param begin Begin range point.
 * @param end End range point.
 * @param value Windows value.
 * @param includeZeros Process missing data (include zeros into mean computation).
 */
void reference_feature::updateRange(int begin, int end, float value, bool includeZeros)
{
    blocks_t::iterator it;
    exons_t::iterator exonIt;
    std::deque<found_item> items;
    uint_t bIdx = 0;

    // loop for all blocks of this feature
    // to simulate that data may fit in more than one block(overlapping features)
    for(it = _blocks.begin(); it != _blocks.end(); ++it, ++bIdx)
    {
        if(!it->isEmpty() && it->isInRange(begin, end))
        {
            // find window to update it, at this point
            // we are really supposed to find the window, if
            // we don't something is really wrong!!
            it->findRange(begin, end, isCrickOriented(), items);

            // update windows
            if(includeZeros)
                for(uint_t i = 0; i < items.size(); ++i)
                    _updateDataZeros(items[i], bIdx, value, begin, end);
            else
                for(uint_t i = 0; i < items.size(); ++i)
                    _updateData(items[i], bIdx, value, begin, end);

            // do not break to simulate that data may fit in more than one block(overlapping features)
            items.clear();
        }

        // do stacked exons when there's some
        if(!it->extrons().empty())
        {
            // then loop for all stacked exons, this loop is not done when the current block have no stacked exons
            for(exonIt = it->extrons().begin(); exonIt != it->extrons().end(); exonIt++)
            {
                if(exonIt->isInRange(begin, end))
                {
                    // find window to update it, at this point
                    // we are really supposed to find the window, if
                    // we don't something is really wrong!!
                    exonIt->findRange(begin, end, isCrickOriented(), items);

                    // update windows
                    if(includeZeros)
                        for(uint_t i = 0; i < items.size(); ++i)
                            _updateDataZeros(items[i], bIdx, value, begin, end);
                    else
                        for(uint_t i = 0; i < items.size(); ++i)
                            _updateData(items[i], bIdx, value, begin, end);

                    // do not break to simulate that data may fit in more than one block(overlapping features)
                    items.clear();
                }
            }
        }
    }
}

/**
 * Updates the weighted mean of all updated windows in the @c update_table.
 *
 * @note Contrary to its counterpart @c updateWeigthedMeanZeros(), this method does not
 * include zeros in the computation of the mean.
 */
void reference_feature::updateWeigthedMean()
{
    typedef util::update_table::type::const_iterator it_t;
    typedef util::update_table::value_type::const_iterator vit_t;

    // Loop for all block in the update table.
    for(it_t blockIt = _updateTable.begin(); blockIt != _updateTable.end(); ++blockIt)
    {
        // Loop for all windows that were updated and update their mean.
        for(vit_t wIt = blockIt->second.begin(); wIt != blockIt->second.end(); ++wIt)
        {
            wIt->second->updateWeigthedMean();
        }
    }
}

/**
 * Updates the weighted mean of all updated windows in the @c update_table.
 *
 * @note Contrary to its counterpart @c updateWeigthedMean(), this method does
 * include zeros in the computation of the mean.
 */
void reference_feature::updateWeigthedMeanZeros()
{
    typedef util::update_table::type::const_iterator it_t;
    typedef util::update_table::value_type::const_iterator vit_t;

    // Loop for all block in the update table.
    for(it_t blockIt = _updateTable.begin(); blockIt != _updateTable.end(); ++blockIt)
    {
        // Loop for all windows that were updated and update their mean.
        for(vit_t wIt = blockIt->second.begin(); wIt != blockIt->second.end(); ++wIt)
        {
            wIt->second->updateWeigthedMeanZeros();
        }
    }
}

/**
 * Flushes feature statistics data only.
 * @note Coordinates remains the same.
 */
void reference_feature::resetData()
{
    for(blocks_t::iterator blockIt = _blocks.begin(); blockIt != _blocks.end(); ++blockIt)
    {
        blockIt->resetData();
    }
}

/**
 * Gets the extra information from the genome for this feature.
 * @return extra information
 */
const std::string &reference_feature::extra() const
{
    return _extra;
}

void reference_feature::setExtra(const std::string &extra)
{
    _extra = extra;
}

/**
 * Sets the @c reference feature of interest for this @c feature.
 * @param begin begin coordinate
 * @param end end coordinate
 */
void reference_feature::setFeatureOfInterestCoordinates(int begin, int end)
{
    _featureOfInterest = coordinates(begin, end);
}

/**
 * Gets the @c reference feature of interest for this @c feature.
 * @return reference feature of interest
 */
const coordinates &reference_feature::featureOfInterest() const
{
    return _featureOfInterest;
}

#ifdef __DEBUG__

/**
 * Prints debug string to console
 * @param str content to print
 */

void reference_feature::print(const std::string & str) const
{
    std::cout << str << std::endl;
}

/**
 * Allows @c reference_feature to be logged into stream
 */
std::ostream &operator<<(std::ostream &out, const reference_feature & rf)
{
    typedef blocks_t::const_iterator it;
    exons_t::const_iterator exonIt;

    out << std::endl
            << "Reference feature properties : " << std::endl
            << "\tName : " << rf.name() << std::endl
            << "\tStrand : " << annotation_strand::toSymbol(rf.strand()) << std::endl
            << "\tChromosome : " << rf.chromosome() << std::endl
            << "\tBegin : " << rf.begin() << std::endl
            << "\tEnd : " << rf.end() << std::endl
            << "\tWidth : " << rf.width() << std::endl
            << "\tType : " << orientation_type::toString(rf.orientation()) << std::endl
            << "\tBlocks : " << rf.count() << std::endl << std::endl;

    // output all blocks, windows
    for(it i = rf._blocks.begin(); i != rf._blocks.end(); ++i)
    {
        out << std::endl << util::distance(i, rf._blocks.begin()) << *i;

        // stacked exons
        for(exonIt = i->extrons().begin(); exonIt != i->extrons().end(); exonIt++)
            out << std::endl << util::distance(exonIt, i->extrons().begin()) << *exonIt;
    }

    return out;
}
#endif
} /* vap namespace end */
