/**
 * @file   block.h
 * @author Charles Coulombe
 *
 * @date 9 November 2012, 15:08
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BLOCK_H
#define	BLOCK_H

// pre-processor directives
#include <vector>
#include <deque>
#include <set>
#include <algorithm>

#include "./window.h"
#include "./coordinates.h"

#include "../defs.h"
#include "../utils/math/rounding.h"
#include "../utils/update_table.h"
#include "../utils/debug/assert.h"
#include "../utils/iterator_utils.h"

#ifdef __DEBUG__
#include <sstream>
#endif
namespace vap
{
/** window sequence type */
typedef std::vector<window> windows_t;

/** median window sequence type */
typedef std::vector< std::deque<float> > mwindows_t;

/** Defines a found item from @c find() */
struct found_item
{
    bool found;
    uint_t index;
    util::wptr window;

    found_item(bool flag = false, uint_t idx = - 1, util::wptr wptr = 0)
    :found(flag), index(idx), window(wptr)
    {}
};

// temporary definition to define the following exon type before
// the actual block definition
class block;

/** exon definition */
typedef block exon;

typedef std::vector<exon> exons_t;

/***/
class block : public coordinates
{
protected:


private:

    // ----------------------- attributes
    /** block alignment */
    block_alignment::alignment _alignment;

    /** block array of windows */
    windows_t _windows;

    /** block's helper windows to compute the median*/
    mwindows_t _medianAccumulator;

    /**
     * block array of exons or introns
     * used only on middle block when we have read extra exons or introns.
     */
    exons_t _extrons;

    /** block fixed capacity of windows */
    uint_t _capacity;

    /** block @c window size, internal use only */
    uint_t _windowSize;

    /** block split offset */
    int _splitOffset;

    /** block split coordinates(helper attribute for searching) */
    uint_t _splitPoint;

    // ----------------------- methods
    // disallow evil copy
    //    block(const block &b);
    //    void operator=(const block &b);

    void _initialize(int begin, int end, uint_t capacity, block_alignment::alignment alignment);
    uint_t _fill(int begin, int end, uint_t offset, bool reverse, bool reverseIndex, bool isAbsolute);
    double _cutpoint(int from, float proportion, float remainingProportion, uint_t windowSize, bool reverse);

public:
    // ----------------------- attributes
    /** block population */
    uint_t count;

    // ----------------------- constructor
    block(int begin = NPOS, int end = NPOS, uint_t capacity = - 1, block_alignment::alignment alignment = block_alignment::alignment::LEFT);
    block(const coordinates &coordinates, uint_t capacity = - 1, block_alignment::alignment alignment = block_alignment::alignment::LEFT);
    ~ block();

    // ----------------------- accessors
    block_alignment::alignment alignment() const;
    uint_t capacity() const;
    const windows_t &windows() const;
    windows_t &windows();
    exons_t &extrons();
    const exons_t &extrons() const;

    // ----------------------- modifiers

    // ----------------------- methods
    void clear();
    bool isEmpty() const;
    bool isInRange(int begin, int end) const;

    static float proportion(uint_t absoluteValue, int width, uint_t capacity, uint_t windowSize, bool leftAlign);
    void fillWindows(int begin, int end, float proportion, uint_t windowSize, bool reverse, bool isAbsolute);
    void findRange(int begin, int end, bool reverse, std::deque<found_item> &items);

    void updateAggregatePhase1(const util::update_table::value_type &windows, const aggregate_data_type::type aggregateDataType, const mean_dispersion_value::type meanDispersionValue);
    void updateAggregatePhase2(const aggregate_data_type::type aggregateDataType);
    void updateWeigthedMean(bool includeZeros);

    void resetData();

    void initializeMedianAccumulator();
#ifdef __DEBUG__
    void printl(const std::string &str) const;
    friend std::ostream &operator<<(std::ostream &out, const block &b);

    bool equal(const block &b) const;
    void assign(const windows_t &w, uint_t splitOffset = - 1, uint_t splitPoint = - 1);
#endif
};
} /* vap namespace end */

#endif	/* BLOCK_H */

