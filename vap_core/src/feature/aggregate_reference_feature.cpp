/**
 * @file   aggregate_reference_feature.cpp
 * @author Charles Coulombe
 *
 * @date July 30 2013, 9:55
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./aggregate_reference_feature.h"

namespace vap
{
////////////////////////////////////////////////////////////////////////////////
//                          RF PRIVATE
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//                          RF PUBLIC
////////////////////////////////////////////////////////////////////////////////

aggregate_reference_feature::aggregate_reference_feature(int begin, int end, uint_t rfPoints, uint_t groupIndex, uint_t entryIndex, const std::string &name, const std::string &chromosome, annotation_strand::strand strand)
: reference_feature(begin, end, rfPoints, groupIndex, entryIndex, name, chromosome, strand), _population(std::vector<uint_t>()) { }

aggregate_reference_feature::aggregate_reference_feature(const coordinates &coordinates, uint_t rfPoints, uint_t groupIndex, uint_t entryIndex, const std::string &name, const std::string &chromosome, annotation_strand::strand strand)
: reference_feature(coordinates, rfPoints, groupIndex, entryIndex, name, chromosome, strand), _population(std::vector<uint_t>()) { }

/**
 * Classic destructor.
 * Clears all content.
 */
aggregate_reference_feature::~aggregate_reference_feature()
{
    clear();
}

void aggregate_reference_feature::initializePopulation()
{
    // resize array of population as well to fit the number of blocks
    _population.resize(blocks().size(), 0);
}

/**
 * Gets a population per @c block
 * @param index @c block index
 * @return population
 */
uint_t aggregate_reference_feature::population(uint_t index) const
{
    ASSERT(index < _population.size(), "Index out of bounds : " << index);
    return _population[index];
}

/**
 * Update the aggregate for all blocks.
 *
 * @param source Reference feature to used as source value.
 * @param aggregateDataType The aggregate type.
 * @param meanDispersionValue mean_dispersion_value type
 */
void aggregate_reference_feature::updateAggregatePhase1(const reference_feature &source, const aggregate_data_type::type aggregateDataType, const mean_dispersion_value::type meanDispersionValue)
{
    typedef util::update_table::type::const_iterator it_t;
    uint_t blockIndex = 0;
    blocks_t::const_iterator blockIt;

    // loop for all block that need to update their respective serie of windows and removes duplicates along the way
    for(it_t blockIt = source.updateTable().begin(); blockIt != source.updateTable().end(); ++blockIt)
    {
        if(aggregateDataType == aggregate_data_type::type::MEDIAN)
            _blocks[blockIt->first/*index*/].initializeMedianAccumulator();

        // update mean for this block set of windows
        _blocks[blockIt->first/*index*/].updateAggregatePhase1(blockIt->second/*unique sequence of windows*/, aggregateDataType, meanDispersionValue); // update mean for the block
    }

    // keep track of the population of each blocks to this mean
    for(blockIndex = 0, blockIt = _blocks.begin(); blockIt != _blocks.end(); ++blockIt, blockIndex++)
        _population[blockIndex] += source.blocks()[blockIndex].count;
}

/**
 * Updates the median aggregate.
 *
 * @param aggregateDataType The aggregate type.
 */
void aggregate_reference_feature::updateAggregatePhase2(const aggregate_data_type::type aggregateDataType)
{
    for(blocks_t::iterator blockIt = _blocks.begin(); blockIt != _blocks.end(); ++blockIt)
        blockIt->updateAggregatePhase2(aggregateDataType);
}

/**
 * Flushes feature statistics data only.
 * @note Coordinates remains the same.
 */
void aggregate_reference_feature::resetData()
{
    for(blocks_t::iterator blockIt = _blocks.begin(); blockIt != _blocks.end(); ++blockIt)
        blockIt->resetData();

    // flushes population data
    for(std::vector<uint_t>::iterator it = _population.begin(); it != _population.end(); ++it)
        *it = 0;
}
} /* vap namespace end */
