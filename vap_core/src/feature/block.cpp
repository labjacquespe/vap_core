/**
 * @file   block.cpp
 * @author Charles Coulombe
 *
 * @date 9 November 2012, 15:08
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./block.h"

namespace vap
{
////////////////////////////////////////////////////////////////////////////////
//                          PRIVATE
////////////////////////////////////////////////////////////////////////////////

/**
 * Calculates cutoff point from the proportions of desired @c windows and
 * the capacity of @c windows.
 * Afterwards, the minimal value between the proportions of desired
 * windows and the quantity of supported windows is returned.
 * @note The minimal value must be considered in case we have a
 * shorter/longer gene than the @c block width
 * @param from point to start from when calculating cut point
 * @param proportion percentage of desired @c windows <b>(/100)</n>
 * @param remainingProportion percentage of @c windows that remains after part of @c block was filled.
 * Only used when @a block_alignement::SPLIT, otherwise the @a remainingProportion should be equal to the @a proportion
 * @param windowSize dimension of @c windows
 * @param reverse calculates cut point reversly
 * @return cut point, more precisly the position including required number of @c windows.
 */
double block::_cutpoint(int from, float proportion, float remainingProportion, uint_t windowSize, bool reverse)
{
    // fill left -> end - cutoff
    // fill right -> begin + cutoff

    // computes cutoff point
    double cutoff = (std::min((_width * proportion), (_capacity * windowSize * remainingProportion)));
    return (reverse ? from + cutoff - 1 : from - cutoff + 1);
}

/**
 * Fill a range of this @c block object. Normally, range would span the entire
 * @c block width. We can fill from both orientation(Crick & Watson) so there's
 * four possible scenerios :
 *      - forward with a reverse index
 *      - forward with a forward index
 *      - backward with a reverse index
 *      - backward with a forward index
 * The filling range (@a begin and @a end) is inclusive, thus this means we can
 * fill a @c window of at least 1 base pair.
 * @param begin begining of range
 * @param end end of range
 * @param offset range offset (@c window size)
 * @param reverse fill from the end when @c True
 * @param reverseIndex @c windows index starts from the end when @c True
 * @param isAbsolute windows coordinates are absolute or relative
 * @return number of @c windows that were filled
 */
uint_t block::_fill(int begin, int end, uint_t offset, bool reverse, bool reverseIndex, bool isAbsolute)
{
    // if we have nothing to fill
    if(end < begin)
        return 0;

    uint_t index, rindex;
    int b, e;

    // This is a little tricky.
    // Because the array do not represent the entire capacity of the block, we need
    // anchor to points to a real position. The anchor is either an "index"
    // or a "rindex"(reverse index) which is actually the position of a window
    // within the container. It is required by the alignment process who
    // combines every window of each feature together.
    uint_t *anchor = &(reverseIndex ? rindex : index);

    // make sure the offset is not larger then the range and rectify if it is the case.
    // this is only usefull when we have only one window, then this window will span
    // over the range which should also be the block width, otherwise it is a mistake
    // coming from the range formulas.
    offset = std::min((uint_t) (end - begin + 1), offset);

    // get the number of windows for this range, +1 because the range is inclusive
    uint_t length = ceil((end - begin + 1) / (float) offset);

    // reserve enough space
    _windows.reserve(length);

    // loop for every window within the defined range and set window coordinates
    //
    // forward scenario :
    //      begin : we use the begining of the range to start from then we add the offset to it
    //      end :   we use the begining of the range plus next iteration and we add the offset to it,
    //              then subtract one as the coordinates must be inclusive
    //
    // reverse scenario :
    //      begin : we use the end of the range to start from then we add the offset to it,
    //              then add one as the coordinates must be inclusive
    //      end :   we use the end of the range minus next iteration and we add the offset to it
    //
    // the beauty of this loop is that reverse coordinates are ALREADY inserted
    // in the correct order by pushing back the window and computing the coordinates
    // from the end range. The same applies for normal order as we are starting from
    // the range begin point
    //
    // while the "index" increments, the "rindex" decrements, ensuring the right anchor(index)
    // for the created window.
    for(index = 0, rindex = _capacity - 1; index < length; index++, rindex--)
    {
        b = reverse ? end - ((index + 1) * offset) + 1 : begin + (index * offset);
        e = reverse ? end - (index * offset) : begin + ((index + 1) * offset) - 1;

        // insert window in a correct order not restricted by the strand of the feature
        _windows.push_back(window(b, e, *anchor));
    }

    // rectify last window coordinates if offset exceed the range(mostly like SPLIT and absolute mapping),
    // then we need to readjust the end value with the minimal one, on the other hand
    // we need to readjust with the maximal value the begin point when we are in reverse mode.
    // The minimal or maximal value is taken because we don't know
    // how much the offset does exceed from the range boundary, thus taking the max or min value ensure
    // us that the coordinates are inclusive and do not exceed. If a relative mapping is required,
    // then the last window only needs to be set to the proper block coordinates.
    if(reverse)
        _windows.back().setBegin(isAbsolute ? std::max(_windows.back().begin(), begin) : _begin);
    else
        _windows.back().setEnd(isAbsolute ? std::min(_windows.back().end(), end) : _end);

    // number of windows that were created
    return index;
}
////////////////////////////////////////////////////////////////////////////////
//                          PUBLIC
////////////////////////////////////////////////////////////////////////////////

/**
 * Constructs a @c block object, initializing its content and its @c coordinates
 * to preferred value.
 * @param begin new preferred begin point.
 * @param end new preferred end point
 * @param capacity maximum number of windows that are contains
 * @param alignment block alignement
 * @note by default the alignement is @a LEFT
 * @see block_alignement
 */
block::block(int begin, int end, uint_t capacity, block_alignment::alignment alignment)
: coordinates(begin, end), _alignment(alignment), _windows(windows_t()), _medianAccumulator(mwindows_t()), _extrons(exons_t()), _capacity(capacity), _windowSize(-1), _splitOffset(0), _splitPoint(-1), count(0)
{}

/**
 * Constructs a @c block object, initializing its content and its @c coordinates
 * to preferred value.
 * @param coords preferred coordinates
 * @param capacity maximum number of windows that are contains
 * @param alignment block alignement
 * @note by default the alignement is @a LEFT
 * @see block_alignement
 */
block::block(const coordinates &coords, uint_t capacity, block_alignment::alignment alignment)
: coordinates(coords), _alignment(alignment), _windows(windows_t()), _medianAccumulator(mwindows_t()), _extrons(exons_t()), _capacity(capacity), _windowSize(-1), _splitOffset(0), _splitPoint(-1), count(0)
{}

/**
 * Destructs @c block object, resetting its content.
 * After this call, windows and exons size are equal to zero, coordinates are equal to @c npos.
 */
block::~block()
{
    clear();
}

/**
 * Gets the block alignement.
 * @see block_alignment
 * @return alignment
 */
block_alignment::alignment block::alignment() const
{
    return _alignment;
}

/**
 * Clears @c block object contents.
 * After this call, @c windows and @c exons size, coordinates are equal to zero.
 * All other properties remain unchange.
 */
void block::clear()
{
    // explicitly call interface method
    coordinates::setCoordinates(NPOS, NPOS);

    _windows.clear();
    _extrons.clear();
}

/**
 * Checks if @c block is empty.
 * @note A @c block empty means either its width is less or equal to zero, or it does not contains @c windows.
 * @return @c True empty, @c False otherwise
 */
bool block::isEmpty() const
{
    return coordinates::isEmpty() || _windows.empty();
}

/**
 * Check if the @a begin and the @a end point are in the current range of this @c block coordinates.
 *
 * @note the range is inclusive.
 *
 * @param begin Begin point.
 * @param end End point.
 *
 * @return @c True when is between the range, @c False otherwise.
 */
bool block::isInRange(int begin, int end) const
{
    return end >= _begin && begin <= _end;
}

/**
 * Gets the maximal capacity of @c windows that this @c block
 * can contained.
 * @return capacity
 */
uint_t block::capacity() const
{
    return _capacity;
}

/**
 * Gets the sequence referring to the extra exons or introns for this @c block.
 * @return extrons sequence
 */
exons_t &block::extrons()
{
    return _extrons;
}

/**
 * Gets the sequence referring to the extra exons or introns for this @c block.
 * @return extrons sequence
 */
const exons_t &block::extrons() const
{
    return _extrons;
}

/**
 * Finds windows in the given range.
 * The found items are returned in the @a items sequence.
 *
 * @param begin Begin range point.
 * @param end End range point.
 * @param reverse Backward search.
 * @param items Found items sequence.
 */
void block::findRange(int begin, int end, bool reverse, std::deque<found_item> &items)
{
    int loffset = 0, roffset = 0;
    // TODO : Is (int) required???
    loffset = (reverse ? _splitOffset : 0) + std::max((int) ((begin - _begin) / (float) _windowSize), 0);
    roffset = (reverse ? 0 : _splitOffset) + std::max((int) ((_end - end) / (float) _windowSize), 0);

    // left part
    for(int wIdx = reverse ? roffset : loffset; wIdx < (int) _splitOffset && wIdx < (int) _windows.size() && _windows[wIdx].isInRange(begin, end); ++wIdx)
    {
        items.push_back(found_item(true, _windows[wIdx].index(), &_windows[wIdx]));
    }

    // right part
    for(int wIdx = reverse ? loffset : roffset; wIdx < (int) _windows.size() && _windows[wIdx].isInRange(begin, end); ++wIdx)
    {
        items.push_back(found_item(true, _windows[wIdx].index(), &_windows[wIdx]));
    }
}

/**
 * Transforms the absolute value(number of @c windows) in the equivalent proportion for a @c block.
 * If the absolute value is superior to the maximum number of supported @c windows,
 * the proportion will be 100%.
 *
 * @note proportion is ALWAYS relative to the maximum number of windows the @c block can contains
 *
 * @param absoluteValue absolute value(number of @c windows)
 * @param width block width
 * @param capacity maximum number of windows
 * @param windowSize dimension of @c window
 * @param leftAlign @c True computes left proportion, @c False computes right proportion
 * @return relative proportion
 */
float block::proportion(uint_t absoluteValue, int width, uint_t capacity, uint_t windowSize, bool leftAlign)
{
    // get the required width from the absolute value
    uint_t requiredWidth = absoluteValue * windowSize;

    // get the maximal possible width
    int maxWidth = capacity * windowSize;

    // calculates the relative proportion taking in consideration the minimal value between
    // the actual width of the block or the maximual possible width
    float roundUpProportion = (requiredWidth / (float) std::min(width, maxWidth));

    // get the maximal number of window for the actual width
    uint_t maxSupportedWindows = ceil(width / (float) windowSize);

    // rectify proportion so we do not exceed the block boundaries
    float prop = (absoluteValue >= maxSupportedWindows || roundUpProportion > 1.0f ? 1.0f : roundUpProportion);

    // finally return proportion for left or right
    return leftAlign ? prop : (1.0f - prop);
}

/**
 * Fills this @c block with the subsequent number of @c windows.
 * Filling can be done forwardly or reversly depending on feature orientation.
 * Filling is restrained by the @c block_alignment and @a reverse flag, thus a @c LEFT align @c block
 * will have its leftmost @c window first(forward) or last(reverse), on the other hand, a @c RIGHT align
 * @c block will have its rightmost @c window first(forward) or last(reverse).
 * For the @c SPLIT align @c block, same logic applies but the split is restrained
 * by the <b>left</b> @a proportion and of course the orientation.
 * @note proportion should <b>always</b> be 1 unless @c block is @c SPLIT aligned.
 * proportion should corresponds to the left desired part.
 * @see block_alignment
 * @note The filling range (@a begin and @a end) is inclusive, thus this means we can
 * fill a @c window of at least 1 base pair and a rounding error of 1 base pair.
 * @param begin begin point of range, usually the @a begin of the @c block
 * @param end end point of range, usually the @a end of the @c block
 * @param proportion left proportion
 * @param windowSize @c window dimension
 * @param reverse @c block orientation filling
 * @param isAbsolute window size is absolute or relative
 */
void block::fillWindows(int begin, int end, float proportion, uint_t windowSize, bool reverse, bool isAbsolute)
{
    // exit if we have nothing to fill
    if(end < begin)
        return;

    // ensure bounds are not greater then the block ones.
    end = std::min(end, _end);
    begin = std::min(begin, _begin);

    _windowSize = windowSize; // store window size for further use
    int cutpoint = 0; // cut point from proportion
    uint_t count = 0; // count the number of windows that were filled

    double used = 0.0f; // percentage of used window upon capacity
    //    const uint_t places = 1;

    switch(_alignment)
    {
        case block_alignment::alignment::LEFT:

            // fill block on left, there is two possible scenario :
            // forward scenario(watson) :
            //      calculate the cutpoint from the begin point as we go forward
            //      filling the LEFTMOST window first
            //
            // reverse scenario(crick) :
            //      calculates the cutpoint from the end point as we go backward
            //      filling the RIGHTMOST window first
            //
            // proportion will always be 100%, this also applies to the remaining proportion
            // since there's only one call to cutpoint method.

            cutpoint = _cutpoint(reverse ? end : begin, proportion, proportion, windowSize, !reverse);
            count = _fill(reverse ? cutpoint : begin, reverse ? end : cutpoint, windowSize, reverse, false/*forward index*/, isAbsolute);
            _splitOffset = count;

            break;

        case block_alignment::alignment::RIGHT:

            // fill block on right, there is two possible scenario :
            // forward scenario(watson) :
            //      calculate the cutpoint from the end point as we go backward
            //      filling the RIGHTMOST window first
            //
            // reverse scenario(crick) :
            //      calculates the cutpoint from the end point as we go backward
            //      filling the LEFTMOST window first
            //
            // proportion will always be 100%, this also applies to the remaining proportion
            // since there's only one call to cutpoint method.

            cutpoint = _cutpoint(reverse ? begin : end, proportion, proportion, windowSize, reverse);
            count = _fill(reverse ? begin : cutpoint, reverse ? cutpoint : end, windowSize, !reverse, true/*reverse index*/, isAbsolute);
            _splitOffset = 0;

            break;

        case block_alignment::alignment::SPLIT:

            // fill block by splitting it in two, there is two possible scenario :
            // forward scenario(watson) :
            //      calculate the cutpoint from the end point as we go backward
            //      filling the RIGHTMOST window first
            //
            // reverse scenario(crick) :
            //      calculates the cutpoint from the end point as we go backward
            //      filling the LEFTMOST window first

            // round up as we only have positive value
            // round up to 1 base pair
            cutpoint = (int) (_cutpoint(reverse ? end : begin, proportion, proportion, windowSize, !reverse) + 0.5);
            count += _fill(reverse ? cutpoint : begin, reverse ? end : cutpoint, windowSize, reverse, false/*forward index*/, isAbsolute);

            _splitOffset = count;
            used = (count / (float) _capacity);

            // round up to 1 base pair
            cutpoint = (int) (_cutpoint(reverse ? begin : end, 1 - proportion, 1 - used, windowSize, reverse) + 0.5);
            count += _fill(reverse ? begin : cutpoint, reverse ? cutpoint : end, windowSize, !reverse, true/*reverse index*/, isAbsolute);

            break;

        case block_alignment::alignment::UNKNOWN:
        default:
            break;
    }

    // make sure we do not exceed capacity
    ASSERT(count <= _capacity, "Block capacity was exceeded, count = " << count << " b=" << _begin << " e=" << _end);
}

/**
 * Initializes the median sequence used to accumulate values.
 */
void block::initializeMedianAccumulator()
{
    // resize to proper size the container for median
    _medianAccumulator.resize(_windows.size(), std::deque<float>());
}

/**
 * Phase 1.
 * Updates mean of a series of @c window.
 * @note this should only be used when updating the array of means
 *
 * @param windows reference sequence of windows
 * @param aggregateDataType aggregate_data_type value
 * @param meanDispersionValue mean_dispersion_value value
 */
void block::updateAggregatePhase1(const util::update_table::value_type &windows, const aggregate_data_type::type aggregateDataType, const mean_dispersion_value::type meanDispersionValue)
{
    typedef util::update_table::value_type::const_iterator wit_t;

    // loop for all windows
    for(wit_t wIndex = windows.begin(); wIndex != windows.end(); ++wIndex)
    {
        switch(aggregateDataType)
        {
            case aggregate_data_type::type::MEAN:
                // update mean and standard deviation
                _windows[wIndex->first].updateMeanStd((*wIndex->second).value());
                break;

            case aggregate_data_type::type::MEDIAN:
                // accumulate value for the median
                _medianAccumulator[wIndex->first].push_back((*wIndex->second).value());
                break;

            case aggregate_data_type::type::MAX:
                // check if value is greater then the actual one
                _windows[wIndex->first].updateMax((*wIndex->second).value());
                break;

            case aggregate_data_type::type::MIN:
                // check if value is lesser then the actual one
                _windows[wIndex->first].updateMin((*wIndex->second).value());
                break;

            default:
            case aggregate_data_type::type::UNKNOWN:
                break;
        }

        if(aggregateDataType == aggregate_data_type::type::MEAN)
        {
            switch(meanDispersionValue)
            {
                case mean_dispersion_value::type::SEM:
                    _windows[wIndex->first].updateStdErrMean();
                    break;

                case mean_dispersion_value::type::SD:
                    _windows[wIndex->first].updateStdDev();
                    break;

                    // NOT YET IMPLEMENTED
                    //                case mean_dispersion_value::type::CI:
                    //                    break;

                default:
                case mean_dispersion_value::type::UNKNOWN:
                    break;
            }
        }
    }
}

/**
 * Phase 2.
 * Only for median
 * @param aggregateDataType The aggregate type.
 */
void block::updateAggregatePhase2(const aggregate_data_type::type aggregateDataType)
{
    uint_t middle = 0, wIndex = 0, count = 0;
    float lValue = 0.0f, rValue = 0.0f, value = 0.0f;

    switch(aggregateDataType)
    {
        default:
            break;

        case aggregate_data_type::type::MEDIAN:
            // loop for all median windows and
            // sort windows according to their value and get the median
            for(mwindows_t::iterator medianWindowIt = _medianAccumulator.begin(); medianWindowIt != _medianAccumulator.end(); ++medianWindowIt, ++wIndex)
            {
                count = medianWindowIt->size(); // get the number of genes that contributed to this window

                if(count > 1)
                {
                    // sort ascending
                    std::sort(medianWindowIt->begin(), medianWindowIt->end());

                    // check if we have an even set of numbers
                    if(count % 2 == 0)
                    {
                        // get the zero-based middle index
                        middle = (count / (float) 2) - 1;

                        lValue = *(medianWindowIt->begin() + middle);
                        rValue = *(medianWindowIt->begin() + middle + 1);
                        value = (lValue + rValue) / (float) 2;

                        _windows[wIndex].setMedian(value, count);
                    }
                    else
                    {
                        // get the middle index
                        middle = (count / (float) 2);
                        value = *(medianWindowIt->begin() + middle);
                        _windows[wIndex].setMedian(value, count);
                    }
                }
                else if(count == 1)
                    _windows[wIndex].setMedian(*(medianWindowIt->begin()), count);
            }
    }
}

/**
 * Flushes @c block data
 */
void block::resetData()
{
    for(windows_t::iterator wIt = _windows.begin(); wIt != _windows.end(); ++wIt)
    {
        wIt->resetData();
    }
}

/**
 * Gets the sequence of @c window
 * @return windows
 */
const windows_t &block::windows() const
{
    return _windows;
}

/**
 * Gets the sequence of @c window
 * @return windows
 */
windows_t &block::windows()
{
    return _windows;
}

#ifdef __DEBUG__

/**
 * Prints debug string to console
 * @param str content to print
 */
void block::printl(const std::string &str) const
{
    std::cout << str << std::endl;
}

/**
 * Allows @c block to be logged into stream
 */
std::ostream &operator<<(std::ostream &out, const block &b)
{
    typedef windows_t::const_iterator it;

    out << "-Bloc Properties : " << std::endl
            << "\t\tAlignement : " << block_alignment::toString(b.alignment()) << std::endl
            << "\t\tNb windows : " << b._windows.size() << std::endl
            << "\t\tCapacity : " << b.capacity() << std::endl
            << "\t\tNb exons : " << b._extrons.size() << std::endl
            << "\t\tBegin : " << b.begin() << std::endl
            << "\t\tEnd : " << b.end() << std::endl
            << "\t\tWidth : " << b.width() << std::endl << std::endl;

    // output all windows
    for(it i = b._windows.begin(); i != b._windows.end(); ++i)
        out << "\t\t\t" << util::distance(i, b._windows.begin()) << *i;

    return out;
}

/**
 * Assign new windows.
 * @param w sequence of windows.
 */
void block::assign(const windows_t& w, uint_t splitOffset, uint_t splitPoint)
{
    _windows.assign(w.begin(), w.end());
    _splitOffset = splitOffset;
    _splitPoint = splitPoint;
}

/**
 * Checks if two @c block are equal
 * @param b other block
 * @return @c True or @c False
 */
bool block::equal(const block& b) const
{
    bool coordinatesEqual = (_begin == b._begin) && (_end == b._end);
    bool sizeEqual = (_windows.size() == b._windows.size());
    bool windowsEqual = false;

    if(!coordinatesEqual)
        printl("Block coordinates does not match");

    if(sizeEqual)
    {
        std::stringstream ss;
        windowsEqual = true;
        for(uint_t i = 0; i < _windows.size(); i++)
        {
            if(_windows[i].index() != b._windows[i].index())
            {
                windowsEqual = false;
                ss << "Window #" << i << " index does not match";

                printl(ss.str());
                ss.str(std::string()); // clear
            }

            if(_windows[i] != b._windows[i])
            {
                windowsEqual = false;
                ss << "Window #" << i << " coordinates does not match";
                printl(ss.str());
                ss.str(std::string()); // clear
            }
        }
    }
    else
        printl("Number of windows does not match");

    return coordinatesEqual && sizeEqual && windowsEqual;
}
#endif
} /* vap namespace end */
