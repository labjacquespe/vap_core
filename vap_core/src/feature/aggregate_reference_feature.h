/**
 * @file   aggregate_reference_feature.h
 * @author Charles Coulombe
 *
 * @date July 30 2013, 9:55
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AGGREGATE_REFERENCE_FEATURE_H
#define	AGGREGATE_REFERENCE_FEATURE_H

#include "./reference_feature.h"
#include "../utils/update_table.h"
#include "../utils/debug/assert.h"

namespace vap
{
/**
 * Specialized reference feature
 */
class aggregate_reference_feature : public reference_feature
{
  private:
    // ----------------------- attributes
    /** statistics to output aggregate information */
    std::vector<uint_t> _population;

    // ----------------------- methods
    // disallow evil copy
//    aggregate_reference_feature(const aggregate_reference_feature &r);
    void operator=(const aggregate_reference_feature &r);

  public:
    // ----------------------- attributes


    // ----------------------- constructors
    aggregate_reference_feature(int begin, int end, uint_t rfPoints, uint_t groupIndex, uint_t entryIndex, const std::string &name = "", const std::string &chromosome = "", annotation_strand::strand strand = annotation_strand::strand::POSITIVE);
    aggregate_reference_feature(const coordinates &coordinates, uint_t rfPoints, uint_t groupIndex, uint_t entryIndex, const std::string &name, const std::string &chromosome, annotation_strand::strand strand);
    ~aggregate_reference_feature();

    // ----------------------- accessors
    uint_t population(uint_t index) const;

    // ----------------------- modifiers
    void initializePopulation();

    // ----------------------- methods

    void updateAggregatePhase1(const reference_feature &source, const aggregate_data_type::type aggregateDataType, const mean_dispersion_value::type meanDispersionValue);
    void updateAggregatePhase2(const aggregate_data_type::type aggregateDataType);

    void resetData();
};
} /* vap namespace end */

#endif	/* AGGREGATE_REFERENCE_FEATURE_H */
