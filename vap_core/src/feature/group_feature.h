/**
 * @file   group_feature.h
 * @author Charles Coulombe
 *
 * @date 7 December 2012, 09:57
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GROUP_FEATURE_H
#define	GROUP_FEATURE_H

// pre-processor directives
#include <deque>
#include <algorithm>
#include <set>

#include "../utils/iterator_utils.h"
#include "../defs.h"
#include "../utils/debug/assert.h"

namespace vap
{

/**
 * Defines a group of @c reference_feature or @c aggregate_reference_feature.
 */
template<class TYPE>
class group_feature : public std::deque<TYPE>
{
private:
    // ----------------------- attributes
    /** group of reference_feature, only the name is kept */
    std::deque<std::string> _groups;
    std::set<std::string> _chromosomes;
    std::map<uint_t, bool> _oneBased;
    std::map<uint_t, bool> _halfOpen;

    // ----------------------- methods
    void _init();

public:

    // ----------------------- constructors
    group_feature();
    group_feature(const group_feature<TYPE> &source);
    ~group_feature();

    // ----------------------- accessors
    std::string groupName(uint_t index) const;
    uint_t groupCount() const;

    bool isOneBased(uint_t index) const;
    bool isHalfOpen(uint_t index) const;

    // ----------------------- modifiers
    void setGroupName(uint_t index);
    void setOneBased(uint_t index, bool oneBase);
    void setHalfOpen(uint_t index, bool halfOpen);

    // ----------------------- methods
    void clean();

    template <class COMPARE>
    void sort(COMPARE compare);

    void addGroupName(const std::string &name);
    int groupIndex(const std::string &name) const;

    bool containsChr(const std::string &chr) const;
    void addChrToFilter(const std::string &chr);

    group_feature<TYPE> &operator=(const group_feature<TYPE> &source);

#ifdef __DEBUG__
    void print(const std::string &str) const;
    template<class T> friend std::ostream &operator<<(std::ostream &out, const group_feature<TYPE> &srf);
#endif
};

////////////////////////////////////////////////////////////////////////////////
//                          PUBLIC
////////////////////////////////////////////////////////////////////////////////

/** Constructor. */
template <class TYPE>
group_feature<TYPE>::group_feature()
{
}

/** Destructor. */
template <class TYPE>
group_feature<TYPE>::~group_feature()
{
    _groups.clear();
}

/**
 * Gets a group name.
 *
 * Subset name corresponds to the group name that defines a specific group of @c reference_feature.
 * @return name
 */
template <class TYPE>
std::string group_feature<TYPE>::groupName(uint_t index) const
{
    ASSERT(index < _groups.size(), "Index out of bounds");
    return _groups[index];
}

/**
 * Gets the number of groups.
 *
 * @return  number of groups
 */
template <class TYPE>
uint_t group_feature<TYPE>::groupCount() const
{
    return _groups.size();
}

/**
 * Adds a group name.
 *
 * This means that a new group is created and @c reference_feature exists in this group.
 * @param name new name
 */
template <class TYPE>
void group_feature<TYPE>::addGroupName(const std::string &name)
{
    _groups.push_back(name);
}

/**
 * Searches @c group name.
 *
 * If value is not found, an
 * @param name value to search for
 * @return group name
 */
template <class TYPE>
int group_feature<TYPE>::groupIndex(const std::string &name) const
{
    std::deque<std::string>::const_iterator it = std::find(_groups.begin(), _groups.end(), name);
    return util::distance(it, _groups.begin());
}

/*
 * Sorts all @c reference_feature entries.
 *
 * All entries must always be ordered using @a compare function(even with base <code>operator<</code>).
 * @note elements that would compare equal to each other are not guaranteed to keep their original relative order.
 * @param compare comparison function object that, taking two values of the same type than those contained in the range,
 * returns @c True if the first argument goes before the second argument in the specific
 * <i>strict weak ordering</i> it defines, and @cfalse otherwise.
 */
template <class TYPE>
template <class COMPARE>
void group_feature<TYPE>::sort(COMPARE compare)
{
    // sort all entries accordingly to the compare function
    std::sort(this->begin(), this->end(), compare);
}

/**
 * Clears all data contained in this set.
 */
template <class TYPE>
void group_feature<TYPE>::clean()
{
    this->clear();
    _groups.clear();
    _chromosomes.clear();
    _oneBased.clear();
    _halfOpen.clear();
}

/**
 * Gets if the group at @a index is oneBased format.
 *
 * @param index Group index.
 *
 * @return @c True if group at @a index is oneBased, @c False otherwise.
 */
template <class TYPE>
bool group_feature<TYPE>::isOneBased(uint_t index) const
{
    return _oneBased.at(index);
}

/**
 * Gets if the group at @a index is halfOpen format.
 *
 * @param index Group index.
 *
 * @return @c True if group at @a index is halfOpen, @c False otherwise.
 */
template <class TYPE>
bool group_feature<TYPE>::isHalfOpen(uint_t index) const
{
    return _halfOpen.at(index);
}

/**
 * Sets if the group at @a index is oneBased format.
 *
 * @param index Group index.
 * @param oneBase Set the flag for one based coordinates for this group.
 */
template <class TYPE>
void group_feature<TYPE>::setOneBased(uint_t index, bool oneBase)
{
    _oneBased[index] = oneBase;
}

/**
 * Sets if the group at @a index is halfOpen format.
 *
 * @param index Group index.
 * @param halfOpen Set the flag for half-open coordinates for this group.
 */
template <class TYPE>
void group_feature<TYPE>::setHalfOpen(uint_t index, bool halfOpen)
{
    _halfOpen[index] = halfOpen;
}

/**
 * Checks if a @a chr is contains in this group.
 * @param chr chromosome to find.
 * @return @c True if chromosome is contained, @c False otherwise.
 */
template <class TYPE>
bool group_feature<TYPE>::containsChr(const std::string &chr) const
{
    std::set<std::string>::const_iterator it = _chromosomes.find(chr);
    return it != _chromosomes.end();
}

/**
 * Add a @a chr to the list of chromosomes.
 * @param chr new chromosome.
 */
template <class TYPE>
void group_feature<TYPE>::addChrToFilter(const std::string &chr)
{
    _chromosomes.insert(chr);
}

#ifdef __DEBUG__

/**
 * Prints debug string to console
 * @param str content to print
 */
template <class TYPE>
void group_feature<TYPE>::print(const std::string & str) const
{
    std::cout << str << std::endl;
}

/**
 * Allows @c set_feature to be logged into stream
 */
template <class TYPE>
std::ostream &operator<<(std::ostream &out, const group_feature<TYPE> &srf)
{
    typedef typename group_feature<TYPE>::const_iterator it_t;

    out << std::endl
            << "Set Reference feature properties : " << std::endl
            << "\tMembers : " << srf.size() << std::endl << std::endl;

    // output all Rf, blocks, windows...
    for(it_t i = srf.begin(); i != srf.end(); ++i)
        out << *i;

    return out;
}
#endif
} /* vap namespace end */

#endif	/* GROUP_FEATURE_H */

