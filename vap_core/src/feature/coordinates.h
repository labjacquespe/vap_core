/**
 * @file   coordinates.h
 * @author Charles Coulombe
 *
 * @date 9 November 2012, 16:58
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COORDINATES_H
#define	COORDINATES_H

#include "../defs.h" // for npos

namespace vap
{
/** Basic coordinates interface*/
class coordinates
{
protected:
    // ----------------------- attributes
    /** Begin point */
    int _begin;

    /** End point */
    int _end;

    /** Width, it can be negative in case we have overlapping coordinates */
    int _width;

    void _updateWidth()
    {
        _width = _end - _begin + 1; // +1 because of inclusive range values
    }

public:
    /**
     * Construct a coordinates with specific bounds.
     *
     * @param b begin point
     * @param e end point
     */
    coordinates(const int b = NPOS, const int e = NPOS)
    : _begin(b), _end(e), _width(e - b + 1)/* +1 because of inclusive range values */
    {}

    /**
     * Construct a coordinates with specific bounds.
     *
     * @param source Other coordinates.
     */
    coordinates(const coordinates &source)
    : _begin(source.begin()), _end(source.end()), _width(source.width())
    {}

    // ----------------------- accessors

    /**
     * Gets begin value.
     *
     * @return begin value
     */
    int begin() const
    {
        return _begin;
    }

    /**
     * Gets end value.
     *
     * @return end value
     */
    int end() const
    {
        return _end;
    }

    /**
     * Gets width value.
     *
     * @return width value
     */
    int width() const
    {
        return _width;
    }

    // ----------------------- modifiers

    /**
     * Sets begin value.
     *
     * @param b new value
     */
    void setBegin(int b)
    {
        _begin = b;
        _updateWidth();
    }

    /**
     * Sets end value.
     *
     * @param e new value
     */
    void setEnd(int e)
    {
        _end = e;
        _updateWidth();
    }

    /**
     * Sets new @c coordinates and computes new width and new middle point.
     *
     * @param b new preferred begin point.
     * @param e new preferred end point
     */
    void setCoordinates(int b, int e)
    {
        // sets window coordinates and computes new width
        _begin = b;
        _end = e;
        _updateWidth();
    }

    /**
     * Checks if object is empty.
     *
     * @return @c True when width is less or equal to zero, @c False otherwise
     */
    virtual bool isEmpty() const
    {
        return _width <= 0;
    }
};
} /* vap namespace end */

#endif	/* COORDINATES_H */

