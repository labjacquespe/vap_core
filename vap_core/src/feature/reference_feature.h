/**
 * @file   reference_feature.h
 * @author Charles Coulombe
 *
 * @date 9 November 2012, 16:38
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REFERENCEFEATURE_H
#define	REFERENCEFEATURE_H

// pre-processor directives
#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include "./block.h"
#include "./coordinates.h"
#include "../defs.h"
#include "../messages/messages.h"
#include "../utils/text/compare.h"
#include "../utils/update_table.h"
#include "../utils/debug/assert.h"
#include "../utils/algorithm/algorithms.h"
#include "../dataset/reference_entry.h"

namespace vap
{
// type definitions
typedef std::vector<block> blocks_t;

class reference_feature : public coordinates, public reference_entry
{
protected:
    // ----------------------- attributes
    /* feature name */
    std::string _name;

    /** feature chromosome */
    std::string _chromosome;

    annotation_strand::strand _strand; // feature strand (- or +)
    orientation_type::type _type; // feature type (PPP,NNN,...)

    std::string _alias;

    blocks_t _blocks; // inner reference points
    uint_t _referencePoints; // number of refence points
    uint_t _groupIndex; // index of group

    /** Coordinates of the reference feature of interest */
    coordinates _featureOfInterest;

    /** Extra genome information. Only used when required(eg when all RF are outputed) */
    std::string _extra;

    // ----------------------- methods
    void _initialize(int begin, int end, uint_t rfPoints, uint_t groupIndex, const std::string &name, const std::string &chromosome, annotation_strand::strand strand);

    /**
     * key : block index
     * value : vector of pairs(window index, window iterator)
     * ONLY USED WITH ORIENTATIONS(APA, APN..)
     */
    util::update_table _updateTable;

    void _setReferencePoints(const std::vector<coordinates> &args);
    void _set_1referencePoint(const std::vector<coordinates> &coords, bool is5p);
    void _set_2referencePoint(const std::vector<coordinates> &coords);
    void _set_3referencePoint(const std::vector<coordinates> &coords);
    void _set_4referencePoint(const std::vector<coordinates> &coords);
    void _set_5referencePoint(const std::vector<coordinates> &coords);
    void _set_6referencePoint(const std::vector<coordinates> &coords);

    void _updateData(found_item item, const uint_t blockIndex, const float value, const int begin, const int end);
    void _updateDataZeros(found_item item, const uint_t blockIndex, const float value, const int begin, const int end);

public:
    // ----------------------- attributes


    // ----------------------- constructors
    reference_feature(int begin, int end, uint_t rfPoints, uint_t groupIndex, uint_t entryIndex, const std::string &name = "", const std::string &chromosome = "", annotation_strand::strand strand = annotation_strand::strand::POSITIVE);
    reference_feature(const coordinates &coordinates, uint_t rfPoints, uint_t groupIndex, uint_t entryIndex, const std::string &name, const std::string &chromosome, annotation_strand::strand strand);
    ~reference_feature();

    // ----------------------- accessors
    uint_t count() const;
    uint_t groupIndex() const;
    uint_t referencePoints() const;
    bool isCrickOriented() const;

    std::string chromosome() const;
    std::string name() const;
    annotation_strand::strand strand() const;
    orientation_type::type orientation() const;
    std::string alias() const;

    const blocks_t &blocks() const;
    blocks_t &blocks();

    const coordinates &featureOfInterest() const;
    const std::string &extra() const;

    const util::update_table &updateTable() const;

    // ----------------------- modifiers
    void setChromosome(const std::string &chromosome);
    void setStrand(annotation_strand::strand strand);
    void setName(const std::string &name);
    void setOrientation(orientation_type::type type);
    void setFeatureOfInterestCoordinates(int begin, int end);
    void setExtra(const std::string &extra);
    void setAlias(const std::string &alias);

    // ----------------------- methods
    void clear();
    void cleanUpdateTable();
    bool isInRange(int begin, int end) const;

    void buildBlocks(uint_t rfPoints, uint_t windowSize, const std::vector<uint_t> &capacities, const std::vector<block_alignment::alignment> &alignments, bool isAbsolute);
    void fillBlocks(const std::vector<block_split_type::type> &absolute, const std::vector<block_split_alignment::type> &leftAlign, const std::vector<uint_t> &proportion, uint_t windowSize, const analysis_mode::type type, bool isAbsolute);
    void setBlocksCoordinates(const std::vector<coordinates> &coords, bool is5p = false);
    void setBlocksCoordinates(const std::vector<exon> &exons, block_alignment::alignment middleAlignment, uint_t exonCapacity, merge_mid_introns::type mergeMiddleIntrons);

//    void updateData(int point, float value);
    void updateRange(int begin, int end, float value, bool includeZeros);
    void updateWeigthedMean();
    void updateWeigthedMeanZeros();

    void resetData();
    void prefillUpdateTable();

#ifdef __DEBUG__
    void print(const std::string &str) const;
    friend std::ostream &operator<<(std::ostream &out, const reference_feature &rf);
#endif
};

inline bool smallest_coordinates_compare(const block &left, const block &right)
{
    return left.begin() < right.begin();
}

inline bool largest_coordinates_compare(const block &left, const block &right)
{
    return left.end() < right.end();
}

/**
 * Compare a @c reference_feature by looking at their respective @a name.
 *
 * Entry will be strictly ordered by ascending values.
 * @note @c name() comparison is case insensitive
 * @param left left argument(compared)
 * @param right right argument(comparing)
 * @return @c True when either @a left comes before @a right, @c False otherwise.
 */
inline bool features_name_less_compare(const reference_feature &left, const reference_feature &right)
{
    return left.name() < right.name();
}

/**
 * Compare a @c reference_feature by looking at their respective @a chromosome, @a begin and @a end point.
 *
 * Entry will be strictly ordered by ascending values except for the @c end point which will be descending.
 * In order to have a entry to comes before another it must have:
 *      - @a left @a chromosome must come before @a right @a chromosome;
 *      - @a left @a begin point must come before @a right @a begin point;
 *      - @a right @a end point must come before @a left @a end point.
 * Ordering can be resume to : chromosome(asc), begin(asc), end(desc).
 *
 * @note @c chromosome comparison is case insensitive
 * @param left left argument(compared)
 * @param right right argument(comparing)
 * @return @c True when either :
 *      - left chromosome comes before right chromosome;
 *      - left begin point comes before right begin point;
 *      - right end point comes before left end point;
 * @c False otherwise
 */
inline bool features_less_compare(const reference_feature &left, const reference_feature &right)
{
    // chromosome : true when left chromosome comes before right chromosome, false otherwise
    // begin : true when left begin point comes before right begin point, false otherwise
    // end : true when right end point comes before left end point
    // ordering : chromosome(asc), begin(asc), end(desc)

    if(left.chromosome() < right.chromosome())
        return true;

    if(left.chromosome() == right.chromosome())
    {
        if(left.begin() < right.begin())
            return true;

        else if(left.begin() == right.begin() && left.end() > right.end())
            return true;
    }

    return false;
}

/**
 * Compares a @c reference_feature by looking at their entry index.
 *
 * Entries will be strictly ordered by ascending values.
 *
 * @param left Left argument(compared).
 * @param right Right argument(comparing).
 *
 * @return @c True when compared @c entryIndex() is lesser than comparing @c entryIndex(),
 * @c False otherwise.
 */
inline bool features_index_less_compare(const reference_feature *left, const reference_feature *right)
{
    return left->index() < right->index();
}
} /* vap namespace end */

#endif	/* REFERENCEFEATURE_H */

