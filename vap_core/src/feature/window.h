/**
 * @file   window.h
 * @author Charles Coulombe
 *
 * @date 9 November 2012, 17:09
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WINDOW_H
#define	WINDOW_H

#ifdef __DEBUG__
#include <iostream>
#include <string>
#include <deque>
#endif

#include <math.h>
#include <cmath>
#include <algorithm>

#include "./coordinates.h"
#include "../defs.h"

namespace vap
{

/**
 * Defines a window reference which is contained in a block.
 * A window holds statistics which are computed along the way.
 */
class window : public coordinates
{
protected:
    // ----------------------- attributes

    /** Number of elements that are considered(population) */
    uint_t _count;

    /** Number of base pair that are considered */
    uint_t _coverage;

    /** Aggregate value */
    float _value;

    /** Aggregate value accumulator */
    float _valueAccumulator;

    /** Standard deviation */
    float _stdDev;

    /** Standard deviation accumulator */
    float _stdDevAccumulator;

    uint_t _index; // reference index

public:
    // ----------------------- constructors
    window(const coordinates &coordinates, uint_t index = -1);
    window(const window &source);
    window(int begin = NPOS, int end = NPOS, uint_t index = -1);
    ~window();

    // ----------------------- accessors
    uint_t count() const;
    float value() const;
    float stdDev() const;
    uint_t index() const;
    uint_t coverage() const;

    // ----------------------- modifiers
    void setMedian(float value, uint_t count);

    // ----------------------- methods
    void clear();
    bool isInRange(int begin, int end) const;
    static bool equal(const window &left, const window &right);
    bool isNotPopulated() const;

    void updateStdDev();
    void updateStdErrMean();
    void updateWeigthedMean();
    void updateWeigthedMeanZeros();
    void updateMeanStd(float value);
    void updateMin(float value);
    void updateMax(float value);

    void accumulateValue(float value);
    void accumulateCoverage(uint_t value);

    void resetData();

    window &operator=(const window &source);

#ifdef __DEBUG__
    void print(const std::string &str) const;
    friend std::ostream &operator<<(std::ostream &out, const window &w);

private:
    std::deque<float> _hitValues;
#endif
};

/**
 * Operator == overload.
 * Checks equality of two @c window objects.
 * @param left left object
 * @param right right object
 * @return @c True when both @c window coordinates are equal, @c False otherwise
 */
inline bool operator==(const window &left, const window &right)
{
    // use window compare methods
    return window::equal(left, right);
}

/**
 * Operator != overload.
 * Checks inequality of two @c window objects.
 * @param left left object
 * @param right right object
 * @return @c True when both @c window coordinates are different, @c False otherwise
 */
inline bool operator!=(const window &left, const window &right)
{
    return !window::equal(left, right);
}
} /* vap namespace end */

#endif	/* WINDOW_H */

