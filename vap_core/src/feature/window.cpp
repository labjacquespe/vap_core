/**
 * @file   window.cpp
 * @author Charles Coulombe
 *
 * @date 9 November 2012, 17:09
 * @version 1.0.0
 *
 * This file is part of VAP.
 * @copyright
 * Copyright (C) 2013-2020 Charles Coulombe, Pierre-Étienne Jacques
 * Université de Sherbrooke. All rights reserved.
 *
 * VAP is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VAP is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./window.h"

namespace vap
{
////////////////////////////////////////////////////////////////////////////////
//                          PUBLIC
////////////////////////////////////////////////////////////////////////////////

/**
 * Constructs a @c window object, initializing its @c coordinates
 * to preferred value and statistics to zero.
 *
 * @param coords preferred coordinates
 * @param index real window position in the window sequence
 */
window::window(const coordinates &coords, uint_t index)
: coordinates(coords), _count(0.0f), _coverage(0.0f), _value(0.0f), _valueAccumulator(0.0f), _stdDev(0.0f), _stdDevAccumulator(0.0f), _index(index)
{}

/**
 * Constructs a @c window object, initializing its @c coordinates
 * to preferred value and statistics to zero..
 *
 * @note  Arguments are optional, in case they are not provided the @c coordinates
 * will be initialized to @c npos
 *
 * @param begin new preferred begin point.
 * @param end new preferred end point
 * @param index real window position in the window sequence
 */
window::window(int begin, int end, uint_t index)
: coordinates(begin, end), _count(0.0f), _coverage(0.0f), _value(0.0f), _valueAccumulator(0.0f), _stdDev(0.0f), _stdDevAccumulator(0.0f), _index(index)
{}

/**
 * Constructs a @c window object from a @a source object.
 * All content is copied.
 *
 * @param source @c window  to copy from
 */
window::window(const window &source)
: coordinates(source._begin, source._end), _count(source._count), _coverage(source._coverage), _value(source._value), _valueAccumulator(source._valueAccumulator), _stdDev(source._stdDev), _stdDevAccumulator(source._stdDevAccumulator), _index(source._index)
{}

/**
 * Destructs @c window object, resetting its content.
 * After this call, statistics and coordinates are equal to zero.
 */
window::~window()
{
    clear();
}

/**
 * Clears @c window object content.
 * After this call, statistics and coordinates are equal to zero.
 */
void window::clear()
{
    // reset stats
    coordinates::setCoordinates(NPOS, NPOS);
    resetData();
    _index = -1;

#ifdef __DEBUG__
    _hitValues.clear();
#endif
}

/**
 * Check if the @a begin point and the @a end point is
 * in the range of this window.
 *
 * @note The range is inclusive.
 *
 * @param begin Begin point.
 * @param end End point.
 * @return @c True when the range is between the range of this window, @c False
 * otherwise.
 */
bool window::isInRange(int begin, int end) const
{
    return end >= _begin && begin <= _end;
}

/**
 * Gets the number of value that contributed for this @c window
 * @note read-only property
 * @return population count
 */
uint_t window::count() const
{
    return _count;
}

/**
 * Gets the mean.
 * @note read-only property
 * @return mean value
 */
float window::value() const
{
    return _value;
}

/**
 * Gets the standard deviation.
 * @note read-only property
 * @return standard deviation value
 */
float window::stdDev() const
{
    return _stdDev;
}

/**
 * Gets the reference index of this @c window.
 * This unique index is stored to retrieve easily its position
 * among any container.
 * @note read-only property and its value is between 0 and @c max() of an <c>unsigned int</c>.
 * @return an index
 */
uint_t window::index() const
{
    return _index;
}

/**
 * Gets the coverage value of this @c window.
 *
 * @return coverage value
 */
uint_t window::coverage() const
{
    return _coverage;
}

/**
 * Updates the final weighted mean for this @c window.
 *
 * @note Contrary to its counterpart @c updateWeigthedMeanZeros(), this method does not
 * include zeros in the computation of the mean.
 */
void window::updateWeigthedMean()
{
    // first increase the number of values that contributed for this window
    // The count value will be at least of 1, meaning the window was populated.
     ++_count;

    // Computes new weighted mean not including the missing data.
    // To include the missing data, simply divide by the window width, hence
    // taking into account every possible base pair : the missing ones are equivalent
    // to a value of zero. Otherwise, only the base pair covered are took into account.
    // Note : to correctly compute the mean in case of missing data, the maximum value
    // between the window width and its coverage must be the divisor.
    _value = _valueAccumulator / _coverage;
}

/**
 * Updates the final weighted mean (and include zeros) for this @c window.
 *
 * @note Contrary to its counterpart @c updateWeigthedMean(), this method does
 * include zeros in the computation of the mean.
 */
void window::updateWeigthedMeanZeros()
{
    // first increase the number of values that contributed for this window
    // The count value will be at least of 1, meaning the window was populated.
     ++_count;

    // Computes new weighted mean including the missing data.
    // To include the missing data, simply divide by the window width, hence
    // taking into account every possible base pair : the missing ones are equivalent
    // to a value of zero. Otherwise, only the base pair covered are took into account.
    // Note : to correctly compute the mean in case of missing data, the maximum value
    // between the window width and its coverage must be the divisor.
    _value = _valueAccumulator / std::max(uint_t(_width), _coverage);
}

/**
 * Updates the final mean for this @c window.
 * This increase the number of elements that contributes to the mean
 * and computes the mean incrementally. Also, the <i>phase 1</i> of standard deviation
 * is done : accumulation of mean.
 *
 * For more information regarding the mean and standard deviation computation :
 * <http://mathcentral.uregina.ca/QQ/database/QQ.09.02/carlos1.html>
 *
 * Actual equation is :
 * @par Phase 1 :
 * \f$M(1) = x(1)\f$, \f$M(k) = M(k-1) + (x(k) - M(k-1)) / k\f$
 * @par
 * \f$S(1) = 0\f$, \f$S(k) = S(k-1) + (x(k) - M(k-1)) * (x(k) - M(k))\f$
 *
 * @par Phase 2 :
 * @code for 2 <= k <= n, then @endcode
 * \f$\sigma = \sqrt{(S(n) / (n - 1))}\f$
 *
 * @note  <i>phase 2</i> is not computed here. To do so, it must be call once
 * we have processed ALL genes.
 *
 * @param value new value to be considered in current mean
 */
void window::updateMeanStd(float value)
{
    // computes standard deviation and mean on the go.
    // equation :
    //
    // phase 1 :
    //      M(1) = x(1), M(k) = M(k-1) + (x(k) - M(k-1)) / k
    //      S(1) = 0, S(k) = S(k-1) + (x(k) - M(k-1)) * (x(k) - M(k))
    //
    // phase 2 :
    //      for 2 <= k <= n, then
    //          sigma = sqrt(S(n) / (n - 1))

    float actualMean = _value; // stores actual mean

    // first increase the number of values that contributed for this window
    ++_count;

    // computes new mean
    _value = (actualMean + ((value - actualMean) / _count));

    // phase 1 : accumulates
    _stdDevAccumulator += ((value - actualMean) * (value - _value));
}

/**
 * Updates the standard error of the mean for this @c window.
 * @note  This should ONLY be call once ALL genes have been processed.
 * @note  only <i>phase 2</i> is computed here.
 *
 * For more information regarding the mean and standard deviation computation :
 * http://mathcentral.uregina.ca/QQ/database/QQ.09.02/carlos1.html
 *
 * Actual equation is :
 * <p>
 *  phase 1 :
 *      M(1) = x(1), M(k) = M(k-1) + (x(k) - M(k-1)) / k
 *      S(1) = 0, S(k) = S(k-1) + (x(k) - M(k-1)) * (x(k) - M(k))
 *
 *  <b>phase 2</b> :
 *      for 2 <= k <= n, then
 *          sigma = sqrt(S(n) / (n - 1))
 * </p>
 */
void window::updateStdErrMean()
{
    // TODO : Upgrade sqrt method with an assembly version for speed since not high precision is required.
    // only done when we have at least 2 values
    // this also helps to eliminates NaN.
    if(_count > 1)
    {
        // phase 2 : computes real standard deviation
        float root = sqrt(_stdDevAccumulator / (_count - 1));
        _stdDev = root / sqrt((float) _count);
    }
}

/**
 * Updates the standard deviation.
 *
 * @note The updates only occurs when there is more than 1 hit
 * for this @c window.
 */
void window::updateStdDev()
{
    // only done when we have at least 2 values
    // this also helps to eliminates NaN.
    if(_count > 1)
    {
        // phase 2 : computes real standard deviation
        _stdDev = sqrt(_stdDevAccumulator / (_count - 1));
    }
}

/**
 * Updates the minimal value.
 *
 * It also increases the number of hit for this @c window.
 * @note The update occurs if it is the first hit or if the value
 * is inferior to the current one.
 *
 * @param value new minimal value
 */
void window::updateMin(float value)
{
    ++_count;

    if(value < _value || _count <= 1)
        _value = value;
}

/**
 * Updates the maximal value.
 *
 * It also increases the number of hit for this @c window.
 * @note The update occurs if it is the first hit or if the value
 * is superior to the current one.
 *
 * @param value new maximal value
 */
void window::updateMax(float value)
{
    ++_count;

    if(value > _value || _count <= 1)
        _value = value;
}

/**
 * Updates the median value.
 *
 * @param value new median value
 * @param count new hits value
 */
void window::setMedian(float value, uint_t count)
{
    // TODO  : Assert ou exception!
    _count = count;
    _value = value;
}

/**
 * Updates the value accumulator.
 *
 * @param value value to be added
 */
void window::accumulateValue(float value)
{
    _valueAccumulator += value;

#ifdef __DEBUG__
    _hitValues.push_back(value);
#endif
}

/**
 * Updates the coverage value.
 *
 * @param value new coverage value
 */
void window::accumulateCoverage(uint_t value)
{
    _coverage += value;
}

/**
 * Flushes @c window statistics data.
 * After a call to resetData(), the number of hits is 0,
 * the value is @c BAD_NUM, the standard deviation is 0.
 *
 * @note Coordinates remains the same.
 */
void window::resetData()
{
    _count = 0;
    _coverage = 0;
    _value = 0.0f;
    _valueAccumulator = 0.0f;
    _stdDevAccumulator = 0.0f;
    _stdDev = 0.0f;

#ifdef __DEBUG__
    _hitValues.clear();
#endif
}

/**
 * Checks if left @c window matches right @c window.
 * The comparison is made on @c window coordinates.
 * @param left left object
 * @param right right object
 * @return @c True when all coordinates are equals, @c False otherwise
 */
bool window::equal(const window& left, const window& right)
{
    // compare all coordinates
    return (left.begin() == right.begin()) && (left.end() == right.end());
}

/**
 * Checks if @c window has never been populated.
 * @return @c True when @c window population is 0, @c False otherwise.
 */
bool window::isNotPopulated() const
{
    // TODO : Rename for isPopulated and change related conditions accordingly!!!

    // if no value populated this window
    return _count == 0;
}

/**
 * Operator = overload. Affects a copy of @a source object to this
 * @c window.
 * @param source object to copy from
 * @return reference to this @c window (*this)
 */
window &window::operator=(const window &source)
{
    if(&source != this)
    {
        coordinates::setCoordinates(source._begin, source._end);
        _count = source._count;
        _coverage = source._coverage;
        _value = source._value;
        _valueAccumulator = source._valueAccumulator;
        _stdDev = source._stdDev;
        _stdDevAccumulator = source._stdDevAccumulator;
        _index = source._index;
    }

    return *this;
}

#ifdef __DEBUG__

/**
 * Prints debug string to console
 * @param str content to print
 */
void window::print(const std::string &str) const
{
    std::cout << str << std::endl;
}

/**
 * Allows @c window to be logged into stream
 */
std::ostream &operator<<(std::ostream &out, const window &w)
{
    out
            << "-Window properties : " << std::endl
            << "\t\t\t\tWindow Index : " << w.index() << std::endl
            << "\t\t\t\tBegin : " << w.begin() << std::endl
            << "\t\t\t\tEnd : " << w.end() << std::endl
            << "\t\t\t\tWidth : " << w.width() << std::endl
            << "\t\t\t\tCount : " << w.count() << std::endl
            << "\t\t\t\tCoverage : " << w.coverage() << std::endl
            << "\t\t\t\tMean : " << w.value() << std::endl
            << "\t\t\t\tStdDev : " << w.stdDev() << std::endl
            << "\t\t\t\tHits: ";

    for(std::deque<float>::const_iterator i = w._hitValues.begin(); i != w._hitValues.end(); ++i)
        out << *i << " ";
    out << std::endl;

    return out;
}
#endif
} /* vap namespace end */