VAP : Examples
==================================

This directory contains test results to make sure everything runs correctly
on your system. Run the tests from the 'vap_core/vap_core' directory.

Run
---

Run the Annotations mode example:

	vap_core -p ../examples/results/test_annot_vap_parameters.txt


Run the Exons mode example:

	vap_core -p ../examples/results/test_exon_vap_parameters.txt


Run the Coordinates mode example:

	vap_core -p ../examples/results/test_coord_vap_parameters.txt


In order to create the coordinates files, the vap_utils/generate_coordinates tool was used with the following commands:

	generate_coordinates -n 4 -i SGDannot_20080202_on_sacCer1.genepred -f group_genes_trxFreq_16-50.txt -o coord_group_genes_trxFreq_16-50 -a trxFreq_16-50
	generate_coordinates -n 4 -i SGDannot_20080202_on_sacCer1.genepred -f group_genes_trxFreq_2-4.txt  -o coord_group_genes_trxFreq_2-4 -a trxFreq_2-4
	generate_coordinates -n 4 -i SGDannot_20080202_on_sacCer1.genepred -f group_genes_trxFreq_-1.txt -o coord_group_genes_trxFreq_-1 -a trxFreq_-1

You may have to change the path if you are building in a different directory than the default one.
If you downloaded the vap_core binaries, you need to place it in the 
'vap_core/vap_core' directory or change the relative path.

Afterwards, you can compare your results with the files prefixed by 'expected'.
